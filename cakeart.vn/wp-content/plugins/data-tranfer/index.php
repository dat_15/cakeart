<?php
/*
	Plugin Name: Transfer Data Plugin
	Description: Read data from Excel
	Author: Nguyễn Tiến Đạt
	Version: 1.0
	Author URI: tiendatbt19@gmail.com

	Copyright 2018 Nguyễn Tiến Đạt
	This plugin was developed by Nguyen Tien Dat
*/

if (!function_exists('create_menu_data_transfer_admin')) {

    function create_menu_data_transfer_admin() {

    	// Tạo menu
        add_menu_page('Chuyển đổi dữ liệu', 'Chuyển đổi dữ liệu', 'manage_options', __FILE__, 'data_transfer', plugins_url('data-tranfer/transfer.png'));

        // Thêm script
	    wp_enqueue_script( 'my-main-script', plugins_url('data-tranfer/js/main.js') , array( 'jquery' ), '1.0.0', true );

	    wp_localize_script( 'ajax-script', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' )) );
    }

}
add_action('admin_menu', 'create_menu_data_transfer_admin');

require_once('functions/function.php');

function data_transfer(){
	require_once('templates/template.php');
}