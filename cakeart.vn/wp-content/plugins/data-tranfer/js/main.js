
jQuery(document).ready(function($) {
	/**
	 * Chuyển đổi danh sách bánh
	 */
    var allRecords = 0;
    var data = {
		'action': 'count_all_available_data'
	};
	$.post(ajaxurl, data, function(res) {
		res = JSON.parse(res);
		if(res.status){
			$("#tab-content1 .count-all-available-records").html('Có ' + res.data + ' bản ghi cần chuyển đổi!');
			allRecords = parseInt(res.data);
			$("#tab-content1 .not-found").css('display', 'none');
			$("#tab-content1 .progress-status .total").text(res.data);
		}else{
			$("$tab-content1 .count-all-available-records").html('');
		}
	});
	

	$(document).on('click', '#tab-content1 .start-transferring', function(event) {
		event.preventDefault();
		$(this).addClass('loading');
		$(this).attr('disabled', 'true');
		var index = parseInt(document.getElementById("start_at").value);
		index = isNaN(index) ? 0 : index;
		transferDataAt(index);
		$("#tab-content1 .progress-wrap").css('display', 'block');
		$("#tab-content1 .progress-status").css('display', 'block');
	});

	function transferDataAt(index = 0){
		var data = {
			'action': 'transfer_data',
			'offset': index
		};

		$.post(ajaxurl, data, function(res) {
			$("#tab-content1.progress-status .transfering").text(++index);
			$("#tab-content1 .progress-wrap").css('width', (index/allRecords*100)+'%');
			res = JSON.parse(res);
			if(res.status) $("#tab-content1 .progress-status > strong").text(res.data);
			if(index < allRecords){
				transferDataAt(index);
			}else return false;
		});
	}


	/**
	 * Chuyển đổi danh mục sản phẩm
	 */
	$(document).on('click', '#tab-content2 .start-transferring', function(event) {
		event.preventDefault();
		var _this = $(this);
		_this.addClass('loading');
		_this.attr('disabled', 'true');
		$.get('http://www.cakeart.vn/tiendat/api/get_all_danhmucbanh', function(data) {
			var res = JSON.parse(data);
			if(res.status){
				var cats = res.data;
				for (var i = 0; i < cats.length; i++) {
					tranferCats(cats[i], null);
				}
				_this.removeClass('loading');
				_this.removeAttr('disabled');
			}else{
				_this.removeClass('loading');
				_this.removeAttr('disabled');
			}
		});
	});

	function tranferCats( cat, parent ){
		if(cat.catID === '0'){
			var dataPost = {
				'action': 'transfer_category',
				'cat': JSON.stringify(cat),
				'parent': JSON.stringify(parent !== null ? parent : {})
			};

			$.post(ajaxurl, dataPost, function(res) {
				res = JSON.parse(res);
				console.log(res);
				if(res.status == 2) cat.catID = res.catID;
				for (var i = 0; i < cat.children.length; i++) {
					tranferCats(cat.children[i], cat);
				}
			});
		}else if(cat.children.length){
			for (var i = 0; i < cat.children.length; i++) {
				tranferCats(cat.children[i], cat);
			}
		}
	}

});