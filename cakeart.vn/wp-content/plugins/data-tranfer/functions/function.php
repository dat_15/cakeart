<?php 

if ( is_admin() ) {
    add_action( 'wp_ajax_count_all_available_data', 'count_all_available_data' );
    add_action( 'wp_ajax_transfer_data', 'transfer_data' );
    add_action( 'wp_ajax_transfer_category', 'transfer_category' );
    add_action( 'wp_ajax_get_all_categories', 'get_all_categories' );
} 
require_once 'db.php';
require_once 'waterMark.php';
require_once(ABSPATH . 'wp-admin/includes/media.php');
require_once(ABSPATH . 'wp-admin/includes/file.php');
require_once(ABSPATH . 'wp-admin/includes/image.php');
require_once(ABSPATH . 'wp-includes/pluggable.php' );
$db = new DB();
function count_all_available_data() {
	$db1 = $GLOBALS['db'];
	$count = $db1->count_all('danhsachbanh');
	if(count($count)) 
		echo json_encode(array(
			'status' => 1,
			'message' => 'success',
			'data' => $count['COUNT(*)']
		));
	else
		echo json_encode(array(
			'status' => 0,
			'message' => 'failure'
		));
	wp_die(); 
}

function transfer_data(){
	global $wpdb; 
	$db = $GLOBALS['db'];
	$offset = intval($_POST['offset']);
	$res = $db->get_table('danhsachbanh', 1, $offset);
	if(!count($res)){
		echo json_encode([
			'status' => 0,
			'message' => $res
		]);
		wp_die();
	}
	$item = $res[0];
	$postID = intval($item['postID']);
	$categories = [];

	$getCatIDByCode = file_get_contents('http://www.cakeart.vn/tiendat/api/getCatIDByCode/' . $item['catCode']);
	$getCatIDByCode = json_decode($getCatIDByCode);
	if($getCatIDByCode->status) $categories = $getCatIDByCode->catID;
	if($postID == 0){

		// Create post object
		$my_post = array(
		  'post_title'    => wp_strip_all_tags( $item['tiengviet'] != '' ? $item['tiengviet'] : $item['tenbanh'] ),
		  'post_content'  => wp_strip_all_tags( $item['mota'] != '' ? $item['mota'] : $item['tiengviet'] ),
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type' 	  => "product"
		);

		// Insert the post into the database
		$postID = wp_insert_post( $my_post );
		if($postID){
			CallAPI(
				'POST', 
				'http://www.cakeart.vn/tiendat/api/update_order', 
				array(
					'field' => 'postID',
					'value' => $postID,
					'order' => $item['stt']
				)
			);
			add_post_meta( $postID, '_price', $item['gia'], true );
			add_post_meta( $postID, '_regular_price', $item['gia'], true );
			add_post_meta( $postID, '_width', $item['kichthuoc'], true );
			add_post_meta( $postID, '_sku', $item['mabanh'], true );
			add_post_meta( $postID, '_bestseller', $item['bestseller'], true );
			add_post_meta( $postID, '_newproduct', $item['newproduct'], true );
			add_post_meta( $postID, '_clap', $item['clap'], true );
		}
	}else{
	    update_post_meta ( $postID, '_price', $item['gia'] );
	    update_post_meta ( $postID, '_regular_price', $item['gia'] );
	    update_post_meta ( $postID, '_width', $item['kichthuoc'] );
	    update_post_meta ( $postID, '_sku', $item['mabanh'], true );
	    update_post_meta ( $postID, '_bestseller', $item['bestseller'] );
	    update_post_meta ( $postID, '_newproduct', $item['newproduct'] );
	    update_post_meta ( $postID, '_clap', $item['clap'] );
	}

	wp_set_post_terms( $postID, [], 'product_cat' );
	wp_set_post_terms( $postID, $categories, 'product_cat' );

	// Add tag for post
	$tagIDs = json_decode($item['keyword1']);
	if(is_array( json_decode($item['keyword2']) ) ) $tagIDs = array_merge($tagIDs, json_decode($item['keyword2']) );
	if(is_array( json_decode($item['keyword3']) ) ) $tagIDs = array_merge($tagIDs, json_decode($item['keyword3']) );
	if(is_array( json_decode($item['keyword4']) ) ) $tagIDs = array_merge($tagIDs, json_decode($item['keyword4']) );
	wp_set_post_terms( $postID, $tagIDs, 'product_tag' );
	// Cập nhật ảnh sản phẩm
    $image_url = get_post_meta( $postID, '_thumbnail_url', true );
    $image_id = get_post_meta( $postID, '_thumbnail_id', true );

    if( $image_url == "" || $image_url != $item['hinh'] ) {
		$filename = explode('', $item['hinh'])[1];
    	$image_url = 'http://www.tinycake.vn/order/thuvienbanh/sourceimg/'.$item['hinh'];
    	if(!is_url_exist($image_url)){
    		$image_url = 'http://www.tinycake.vn/order/thuvienbanh/' . $item['hinh'];
    	}

    	if(is_url_exist($image_url)) {
    		echo json_encode([
				'status' => 1,
				'data' => "Ảnh không tồn tại"
			]);
			wp_die();
    	}
    	$magicianObj = new imageLib($image_url);
		$decoded = $magicianObj->addText('+84 902 846 586', 'b', 100);
		$decoded = $magicianObj->addText($item['mabanh'], 'b', 150);
		$decoded = $magicianObj->addText('cakeart.vn', 'tl', 50, '#fff', 0, 'Dragon-is-coming.otf');
    	$upload_dir = wp_upload_dir();

		// @new/
		$upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;

		// // @new
		$image_upload = file_put_contents( $upload_path . $filename, $decoded );
		// @new
		$file             = array();
		$file['error']    = '';
		$file['tmp_name'] = $upload_path . $filename;
		$file['name']     = $filename;
		$file['type']     = 'image/png';
		$file['size']     = filesize( $upload_path . $filename );

		// upload file to server
		// @new use $file instead of $image_upload
		$file_return = wp_handle_sideload( $file, array( 'test_form' => false ) );
		$filename = $file_return['file'];
		$attachment = array(
			'post_mime_type' => $file_return['type'],
			'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
			'post_content' => '',
			'post_status' => 'inherit',
			'guid' => $wp_upload_dir['url'] . '/' . basename($filename)
		);
		$attach_id = wp_insert_attachment( $attachment, $filename );

	    if(!add_post_meta( $postID, '_thumbnail_url', $item['hinh'], true )){
	    	update_post_meta ( $postID, '_thumbnail_url', $item['hinh'] );
	    }
	    
	    $thumb_id = media_sideload_image($image_url, $postID, $item['hinh'], 'id');
	    
	    // Finally! set our post thumbnail
	    if(!add_post_meta( $postID, '_thumbnail_id', $thumb_id, true )){
		    update_post_meta( $postID, '_thumbnail_id', $thumb_id );
	    }
    }
    
	echo json_encode([
		'status' => 1,
		'data' => wp_strip_all_tags( $item['tiengviet'] != '' ? $item['tiengviet'] : $item['tenbanh'] ),
		'data2' => $tagIDs
	]);
	wp_die();
}

function is_url_exist($url){
    $ch = curl_init($url);    
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if($code == 200){
       $status = true;
    }else{
      $status = false;
    }
    curl_close($ch);
   return $status;
}

function CallAPI($method, $url, $data = false)
{
    $curl = curl_init();

    switch ($method)
    {
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);

            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_PUT, 1);
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }

    // Optional Authentication:
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_USERPWD, "username:password");

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($curl);

    curl_close($curl);

    return $result;
}

function transfer_category(){
	$cat = json_decode(stripslashes($_POST['cat']), true);
	$parent = json_decode( stripslashes($_POST['parent']), true );
	
	$cid = $cat['catID'];
	if($cat['catID'] == 0){
		$cid = wp_insert_term(
	        $cat['name'], // the term 
	        'product_cat', // the taxonomy
	        array(
	        	// 'slug' => $cat['code'],
	            'parent' => !empty($parent) ? $parent['id'] : ''
	        )
	    );
	    if(is_array($cid) && isset($cid['term_id'])){
		    file_get_contents('http://www.cakeart.vn/tiendat/api/updateCatID/' . $cid['term_id'] . '/' . $cat['id']);
			echo json_encode([ 
				'status' => 2, 
				'catID' => $cid['term_id'], 
				'updateURL' => 'http://www.cakeart.vn/tiendat/api/updateCatID/' . $cid['term_id'] . '/' . $cat['id']
			]);
	    }else{
	    	echo json_encode([ 'status' => 0, 'message' => 'Cập nhật thất bại']);
	    }
	}else{
		wp_update_term($cid, 'product_cat', array(
		  	'name' => $cat['name'],
		  	array(
	        	// 'slug' => $cat['code'],
	            'parent' => !empty($parent) ? $parent['id'] : ''
	        )
		));
		echo json_encode([ 'status' => 1, 'message' => 'Cập nhật thành công']);
	}
    wp_die();
}