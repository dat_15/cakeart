<style>
	.tabs {
		width: 95%;
		float: none;
		list-style: none;
		position: relative;
		margin: 80px 0 0 0px;
		text-align: left;
	}
	.tabs li {
		float: left;
		display: block;
	}
	.tabs input[type="radio"] {
		position: absolute;
		top: 0;
		left: -9999px;
	}
	.tabs label {
		display: block;
		padding: 14px 21px;
		border-radius: 2px 2px 0 0;
		font-size: 20px;
		font-weight: normal;
		text-transform: uppercase;
		background: rgba(249, 160, 33, 0.56);
		color: #fff;
		cursor: pointer;
		position: relative;
		top: 4px;
		-moz-transition: all 0.2s ease-in-out;
		-o-transition: all 0.2s ease-in-out;
		-webkit-transition: all 0.2s ease-in-out;
		transition: all 0.2s ease-in-out;
	}
	.tabs label:hover {
		background: #f9a021;
	}
	.tabs .tab-content {
		z-index: 2;
		display: none;
		overflow: hidden;
		width: 100%;
		font-size: 17px;
		line-height: 25px;
		padding: 25px;
		position: absolute;
		top: 53px;
		left: 0;
		background: #fff;
	}
	.tabs [id^="tab"]:checked + label {
		top: 0;
		padding-top: 17px;
		background: #f9a021;
	}
	.tabs [id^="tab"]:checked ~ [id^="tab-content"] {
		display: block;
	}

	#wpbody{
		padding-right: 15px;
	}
	.progress {
		width: 0%;
		height: 25px;
	}

	.progress-wrap {
		background: #f80;
		margin: 20px 0;
		overflow: hidden;
		position: relative;
		display: none;
		-webkit-transition: all .25s linear;
		   -moz-transition: all .25s linear;
		    -ms-transition: all .25s linear;
		     -o-transition: all .25s linear;
		        transition: all .25s linear;
	}

	.progress-status,
	.update-status{
		display: none;
		-webkit-transition: all .25s linear;
		   -moz-transition: all .25s linear;
		    -ms-transition: all .25s linear;
		     -o-transition: all .25s linear;
		        transition: all .25s linear;
	}
	
	.not-found{
		display: none;
	}

	.start-transferring {
	    padding: 10px 20px;
	    background-color: #f9a021;
	    color: #fff;
	    text-transform: uppercase;
	    border: 1px solid;
	    outline: none;
	    cursor: pointer;
	    font-weight: 600;
	    position: relative;
	    height: 45px;
	}
	button.start-transferring.loading:before {
	    content: '';
	    display: block;
	    width: 100%;
	    height: 45px;
	    position: absolute;
	    top: 0;
	    left: 0;
	    z-index: 1000;
	    background-color: inherit;
	}
	button.start-transferring.loading:after {
	    content: '';
	    text-align: center;
	    display: block;
	    align-items: center;
	    width: 20px;
	    position: absolute;
	    height: 20px;
	    top: 10px;
	    left: 40%;
	    border: 3px solid #ccc;
	    border-top: 3px solid #fff;
	    border-radius: 50%;
	    z-index: +100000;
	    -webkit-animation: spin 1s infinite linear;
	       -moz-animation: spin 1s infinite linear;
	        -ms-animation: spin 1s infinite linear;
	         -o-animation: spin 1s infinite linear;
	    		animation: spin 1s infinite linear;
	}

	@keyframes spin {
		from{
			-webkit-transform: rotate(0);
			   -moz-transform: rotate(0);
			    -ms-transform: rotate(0);
			     -o-transform: rotate(0);
			        transform: rotate(0);
		}
		to{
			-webkit-transform: rotate(360deg);
			   -moz-transform: rotate(360deg);
			    -ms-transform: rotate(360deg);
			     -o-transform: rotate(360deg);
			        transform: rotate(360deg);
		}
	}

	@-webkit-keyframes spin {
		from{
			-webkit-transform: rotate(0);
			   -moz-transform: rotate(0);
			    -ms-transform: rotate(0);
			     -o-transform: rotate(0);
			        transform: rotate(0);
		}
		to{
			-webkit-transform: rotate(360deg);
			   -moz-transform: rotate(360deg);
			    -ms-transform: rotate(360deg);
			     -o-transform: rotate(360deg);
			        transform: rotate(360deg);
		}
	}
	@-moz-keyframes spin {
		from{
			-webkit-transform: rotate(0);
			   -moz-transform: rotate(0);
			    -ms-transform: rotate(0);
			     -o-transform: rotate(0);
			        transform: rotate(0);
		}
		to{
			-webkit-transform: rotate(360deg);
			   -moz-transform: rotate(360deg);
			    -ms-transform: rotate(360deg);
			     -o-transform: rotate(360deg);
			        transform: rotate(360deg);
		}
	}
	input#start_at {
	    width: 250px;
	    height: 44px;
	    font-size: 18px;
	    border: 1px solid #f80;
	    padding: 5px 15px;
	    margin-left: 80px;
	    line-height: 45px;
	}
</style>


<ul class="tabs" role="tablist">
    <li>
        <input type="radio" name="tabs" id="tab1" checked />
        <label for="tab1" 
           role="tab" 
           aria-selected="true" 
           aria-controls="panel1" 
           tabindex="0">
           Danh sách bánh
        </label>
        <div id="tab-content1" 
            class="tab-content" 
            role="tabpanel" 
            aria-labelledby="description" 
            aria-hidden="false">
         	<h2>Chuyển đổi danh sách bánh từ tinycake</h2>
			<hr>
			<h4 class="count-all-available-records">Có 0 bản ghi cần chuyển đổi!</h4>
			<button class="start-transferring">Bắt đầu ngay</button>
			<input type="text" id="start_at" placeholder="bắt đầu ở | 200">
			<!-- Change the below data attribute to play -->
			<div class="progress-wrap progress" data-progress-percent="25">
			</div>
			<div class="progress-status">
				Đã chuyển đổi <b class="transfering">0</b> / <b class="total">0</b><br>
				<strong></strong>
			</div>

			<div class="not-found">Không có bản ghi nào để cập nhật</div>
        </div>
    </li>
  
    <li>
        <input type="radio" name="tabs" id="tab2" />
        <label for="tab2"
           role="tab" 
           aria-selected="false" 
           aria-controls="panel2" 
           tabindex="0">
           Danh mục sản phẩm
       </label>
        <div id="tab-content2" 
            class="tab-content"
            role="tabpanel" 
            aria-labelledby="specification" 
            aria-hidden="true">
			<h2>Chuyển đổi danh mục sản phẩm</h2>
          	<hr>
			<button class="start-transferring">Chuyển đổi ngay</button>
			<!-- Change the below data attribute to play -->
			</div>
			<div class="update-status">
				Đã chuyển đổi <strong></strong>
			</div>
			<div class="not-found">Không có bản ghi nào để cập nhật</div>
        </div>
    </li>
</ul>