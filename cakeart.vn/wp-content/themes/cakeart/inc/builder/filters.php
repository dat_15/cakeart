<?php
add_filter( 'woocommerce_product_related_posts', 'wpse_123436_change_wc_related_products_relation_to_and' );
function wpse_123436_change_wc_related_products_relation_to_and() {
	echo "string";
    $get_related_products_args = array(
        'orderby' => 'rand',
        'posts_per_page' => $limit,
        'post_type' => 'product',
        'fields' => 'ids',
        'meta_query' => $meta_query,
        'tax_query' => array(
            'relation' => 'AND',
            array(
                'taxonomy' => 'product_cat',
                'field' => 'id',
                'terms' => $cats_array
            ),
            array(
                'taxonomy' => 'product_tag',
                'field' => 'id',
                'terms' => $tags_array
            )
        )
    );
    return $get_related_products_args;
}