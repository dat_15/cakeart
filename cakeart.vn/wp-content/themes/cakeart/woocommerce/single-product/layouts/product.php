<div class="product-container">

<div class="product-main">
	<div class="row content-row mb-0">

		<div class="product-gallery col large-<?php echo flatsome_option('product_image_width'); ?>">

		<?php
			// $cats = get_the_terms( get_the_ID(), 'product_tag' );
			// foreach ($cats as $cat) {
			// 	print_r($cat);break;
			// 	// echo $cat->name .' ';
			// }
			/**
			 * woocommerce_before_single_product_summary hook
			 *
			 * @hooked woocommerce_show_product_images - 20
			 */
			do_action( 'woocommerce_before_single_product_summary' );
		?>
		</div>

		<div class="product-info summary col-fit col entry-summary <?php flatsome_product_summary_classes();?>">
			<?php
				/**
				 * woocommerce_single_product_summary hook
				 *
				 * @hooked woocommerce_template_single_title - 5
				 * @hooked woocommerce_template_single_rating - 10
				 * @hooked woocommerce_template_single_price - 10
				 * @hooked woocommerce_template_single_excerpt - 20
				 * @hooked woocommerce_template_single_add_to_cart - 30
				 * @hooked woocommerce_template_single_meta - 40
				 * @hooked woocommerce_template_single_sharing - 50
				 */
				do_action( 'woocommerce_single_product_summary' );
			?>

		</div><!-- .summary -->


		<div id="product-sidebar" class="col large-2 hide-for-medium product-sidebar-small">
			<?php
				do_action('flatsome_before_product_sidebar');
				/**
				 * woocommerce_sidebar hook
				 *
				 * @hooked woocommerce_get_sidebar - 10
				 */
				if (is_active_sidebar( 'product-sidebar' ) ) {
					dynamic_sidebar('product-sidebar');
				}
			?>
		</div>

	</div><!-- .row -->
</div><!-- .product-main -->

<div class="product-footer">
	<div class="container">
		<?php
			/**
			 * woocommerce_after_single_product_summary hook
			 *
			 * @hooked woocommerce_output_product_data_tabs - 10
			 * @hooked woocommerce_upsell_display - 15
			 * @hooked woocommerce_output_related_products - 20
			 */
			do_action( 'woocommerce_after_single_product_summary' );
		?>
		<div class="related related-products-wrapper product-section">
			<h3 class="product-section-title container-width pt-half pb-half uppercase">
				phụ kiện sinh nhật
			</h3>
			<div class="products row row-small large-columns-4 medium-columns-2 small-columns-2 has-shadow row-box-shadow-1 row-box-shadow-2-hover">
				<?php query_posts(
			   		array(
			   			'post_type' => 'product', 
			   			'orderby' 	=> 'meta_value_num' , 
			   			'showposts' => 4,
			   			'meta_key' 	=> '_newproduct'
			   		)
			   	);
			   	if (have_posts()) :
			      	while (have_posts()) : 
			      		the_post();?>
						<div class="product-small col has-hover product type-product status-publish has-post-thumbnail instock shipping-taxable product-type-simple">
				         	<?php wc_get_template_part( 'content', 'product' );?>
						</div>
			      	<?php endwhile;
			   	endif;
			   	wp_reset_query(); ?>
			</div>
		</div>
	</div><!-- .container -->
</div><!-- .product-footer -->
</div><!-- .product-container -->
<script>
	jQuery(document).ready(function($) {
		$("div.product-main button[name=add-to-cart]").text('Nhấn vào để được tư vấn').css({'font-size':'14px', 'padding': '5px'});
		$('div.quantity.buttons_added').remove();
		$(".product-section-title-related").text($(".product-section-title-related").text() + ' - kèm theo');
		$(document).on('click', 'div.product-main button[name=add-to-cart]', function(event) {
			event.preventDefault();
			/* Act on the event */
			$('#fbmsg .fbmsg-badge').click();
			return false;
		});
	});
</script>