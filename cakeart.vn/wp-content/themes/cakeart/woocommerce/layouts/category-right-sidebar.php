<div class="row category-page-row">
	<div class="col large-9">
		<?php
		do_action('flatsome_products_before');

		/**
		* Hook: woocommerce_before_main_content.
		*
		* @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		* @hooked woocommerce_breadcrumb - 20 (FL removed)
		* @hooked WC_Structured_Data::generate_website_data() - 30
		*/
		do_action( 'woocommerce_before_main_content' );

		?>

		<?php
		/**
		 * Hook: woocommerce_archive_description.
		 *
		 * @hooked woocommerce_taxonomy_archive_description - 10
		 * @hooked woocommerce_product_archive_description - 10
		 */
		// do_action( 'woocommerce_archive_description' );
		?>
		<?php
		
		if ( have_posts() ) {
			/**
			 * Hook: woocommerce_before_shop_loop.
			 *
			 * @hooked wc_print_notices - 10
			 * @hooked woocommerce_result_count - 20 (FL removed)
			 * @hooked woocommerce_catalog_ordering - 30 (FL removed)
			 */
			do_action( 'woocommerce_before_shop_loop' );
			woocommerce_product_loop_start();

			if ( wc_get_loop_prop( 'total' ) ) {
				while ( have_posts() ) {
					the_post();

					/**
					 * Hook: woocommerce_shop_loop.
					 *
					 * @hooked WC_Structured_Data::generate_product_data() - 10
					 */
					do_action( 'woocommerce_shop_loop' );

					wc_get_template_part( 'content', 'product' );
				}
			}

			woocommerce_product_loop_end();

			/**
			 * Hook: woocommerce_after_shop_loop.
			 *
			 * @hooked woocommerce_pagination - 10
			 */
			do_action( 'woocommerce_after_shop_loop' );
		} else {
			/**
			 * Hook: woocommerce_no_products_found.
			 *
			 * @hooked wc_no_products_found - 10
			 */
			do_action( 'woocommerce_no_products_found' );
		}
		?>

		<?php
		/**
		 * Hook: flatsome_products_after.
		 *
		 * @hooked flatsome_products_footer_content - 10
		 */
		do_action( 'flatsome_products_after' );
		/**
		 * Hook: woocommerce_after_main_content.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
		?>

		<?php do_action( 'flatsome_products_page_loader' ); ?>
		</div><!-- col-fit  -->

		<div class="large-3 col hide-for-medium <?php flatsome_sidebar_classes(); ?>">
			<!-- Datnt custom -->
			<?php $cat = get_queried_object();
				$args = array(
				    'taxonomy'   => "product_cat",
				    'hierarchical'     => 1,
				    'parent'    => $cat->term_id,
				    'hide_empty' => 0
				);
			  	$cats = get_categories($args);
			?>
			<div id="shop-sidebar" class="sidebar-inner">
				<?php if (count($cats)): ?>
					<?php foreach ($cats as $scat): 
						$args = array(
						    'taxonomy'   => "product_cat",
						    'hierarchical'     => 1,
						    'parent'    => $scat->term_id,
						    'hide_empty' => 0
						);
					  	$listSubCats = get_categories($args); ?>
						<aside id="nav_menu-<?=$scat->term_id?>" class="widget widget_nav_menu">
							<a href="<?= get_term_link( $scat->slug, 'product_cat' );?>">
								<span class="widget-title shop-sidebar"><?=$scat->name?></span>
							</a>
							<div class="is-divider small"></div>
							<div class="menu-<?=$scat->slug?>-container">
								<ul id="menu-<?=$scat->slug?>" class="menu">
									<?php foreach ($listSubCats as $item): ?>
										<li id="menu-item-1770" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1770">
											<a href="<?= get_term_link( $item->slug, 'product_cat' );?>">
												<?=$item->name?>
											</a>
										</li>
									<?php endforeach ?>
								</ul>
							</div>
						</aside>
						
					<?php endforeach ?>
				<?php else: 
				  $no_widgets_msg = '<p>Không có danh mục con</p>';
				  echo $no_widgets_msg;
				endif ?>
			</div><!-- .sidebar-inner -->
			<!-- /Datnt custom -->
		</div><!-- large-3 -->
</div>
