<?php
/**
 * The template for displaying the footer.
 *
 * @package flatsome
 */

global $flatsome_opt;
?>

</main><!-- #main -->

<footer id="footer" class="footer-wrapper">

	<?php do_action('flatsome_footer'); ?>

</footer><!-- .footer-wrapper -->

</div><!-- #wrapper -->

<?php wp_footer(); ?>
<?php if (is_front_page()): ?>
	<script>
		var index = 0;
		jQuery(document).ready(function($) {
			if($( window ).width() < 640){
				jQuery('#content > div.searchform-wrapper.row.custom-searchform.ux-search-box.relative.form-.is-normal > form > div.flex-row.relative > div.flex-col.flex-grow > input.search-field.mb-0').attr('placeholder', 'Tìm kiếm ...');
		  	}else{
		  		var sliderSwapper = $(".new-posts-shortcode-template");
				var slides = sliderSwapper.find('div.product-small.col');
				var slideHeight = slides.eq(0).outerHeight();
				
				sliderSwapper.height( slideHeight * 2 - 10);
				$('.move-down').click(function(){
			        index += 2;
			        if(index >= slides.length){
			        	index = 0;
			        }
					slides.eq(0).css ({
			        	'margin-top': - slideHeight * index 
			      	});	
				});
				$('.move-up').click(function(){
			        index -= 2;
			        if(index <= 0){
			        	index = slides.length - 2;
			        }
					slides.eq(0).css ({
			        	'margin-top': - slideHeight * index
			      	});	
				});
		  	}
			$( window ).resize(function() {
			  	if($( window ).width() > 640){
					jQuery('#content > div.searchform-wrapper.row.custom-searchform.ux-search-box.relative.form-.is-normal > form > div.flex-row.relative > div.flex-col.flex-grow > input.search-field.mb-0').attr('placeholder', 'Gõ vào đây đế tìm bánh ưng ý nhé...');
			  	}else{
					jQuery('#content > div.searchform-wrapper.row.custom-searchform.ux-search-box.relative.form-.is-normal > form > div.flex-row.relative > div.flex-col.flex-grow > input.search-field.mb-0').attr('placeholder', 'Tìm kiếm ...');
			  	}
			});

			
		});
	</script>
<?php endif ?>
</body>
</html>
