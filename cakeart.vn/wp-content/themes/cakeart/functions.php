<?php
/**
 * Flatsome functions and definitions
 *
 * @package flatsome
 */

require get_template_directory() . '/inc/init.php';

/**
 * Note: It's not recommended to add any custom code here. Please use a child theme so that your customizations aren't lost during updates.
 * Learn more here: http://codex.wordpress.org/Child_Themes
 */


function get_all_categories_by_recurie($cat, $parent){
	$res = $GLOBALS['db']->get_rows('cakecategory', ['parent' => $parent]);
	if(!count($res)) return [];
	$cat = $res;
	for ($i=0; $i < count($cat); $i++) { 
		$cat[$i]['children'] = get_all_categories_by_recurie([], $cat[$i]['code']);
	}
	return $cat;
}
function excuteTranfer(){
	$db = new DB();
	$db->VN_tranfer();
	$cats = get_all_categories_by_recurie([], 0);

	foreach ($cats as $cat) {
		tranferData($cat, ['parent' => '', 'id' => 0]);
	}
}

function tranferData($cat, $parent){
	$cid = $cat['catID'];
	if($cat['catID'] == 0){
		$cid = wp_insert_term(
	        $parent['parent'] . ' ' . $cat['name'], // the term 
	        'product_cat', // the taxonomy
	        array(
	        	// 'slug' => $cat['code'],
	            'parent' => $parent['id']
	        )
	    );
	    if(is_array($cid) && isset($cid['term_id'])){
		    $GLOBALS['db']->update('cakecategory', ['catID' => $cid['term_id']], [ 'id' => $cat['id'] ]);
		    $cid = $cid['term_id'];
	    }
	}else{
		wp_update_term($cid, 'product_cat', array(
		  'name' => $cat['name']
		));
	}
    if(count($cat['children'])){
    	foreach ($cat['children'] as $subCat) {
    		tranferData($subCat, ['name' => $cat['parent'], 'id' => $cid]);
    	}
    }
}

/**
 * SQL to delete
DELETE a,c FROM wp_terms AS a 
LEFT JOIN wp_term_taxonomy AS c ON a.term_id = c.term_id
LEFT JOIN wp_term_relationships AS b ON b.term_taxonomy_id = c.term_taxonomy_id
WHERE c.taxonomy = 'product_tag' AND a.term_id > 120;

DELETE a,c FROM wp_terms AS a
LEFT JOIN wp_term_taxonomy AS c ON a.term_id = c.term_id
LEFT JOIN wp_term_relationships AS b ON b.term_taxonomy_id = c.term_taxonomy_id
WHERE c.taxonomy = 'product_cat' AND a.term_id > 120
 */


/**
 * Cập nhật danh mục và đẩy các danh mục vào menu
 */

// $args = array(
//     'taxonomy'   => "product_cat",
//     'hierarchical'     => 1,
//     'parent'    => 0,
//     'hide_empty' => 0,
//     'exclude'    => array(15, 90, 98, 77, 64, 114)
// );
// $next = get_categories($args);
// if( count($next) > 0){
// 	foreach ($next as $cat) {
// 		$menu_id = wp_update_nav_menu_item(61, 0, array(
// 		    'menu-item-title' => $cat->name,
// 		    'menu-item-object-id' => $cat->term_id,
// 		    'menu-item-url' => get_term_link( $cat->slug, 'product_cat' ),
// 		    'menu-item-status' => 'publish'
// 		));
// 		$args2 = array(
// 		    'taxonomy'   => "product_cat",
// 		    'hierarchical' => 1,
// 		    'parent'    => $cat->term_id,
// 		    'hide_empty' => 0
// 		);
// 	  	$nextpage = get_categories($args2);
// 	  	foreach ($nextpage as $item) {
// 	  		wp_update_nav_menu_item(61, 0, array(
// 			    'menu-item-title' => $item->name,
// 			    'menu-item-object-id' => $item->term_id,
// 			    'menu-item-parent-id' => $menu_id,
// 			    'menu-item-url' => get_term_link( $item->slug, 'product_cat' ),
// 			    'menu-item-status' => 'publish'
// 			));
// 	  	}
// 	}
// }  
// 
