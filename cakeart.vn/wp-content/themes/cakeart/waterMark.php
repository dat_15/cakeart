<?php

class imageLib {

    private $fileName;
    private $image;
    private $width;         # Current width (width after resize)
    private $height;
    private $imageSize;
    protected $imageResized;
    private $fileExtension;
    private $fillColorArray = array('r'=>255, 'g'=>255, 'b'=>255);
    private $fontDir = 'font';

    function __construct($fileName) {
        if (!$this->testGDInstalled()) { 
            die('The GD Library is not installed.'); 
        };
        $this->fileName = $fileName;
        $this->fileExtension = strtolower(strrchr($fileName, '.'));
        $this->image = $this->openImage($fileName);
        $this->width  = imagesx($this->image);
        $this->height = imagesy($this->image);
        $this->imageSize = getimagesize($this->fileName);
        $this->imageResized = $this->image;
    }

    public function addText($text, $pos = '20x20', $padding = 00, $fontColor='#fff', $angle = 0, $font = null){
        $rgbArray = $this->formatColor($fontColor);
        $r = $rgbArray['r'];
        $g = $rgbArray['g'];
        $b = $rgbArray['b'];
        $fontSize = ceil($this->height * 0.05);
        $font = $this->getTextFont($font);
        $textSizeArray = $this->getTextSize($fontSize, $angle, $font, $text);
        $textWidth = $textSizeArray['width'];
        $textHeight = $textSizeArray['height'];
        $posArray = $this->calculatePosition($pos, $padding, $textWidth, $textHeight, false);
        $x = $posArray['width'];
        $y = $posArray['height'];
        $fontColor = imagecolorallocate($this->imageResized, $r, $g, $b);

        imagettftext($this->imageResized, $fontSize, $angle, $x, $y, $fontColor, $font, $text);
        ob_start(); 
        switch($this->fileExtension)
        {
            case '.jpg':
            case '.jpeg':
		        imagejpeg($this->image); 
                break;
            case '.gif':
		        imagegif($this->image); 
                break;
            case '.png':
		        imagepng($this->image); 
                break;
        }
        $contents = ob_get_contents(); 
        ob_end_clean(); 
        return $contents;
    }


    private function getTextFont($font){
        // *** Font path (shou
        $fontPath =  dirname(__FILE__) . '/' . $this->fontDir;
        putenv('GDFONTPATH=' . realpath('.'));
        if ($font == null || !file_exists($font)) {
            $font = $fontPath . '/Arial.ttf';
            if (!file_exists($font)) {
                if ($this->debug) { die('Font not found'); }else{ return false; }
            }
        }
        return $font;
    }


    private function getTextSize($fontSize, $angle, $font, $text) {
        $box = @imageTTFBbox($fontSize, $angle, $font, $text);
        $textWidth = abs($box[4] - $box[0]);
        $textHeight = abs($box[5] - $box[1]);
        return array('height' => $textHeight, 'width' => $textWidth);
    }

    private function calculatePosition($pos, $padding, $assetWidth, $assetHeight, $upperLeft = true){
        $pos = strtolower($pos);
        if (strstr($pos, 'x')) {
            $pos = str_replace(' ', '', $pos);
            $xyArray = explode('x', $pos);
            list($width, $height) = $xyArray;
        } else {
            switch ($pos) {
                case 'tl':
                    $width = 0 + $padding;
                    $height = 0 + $padding;
                    break;

                case 't':
                    $width = ($this->width / 2) - ($assetWidth / 2);
                    $height = 0 + $padding;
                    break;

                case 'tr':
                    $width = $this->width - $assetWidth - $padding;
                    $height = 0 + $padding;;
                    break;

                case 'l':
                    $width = 0 + $padding;
                    $height = ($this->height / 2) - ($assetHeight / 2);
                    break;

                case 'm':
                    $width = ($this->width / 2) - ($assetWidth / 2);
                    $height = ($this->height / 2) - ($assetHeight / 2);
                    break;

                case 'r':
                    $width = $this->width - $assetWidth - $padding;
                    $height = ($this->height / 2) - ($assetHeight / 2);
                    break;

                case 'bl':
                    $width = 0 + $padding;
                    $height = $this->height - $assetHeight - $padding;
                    break;

                case 'b':
                    $width = ($this->width / 2) - ($assetWidth / 2);
                    $height = $this->height - $assetHeight - $padding;
                    break;

                case 'br':
                    $width = $this->width - $assetWidth - $padding;
                    $height = $this->height - $assetHeight - $padding;
                    break;

                default:
                    $width = 0;
                    $height = 0;
                    break;
            }
        }
        if (!$upperLeft) {
            $height = $height + $assetHeight;
        }
        return array('width' => $width, 'height' => $height);
    }

    private function openImage($file) {

        if (!file_exists($file) && !$this->checkStringStartsWith('http://', $file)) { if ($this->debug) { die('Image not found.'); }else{ die(); }};

        $extension = strrchr($file, '.');
        $extension = strtolower($extension);

        switch($extension)
        {
            case '.jpg':
            case '.jpeg':
                    $img = @imagecreatefromjpeg($file);
                    break;
            case '.gif':
                    $img = @imagecreatefromgif($file);
                    break;
            case '.png':
                    $img = @imagecreatefrompng($file);
                    break;
            default:
                    $img = false;
                    break;
        }

        return $img;
    }


    public function testGDInstalled() {
        if(extension_loaded('gd') && function_exists('gd_info')) {
            $gdInstalled = true;
        }
        else {
            $gdInstalled = false;
        }
        return $gdInstalled;
    }


    public function setFile($fileName) {
        self::__construct($fileName);
    }


    public function getFileName() {
        return $this->fileName;
    }


    public function getHeight() {
        return $this->height;
    }


    public function getWidth() {
        return $this->width;
    }

    public function getExtention(){
    	return $this->fileExtension;
    }


    protected function formatColor($value) {
        $rgbArray = array();

        // *** If it's an array it should be R, G, B
        if (is_array($value)) {

            if (key($value) == 0 && count($value) == 3) {

                $rgbArray['r'] = $value[0];
                $rgbArray['g'] = $value[1];
                $rgbArray['b'] = $value[2];

            } else {
                $rgbArray = $value;
            }
        } else if (strtolower($value) == 'transparent') {

            $rgbArray = array(
                'r' => 255,
                'g' => 255,
                'b' => 255,
                'a' => 127
            );

        } else {
            $rgbArray = $this -> hex2dec($value);
        }

        return $rgbArray;
    }

    function hex2dec($hex)
    # Purpose:  Convert #hex color to RGB
    {
        $color = str_replace('#', '', $hex);

        if (strlen($color) == 3) {
            $color = $color . $color;
        }

        $rgb = array(
            'r' => hexdec(substr($color, 0, 2)),
            'g' => hexdec(substr($color, 2, 2)),
            'b' => hexdec(substr($color, 4, 2)),
            'a' => 0
        );
        return $rgb;
    }


    function checkStringStartsWith($needle, $haystack) {
        return (substr($haystack, 0, strlen($needle))==$needle);
    }

    public function __destruct() {
        if (is_resource($this->imageResized)) {
            imagedestroy($this->imageResized);
        }
    }


}

?>
