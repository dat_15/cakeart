<?php 

/**
 * Biến chung
 */

// Đường dẫn
define('base_url', 'http://www.cakeart.vn/order/');
define('DOMAIN', 'http://www.cakeart.vn/order/');
define('LOGIN_PAGE', DOMAIN.'login');
define('LOGOUT_PAGE', DOMAIN.'logout');
define('REGISTER_PAGE', DOMAIN.'register');
define('UPLOAD_PATH', 'http://www.cakeart.vn/intranet/images/');
define('EMPLOYER_IMAGE_PATH', UPLOAD_PATH.'nhanvien/');


// Config 
define('PER_PAGE', 10);
define('ENCODE_1', 985200459);
define('ENCODE_2', 7713);




/**
 * Bảng trong DB
 */
define('USER_TABLE', 'nhanvien');
define('USER_TYPE_TABLE', 'kieunhanvien');
define('USER_PERMISSION_TABLE', 'quyennhanvien');
define('USER_MODULES_TABLE', 'modules');
define('CUSTOMER_TABLE', 'khachhang');
define('INSERT_ORDER_TABLE', 'nhaphoadon');
define('ORDER_TABLE', 'orders');
define('ORDER_DETAIL_TABLE', 'order_details');
define('WORK_ORDER_TABLE', 'congviec');
define('DELIVERY_TABLE', 'quanlydonnhan');


/**
 * trạng thái công việc
 */

define('NOT_START', 0);
define('WORKING', 1);
define('DONE', 2);



 ?>