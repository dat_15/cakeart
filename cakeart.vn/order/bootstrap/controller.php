<?php
/**
* Author : Nguyen Tien Dat
*/
abstract class Controller{
  	private $data;
    protected $name;
  	protected $userInfo;
  	public function __construct(){
        $this->name = strtolower(get_class($this));
        
        self::check_auth(); 
    }

   	protected function __get($varName){
      	if (!array_key_exists($varName,$this->data)){
          	//this attribute is not defined!
          	throw new Exception();
      	}
      	else return $this->data[$varName];
   	}
    /**
     * load model và tự tạo biến mới
     * @param  [string] $model [tên model]
     * @return [object]        [null hoặc model]
     */
   	protected function load_model($name = ''){
        if ($name == '') $name = $this->name;
        if (file_exists("modules/".strtolower($name)."/model.php")) {
            require "modules/".strtolower($name)."/model.php";
            $cls1 = $name."_model";
            return new $cls1;
        }else{
            echo "Model " . $name . " does not existed";
            return;
        }
   	}
    /**
     * Load view trên controller, biến sử dụng bên dạng $ + index của mảng
     * Ex: Controller: $data['title'] = "Page title";
     *     View: echo $title
     * @param  [string] $view  [tên view]
     * @param  array  $param [mảng chứa các biến truyền vào view]
     * @return [null]        [nếu không tìm thấy file sẽ trả về null]
     */
    protected function load_view($view, $param = array(), $module = ''){
        if($module == ''){
            $module = $this->name;
        }
        if (file_exists("modules/". $module . "//views/" . $view . ".php")) {
            if (!is_array($param)) {
                $param = is_object($param) ? get_object_vars($param) : array();
            }
            extract($param);
            // include file 
            include_once "modules/". $module . "//views/" . $view . ".php";
        }else{
            echo "View " . $view . " does not existed";
            return;
        }
    }


    protected function randomToken($length) {
        $characters = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randstring;
    }

    protected function check_auth(){
        $isLoggedIn = Session::get_session('isLoggedIn');
        $manv       = Session::get_session('manv', $user['manv']);
        $tendangnhap = Session::get_session('tendangnhap', $user['tendangnhap']);
        if($isLoggedIn !== true){
            return false;
        }
        elseif($manv === null || $manv === '' || $tendangnhap === null || $tendangnhap === ''){
            return false;
        }
        else{
            $table = USER_TABLE;
            $db = new DB();
            $db->connect_to('tinyorder');
            $checkUser = $db->get_row($table, [
              'tendangnhap' => $tendangnhap,
              'manv' => $manv
            ]);
            if(!count($checkUser)) return false;
            $this->userInfo = $checkUser;
        }
    }

    protected function checkValidEmail($email){
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    protected function checkValidPhoneNumber($phonenumber){
        return preg_match('/^0(1\d{9}|9\d{8})$/', $phonenumber);
    }

    protected function checkValidPassword($password){
        return preg_match('/^(?=.*[A-Z])(?=.*[!@#_])(?=.*[0-9])(?=.*[a-z]).{8,30}$/', $phonenumber);
    }

    protected function checkValidUsername($username){
        return preg_match('/^[a-zA-Z0-9_]{8,30}$/', $username);
    }

    protected function checkValidFullname($fullname){
        // return preg_match('/^[a-zA-Z0-9 ]{8,30}$/', self::cleanString($fullname) );
        return preg_match('/^[a-zA-Z0-9 ]{8,30}$/', $fullname );
    }

    protected function check_valid_fullname($fullname){
        return preg_match('/^[ÀÁẠẢÃÂẦẤẬẨẪĂẮẲẶẴẰÈÉẸẺẼÊỀẾỆỂỄÌÍỊỈĨÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠÙÚỤỦŨƯỪỨỰỬỮỲÝỴỶỸĐàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđa-zA-Z0-9 ]{5,30}$/', $fullname);
    }
    protected function check_valid_address($fullname){
        return preg_match('/^[ÀÁẠẢÃÂẦẤẬẨẪĂẮẲẶẴẰÈÉẸẺẼÊỀẾỆỂỄÌÍỊỈĨÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠÙÚỤỦŨƯỪỨỰỬỮỲÝỴỶỸĐàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđa-zA-Z0-9, ]{2,30}$/', $fullname);
    }

    protected function check_valid_sirname($sirname){
        return preg_match('/^[ÀÁẠẢÃÂẦẤẬẨẪĂẮẲẶẴẰÈÉẸẺẼÊỀẾỆỂỄÌÍỊỈĨÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠÙÚỤỦŨƯỪỨỰỬỮỲÝỴỶỸĐàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđa-zA-Z0-9 ]{2,25}$/', $sirname);
    }


    protected function check_valid_price($num){
        return preg_match('/^[1-9]\d{1,10}$/', $num);
    }

    protected function check_valid_quantity($num){
        return preg_match('/^[1-9]\d{0,10}$/', $num);
    }


    // protected function cleanString($text) {
    //     $text = strtolower($text);
    //     // $text = str_replace('/', '-', $text);
    //     // $text = str_replace('"', '', $text);
    //     // $text = str_replace("'", "", $text);
    //     $utf8 = array(
    //         '/[áàâãªäăạảắẳẵằặấầẩẫậ]/u'      =>   'a',
    //         // '/[ÁÀÂÃÄĂẠẢẴẮẲẶẰẦẤẬẨ]/u'        =>   'a',
    //         // '/[ÍÌÎÏỊĨỈ]/u'                  =>   'i',
    //         '/[íìîïịĩỉ]/u'                  =>   'i',
    //         '/[éèêëẹẽếềễệẻể]/u'             =>   'e',
    //         // '/[ÉÈÊËẸẼẺẾỀỂỄỆ]/u'             =>   'e',
    //         '/[óòôõºöọỏơờởớợỡồổốộ]/u'       =>   'o',
    //         // '/[ÓÒÔÕÖỎỌƠỞỢỚỜỠỒỔỐỖỘ]/u'       =>   'o',
    //         '/[úùûüũụủưứừửữự]/u'            =>   'u',
    //         // '/[ÚÙÛÜŨỤỦƯỨỪỬỮỰ]/u'            =>   'u',
    //         '/ç/'                           =>   'c',
    //         // '/Ç/'                           =>   'c',
    //         '/ñ/'                           =>   'n',
    //         // '/Ñ/'                           =>   'n',
    //         '/–/'                           =>   '-', // UTF-8 hyphen to "normal" hyphen
    //         // '/[’‘‹›‚]/u'                    =>   '', // Literally a single quote
    //         // '/[“”«»„]/u'                    =>   '' // Double quote
    //     );
    //     return preg_replace(array_keys($utf8), array_values($utf8), $text);
    // }
}