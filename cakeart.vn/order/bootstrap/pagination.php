<?php 


/**
 * Phân trang
 */
class Pagination {
	protected $total;
	protected $totalPage;
	protected $perPage;
	protected $currentPage;
	protected $nextPage;
	protected $previousPage;
	protected $wrapperClass;
	protected $ulClass;
	protected $liClass;
	protected $pageToShow;
	protected $link;
	protected $showHTML;
	// protected $showGoToPage;

	function __construct($option){
		$this->total 		= (int) $option['total'];
		$this->perPage 		= (int) $option['perPage'];
		$this->totalPage 	= ceil($this->total / $this->perPage);
		$this->currentPage 	= (int) $option['currentPage'];
		$this->nextPage 	= $this->totalPage == $this->currentPage ? null : $this->currentPage + 1;
		$this->previousPage	= 0 == $this->currentPage ? null : $this->currentPage - 1;
		$this->wrapperClass = isset($option['wrapperClass']) ? $option['wrapperClass'] : 'uk-container';
		$this->ulClass 		= isset($option['ulClass']) ? $option['ulClass'] : 'uk-pagination uk-margin-medium-top';
		$this->pageToShow 	= isset($option['pageToShow']) ? (int) $option['pageToShow'] : 4;
		$this->link 		= isset($option['link']) ? $option['link'] : DOMAIN;
		$this->showHTML 	= isset($option['showHTML']) ? $option['showHTML'] : true;
		// $this->showGoToPage	= isset($option['showGoToPage']) ? $option['showGoToPage'] : true;
		if($this->currentPage < 1) $this->currentPage = 1;
		if($this->currentPage > $this->totalPage) $this->currentPage = $this->totalPage;
	}

	public function getCurrentPage(){
		return $this->currentPage;
	}

	public function getLink(){
		return $this->link;
	}

	public function getTemplate(){
		if($this->totalPage < 2) return "<strong>Có " . $this->total . " kết quả</strong>";
		return "
			<div class='".$this->wrapperClass."'>
				<ul class='".$this->ulClass."' uk-margin>
					". self::getPreviousTemplate(). "
					<li class='active'>
						<span>".$this->currentPage."</span>
					</li>
					". self::getNextsTemplate(). "
					". self::getHTML() ."
					". self::getGoToPage() ."
				</ul>
			</div>
		";
	}

	private function getPreviousTemplate(){
		$template = '';
		if($this->currentPage > $this->pageToShow+2){
			$template = "<li>
				<a href='".$this->link."/1' >Về đầu</a>
			</li>
			<li class='uk-disabled'><span>...</span></li>";
		}
		for ($i= $this->currentPage - $this->pageToShow; $i < $this->currentPage; $i++) { 
			if($i < 1) continue;
			$template .= "<li>
				<a href='".$this->link.'/'.$i."' >".$i."</a>
			</li>";
		}
		return $template;
	}

	private function getNextsTemplate(){
		$template = '';
		for ($i= $this->currentPage + 1 ; $i < $this->currentPage + $this->pageToShow; $i++) { 
			if($i > $this->totalPage) continue;
			$template .= "<li>
				<a href='".$this->link.'/'.$i."' >".$i."</a>
			</li>";
		}
		if($this->currentPage < $this->totalPage - 2){
			$template .= "
				<li class='uk-disabled'><span>...</span></li>
				<li>
					<a href='".$this->link."/".$this->totalPage."' >Về Cuối</a>
				</li>
			";
		}
		return $template;
	}

	private function getHTML(){
		if( !$this->showHTML ) return '';
		return "<strong>Trang số " . $this->currentPage . " trên " . $this->totalPage . "</strong>";
	}

	private function getGoToPage(){
		if( !$this->showGoToPage ) return '';
		return "
	        <div class=\"uk-form-controls\">
	            <select class=\"uk-select\" id=\"form-stacked-select\">
	                <option>Option 01</option>
	                <option>Option 02</option>
	            </select>
	        </div>
		";
	}
}

 ?>