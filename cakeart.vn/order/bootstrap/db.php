<?php 

/**
* Author Nguyễn Tiến Đạt
*/
class DB {
	private $HOST_NAME = "mysql.tinycake.vn";
    private $USER_NAME = "tinycakevn";
    private $USER_PASS = "DjjfVX10Rmk";
    private $DB_NAME = "nguyenlieutiny";
    
 //    private $HOST_NAME = "localhost";
	// private $USER_NAME = "root";
	// private $USER_PASS = "";
	// private $DB_NAME = "test";
	private $conn;
    private $maxLimit = 1000;
	function __construct(){
		$this->conn = new mysqli($this->HOST_NAME, $this->USER_NAME, $this->USER_PASS, $this->DB_NAME);
        if ($this->conn->connect_errno) {
            die ("Failed to connect to MySQL: (" . $this->conn->connect_errno . ") " . $this->conn->connect_error );
        }
    }

    public function connect_to($db_name){
        $new_conn = new mysqli($this->HOST_NAME, $this->USER_NAME, $this->USER_PASS, $db_name);
        if ($new_conn->connect_errno) {
            die ("Failed to connect to MySQL: (" . $new_conn->connect_errno . ") " . $new_conn->connect_error );
        }else{
            $this->conn = $new_conn;
        }
    }

    public function VN_tranfer(){
    	$stmt = $this->conn->prepare('SET NAMES utf8');
    	$stmt->execute();

    	$stmt = $this->conn->prepare("set collation_connection='utf8_general_ci'");
    	$stmt->execute();

    }

	/**
     * [get_table Show table's records]
     * @param  [string] $table_name [Table name]
     * @param  [int]    $limit      [Limit number of records received]
     * @return [array]  [An Array contains all record's data ]
     */
    public function get_table($table_name, $limit = 1000, $offset = 0) {

        $result = $this->conn->query("SELECT * FROM $table_name LIMIT $limit OFFSET $offset;");
        $data = array();

        // Kiểm tra có kết quả trả về hay không
        if ($result !== false && $result->num_rows > 0) {

            // Gán dữ liệu từ sql vào mảng trả về
            while($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
        }

        return $data;
    }
    /**
     * [sql Handle an sql query]
     * @param  [string] $sql [sql query]
     * @return [array]      [trả về mảng dữ liệu]
     */
    public function query($sql) {

        $result = $this->conn->query($sql);
        $data = array();

        // Kiểm tra có kết quả trả về hay không
        if ($result !== false && $result->num_rows > 0) {
            // Gán dữ liệu từ sql vào mảng trả về
            while($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
            return $data;
        }
        elseif($result !== false && $result->num_rows < 1){
            // Chỉ thực hiện lệnh bình thường
            return true;
        }
        // Lỗi
        return false;

    }

    /**
     * [add_row Thêm một bản ghi mới vào bảng]
     * @param [array] $data  [mảng dữ liệu đầu vào]
     * @param [string] $table [tên bảng để thêm dữ liệu]
     * @return [boolean/int] [trạng thái xử lý lệnh (false) hoặc ID bản ghi mới thêm]
     */
    public function add_row($data, $table){
        // Lấy tên các trường trong bảng
        $key = array_keys($data);
        $sql = "INSERT INTO `" . $table . "`(" . "`" . implode("`,`",$key) . "`" . ") VALUES (" . "\"" . implode("\",\"",$data) . "\"" . ");";
        if ( $this->conn->query($sql) )
            // Trả về ID bản ghi mới thêm
            return intval($this->conn->insert_id);
        else 
            // Lỗi insert
            return $this->conn->error;
    }


    /**
     * [get_row Lấy dữ liệu trên một bản ghi]
     * @param  [string] $table [Tên bảng]
     * @param  [array] $where [mảng điều kiện]
     * @return [array]        [mảng dữ liệu trả về nếu có hoặc một mảng rỗng]
     */
    public function get_row($table, $where){

        $sql = "SELECT * FROM $table";
        $sql .= self::build_where($where);

        $result = $this->conn->query($sql);
        // Kiểm tra dữ liệu trả về có hoặc không
        if ($result !== false && $result->num_rows > 0)
            return $result->fetch_assoc();
        else 
            return array();
    }


    /**
     * [get_rows Lấy dữ liệu trên nhiều bản ghi]
     * @param  [string] $table [Tên bảng]
     * @param  [array] $where [mảng điều kiện]
     * @return [array]        [mảng dữ liệu trả về nếu có hoặc một mảng rỗng]
     */
    public function get_rows($table, $where, $limit = 10000, $offset = 0, $orderby = ''){

        $sql = "SELECT * FROM $table ";
        $sql .= self::build_where($where);
        $sql .= $orderby;
        $sql .= " LIMIT $limit OFFSET $offset;";
        $result = $this->conn->query($sql);
        $data = array();
        // Kiểm tra dữ liệu trả về có hoặc không
        if ($result !== false && $result->num_rows > 0) {
            // Gán dữ liệu vào mảng trả về
            while($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
        }

        return $data;
    }



    /**
     * [get_rows Lấy dữ liệu trên nhiều bản ghi]
     * @param  [string] $table [Tên bảng]
     * @param  [array] $where [mảng điều kiện]
     * @return [array]        [mảng dữ liệu trả về nếu có hoặc một mảng rỗng]
     */
    public function count_all($table){
        $sql = "SELECT COUNT(*) FROM $table;";
        $result = $this->conn->query($sql);
        if ($result !== false && $result->num_rows > 0)
            return $result->fetch_assoc();
        else 
            return array();
    }

    /**
     * [update Cập nhật một bản ghi]
     * @param  [string] $table [tên bảng]
     * @param  [array] $data  [thông tin cập nhật]
     * @param  [array] $where [Điều kiện]
     * @return [boolean]        [trạng thái cập nhật(true/false)]
     */
    public function update($table, $data, $where){

        $sql = "UPDATE `$table` SET ";
        $i = 0;
        // Set thông tin cập nhật
        foreach ($data as $key => $value) {
            $sql .= "`" . $key . "`='" . $value ."' ";
            if (count($data) < 2) {
                break;
            }
            elseif (++$i !== count($data)) {
                $sql .= ", ";
            }
            
        }

        $sql .= self::build_where($where);
        return $this->conn->query($sql);

    }

    protected function build_where($_where){
        if (!is_array($_where) && $_where == 1) return "";
        if (!is_array($_where) && $_where != 1) return " WHERE " . $_where;
        $condition = " WHERE ";
        $i = 0;
        // Xử lý mảng điều kiện
        foreach ($_where as $key => $value) {
            if ($key == 1) return '';
            // Gán điều kiện
            if (strpos('<', $key) || strpos('>', $key)) {
                if (is_int($value)) {
                    $condition .= $key . " " . $value;
                } else {
                    $condition .= $key . " '" . $value . "' ";
                }
            } elseif (strpos(strtolower($key), 'or_like') != false) {
                $condition .= explode('or_like', $key)[0] . " LIKE '%" . $value . "%' ";
            } elseif (strpos(strtolower($key), 'like') != false) {
                $condition .= explode('like', $key)[0] . " LIKE '%" . $value . "%' ";
            } else {
                $condition .= $key . "='" . $value . "' ";
            }


            // Nếu mảng chỉ có một phần tử thì không thêm dấu ','
            // EX: "SELECT * FROM 'table' WHERE ID=1;"
            if (count($_where) < 2) {
                break;
            }
            // Thêm liên từ
            elseif (++$i !== count($_where)) {
                $condition .= strpos(strtolower($key), 'or_like') ? 'OR ' : "AND ";
            }
        }
        return $condition;
    }


    /**
     * [delete Xóa một bản ghi]
     * @param  [string] $table [tên bảng]
     * @param  [array] $where Mảng các điều kiện ràng buộc
     * @return [boolean]        [trạng thái thực thi lệnh(true/false)]
     */
    public function delete($table, $where){

        $sql = "DELETE FROM $table";

        $sql .= self::build_where($where);
        
        return $this->conn->query($sql);
    }


    public function __destruct(){
        $this->conn->close();
    }
}

 ?>