<?php  


/**
 * Author: Nguyen Tien Dat
 */
class Session
{
    public function __construct(){}

    public function get_session($key){
    	return $_SESSION[$key];
    }

    public function set_session($key, $value){
    	$_SESSION[$key] = $value;
    }

    public function delete_session($key){
    	$_SESSION[$key] = null;
    }

    public function set_flash_message($key, $value){
        self::set_session($key, $value);
    }

    public function get_flash_session($key){
        $val = $_SESSION[$key];
        self::delete_session($key);
        return $val;
    }

    public function destroy_session(){
    	session_unset();
    	session_destroy();
    }
}

?>