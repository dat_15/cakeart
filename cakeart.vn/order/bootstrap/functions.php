<?php 

/*
 * en - de : code id
 */
function en_id($id){
	$id = intval($id) * ENCODE_2; //  15000
	$id = dechex($id + ENCODE_1);
	$id = str_replace(1,'J',$id);
	$id = str_replace(6,'V',$id);
	$id = str_replace(8,'S',$id);
	$id = str_replace(9,'R',$id);
	$id = str_replace(7,'T',$id);
	return strtoupper($id);
}
function de_id($id) {
	$id = str_replace('J',1,$id);
	$id = str_replace('V',6,$id);
	$id = str_replace('S',8,$id);
	$id = str_replace('R',9,$id);
	$id = str_replace('T',7,$id);
	$id = (hexdec($id) - ENCODE_1 )/ ENCODE_2;
	return $id;
}


function input($k = '*', $type='GP', $default = NULL) {
	$type = strtoupper($type);
	switch($type) {
		case 'G': $var = $_GET; break;
		case 'P': $var = &$_POST; break;
		case 'C': $var = &$_COOKIE; break;
		default:
			if(isset($_GET[$k])) {
				$var = &$_GET;
			} else {
				$var = &$_POST;
			}
			break;
	}

	$val =  isset($var[$k]) ? $var[$k] : $default;
	if($k === '*'){
		$val =  &$_POST;
	}
    if($key === '*' && $type == 'G') $val = &$_GET;
	if(!is_array($val)) {
        $val = cleanXssAndSqlInjection( trim($val, " \n") );
    }
    else{
    	foreach ($val as &$value) {
    		if(!is_array($value)) {
		        $value = cleanXssAndSqlInjection( trim($value, " \n") );
		    }
    	}
    }
	return $val;
}


function cleanXssAndSqlInjection($val) {
    $searchXss = array(
        '@SLEEP@si',
        '@sleep@si',

        '@</script[^>]*?>@si',
        '@<script[^>]*?>@si',
        '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
        "@<style[^>]*?>.*?</style>@siU",    // Strip style tags properly
        '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
        ,'@alert@isU'
        ,'@prompt@isU'
        ,'@window.location@isU'
        ,'@FSCommand@isU','@onAbort@isU','@onActivate@isU','@onAfterPrint@isU','@onAfterUpdate@isU','@onBeforeActivate@isU',
        '@onBeforeCopy@isU','@onBeforeCut@isU','@onBeforeDeactivate@isU','@onBeforeEditFocus@isU','@onBeforePaste@isU',
        '@onBeforePrint@isU','@onBeforeUnload@isU','@onBeforeUpdate@isU','@onBegin@isU','@onBlur@isU','@onBounce@isU',
        '@onCellChange@isU','@onChange@isU','@onClick@isU','@onContextMenu@isU','@onControlSelect@isU','@onCopy@isU',
        '@onCut@isU','@onDataAvailable@isU','@onDataSetChanged@isU','@onDataSetComplete@isU','@onDblClick@isU','@onDeactivate@isU',
        '@onDrag@isU','@onDragEnd@isU','@onDragLeave@isU','@onDragEnter@isU','@onDragOver@isU','@onDragDrop@isU','@onDragStart@isU',
        '@onDrop@isU','@onEnd@isU','@onError@isU','@onErrorUpdate@isU','@onFilterChange@isU','@onFinish@isU','@onFocus@isU','@onFocusIn@isU',
        '@onFocusOut@isU','@onHashChange@isU','@onHelp@isU','@onInput@isU','@onKeyDown@isU','@onKeyPress@isU','@onKeyUp@isU','@onLayoutComplete@isU',
        '@onLoad@isU','@onLoseCapture@isU','@onMediaComplete@isU','@onMediaError@isU','@onMessage@isU','@onMouseDown@isU','@onMouseEnter@isU',
        '@onMouseLeave@isU','@onMouseMove@isU','@onMouseOut@isU','@onMouseOver@isU','@onMouseUp@isU','@onMouseWheel@isU','@onMove@isU',
        '@onMoveEnd@isU','@onMoveStart@isU','@onOutOfSync@isU','@onPaste@isU','@onPause@isU','@onPopState@isU',
        '@onProgress@isU','@onPropertyChange@isU','@onReadyStateChange@isU','@onRedo@isU','@onRepeat@isU','@onReset@isU','@onResize@isU','@onResizeEnd@isU',
        '@onResizeStart@isU','@onResume@isU','@onReverse@isU','@onRowsEnter@isU','@onRowExit@isU','@onRowDeletet@isU','@onRowInserted@isU','@onScroll@isU',
        '@onSeek@isU','@onSelect@isU','@onSelectionChange@isU','@onSelectStart@isU','@onStart@isU','@onStorage@isU','@onSyncRestored@isU','@onSubmit@isU',
        '@onTimeErrort@isU','@onTrackChange@isU','@onUndo@isU','@onUnload@isU','@onURLFlip@isU','@seekSegmentTime@isU', '@iframe@isU' ,
    );
    $val=preg_replace($searchXss, '', $val);

    $searchSqlKey = array(
        '@database@isU',
        /*'@ OR@isU',*/
        /*'@OR @isU',*/
        '@BENCHMARK@isU',
        '@DROP@isU',
        '@DROP/*@isU',
        '@/\*@isU',
        '@\*/@isU',
        '@CONCAT@isU',
        '@UNION @isU',
        '@LOAD_FILE@isU',
        '@CREATE @isU',
        '@GRANT@isU',
        '@PRIVILEGES@isU',
        '@information_schema@isU',
        '@datadir@isU',
        '@CHR @isU',
        '@WAITFOR@isU',
        '@OUTFILE@isU',
        /*'@0x@isU',*/
        '@"HEX@isU',
        '@ASCII@isU',
        '@SUBSTRING@isU',
    );

    return preg_replace($searchSqlKey, '', $val);
}


function send_json($data){
	echo json_encode($data);
	die();
}