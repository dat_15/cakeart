<?php
class deliveryorder extends Controller {
    private $session;
    private $model;
    private $user_model;
    private $callbackUrl;
    private $modules = [];
    public function __construct() {
        parent::__construct(__CLASS__);
        $this->session = new Session();
        $this->model = $this->load_model();
        $this->user_model = $this->load_model('users');
        $this->callbackUrl = $this->session->get_session('goToUrl');
        if ($this->callbackUrl == '' || $this->callbackUrl == null) {
            $this->callbackUrl = DOMAIN;
        }
        if (!count($this->userInfo)) {
            header('Location: '.$this->callbackUrl);
        }
        if( !$this->user_model->isAccessibleModule('orders_input', (int) $this->userInfo['manv']) ) header('Location: '.DOMAIN);
        $this->modules = $this->user_model->getUserModules((int) $this->userInfo['manv']);
    }


    public function index() {
        $from = input('from', 'gpc', 'NOW');
        $nextweek = new DateTime($from);
        $from = new DateTime($from);
        $nextweek->add(new DateInterval('P7D'));
        $res = $this->model->getOrderByRang($from->format('Y-m-d'), $nextweek->format('Y-m-d'));
        $this->load_view('main-header', [
            'base_url'  => 'modules/header/',
            'title'     => 'Quản lý đơn hàng từ ngày '.$from->format('Y-m-d') . ' đến ' . $nextweek->format('Y-m-d'),
            'user'      => $this->userInfo
        ], 'header');
        $this->load_view('menu-top', ['modules' => $this->modules], 'header');
        $this->load_view(
            'index', 
            [
                'orders' => $res, 
                'from' => $from->format('Y-m-d'),
                'users' => $this->user_model->getUsersActive(100, 0, "manv,tendaydu"),
            ]
        );
        $this->load_view('footer', [], 'footer');
    }

    public function getWorkOfOrder(){
        $order_id = (int) input('order_id', '', 0);
        if ($order_id < 1) {
            echo json_encode([
                'status' => 0,
                'msg' => 'Order not found!'
            ]);
            die();
        }
        $order = $this->model->getOrderByID($order_id);
        if ($order === true || !count($order)) {
            echo json_encode([
            'status' => 0,
            'msg' => 'Order not found!'
            ]);
        }else{
            $schedue = $this->model->getWorkOfOrder($order_id);
            echo json_encode([
                'status' => count($schedue) ? 2 : 1,
                'data'  => $schedue,
                'msg'   => count($schedue) ? 'success' : 'Schedue not found'
            ]);
        }
    }

    public function addNewTask(){
        $data = input();
        unset($data['id']);
        unset($data['nhanvien']);
        $taskID = $this->model->addNewTaskToDB($data);
        if($taskID){
            echo json_encode([
                'status' => 1,
                'taskID' => $taskID
            ]);
        }else{
            echo json_encode([
                'status' => 0,
                'msg' => 'Failed'
            ]);
        }
    }

    public function deleteTask(){
        $id = input('id', '', 0);
        if(!$id){
            echo json_encode([
                'status' => 0,
                'msg' => 'No param id!'
            ]);
            die();
        }
        $this->model->deleteTask(['id' => $id]);
        echo json_encode([
            'status' => 1,
            'msg' => 'Xóa thành công!'
        ]);
        die();
    }

    public function saveTask(){
        $data = input();
        $this->model->updateTask(
            [
                'manv' => $data['manv'],
                'note'     => $data['note'],
                'tienthanhtoan' => (int) $data['tienthanhtoan'],
                'phi_ship'  => $data['phi_ship']
            ],
            [ 'id' => (int) $data['id'] ]
        );
        echo json_encode([
            'status'=> 1,
            'msg'   => 'Cập nhật thành công!',
        ]);
        die();
    }
}

 ?>
