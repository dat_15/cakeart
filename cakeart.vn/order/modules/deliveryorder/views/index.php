<script type="text/javascript" src="assets/js/moment.min.js?v=1528388700"></script>
<script type="text/javascript" src="assets/js/daterangepicker.min.js?v=1528388700"></script>
<link rel="stylesheet" type="text/css" href="assets/css/daterangepicker.css?v=1528388700" />
<style>
    td li {
        display: inline;
        list-style: square;
    }
    td li b {
        color: saddlebrown;
    }
    article h1 p li {
        display: inline;
        list-style: square;
    }
    article h3 p li b {
        color: saddlebrown;
    }
    li {
        display: inline;
        list-style: square;
    }
    li b {
        color: saddlebrown;
    }
    .header {
        text-align: center;
        font-size: 22px;
    }
    .uk-lightbox{
        background-color: rgba(102, 102, 102, 0.6);
    }
    thead {
        background-color: #333;
        color: #fff;
        font-weight: bold;
    }
    th {
        color: #fff !important;
        font-weight: bold !important;
        font-size: 12px !important;
    }
    td {
        padding: 0 0;
    }
    table {
        border-spacing: 0 0;
    }
    *+.uk-grid-margin-small, .uk-grid+.uk-grid-small, .uk-grid-small>.uk-grid-margin{
        margin-top: 0;
    }
    .loading{
        width: 100%;
        height: 100vh;
        margin: 0 auto;
        text-align: center;
        float: none;
        display: block;
        z-index: +1000;
        position: absolute;
        background: #fff;
        padding-top: 30%;
    }
    .uk-padding-small{padding: 1px!important;}
    .input-editting {
        border: 0;
        height: 30px!important;
        background: transparent;
        border-bottom: 1px dashed;
    }
    table.uk-table.uk-table-divider.uk-table-hover.uk-table-striped.uk-table-justify.uk-table-middle{
        width: 100%;
        overflow-x: auto;
        padding-right: 0px;
        padding-left: 0px;
    }
    [uk-lightbox] a img {
        float: left;
        width: 100px;
        height: 100px;
        margin-right: 10px;
    }
    article.uk-article {
        padding: 10px;
    }
</style>
<script type="text/javascript">
    let _listOrder = [];
    <?php foreach ($orders as $order): ?>
        _listOrder.push(<?php echo json_encode($order);?>);
    <?php endforeach ?>
    let _listUser = [];
    <?php foreach ($users as $user): ?>
        _listUser.push(<?php echo json_encode($user);?>);
    <?php endforeach ?>
    var orderApp = angular.module('order-app', []);
    let domain = "<?php echo DOMAIN; ?>";
</script>

<script type="text/javascript" src="assets/js/deliveryorder.js?v=<?= time()?>"></script>

<div class="uk-section" ng-app="order-app" ng-controller="deliveryorder-controller">
    <form class="uk-container uk-form uk-margin-bottom" action="" method="POST">
        <div class="uk-grid">
            <div class="uk-width-3-5">
                <input class="uk-input go-to-datepicker" name="from" value="<?= $from; ?>" type="text" >
            </div>
            <div class="uk-width-2-5">
                <button class="uk-button" type="submit">
                    Chọn
                </button>
            </div>
        </div>
    </form>
    <div>
        <div class="loading" uk-spinner="ratio: 3" ng-show="showLoading"></div>
        <!-- accordion kết hợp article-->
        <div id="my-accordion" class="uk-accordion" data-uk-accordion collapse>
        <div ng-show="listDates.length" ng-repeat="dat in listDates">
            <h3 class="uk-accordion-title uk-text-center">Đơn hàng giao ngày {{dat}}</h3>
            <div class="uk-accordion-content">
                <div ng-repeat="order in listOrder[dat]" >
                    <article class="uk-article">
                        <h4>
                            {{order.tenKH}} - {{order.giogiao}} 
                            <button uk-icon="icon: pencil" 
                                uk-toggle="target: #edit-order" 
                                type="button" 
                                ng-click="showEditOrderForm(order)"
                                class="uk-button uk-button-small uk-button-default uk-float-right">
                            </button>
                        </h4>
                        <p class="uk-article-meta">
                            <div uk-lightbox ng-if="order.hinhminhhoa != 'no.jpg' && order.hinhminhhoa != ''">
                                <a href="http://tinycake.vn/order/images/hinhminhhoa/small{{order.hinhminhhoa}}"  data-caption="{{order.loaibanh}}">
                                    <img ng-src="http://tinycake.vn/order/images/hinhminhhoa/small{{order.hinhminhhoa}}">
                                </a>
                            </div>
                            <div uk-lightbox ng-if="order.hinhketqua != 'no.jpg' && order.hinhketqua != ''">
                                <a
                                    ng-href="order.hinhketqua !== 'no.jpg' ? 'http://tinycake.vn/order/thuvienbanh/' + order.hinhketqua : 'http://tinycake.vn/order/images/hinhminhhoa/smallno.jpg'" 
                                    data-caption="{{order.loaibanh}}"
                                >
                                    <img ng-show="order.hinhketqua !== 'no.jpg'" ng-src="http://tinycake.vn/order/thuvienbanh/{{order.hinhketqua}}">
                                </a>
                            </div>
                            <li ng-show="order.diachi !=''">Địa chỉ giao: <b>{{order.diachi}}</b></li> 
                            <li ng-show="order.loaibanh !=''">Loại bánh: <b>{{order.loaibanh}}</b></li>
                            <li ng-show="order.kichthuoc !=''">Kích thước : {{order.kichthuoc}}</li>
                            <li ng-show="order.chieucao !=''"> Cao: {{order.chieucao}} </li>
                            <li ng-show="order.soluong > 0">Số lượng: {{order.soluong}} cái</li>
                            <li ng-show="order.mota != ''">Mô tả: {{order.mota | limitTo:50 }}</li>
                            <li ng-show="order.mausac != ''">Màu: {{order.mausac}}</li>
                            <li ng-show="order.nhanbanh != ''">Nhân: {{order.nhanbanh}}</li>
                            <li ng-show="order.cotbanh != ''">Cốt: {{order.cotbanh}}</li>
                            <li ng-show="order.sotang > 0">{{order.sotang}} tầng</li>
                            <li ng-show="order.chutrenbanh != ''">Chữ trên bánh: {{order.chutrenbanh}}</li>
                            <li ng-show="order.luuy!= ''">Lưu ý: <b>{{order.luuy}}</b></li><br>
                            <li ng-show="order.dongia!= ''">Doanh thu: <b>{{order.soluong * order.dongia|number}}</b></li>
                            <li ng-show="order.tiencoc!= ''"> - Cọc : <b>{{order.tiencoc |number}}</b></li>
                            <li ng-show="order.dongia!= ''"> Phải thu : <b>{{order.soluong * order.dongia - order.tiencoc |number}}</b></li>
                        </p>
                        <hr class="uk-article-divider">
                    </article>
                </div>
            </div>
        </div>

        <!-- This is the modal -->
        <div id="edit-order" class="uk-modal-full" uk-modal data-uk-modal="{bgclose:false, modal: false, keyboard: false}">
            <div class="uk-modal-dialog uk-modal-body uk-padding-small uk-padding-remove-horizontal uk-height-1-1" uk-overflow-auto>
                <!-- <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical uk-height-1-1" uk-overflow-auto> -->
                <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
                <h3 class="uk-modal-title uk-text-center">
                    Thông tin đơn hàng của {{curOrder.tenKH}}
                </h3>
                <article class="uk-article">
                    <h4>
                        {{curOrder.tenKH}} - {{curOrder.giogiao}} 
                    </h4>
                    <p class="uk-article-meta">
                        <div uk-lightbox ng-if="curOrder.hinhminhhoa != 'no.jpg' && curOrder.hinhminhhoa != ''">
                            <a href="http://tinycake.vn/order/images/hinhminhhoa/small{{curOrder.hinhminhhoa}}"  data-caption="{{curOrder.loaibanh}}">
                                <img ng-src="http://tinycake.vn/order/images/hinhminhhoa/small{{curOrder.hinhminhhoa}}" title="{{curOrder.hinhminhhoa}}">
                            </a>
                        </div>
                        <div uk-lightbox ng-if="curOrder.hinhketqua != 'no.jpg' && curOrder.hinhketqua != ''">
                            <a
                                ng-href="'http://tinycake.vn/order/thuvienbanh/' + curOrder.hinhketqua" 
                                data-caption="{{curOrder.loaibanh}}"
                            >
                                <img ng-src="http://tinycake.vn/order/thuvienbanh/{{curOrder.hinhketqua}}" title="{{curOrder.hinhketqua}}">
                            </a>
                        </div>
                        <li ng-show="curOrder.diachi !=''">Địa chỉ giao: <b>{{curOrder.diachi}}</b></li> 
                        <li ng-show="curOrder.loaibanh !=''">Loại bánh: <b>{{curOrder.loaibanh}}</b></li>
                        <li ng-show="curOrder.kichthuoc !=''">Kích thước : {{curOrder.kichthuoc}}</li>
                        <li ng-show="curOrder.chieucao !=''"> Cao: {{curOrder.chieucao}} </li>
                        <li ng-show="curOrder.soluong > 0">Số lượng: {{curOrder.soluong}} cái</li>
                        <li ng-show="curOrder.mota != ''">Mô tả: {{curOrder.mota}}</li>
                        <li ng-show="curOrder.mausac != ''">Màu: {{curOrder.mausac}}</li>
                        <li ng-show="curOrder.nhanbanh != ''">Nhân: {{curOrder.nhanbanh}}</li>
                        <li ng-show="curOrder.cotbanh != ''">Cốt: {{curOrder.cotbanh}}</li>
                        <li ng-show="curOrder.sotang > 0">{{curOrder.sotang}} tầng</li>
                        <li ng-show="curOrder.chutrenbanh != ''">Chữ trên bánh: {{curOrder.chutrenbanh}}</li>
                        <li ng-show="curOrder.luuy!= ''">Lưu ý: <b>{{curOrder.luuy}}</b></li><br>
                        <li ng-show="curOrder.dongia!= ''">Doanh thu: <b>{{curOrder.soluong * curOrder.dongia|number}}</b></li>
                        <li ng-show="curOrder.tiencoc!= ''"> - Cọc : <b>{{curOrder.tiencoc |number}}</b></li>
                        <li ng-show="curOrder.dongia!= ''"> Phải thu : <b>{{curOrder.soluong * curOrder.dongia - curOrder.tiencoc |number}}</b></li>
                    </p>
                    <hr class="uk-article-divider">
                </article>
                    
                <div class="uk-width-3-4@xl uk-width-3-4@l uk-width-1-1 uk-align-center">
                    <table class="uk-table uk-table-striped uk-table-hover uk-table-divider uk-table-responsive">
                        <thead>
                            <th>Người giao</th>
                            <th>Ghi chú</th>
                            <th>Thu tiền</th>
                            <th>Phí ship thu bởi shipper</th>
                            <th>Hành động</th>
                        </thead>
                        <tbody>
                            <!-- Danh sách công việc -->
                            <tr ng-repeat="task in listTasks">
                                
                                <td>
                                    <span ng-show="!isEditting || currentTaskID !== task.id">{{getUser(task.manv)}}</span>
                                    <select type="text" class="uk-select input-editting" ng-model="task.manv"  ng-show="isEditting && currentTaskID == task.id">
                                        <option disabled="disabled" value="0">Chọn người thực hiện</option>
                                        <option ng-repeat="nhanvien in listUsers" ng-value="nhanvien.manv" ng-selected="task.manv === nhanvien.manv">
                                            {{nhanvien.tendaydu}}
                                        </option>
                                    </select>
                                </td>

                                <td>
                                    <span ng-show="!isEditting || currentTaskID !== task.id">{{task.note}}</span>
                                    <input type="text" class="uk-input input-editting" ng-model="task.note" ng-show="isEditting && currentTaskID == task.id">
                                </td>
                                <td>
                                    <span ng-show="!isEditting || currentTaskID !== task.id">{{task.tienthanhtoan}}</span>
                                    <input type="text" class="uk-input input-editting" ng-model="task.tienthanhtoan" ng-show="isEditting && currentTaskID == task.id">
                                </td>
                                <td>
                                    <span ng-show="!isEditting || currentTaskID !== task.id">{{task.phi_ship}}</span>
                                    <input type="text" class="uk-input input-editting" ng-model="task.phi_ship" ng-show="isEditting && currentTaskID == task.id">
                                </td>
                                <td>
                                    <button class="uk-button uk-button-small" 
                                        ng-click="editTask(task)" 
                                        ng-show="!isEditting || currentTaskID !== task.id">
                                        <span uk-icon="pencil"></span>
                                    </button>
                                    <button class="uk-button uk-button-small" 
                                        ng-click="saveTask(task)" 
                                        ng-show="isEditting && currentTaskID == task.id">
                                        <span uk-icon="check"></span>
                                    </button>
                                    <button class="uk-button uk-button-small" 
                                        ng-click="deleteTask(task, $event)" 
                                        ng-show="!isEditting || currentTaskID !== task.id">
                                        <span uk-icon="trash"></span>
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select type="text" class="uk-select" ng-model="newTask.manv" >
                                        <option disabled="disabled" value="0" selected="selected">Chọn người thực hiện</option>
                                        <option ng-repeat="nhanvien in listUsers" ng-value="nhanvien.manv">
                                            {{nhanvien.tendaydu}}
                                        </option>
                                    </select>
                                </td>
                                
                                <td>
                                    <input type="text" class="uk-input" ng-model="newTask.note" placeholder="Ghi chú việc giao bánh">
                                </td>
                                <td>
                                    <input type="text" class="uk-input" ng-model="newTask.tienthanhtoan" placeholder="Số tiền của khách">
                                </td>
                                <td>
                                    <input type="text" class="uk-input" ng-model="newTask.phi_ship" placeholder="Phí ship bánh">
                                </td>
                                <td>
                                    <button class="uk-button" ng-click="addNewTask()">Thêm</button>
                                </td>
                            </tr>

                            
                            <!-- Chưa phân công -->
                            <tr class="uk-text-muted uk-text-center" ng-show="!listTasks.length">
                                <td colspan="6">
                                    <h3>
                                        Chưa phân công cho nhân viên
                                    </h3>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <h3 class="uk-text-muted uk-text-center" ng-show="!listDates.length"><i>Chưa có đơn hàng nào</i></h3>
    </div>
</div>
<script>
    window.onload = function(){
        
        $('input.go-to-datepicker').daterangepicker({
            buttonClasses: 'uk-button uk-button-small uk-button-default',
            applyButtonClasses: 'uk-button-primary',
            autoUpdateInput: true,
            timePicker24Hour: false,
            timePicker: false,
            singleDatePicker: true,
            locale: {
                format: 'YYYY-M-DD',
                cancelLabel: 'Hủy',
                applyLabel: 'Chọn'
            }
        });
    }
</script>