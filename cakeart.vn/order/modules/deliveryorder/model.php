<?php 

/**
 * 
 */
class deliveryorder_model extends Model{
	public function __construct(){
		parent::__construct();
		$this->db->connect_to('tinyorder');
	}

    public function getOrderByRang($from, $to){
        $sql = "SELECT HD.*,KH.tenKH, KH.sdt  FROM `nhaphoadon` as HD INNER JOIN khachhang as KH ON KH.makh= HD.makh WHERE DATE_FORMAT(STR_TO_DATE(HD.ngaygiao, '%d/%m/%Y'), '%Y-%m-%d') BETWEEN '" . $from . "' AND '" . $to . "' and lcase(loaibanh) not like 'ship' ORDER BY DATE_FORMAT(STR_TO_DATE(HD.ngaygiao, '%d/%m/%Y'), '%Y-%m-%d'), HD.giogiao ASC;";
        return $this->db->query($sql);
    }


    public function getOrderByID($order_id){
        $sql = "SELECT HD.*,KH.tenKH, KH.sdt  FROM `nhaphoadon` as HD INNER JOIN khachhang as KH ON KH.makh= HD.makh WHERE HD.mahd=$order_id;";
        return $this->db->query($sql);
    }

    public function getWorkOfOrder($order_id){
        return $this->db->get_rows(DELIVERY_TABLE, ['mahd' => (int) $order_id]);
    }

    public function addNewTaskToDB($data){
        return $this->db->add_row($data, DELIVERY_TABLE);
    }
    
    public function deleteTask($where){
        return $this->db->delete(DELIVERY_TABLE, $where);
    }

    public function updateTask($data, $where){
        return $this->db->update(DELIVERY_TABLE, $data, $where);
    }
}
 ?>