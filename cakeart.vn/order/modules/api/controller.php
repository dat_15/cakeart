<?php 

/**
* API go here
*/
class Api extends Controller{
	private $db;
	private $catTable = 'cakecategory';
	private $customer_model;
	private $sales_orders_model;
	// private $catTable = 'test';
	private $cakeTable = 'danhsachbanh';
	private $customerTable = 'khachhang';
	private $user_model;

	function __construct() {
		parent::__construct(__CLASS__);
		$this->db = new DB();
		// if(empty($this->userInfo)) die;
		$this->user_model = $this->load_model('users');
		$this->customer_model = $this->load_model('customer');
		$this->sales_orders_model = $this->load_model('sales_orders');
	}
	public function find_customer(){
		$this->db->connect_to('tinyorder');
		$keyword = strtolower(input('keyword', 'P', '') );
		if(empty($keyword)) {
			send_json(array(
				'status' => 0,
				'message' => 'Missing data',
			));
		}
		// else{
		// 	send_json(array(
		// 		'status' => 1,
		// 		'keyword' => $keyword,
		// 	));
		// }
		//echo $keyword;
		$sql = "SELECT * FROM ".$this->customerTable." WHERE  lower(tenkh) LIKE '%$keyword%' OR lower(sdt) LIKE '%$keyword%' OR lower(diachi) LIKE '%$keyword%' ORDER BY makh DESC LIMIT 100;";
		//send_json( $sql);
		// send_json(array(
		// 		'status' => 1,
		// 		'sql' => $sql,
		// 	));
		$res = $this->db->query($sql);
		if(count($res)){
			foreach ($res as &$customer) {
				$customer['makh'] = en_id($customer['makh']);
			}
		}
		send_json(array(
			'status' => (is_array($res) && count($res)) ? 1 : 0,
			'data' => (is_array($res) && count($res)) ? $res : [],
			'message' => (is_array($res) && count($res)) ? 'success' : 'failed',
		));
		
	}

	/**
	 * Lấy toàn bộ danh mục cha 
	 * @return [json object]
	 */
	public function get_all_parent_categories(){
		$this->db->VN_tranfer();
		$res = $this->db->get_rows($this->catTable, ['parent' => '0']);

		send_json([
			'status' => count($res) ? 1 : 0,
			'data' => $res,
			'message' => count($res) ? 'Success' : 'Failed'
		]);
	}

	/**
	 * Lấy danh mục con của danh mục
	 * @param  $parent :danh mục cha
	 * @return         [json object]
	 */
	public function get_all_child_of($parent){
		$this->db->VN_tranfer();
		$res = $this->db->get_rows($this->catTable, ['parent' => $parent]);

		send_json([
			'status' => count($res) ? 1 : 0,
			'data' => $res,
			'message' => count($res) ? 'Success' : 'Failed'
		]);
		
	}

	/**
	 * Lấy danh sách order theo
	 * @param  $page [Thứ tự trang]
	 * @return       [json object]
	 */
	public function get_page_danhsachbanh($page){
		$res = $this->db->get_table($this->cakeTable, 10, ($page-1)*10);

		send_json([
			'status' => count($res) ? 1 : 0,
			'data' => $res,
			'message' => count($res) ? 'Success' : 'Failed'
		]);
	}

	/**
	 * Đếm toàn bộ bản ghi trong bảng
	 * @param  $table [tên bảng]
	 * @return        [json object]
	 */
	public function count_all($table){
		$res = $this->db->count_all($table);

		send_json([
			'status' => count($res) ? 1 : 0,
			'data' => count($res) ? $res["COUNT(*)"] : 0,
			'message' => count($res) ? 'Success' : 'Failed'
		]);
		
	}


	/**
	 * Cập nhật đơn hàng theo danh mục
	 * @return [description]
	 */
	public function update_cat_of_order(){
		$order = input('order');
		if( $order === NULL){
			send_json([
				'status' => 0,
				'message' => 'Failed! Missing order argument'
			]);
			
		}
		$code = input('code');
		if( $code === NULL ){
			send_json([
				'status' => 0,
				'message' => 'Failed! Missing code argument'
			]);
			
		}
		$name = input('name');
		if( $name === NULL ){
			send_json([
				'status' => 0,
				'message' => 'Failed! Missing name argument'
			]);
			
		}

		$res = $this->db->update(
			$this->cakeTable, 
			array('catCode' => $code, 'catName' => $name), 
			array('stt' => $order));
		if($res){
			send_json([
				'status' => 1,
				'message' => 'Updated'
			]);
		}else{
			send_json([
				'status' => 0,
				'message' => 'Failed! Can not update order'
			]);
		}
	}

	/**
	 * Cập nhật các giá trị khác của đơn hàng
	 * @return [description]
	 */
	public function update_order(){
		$order = input('order');
		if( $order === NULL ){
			send_json([
				'status' => 0,
				'message' => 'Failed! Missing order argument'
			]);
			
		}
		$field = input('field');
		if( $field === NULL ){
			send_json([
				'status' => 0,
				'message' => 'Failed! Missing field argument'
			]);
			
		}
		$value = input('value');
		if( $value === NULL ){
			send_json([
				'status' => 0,
				'message' => 'Failed! Missing value argument'
			]);
			
		}

		$res = $this->db->update(
			$this->cakeTable, 
			array( $field => $value), 
			array('stt' => $order));
		if($res){
			send_json([
				'status' => 1,
				'message' => 'Updated'
			]);
		}else{
			send_json([
				'status' => 0,
				'message' => 'Failed! Can not update order'
			]);
		}
	}

	/**
	 * lấy toàn bộ danh mục dạng khử đệ quy
	 */
	
	public function get_all_categories(){
		$this->db->VN_tranfer();
		$cat = $this->db->get_rows($this->catTable, ['parent' => 0]);
		for ($i=0; $i < count($cat); $i++) { 
			$cat[$i]['children'] = $this->db->get_rows($this->catTable, ['parent' => $cat[$i]['code']]);
			if (count($cat[$i]['children'])) {
				for ($j=0; $j < count($cat[$i]['children']); $j++) { 
					$cat[$i]['children'][$j]['children'] = $this->db->get_rows($this->catTable, ['parent' => $cat[$i]['children'][$j]['code']]);
				}
			}
		}
		send_json([
			'status' => 1,
			'data' => $cat
		]);
	}	

	public function get_all_categories_for_wp(){
		$this->db->VN_tranfer();
		$cat = $this->db->get_table($this->catTable, 10000);
		send_json([
			'status' => 1,
			'data' => $cat
		]);
	}


	/**
	 * Tìm kiếm từ khóa và phân trang
	 * @param key  [Từ khóa]
	 * @param  $page [Đánh số trang]
	 * @return        [json object]
	 */
	public function search($key, $page = 1){
		$key = $this->cleanString($key);
		if($key == '') {
			send_json(array(
				'status' =>  0,
				'message' => 'fail'
			));
		}
		$sql = 'SELECT * FROM '.$this->cakeTable.' WHERE lower(tenbanh) LIKE "%'.$key.'%" OR '.
			' lower(tiengviet) LIKE "%'.$key.'%" OR ' .
			' lower(mabanh) LIKE "%'.$key.'%" OR ' .
			' lower(tienganh) LIKE "%'.$key.'%" OR ' .
			' lower(keyword1) LIKE "%'.$key.'%" OR ' .
			' lower(keyword2) LIKE "%'.$key.'%" OR ' .
			' lower(keyword3) LIKE "%'.$key.'%" OR ' .
			' lower(keyword4) LIKE "%'.$key.'%" ORDER BY clap DESC LIMIT 10 OFFSET '.($page-1)*10 . ';';
		$res = $this->db->query($sql);
		send_json(array(
			'status' => count($res) ? 1 : 0,
			'data' => $res,
			'message' => count($res) ? 'success' : 'fail'
		));
	}

	/**
	 * Đếm số lượng bản ghi hợp lệ trả về khi tìm kiếm
	 * @param $key [từ khóa tìm kiếm]
	 * @return     [json object]
	 */
	public function count_all_search_sesult($key){
		$key = $this->cleanString($key);
		if($key == ''){ 
			send_json(array(
				'status' =>  0,
				'message' => 'fail'
			));
		}
		$sql = 'SELECT count(*) FROM '.$this->cakeTable.' WHERE lower(tenbanh) LIKE "%'.$key.'%" OR '.
			' lower(tiengviet) LIKE "%'.$key.'%" OR ' .
			' lower(mabanh) LIKE "%'.$key.'%" OR ' .
			' lower(tienganh) LIKE "%'.$key.'%" OR ' .
			' lower(keyword1) LIKE "%'.$key.'%" OR ' .
			' lower(keyword2) LIKE "%'.$key.'%" OR ' .
			' lower(keyword3) LIKE "%'.$key.'%" OR ' .
			' lower(keyword4) LIKE "%'.$key.'%"';
		$res = $this->db->query($sql);
		send_json(array(
			'status' => count($res) ? 1 : 0,
			'data' => count($res) ? $res[0]["count(*)"] : 0,
			'message' => count($res) ? 'success' : 'fail'
		));
		
	}


	// // Chuyển đổi chuỗi tiếng Việt có dấu sang không dấu
	private function cleanString($text) {
        // $text = str_replace('/', '-', $text);
        // $text = str_replace('"', '', $text);
        $text = strtolower($text);
        $utf8 = array(
            '/[áàâãăạảắẳẵằặấầẩẫậ]/u' => 'a',
            '/[íìịĩỉ]/u'             => 'i',
            '/[éèêẹẽếềễệẻể]/u'       => 'e',
            '/[óòôõọỏơờởớợỡồổốộ]/u'  => 'o',
            '/[úùũụủưứừửữự]/u'       => 'u',
            '/[đ]/u'                 => 'd',
            '/–/'                    => '-', // UTF-8 hyphen to "normal" hyphen
            '/[’‘‹›‚]/u'             => '', // Literally a single quote
            '/[“”«»„]/u'             => '' // Double quote
            
        );
        if ($remove_space) {
            $utf8['/ /'] = '-';
        }
        return preg_replace(array_keys($utf8), array_values($utf8), $text);
    }
    private function cleanStringUTF8($text) {
        // $text = str_replace('/', '-', $text);
        // $text = str_replace('"', '', $text);
        $text = strtolower($text);
        $utf8 = array(
            '/[áàâãăạảắẳẵằặấầẩẫậ]/u' => 'a',
            '/[íìịĩỉ]/u'             => 'i',
            '/[éèêẹẽếềễệẻể]/u'       => 'e',
            '/[óòôõọỏơờởớợỡồổốộ]/u'  => 'o',
            '/[úùũụủưứừửữự]/u'       => 'u',
            '/[đ]/u'                 => 'd',
            '/–/'                    => '-', // UTF-8 hyphen to "normal" hyphen
            '/[’‘‹›‚]/u'             => '', // Literally a single quote
            '/[“”«»„]/u'             => '' // Double quote
            
        );
        if ($remove_space) {
            $utf8['/ /'] = '-';
        }
        return preg_replace(array_keys($utf8), array_values($utf8), $text);
    }

    /**
     * Lấy danh mục sử dụng đệ quy
     * @return [type] [description]
     */
    public function get_all_danhmucbanh() {
		$this->db->VN_tranfer();
		$cats = $this->get_all_categories_by_recurie([], 0);
		send_json(array(
			'status' => count($cats) ? 1 : 0,
			'data' => count($cats) ? $cats : 0,
			'message' => count($cats) ? 'success' : 'fail'
		));
	}

	public function get_all_categories_by_recurie($cat, $parent){
		$res = $this->db->get_rows($this->catTable, ['parent' => $parent]);
		if(!count($res)) return [];
		$cat = $res;
		for ($i=0; $i < count($cat); $i++) { 
			$cat[$i]['children'] = $this->get_all_categories_by_recurie([], $cat[$i]['code']);
		}
		return $cat;
		
	}

	
	public function add_node(){
		$parent = input('parent');
		if( $parent === NULL ){
			send_json(['status' => 0, 'message' => 'Missing parent']);
			
		}
		$data = $this->db->get_rows($this->catTable, ['parent' => $parent]);
		$number = rand(0, 100);
		$code = '';
		if(count($data)){
			$listCode = [];
			foreach ($data as $cat) {
				$listCode[] = (int)substr($cat['code'], -2);
			}
			while(in_array($number, $listCode)){
				$number = rand(0, 99);
			}
			$code = $this->str_lreplace(
				substr($cat['code'], -2),
				($number < 10) ? '0' . $number: $number,
				$cat['code']
			);
		}else{
			$code = $this->str_lreplace(
				substr($parent, -2),
				($number < 10) ? '0' . $number: $number,
				$parent
			);
		}
		$this->db->VN_tranfer();
		$newCatID = $this->db->add_row(
			array(
				'code' => $code,
				'name' => 'Danh mục mới',
				'parent' => $parent
			),
			$this->catTable
		);
		if($newCatID){
			send_json([
				'status' => 1,
				'data' => [
					'id' => $newCatID,
					'code' => $code,
					'name' => 'Danh mục mới',
					'created_date' => date("Y-m-d h:i:s")
				]
			]);
		}
	}

	protected function str_lreplace($search, $replace, $subject){
	    $pos = strrpos($subject, $search);

	    if($pos !== false) {
	        $subject = substr_replace($subject, $replace, $pos, strlen($search));
	    }

	    return $subject;
	}

	public function update_node(){
		$id = input('id');
		if( $id === NULL ){
			send_json(['status' => 0, 'message' => 'Missing argument (id)']);
			
		}
		$name = input('name');
		if( $name === NULL ){
			send_json(['status' => 0, 'message' => 'Missing argument (name)']);
			
		}
		$this->db->VN_tranfer();
		if($this->db->update($this->catTable, ['name' => $name], [ 'id' => $id ])){
			send_json(['status' => 1, 'message' => 'Success']);
		}else{
			send_json(['status' => 0, 'message' => 'Faild']);
		}
	}


	public function update_category_ID($id, $cid){
		
		$this->db->VN_tranfer();
		if($this->db->update($this->catTable, ['catID' => $cid], [ 'id' => $id ])){
			send_json(['status' => 1, 'message' => 'Success']);
		}else{
			send_json(['status' => 0, 'message' => 'Faild']);
		}
	}

	public function remove_node($nodeID){
		if ($_SERVER['REQUEST_METHOD'] !== 'DELETE') {
			send_json(['status' => 0, 'message' => 'Internal server']);
			
		}
		$res = $this->db->get_row($this->catTable, ['id' => $nodeID]);
		if(count($this->db->get_rows($this->catTable, ['parent' => $res['code']]))){
			send_json(['status' => 0, 'message' => 'Faild']);
			
		}
		if($this->db->delete($this->catTable, [ 'id' => $nodeID ])){
			send_json(['status' => 1, 'message' => 'Success']);
		}else{
			send_json(['status' => 0, 'message' => 'Faild']);
		}
	}

	public function getCatIDByCode($codes){
		$codes = explode(',', $codes);
		$codes = array_filter($codes, function($code){
			return $code !== null || $code !== '';
		});
		if (empty($codes)) {
			send_json(['status' => 0, 'message' => 'Empty']);
		}
		$codes = "'" . implode("','", $codes) . "'";
		$sql = "SELECT catID FROM " . $this->catTable . " WHERE `code` IN ($codes)";
		$catID = $this->db->query($sql);
		foreach ($catID as &$cat) {
			$cat = $cat['catID'];
		}
		if(count($catID)){
			send_json(['status' => 1, 'message' => 'Success', 'catID' => $catID]);
		}else{
			send_json(['status' => 0, 'message' => 'Empty']);
		}
	}

	public function updateCatID($catID, $id){
		$this->db->update('cakecategory', ['catID' => $catID], [ 'id' => $id ]);
	}

	public function getUserModules(){
		if (empty($this->userInfo)) {
			return json_encode([]);
		}
		send_json( $this->user_model->getUserModules((int) $this->userInfo['manv']) );
	}



	public function find_customer_by_key(){
		$this->db->connect_to('tinyorder');
		$field = strtolower(input('key', 'gpc', '') );
		$value = strtolower(input('value', 'gpc', '') );
		if(empty($field) || empty($value)) {
			send_json(array(
				'status' => 0,
				'msg' => 'Thiếu biến'
			));
		}
		$sql = "SELECT * FROM ".$this->customerTable." WHERE  lower($field) = '$value' ORDER BY makh DESC LIMIT 10;";
		$res = $this->db->query($sql);
		if(count($res) && is_array($res) ){
			send_json(array(
				'status' => 1,
				'msg' => 'Thành công',
				'data' => $res
			));
		}else{
			send_json(array(
				'status' => 0,
				'msg' => 'Not found'
			));
		}

	}

	public function add_new_customer(){
		if (empty($this->userInfo)) {
			send_json([
				'status' => 0,
				'msg' => 'Access denied. Please login first'
			]);
		}
		$data['tenkh'] = input('tenkh', 'g', '');
		$data['sdt'] = input('sdt', 'g', '');
		$data['diachi'] = input('diachi', 'g', '');
		$data['lat_lng'] = input('lat_lng', 'g', '');
		$data['email'] = input('email', 'g', '');
		$data['fb'] = input('fb', 'g', '');
		// $data['ngaysinh'] = input('ngaysinh', 'g', '');
		$data['nguoinhan'] = input('nguoinhan', 'g', '');
		$data['sdtnguoinhan'] = input('stdnguoinhan', 'g', '');
		$data['ghichu'] = input('ghichu', 'g', '');
		$data['nhanvien'] = $this->userInfo['tendaydu'];
		$data['nhanvien_id'] = $this->userInfo['manv'];
		if(empty($data['tenkh']) || empty($data['sdt']) || empty($data['diachi']) ){
			send_json([
				'status' => 0,
				'msg' => 'Missing data'
			]);
		}else{
			$customerByPhone = $this->customer_model->getCustomer(['sdt' => $data['sdt'] ]);
			$customerByEmail = $this->customer_model->getCustomer(['email' => $data['email'] ]);
			if( (count($customerByPhone) && is_array($customerByPhone)) || count($customerByEmail) && is_array($customerByEmail)){
				send_json([
					'status' => 0,
					'msg' => 'Customer existed'
				]);
			}else{
				$customerID = $this->customer_model->addCustomer($data);
				if($customerID){
					Session::set_session('flash-message', 'Thêm mới khách hàng thành công.');
					header('Location: '.DOMAIN.'/sales_orders/insertorder/'.en_id( (int) $customerID));
				}
				else{
					send_json([
						'status' => 0,
						'msg' => 'Fail',
						'customerID' => $customerID
					]);
				}
			}
		}
	}

	public function add_new_order(){
		$data = input();
		unset($data['sdt']);
		if(empty($data['order_taken_by'])){
			$data['order_taken_by'] = $this->userInfo['manv'];
		}else{
			$data['order_taken_by'] = de_id($data['order_taken_by']);
		}
		$data['customer_id'] = (int) de_id($data['customer_id']);
		$orderID = $this->sales_orders_model->addOrder($data);
		if($orderID){
			
			Session::set_session('flash-message', 'Thêm mới đơn hàng thành công.');
			header('Location: '.DOMAIN. 'sales_orders/add_product_to_order/'.en_id( (int) $orderID));
		}
		else{
			send_json([
				'status' => 0,
				'msg' => 'Fail',
				'orderID' => $orderID
			]);
		}
	}

	public function findUsers(){
		$key = input('q', 'gpc', '');
		if (empty($key)) {
			send_json([
				'status' => 0,
				'msg' 	=> 'Missing argument',
				'items' => []
			]);
		}
		$items = $this->user_model->findUsers($key);
		foreach ($items as &$item) {
			$item = ['id' => en_id($item['manv']), 'text' => $item['tendaydu']];
		}
		send_json([
			'status' => 1,
			'msg' => 'Success',
			'items' => $items
		]);
	}

	public function find_product(){
		if (empty($this->userInfo)) {
			send_json([
				'status' => 0,
				'msg' => 'Access denied. Please login first'
			]);
		}

		$keyword = input('keyword', 'gpc', '');
		if (empty($keyword)) {
			send_json([
				'status' => 0,
				'msg' => 'Missing argumet'
			]);
		}
		$type = input('type', 'gpc', '');
		if (empty($type) || !in_array($type, ['banh-kem', 'banh-trung-thu'])) {
			send_json([
				'status' => 0,
				'msg' => 'Invalid argument'
			]);
		}
		$products = $this->sales_orders_model->get_products([
			//'lower(loaibanh)' => $type === 'banh-kem' ? 'bánh kem' : 'bánh trung thu',
			'lower(loaibanh) or_like' => strtolower($keyword),
			'lower(kichthuoc) or_like' => strtolower($keyword),
			'lower(chieucao) or_like' => strtolower($keyword),
			'lower(hinhdang) or_like' => strtolower($keyword),
			'lower(sotang) or_like' => strtolower($keyword),
			'lower(mausac) or_like' => strtolower($keyword),
			'lower(cotbanh) or_like' => strtolower($keyword),
			'lower(nhanbanh) or_like' => strtolower($keyword),
			'lower(chutrenbanh) or_like' => strtolower($keyword),
		], 20, 0, ' thoigian DESC ');

		foreach ($products as &$p) {
			$p['mahd'] = en_id($p['mahd']);
		}
		send_json([
			'status' => 1,
			'data' => $products
		]);
	}

	function saveOrderDetail(){
		send_json([
			'status' => 1,
			'msg' => 'Success',
			'data' => input()
		]);
	}


	function addProduct(){
		$product = input();
		$product['makh'] = (int) de_id($product['makh']);
		$product['mahd'] = $this->sales_orders_model->get_last_ID()+1;
		$order_id = (int) de_id($product['order']);
		unset($product['tenKH']);
		unset($product['hinhminhhoa']);
		unset($product['hinhketqua']);
		unset($product['colorcode']);
		unset($product['sdt']);
		unset($product['order']);
		unset($product['$$hashKey']);
		$newID = $this->sales_orders_model->addProduct($product);
		if($newID){
			$this->sales_orders_model->addOrderDetail([
				'order_id' => $order_id,
				'product_id' => $newID,
				'quatity' => $product['soluong']
			]);
			send_json(['status' => $newID]);
		}else{
			send_json(['status' => 0]);
		}
		
	}
}
?>
