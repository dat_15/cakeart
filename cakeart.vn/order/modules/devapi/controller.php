<?php 

/**
* API go here
*/
class Dev extends Controller{
	private $db;
	private $catTable = 'cakecategory';
	// private $catTable = 'test';
	private $cakeTable = 'danhsachbanh';
	function __construct() {
		parent::__construct(__CLASS__);
		$this->db = new DB();
	}

	public function copyCat(){
		$this->db->VN_tranfer();
		$res = $this->db->get_row($this->catTable, ['id' => 224]);
		$data = $this->db->get_rows($this->catTable, ['parent' => $res['code']]);
		foreach ($data as &$child) {
			// Update 
			$code = 'CC_'.$child['code'];
			// $this->db->add_row(
			// 	array(
			// 		'code' => 'CC_'.$child['code'],
			// 		'name' => $child['name'],
			// 		'parent' => 'CC1000'
			// 	),
			// 	$this->catTable
			// );
			$child['children'] = $this->db->get_rows($this->catTable, ['parent' => $child['code']]);
			foreach ($child['children'] as &$grandChild) {
				$code2 = 'CC_'.$grandChild['code'];
				// $this->db->add_row(
				// 	array(
				// 		'code' => $code2,
				// 		'name' => $grandChild['name'],
				// 		'parent' => $code
				// 	),
				// 	$this->catTable
				// );
				$grandChild['children'] = $this->db->get_rows($this->catTable, ['parent' => $grandChild['code']]);
				foreach ($grandChild['children'] as &$supergrandChild) {
					$code3 = 'C