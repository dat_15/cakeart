<?php 
/**
 * 
 */
class inventory_model extends Model{
	protected $table;
	public function __construct(){
		parent::__construct();
		$this->db->connect_to('tinyorder');
	}
	public function get_all_suppliers(){
		return $this->db->get_table('suppliers');
	}

	public function get_all_ingredients(){
		return $this->db->get_table('ingredients');
	}
	public function get_all_goods(){
		return $this->db->get_table('goods');
	}

	public function add_supplier($data){
		return $this->db->add_row($data, 'suppliers');
	}

	public function add_ingredient($data){
		return $this->db->add_row($data, 'ingredients');
	}

	public function add_goods($data){
		return $this->db->add_row($data, 'goods');
	}

	public function update_supplier($data, $where){
		return $this->db->update('suppliers', $data, $where);
	}

	public function update_ingredient($data, $where){
		return $this->db->update('ingredients', $data, $where);
	}

	public function update_goods($data, $where){
		return $this->db->update('goods', $data, $where);
	}

	public function delete_supplier($where){
		return $this->db->delete('suppliers', $where);
	}
	
	public function delete_ingredient($where){
		return $this->db->delete('ingredients', $where);
	}
	public function delete_goods($where){
		return $this->db->delete('goods', $where);
	}
}
?>