<?php
/**
* index controller
*/
class inventory extends Controller {
	private $session;
	private $model;
	function __construct(){
		parent::__construct(__CLASS__);
		$this->session = new Session();
		$this->model = $this->load_model(__CLASS__);
		$this->user = $this->load_model('users');
		if(empty($this->userInfo)) header('Location: '.LOGIN_PAGE);
		if( !$this->user->isAccessibleModule('inventory', (int) $this->userInfo['manv']) ) header('Location: '.DOMAIN);
		$this->modules = $this->user->getUserModules((int) $this->userInfo['manv']);
	}

	function index(){
		$this->load_view('main-header', [
			'base_url' 	=> 'modules/header/', 
			'title' 	=> 'Quản lý nguyên liệu',
			'user' 		=> $this->userInfo
		], 'header');
		$this->load_view('menu-top',['modules' => $this->modules, 'mod' => 'inventory'], 'header');
		$this->load_view('index', [
		]);
		$this->load_view('footer',[], 'footer');
	}

	public function suppliers(){
		$this->load_view('main-header', [
			'base_url' 	=> 'modules/header/', 
			'title' 	=> 'Quản lý nhà cung cấp',
			'user' 		=> $this->userInfo
		], 'header');
		$this->load_view('menu-top',['modules' => $this->modules, 'mod' => 'inventory'], 'header');
		$this->load_view('suppliers', [
			'suppliers' => $this->model->get_all_suppliers()
		]);
		$this->load_view('footer',[], 'footer');
	}

	public function ingredients(){
		$this->load_view('main-header', [
			'base_url' 	=> 'modules/header/', 
			'title' 	=> 'Quản lý nguyên vật liệu',
			'user' 		=> $this->userInfo
		], 'header');
		$this->load_view('menu-top',['modules' => $this->modules, 'mod' => 'inventory'], 'header');
		$this->load_view('ingredients', [
			'suppliers' => $this->model->get_all_suppliers(),
			'ingredients' => $this->model->get_all_ingredients()
		]);
		$this->load_view('footer',[], 'footer');
	}

	public function imported_goods(){
		$this->load_view('main-header', [
			'base_url' 	=> 'modules/header/', 
			'title' 	=> 'Nhập nguyên liệu',
			'user' 		=> $this->userInfo
		], 'header');
		$this->load_view('menu-top',['modules' => $this->modules, 'mod' => 'inventory'], 'header');
		$this->load_view('goods', [
			'listGoods' => $this->model->get_all_goods(),
			'ingredients' => $this->model->get_all_ingredients()
		]);
		$this->load_view('footer',[], 'footer');
	}

	public function addnewsupplier(){
		$data = input();
		if(empty($data['name'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu tên nhà cung cấp'
			]);
		}
		if (!$this->check_valid_fullname($data['name'])) {
			send_json([
				'status' => 0,
				'msg' => 'Tên nhà cung cấp không hợp lệ'
			]);
		}
		if(empty($data['phone_number'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu số liên hệ'
			]);
		}
		if (!$this->checkValidPhoneNumber($data['phone_number'])) {
			send_json([
				'status' => 0,
				'msg' => 'Số điện thoại không hợp lệ'
			]);
		}
		
		if(empty($data['address'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu số liên hệ'
			]);
		}
		if (!$this->check_valid_address($data['address'])) {
			send_json([
				'status' => 0,
				'msg' => 'Địa chỉ không hợp lệ'
			]);
		}
		if(empty($data['goods'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu mặt hàng cung cấp'
			]);
		}
		if (!$this->check_valid_address($data['goods'])) {
			send_json([
				'status' => 0,
				'msg' => 'Mặt hàng cung cấp không hợp lệ'
			]);
		}
		$supplier = [
			'name' => $data['name'],
			'phone_number' => $data['phone_number'],
			'address' => $data['address'],
			'goods' => $data['goods']
		];
		$newID = $this->model->add_supplier($supplier);
		if($newID){
			send_json([
				'status' => 1,
				'supplierID' => en_id($newID)
			]);
		}else{
			send_json(['status' => 0]);
		}
	}
	public function savesupplier(){
		$data = input();
		if(empty($data['name'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu tên nhà cung cấp'
			]);
		}
		if (!$this->check_valid_fullname($data['name'])) {
			send_json([
				'status' => 0,
				'msg' => 'Tên nhà cung cấp không hợp lệ'
			]);
		}
		if(empty($data['phone_number'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu số liên hệ'
			]);
		}
		if (!$this->checkValidPhoneNumber($data['phone_number'])) {
			send_json([
				'status' => 0,
				'msg' => 'Tên nhà cung cấp không hợp lệ'
			]);
		}
		if(empty($data['address'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu số liên hệ'
			]);
		}
		if (!$this->check_valid_address($data['address'])) {
			send_json([
				'status' => 0,
				'msg' => 'Tên nhà cung cấp không hợp lệ'
			]);
		}
		if(empty($data['address'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu số liên hệ'
			]);
		}
		if (!$this->check_valid_address($data['address'])) {
			send_json([
				'status' => 0,
				'msg' => 'Địa chỉ không hợp lệ'
			]);
		}
		if(empty($data['goods'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu mặt hàng cung cấp'
			]);
		}
		if (!$this->check_valid_address($data['goods'])) {
			send_json([
				'status' => 0,
				'msg' => 'Mặt hàng cung cấp không hợp lệ'
			]);
		}
		if(empty($data['id'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu id'
			]);
		}
		if ((int) de_id($data['id']) < 1) {
			send_json([
				'status' => 0,
				'msg' => 'ID không hợp lệ'
			]);
		}
		$supplier = [
			'name' => $data['name'],
			'phone_number' => $data['phone_number'],
			'address' => $data['address'],
			'goods' => $data['goods']
		];
		
		if($this->model->update_supplier($supplier, ['id' => (int) de_id($data['id'])])){
			send_json(['status' => 1]);
		}else{
			send_json(['status' => 0]);
		}
	}

	public function deletesupplier($id){
		
		if(empty($id)){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu id'
			]);
		}
		if ((int) de_id($id) < 1) {
			send_json([
				'status' => 0,
				'msg' => 'ID không hợp lệ'
			]);
		}
		
		if($this->model->delete_supplier(['id' => (int) de_id($id)])){
			send_json(['status' => 1]);
		}else{
			send_json(['status' => 0]);
		}
	}

	public function deleteingredient($id){
		
		if(empty($id)){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu id'
			]);
		}
		if ((int) de_id($id) < 1) {
			send_json([
				'status' => 0,
				'msg' => 'ID không hợp lệ'
			]);
		}
		
		if($this->model->delete_ingredient(['id' => (int) de_id($id)])){
			send_json(['status' => 1]);
		}else{
			send_json(['status' => 0]);
		}
	}

	public function addnewingredient(){
		$data = input();
		if(empty($data['name'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu tên nguyên liệu'
			]);
		}
		if (!$this->check_valid_sirname($data['name'])) {
			send_json([
				'status' => 0,
				'msg' => 'Tên nguyên liệu không hợp lệ'
			]);
		}
		if(empty($data['description'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu mô tả'
			]);
		}
		if (!$this->check_valid_fullname($data['description'])) {
			send_json([
				'status' => 0,
				'msg' => 'Mô tả không hợp lệ'
			]);
		}
		
		if(empty($data['price'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu số liên hệ'
			]);
		}
		if (!$this->check_valid_price($data['price']/1)) {
			send_json([
				'status' => 0,
				'msg' => 'Giá nguyên liệu không hợp lệ',
				'data' => $data['price']/1
			]);
		}
		if(empty($data['unit'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu đơn vị tính'
			]);
		}
		if(empty($data['inventory'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu TỒN KHO ĐẦU KỲ'
			]);
		}
		if(empty($data['supplier_id'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu nhà cung cấp'
			]);
		}
		$data['supplier_id'] = de_id($data['supplier_id']);
		$ingredient = [
			'supplier_id' 	=> $data['supplier_id'],
			'name' 			=> $data['name'],
			'description' 	=> $data['description'],
			'price' 		=> $data['price']/1,
			'unit' 			=> $data['unit'],
			'inventory' 	=> $data['inventory']
		];
		$newID = $this->model->add_ingredient($ingredient);
		if( is_int($newID) ){
			send_json([
				'status' => 1,
				'ingredientID' => en_id($newID)
			]);
		}else{
			send_json(['status' => 0, 'msg' => $newID]);
		}
	}

	public function saveingredient(){
		$data = input();
		if(empty($data['name'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu tên nguyên liệu'
			]);
		}
		if (!$this->check_valid_sirname($data['name'])) {
			send_json([
				'status' => 0,
				'msg' => 'Tên nguyên liệu không hợp lệ'
			]);
		}
		if(empty($data['description'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu mô tả'
			]);
		}
		if (!$this->check_valid_fullname($data['description'])) {
			send_json([
				'status' => 0,
				'msg' => 'Mô tả không hợp lệ'
			]);
		}
		
		if(empty($data['price'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu số liên hệ'
			]);
		}
		if (!$this->check_valid_price($data['price']/1)) {
			send_json([
				'status' => 0,
				'msg' => 'Giá nguyên liệu không hợp lệ',
				'data' => $data['price']/1
			]);
		}
		if(empty($data['unit'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu đơn vị tính'
			]);
		}
		if(empty($data['inventory'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu TỒN KHO ĐẦU KỲ'
			]);
		}
		if(empty($data['supplier_id'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu nhà cung cấp'
			]);
		}
		$data['supplier_id'] = de_id($data['supplier_id']);
		$ingredient = [
			'supplier_id' 	=> $data['supplier_id'],
			'name' 			=> $data['name'],
			'description' 	=> $data['description'],
			'price' 		=> $data['price']/1,
			'unit' 			=> $data['unit'],
			'inventory' 	=> $data['inventory']
		];
		if($this->model->update_ingredient($ingredient, ['id' => (int) de_id($data['id'])])){
			send_json(['status' => 1]);
		}else{
			send_json(['status' => 0]);
		}
	}
	public function deletegoods($id){
		
		if(empty($id)){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu id'
			]);
		}
		if ((int) de_id($id) < 1) {
			send_json([
				'status' => 0,
				'msg' => 'ID không hợp lệ'
			]);
		}
		
		if($this->model->delete_goods(['id' => (int) de_id($id)])){
			send_json(['status' => 1]);
		}else{
			send_json(['status' => 0]);
		}
	}

	public function addnewgoods(){
		$data = input();
		if(empty($data['name'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu tên mặt hàng'
			]);
		}
		if (!$this->check_valid_sirname($data['name'])) {
			send_json([
				'status' => 0,
				'msg' => 'Tên mặt hàng không hợp lệ'
			]);
		}
		if(empty($data['ingredient_id'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu nguyên liệu'
			]);
		}
		if (!is_int(de_id($data['ingredient_id']))) {
			send_json([
				'status' => 0,
				'msg' => 'Tên mặt hàng không hợp lệ'
			]);
		}
		if(empty($data['quantity'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu số lượng'
			]);
		}
		if (!$this->check_valid_quantity($data['quantity'])) {
			send_json([
				'status' => 0,
				'msg' => 'Số lượng không hợp lệ'
			]);
		}
		if(empty($data['delivery_quantity'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu số lượng nhận'
			]);
		}
		if (!$this->check_valid_quantity($data['delivery_quantity'])) {
			send_json([
				'status' => 0,
				'msg' => 'Số lượng nhận không hợp lệ'
			]);
		}
		
		if(empty($data['price'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu số giá mặt hàng'
			]);
		}
		if (!$this->check_valid_price($data['price']/1)) {
			send_json([
				'status' => 0,
				'msg' => 'Giá mặt hàng không hợp lệ',
				'data' => $data['price']/1
			]);
		}
		$data['ingredient_id'] = de_id($data['ingredient_id']);
		$goods = [
			'ingredient_id' => $data['ingredient_id'],
			'name' 			=> $data['name'],
			'quantity' 		=> $data['quantity'],
			'price' 		=> $data['price']/1,
			'delivery_quantity'=> $data['delivery_quantity']
		];
		$newID = $this->model->add_goods($goods);
		if( is_int($newID) ){
			send_json([
				'status' => 1,
				'goodsID' => en_id($newID)
			]);
		}else{
			send_json(['status' => 0, 'msg' => $newID]);
		}
	}

	public function savegoods(){
		$data = input();
		if(empty($data['name'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu tên mặt hàng'
			]);
		}
		if (!$this->check_valid_sirname($data['name'])) {
			send_json([
				'status' => 0,
				'msg' => 'Tên mặt hàng không hợp lệ'
			]);
		}
		if(empty($data['ingredient_id'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu nguyên liệu'
			]);
		}
		if (!is_int(de_id($data['ingredient_id']))) {
			send_json([
				'status' => 0,
				'msg' => 'Tên mặt hàng không hợp lệ'
			]);
		}
		if(empty($data['quantity'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu số lượng'
			]);
		}
		if (!$this->check_valid_quantity($data['quantity'])) {
			send_json([
				'status' => 0,
				'msg' => 'Số lượng không hợp lệ'
			]);
		}
		if(empty($data['delivery_quantity'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu số lượng nhận'
			]);
		}
		if (!$this->check_valid_quantity($data['delivery_quantity'])) {
			send_json([
				'status' => 0,
				'msg' => 'Số lượng nhận không hợp lệ'
			]);
		}
		
		if(empty($data['price'])){
			send_json([
				'status' => 0,
				'msg' => 'Thiếu số giá mặt hàng'
			]);
		}
		if (!$this->check_valid_price($data['price']/1)) {
			send_json([
				'status' => 0,
				'msg' => 'Giá mặt hàng không hợp lệ',
			]);
		}
		$data['ingredient_id'] = de_id($data['ingredient_id']);
		$goods = [
			'ingredient_id' => $data['ingredient_id'],
			'name' 			=> $data['name'],
			'quantity' 		=> $data['quantity'],
			'price' 		=> $data['price']/1,
			'delivery_quantity'=> $data['delivery_quantity']
		];
		if($this->model->update_goods($goods, ['id' => (int) de_id($data['_id'])])){
			send_json(['status' => 1]);
		}else{
			send_json(['status' => 0]);
		}
	}
}