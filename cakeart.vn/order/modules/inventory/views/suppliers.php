<style>
    .loading{
        width: 100%;
        height: 100vh;
        margin: 0 auto;
        text-align: center;
        float: none;
        display: block;
        z-index: +1000;
        position: fixed;
        background: #fff;
        padding-top: 25%;
        top: 0;
        left: 0;
    }
    .uk-padding-small{padding: 5px!important;}
    .input-editting {
        border: 0;
        height: 30px!important;
        background: transparent;
        border-bottom: 1px dashed;
    }
    h3 button.uk-button.uk-button-small{
        width: 50px;
        overflow: hidden;
        display: block;
        max-width: 50px;
        margin-right: 10px;
        float: left;
    }
</style>
<script type="text/javascript">
    let _listSuppliers = [];
    <?php foreach ($suppliers as $supplier):
        $supplier['id'] = en_id($supplier['id']); ?>
        _listSuppliers.push(<?php echo json_encode($supplier);?>);
    <?php endforeach ?>
    var supplierApp = angular.module('supplier-app', []);
    let domain = "<?php echo DOMAIN; ?>";

</script>
<script type="text/javascript" src="assets/js/suppliers.js?v=<?= time()?>"></script>
<div class="uk-section" ng-app="supplier-app" ng-controller="suppliers-controller">
    <div class="loading" uk-spinner="ratio: 3" ng-show="showLoading"></div>
    <div class="uk-container">
        <div uk-grid>
            <h3>
                <button class="uk-button uk-button-small">
                    <a href="<?= base_url ?>inventory">
                        <span uk-icon="arrow-left"></span>
                    </a>
                </button>
                Danh sách nhà cung cấp
            </h3>
		    <table class="uk-table uk-table-striped uk-table-hover uk-table-divider uk-table-responsive">
                <thead>
                    <tr>
                        <th>Tên</th>
                        <th>Số điện thoại</th>
                        <th>Địa chỉ</th>
                        <th>Cung cấp mặt hàng</th>
                        <th>Hành động</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <input type="text" class="uk-input" ng-model="newSupplier.name" placeholder="Tên nhà cung cấp">
                        </td>
                        
                        <td>
                            <input type="text" class="uk-input" ng-model="newSupplier.phone_number" placeholder="Số liên hệ">
                        </td>
                        <td>
                            <input type="text" class="uk-input" ng-model="newSupplier.address" placeholder="Địa chỉ">
                        </td>
                        <td>
                            <input type="text" class="uk-input" ng-model="newSupplier.goods" placeholder="Mặt hàng cung cấp">
                        </td>
                        <td>
                            <button class="uk-button" ng-click="addNewSupplier()">Thêm</button>
                        </td>
                    </tr>
                    <tr ng-repeat="supplier in listSuppliers">
                        <td>
                            <span ng-show="!isEditting || currentSupplierID !== supplier.id">{{supplier.name}}</span>
                            <input type="text" class="uk-input input-editting" ng-model="supplier.name"  ng-show="isEditting && currentSupplierID == supplier.id">
                        </td>

                        <td>
                            <span ng-show="!isEditting || currentSupplierID !== supplier.id">{{supplier.phone_number}}</span>
                            <input type="text" class="uk-input input-editting" ng-model="supplier.phone_number" ng-show="isEditting && currentSupplierID == supplier.id">
                        </td>
                        <td>
                            <span ng-show="!isEditting || currentSupplierID !== supplier.id">{{supplier.address}}</span>
                            <input type="text" class="uk-input input-editting" ng-model="supplier.address" ng-show="isEditting && currentSupplierID == supplier.id">
                        </td>
                        <td>
                            <span ng-show="!isEditting || currentSupplierID !== supplier.id">{{supplier.goods}}</span>
                            <input type="text" class="uk-input input-editting" ng-model="supplier.goods" ng-show="isEditting && currentSupplierID == supplier.id">
                        </td>
                        <td>
                            <button class="uk-button uk-button-small" 
                                ng-click="editSupplier(supplier)" 
                                ng-show="!isEditting || currentSupplierID !== supplier.id">
                                <span uk-icon="pencil"></span>
                            </button>
                            <button class="uk-button uk-button-small" 
                                ng-click="saveSupplier(supplier)" 
                                ng-show="isEditting && currentSupplierID == supplier.id">
                                <span uk-icon="check"></span>
                            </button>
                            <button class="uk-button uk-button-small" 
                                ng-click="deleteSupplier(supplier, $event)" 
                                ng-show="!isEditting || currentSupplierID !== supplier.id">
                                <span uk-icon="trash"></span>
                            </button>
                        </td>
                    </tr>
                    <tr class="uk-text-muted uk-text-center" ng-show="!listSuppliers.length">
                        <td colspan="6">
                            <h3>
                                Chưa có nhà cung cấp nào
                            </h3>
                        </td>
                    </tr>
                </tbody>
            </table>
		</div>
	</div>
</div>