<style>
    .loading{
        width: 100%;
        height: 100vh;
        margin: 0 auto;
        text-align: center;
        float: none;
        display: block;
        z-index: +1000;
        position: fixed;
        background: #fff;
        padding-top: 25%;
        top: 0;
        left: 0;
    }
    .uk-padding-small{padding: 5px!important;}
    .input-editting {
        border: 0;
        height: 30px!important;
        background: transparent;
        border-bottom: 1px dashed;
    }
    h3 button.uk-button.uk-button-small{
        width: 50px;
        overflow: hidden;
        display: block;
        max-width: 50px;
        margin-right: 10px;
        float: left;
    }
</style>
<script type="text/javascript">
    let _listIngredients = [];
    <?php foreach ($ingredients as $ingredient):
        $ingredient['id'] = en_id($ingredient['id']);
        $ingredient['supplier_id'] = en_id($ingredient['supplier_id']); ?>
        _listIngredients.push(<?php echo json_encode($ingredient);?>);
    <?php endforeach ?>
    let _listSuppliers = [];
    <?php foreach ($suppliers as $supplier):
        $supplier['id'] = en_id($supplier['id']); ?>
        _listSuppliers.push(<?php echo json_encode($supplier);?>);
    <?php endforeach ?>
    var ingredientApp = angular.module('ingredient-app', []);
    let domain = "<?php echo DOMAIN; ?>";

</script>
<script type="text/javascript" src="assets/js/ingredients.js?v=<?= time()?>"></script>
<div class="uk-section" ng-app="ingredient-app" ng-controller="ingredients-controller">
    <div class="loading" uk-spinner="ratio: 3" ng-show="showLoading"></div>
    <div class="uk-container">
        <div uk-grid>
            <h3>
                <button class="uk-button uk-button-small">
                    <a href="<?= base_url ?>inventory">
                        <span uk-icon="arrow-left"></span>
                    </a>
                </button>
                Danh sách nguyên liệu
            </h3>
		    <table class="uk-table uk-table-striped uk-table-hover uk-table-divider uk-table-responsive">
                <thead>
                    <tr>
                        <th>Nhà cung cấp</th>
                        <th>Tên nguyên liệu</th>
                        <th>Mô tả</th>
                        <th>Giá</th>
                        <th>Đơn vị tính</th>
                        <th>Tồn kho đầu kỳ</th>
                        <th>Hành động</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <select ng-model="newIngredient.supplier_id" class="uk-select">
                                <option value="{{s.id}}" ng-repeat="s in listSuppliers">{{s.name}}</option>
                            </select>
                        </td>
                        <td>
                            <input type="text" class="uk-input" ng-model="newIngredient.name" placeholder="Tên nguyên liệu">
                        </td>
                        
                        <td>
                            <input type="text" class="uk-input" ng-model="newIngredient.description" placeholder="Mô tả">
                        </td>
                        <td>
                            <input type="text" class="uk-input" ng-model="newIngredient.price" placeholder="Giá">
                        </td>
                        <td>
                            <input type="text" class="uk-input" ng-model="newIngredient.unit" placeholder="Đơn vị tính">
                        </td>
                        <td>
                            <input type="text" class="uk-input" ng-model="newIngredient.inventory" placeholder="Tồn kho đầu kỳ">
                        </td>
                        <td>
                            <button class="uk-button" ng-click="addNewIngredient()">Thêm</button>
                        </td>
                    </tr>
                    <tr ng-repeat="ingredient in listIngredients">
                        <td>
                            <span ng-show="!isEditting || currentIngredientID !== ingredient.id">
                                {{getSupplierName(ingredient.supplier_id)}}
                            </span>
                            <select 
                                ng-model="ingredient.supplier_id" class="uk-select" 
                                ng-show="isEditting && currentIngredientID == ingredient.id">
                                <option value="{{s.id}}" {{ingredient.supplier_id==s.id ? 'selected' : ''}} ng-repeat="s in listSuppliers">{{s.name}}</option>
                            </select>
                        </td>
                        <td>
                            <span ng-show="!isEditting || currentIngredientID !== ingredient.id">{{ingredient.name}}</span>
                            <input type="text" class="uk-input input-editting" ng-model="ingredient.name"  ng-show="isEditting && currentIngredientID == ingredient.id">
                        </td>

                        <td>
                            <span ng-show="!isEditting || currentIngredientID !== ingredient.id">{{ingredient.description}}</span>
                            <input type="text" class="uk-input input-editting" ng-model="ingredient.description" ng-show="isEditting && currentIngredientID == ingredient.id">
                        </td>
                        <td>
                            <span ng-show="!isEditting || currentIngredientID !== ingredient.id">{{ingredient.price}}</span>
                            <input type="text" class="uk-input input-editting" ng-model="ingredient.price" ng-show="isEditting && currentIngredientID == ingredient.id">
                        </td>
                        <td>
                            <span ng-show="!isEditting || currentIngredientID !== ingredient.id">{{ingredient.unit}}</span>
                            <input type="text" class="uk-input input-editting" ng-model="ingredient.unit" ng-show="isEditting && currentIngredientID == ingredient.id">
                        </td>
                        <td>
                            <span ng-show="!isEditting || currentIngredientID !== ingredient.id">{{ingredient.inventory}}</span>
                            <input type="text" class="uk-input input-editting" ng-model="ingredient.inventory" ng-show="isEditting && currentIngredientID == ingredient.id">
                        </td>
                        <td>
                            <button class="uk-button uk-button-small" 
                                ng-click="editIngredient(ingredient)" 
                                ng-show="!isEditting || currentIngredientID !== ingredient.id">
                                <span uk-icon="pencil"></span>
                            </button>
                            <button class="uk-button uk-button-small" 
                                ng-click="saveIngredient(ingredient)" 
                                ng-show="isEditting && currentIngredientID == ingredient.id">
                                <span uk-icon="check"></span>
                            </button>
                            <button class="uk-button uk-button-small" 
                                ng-click="deleteIngredient(ingredient, $event)" 
                                ng-show="!isEditting || currentIngredientID !== ingredient.id">
                                <span uk-icon="trash"></span>
                            </button>
                        </td>
                    </tr>
                    <tr class="uk-text-muted uk-text-center" ng-show="!listIngredients.length">
                        <td colspan="6">
                            <h3>
                                Chưa có nguyên liệu nào
                            </h3>
                        </td>
                    </tr>
                </tbody>
            </table>
		</div>
	</div>
</div>