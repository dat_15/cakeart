<style>
    .loading{
        width: 100%;
        height: 100vh;
        margin: 0 auto;
        text-align: center;
        float: none;
        display: block;
        z-index: +1000;
        position: fixed;
        background: #fff;
        padding-top: 25%;
        top: 0;
        left: 0;
    }
    .uk-padding-small{padding: 5px!important;}
    .input-editting {
        border: 0;
        height: 30px!important;
        background: transparent;
        border-bottom: 1px dashed;
    }
    h3 button.uk-button.uk-button-small{
        width: 50px;
        overflow: hidden;
        display: block;
        max-width: 50px;
        margin-right: 10px;
        float: left;
    }
</style>
<script type="text/javascript">
    let _listIngredients = [];
    <?php foreach ($ingredients as $ingredient):
        $ingredient['_id'] = en_id($ingredient['id']);?>
        _listIngredients.push(<?php echo json_encode($ingredient);?>);
    <?php endforeach ?>
    let _listGoods = [];
    <?php foreach ($listGoods as $goods):
        $goods['_id'] = en_id($goods['id']);
        $goods['ingredient_id'] = en_id($goods['ingredient_id']); ?>
        _listGoods.push(<?php echo json_encode($goods);?>);
    <?php endforeach ?>
    var goodsApp = angular.module('goods-app', []);
    let domain = "<?php echo DOMAIN; ?>";

</script>
<script type="text/javascript" src="assets/js/goods.js?v=<?= time()?>"></script>
<div class="uk-section" ng-app="goods-app" ng-controller="goods-controller">
    <div class="loading" uk-spinner="ratio: 3" ng-show="showLoading"></div>
    <div class="uk-container">
        <div uk-grid>
            <h3>
                <button class="uk-button uk-button-small">
                    <a href="<?= base_url ?>inventory">
                        <span uk-icon="arrow-left"></span>
                    </a>
                </button>
                Danh sách nguyên liệu
            </h3>
		    <table class="uk-table uk-table-striped uk-table-hover uk-table-divider uk-table-responsive">
                <thead>
                    <tr>
                        <th>Nguyên liệu</th>
                        <th>Tên mặt hàng</th>
                        <th>Số lượng</th>
                        <th>Giá</th>
                        <th>Số lượng nhận</th>
                        <th>Hành động</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <select ng-model="newGoods.ingredient_id" class="uk-select">
                                <option value="{{s._id}}" ng-repeat="s in listIngredients">{{s.name}}</option>
                            </select>
                        </td>
                        <td>
                            <input type="text" class="uk-input" ng-model="newGoods.name" placeholder="Tên mặt hàng">
                        </td>
                        
                        <td>
                            <input type="text" class="uk-input" ng-model="newGoods.quantity" placeholder="Số lượng">
                        </td>
                        <td>
                            <input type="text" class="uk-input" 
                                ng-model="newGoods.price" 
                                ng-value="{{newGoods.quantity*getIngredientPrice(newGoods.ingredient_id)}}" 
                                placeholder="Giá">
                        </td>
                        <td>
                            <input type="text" class="uk-input" ng-model="newGoods.delivery_quantity" placeholder="Số lượng nhận">
                        </td>
                        <td>
                            <button class="uk-button" ng-click="addNewGoods()">Thêm</button>
                        </td>
                    </tr>
                    <tr ng-repeat="goods in listGoods">
                        <td>
                            <span ng-show="!isEditting || currentGoodsID !== goods.id">
                                {{getIngredientName(goods.ingredient_id)}}
                            </span>
                            <select 
                                ng-model="goods.ingredient_id" class="uk-select" 
                                ng-show="isEditting && currentGoodsID == goods.id">
                                <option 
                                    value="{{s._id}}" {{goods.ingredient_id==s._id ? 'selected' : ''}} 
                                    ng-repeat="s in listIngredients">
                                    {{s.name}}
                                </option>
                            </select>
                        </td>
                        <td>
                            <span ng-show="!isEditting || currentGoodsID !== goods.id">{{goods.name}}</span>
                            <input type="text" class="uk-input input-editting" ng-model="goods.name"  ng-show="isEditting && currentGoodsID == goods.id">
                        </td>

                        <td>
                            <span ng-show="!isEditting || currentGoodsID !== goods.id">{{goods.quantity}}</span>
                            <input type="text" class="uk-input input-editting" ng-model="goods.quantity" ng-show="isEditting && currentGoodsID == goods.id">
                        </td>
                        <td>
                            <span ng-show="!isEditting || currentGoodsID !== goods.id">{{goods.price}}</span>
                            <input type="text" class="uk-input input-editting" ng-model="goods.price" ng-show="isEditting && currentGoodsID == goods.id">
                        </td>
                        <td>
                            <span ng-show="!isEditting || currentGoodsID !== goods.id">{{goods.delivery_quantity}}</span>
                            <input type="text" class="uk-input input-editting" ng-model="goods.delivery_quantity" ng-show="isEditting && currentGoodsID == goods.id">
                        </td>
                        <td>
                            <button class="uk-button uk-button-small" 
                                ng-click="editGoods(goods)" 
                                ng-show="!isEditting || currentGoodsID !== goods.id">
                                <span uk-icon="pencil"></span>
                            </button>
                            <button class="uk-button uk-button-small" 
                                ng-click="saveGoods(goods)" 
                                ng-show="isEditting && currentGoodsID == goods.id">
                                <span uk-icon="check"></span>
                            </button>
                            <button class="uk-button uk-button-small" 
                                ng-click="deleteGoods(goods, $event)" 
                                ng-show="!isEditting || currentGoodsID !== goods.id">
                                <span uk-icon="trash"></span>
                            </button>
                        </td>
                    </tr>
                    <tr class="uk-text-muted uk-text-center" ng-show="!listGoods.length">
                        <td colspan="6">
                            <h3>
                                Chưa có mặt hàng nào
                            </h3>
                        </td>
                    </tr>
                </tbody>
            </table>
		</div>
	</div>
</div>