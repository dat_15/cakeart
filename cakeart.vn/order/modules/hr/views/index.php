<link rel="stylesheet" href="assets/css/users-management.css?v=<?php echo time();?>" />
<div class="uk-section" id="user-profile">
	<div class="uk-container">
		<h3><caption>Danh sách nhân viên</caption></h3>
		<?php echo $pagination->getTemplate(); ?>
	</div>
	<div class="uk-container" id="main-content">
		<table class="uk-table uk-table-divider uk-table-striped uk-table-hover uk-table-justify uk-overflow-auto">
			<thead>
				<tr>
					<th>STT</th>
					<th>Hình</th>
					<th style="min-width: 5%">Họ tên</th>
					<th style="min-width: 10%">Username</th>
					<th style="min-width: 10%">Email</th>
					<th style="min-width: 10%">Số điện thoại</th>
					<th style="min-width: 20%">Địa chỉ</th>
					<th style="min-width: 10%">Quyền nhân viên</th>
					<th style="min-width: 10%">Mã nhân viên</th>
					<th style="min-width: 5%">Hành động</th>
				</tr>
			</thead>
			<tbody>
				<?php $index = 1; foreach ($users as $user): ?>
					<tr>
						<td><?php echo $index++; ?></td>
						<td>
							<?php if ($user['hinh'] !== ''): ?>
								<div uk-lightbox>
									<a 
										href="<?php echo EMPLOYER_IMAGE_PATH.$user['hinh'] ?>" 
										data-caption="<?php echo $user['tendangnhap'] ?>">
										<img 
											src="<?php echo EMPLOYER_IMAGE_PATH.$user['hinh'] ?>" 
											alt="<?php echo $user['tendangnhap'] ?>" 
											width="50" height="50" 
											class="uk-border-circle">
									</a>
								</div>
							<?php endif ?>
						</td>
						<td><?php echo $user['tendaydu'] ?></td>
						<td><?php echo $user['tendangnhap'] ?></td>
						<td><?php echo $user['email'] ?></td>
						<td><?php echo $user['sodt'] ?></td>
						<td><?php echo $user['diachi'] ?></td>
						<td><?php echo $user['quyennhanvien'] ?></td>
						<td><?php echo $user['manv'] ?></td>
						<td>
							<button class="uk-button uk-button-default" type="button">
								<span uk-icon="settings"></span>
							</button>
							<div uk-dropdown="mode: click">
							    <ul class="uk-nav uk-dropdown-nav">
							        <li class="uk-active">
							        	<a href="<?php echo $pagination->getLink().'/edit/'.en_id($user['manv']); ?>">
								        	<span uk-icon="cog"></span> Sửa
								        </a>
							        </li>
							        <li>
							        	<a href="<?php echo $pagination->getLink().'/editPermission/'.en_id($user['manv']); ?>">
							        		<span uk-icon="users"></span> Phân quyền
							        	</a>
							        </li>
							        <li>
								        <a href="#">
								        	<span uk-icon="ban"></span> Deactive
									    </a>
							        </li>
							        <li class="uk-nav-divider"></li>
							        <li>
							        	<a href="#">
								        	<span uk-icon="close"></span> Xóa
								        </a>
							        </li>
							    </ul>
							</div>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
		<?php echo $pagination->getTemplate(); ?>
	</div>
</div>
