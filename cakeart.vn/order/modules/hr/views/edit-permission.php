<script src="assets/lib/vuejs/vue.min.js?v=<?php echo time(); ?>"></script>
<div class="uk-section" id="user-profile">
	<div class="uk-container">
		<div class="uk-margin-medium-top" uk-grid>
			<?php if ($error !== ''): ?>
            	<div class="uk-alert-warning uk-padding-small" uk-alert>
            		<a class="uk-alert-close" uk-close></a>
            		<span uk-icon="warning"></span> 
            		<?php echo $error; ?>
            	</div>
            <?php endif ?>
            <?php if ($msg !== '' && $msg !== NULL): ?>
            	<div class="uk-alert-success uk-padding-small" uk-alert>
            		<a class="uk-alert-close" uk-close></a>
            		<span uk-icon="check"></span> <?php echo $msg; ?>
            	</div>
            <?php endif ?>
			<form action="" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="token" value="<?php echo $token;?>" required>
            	<input type="hidden" name="action" value="update-permission-action" required>
			    <div class="uk-margin">
		            <select class="uk-select" name="quyennhanvien" v-on:change="selectedPermission" v-model="selected.id">
		                <option v-for="option in options" v-bind:value="option.id">
						    {{ option.type }}
						</option>
		            </select>
		            <span class="uk-text-muted" id="mota">{{selected.mota}}</span>
		        </div>
			    <button class="uk-button uk-button-primary uk-margin-small-top" type="submit">
			    	Cập nhật thông tin
			    </button>
			</form>
		</div>
	</div>
</div>

<script>
	var app = new Vue({
	  	el: '#user-profile',
	  	data:  {
		    selected: {
		    	id: <?php echo $user['quyennhanvien'] ?>,
		    	mota: 'Lựa chọn quyền cho nhân viên'
		    },
		    options: [
		    	<?php foreach ($types as $type): ?>
			      	{ 
			      		mota: '<?php echo str_replace($type['mota']); ?>', 
			      		type: '<?php echo $type['kieunhanvien'] ?>',
			      		id: <?php echo $type['id'] ?>
			      	},
                <?php endforeach ?>
		    ]
	  	},

	  	methods: {
	  		selectedPermission: function(e){
	  			var _this = this;
	  			_this.selected = _this.options.filter(function(opt) {
	  				return opt.id == _this.selected.id
	  			})[0];
	  		}
	  	}
	});
</script>