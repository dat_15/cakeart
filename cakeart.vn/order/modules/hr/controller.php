<?php
/**
* index controller
*/
class Hr extends Controller {
	private $session;
	private $model;
	private $modules;
	private $callbackUrl;
	function __construct(){
		parent::__construct(__CLASS__);
		$this->session = new Session();
		$this->model = $this->load_model('users');
		Session::set_session('goToUrl', DOMAIN.'hr');
		if(empty($this->userInfo)) header('Location: '.LOGIN_PAGE);
		if( !$this->model->isAccessibleModule('hr', (int) $this->userInfo['manv']) ) header('Location: '.DOMAIN);
		$this->modules = $this->model->getUserModules((int) $this->userInfo['manv']);
	}

	function index($page = 1){
		$total = $this->model->countActiveUsers();
		$pagination = new Pagination([
			'total' 		=> $total, 
			'perPage' 		=> PER_PAGE*3,
			'pageToShow' 	=> 4,
			'currentPage' 	=> $page,
			'link' 			=> DOMAIN . 'hr',
		]);
		$page = $pagination->getCurrentPage();
		$this->load_view('main-header', [
			'base_url' 	=> 'modules/header/', 
			'title' 	=> 'Quản lý nhân viên',
			'user' 		=> $this->userInfo
		], 'header');
		$this->load_view('menu-top',['modules' => $this->modules, 'mod' => 'hr'], 'header');
		$this->load_view('index', [
			'users' 	=> $this->model->getUsersActive(PER_PAGE*3, ($page-1)*PER_PAGE*3 ),
			'pagination'=> $pagination
		]);
		$this->load_view('footer',[], 'footer');
	}

	public function edit($manv){
		$manv = de_id($manv);
		$error = '';
		$msg = '';
		$nhanvien = $this->model->getUser(['manv' => $manv]);
		$action = input('action', 'p', '');
		if( !empty($action) ){
			$token  = input('token', 'p', '');
			if($action !== 'update-profile-action'){
				$error = 'Action đã bị thay đổi';
			}elseif($token !== $this->session->get_session('edit-nhanvien-token')){
				$error = 'Token không khớp ';
			}else{
				$tendaydu 	= input('tendaydu', 'p', '');
				$email 		= input('email', 'p', '');
				$sodt 		= input('sodt', 'p', '');
				$diachi 	= input('diachi', 'p', '');
				$ngaysinh 	= input('ngaysinh', 'p', '');
				$facebook 	= input('facebook', 'p', '');
				if(
					$tendaydu === '' || 
					$email === '' ||
					$sodt === '' ||
					$diachi === '' ||
					$ngaysinh === '' ||
					$facebook === ''
				){
					$error = 'Không được để trống các trường dưới đây!';
				}
				elseif(!$this->checkValidEmail($email)){
					$error = 'Email không hợp lệ';
				}
				else{
					$dataUpdate = [
						'tendaydu' 	=> $tendaydu,
						'email' 	=> $email,
						'sodt' 		=> $sodt,
						'diachi' 	=> $diachi,
						'ngaysinh' 	=> $ngaysinh,
						'facebook' 	=> $facebook
					];
					$this->model->updateUserData($dataUpdate, ['manv' => $manv]);
					
					if(
						isset($_FILES['hinh']) &&
						$_FILES['hinh']['error'] == 0 && 
						$_FILES['hinh']['size'] > 0
					){
						$allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
						$detectedType = exif_imagetype($_FILES['hinh']['tmp_name']);
						if ($_FILES["hinh"]["size"] > 5000000) {
						    echo $error = "File bạn chọn quá lớn.";
						}
						elseif(!in_array($detectedType, $allowedTypes)){
							$error = 'File bạn chọn không đúng định dạng';
						}else{
							unlink(__DIR__.'/../../../intranet/images/nhanvien/'.$nhanvien['hinh']);
							self::uploadImage($manv);

						}
					}
					$nhanvien = $this->model->getUser(['manv' => $manv]);
					$msg = 'Cập nhật thành công';
				}
			}
		}
		$token = md5($this->randomToken(10).time());
		$this->session->set_session('edit-nhanvien-token', $token);
		$this->load_view('header-no-angular', [
			'base_url' 	=> 'modules/header/', 
			'title' 	=>  'Chỉnh sửa thông tin - '.$nhanvien['tendaydu'],
			'user' 		=> $this->userInfo
		], 'header');
		$this->load_view('menu-top',['modules' 	=> $this->modules, 'mod' => 'hr'], 'header');
		$this->load_view('index', [
			'user' 		=> $nhanvien,
			'error' 	=> $error,
			'msg' 		=> $msg,
			'token' 	=> $token,
		], 'profile');
		$this->load_view('footer',[], 'footer');
	}


	private function uploadImage($manv){
		$file = $_FILES['hinh']['tmp_name']; 
		$sourceProperties = getimagesize($file);
        $fileNewName = time();
        $folderPath = __DIR__.'/../../../intranet/images/nhanvien/';
        $ext = pathinfo($_FILES['hinh']['name'], PATHINFO_EXTENSION);
        $imageType = $sourceProperties[2];
        switch ($imageType) {

            case IMAGETYPE_PNG:
                $imageResourceId = imagecreatefrompng($file); 
                $file = self::imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                imagepng($file,$folderPath. $fileNewName. ".". $ext);
                break;

            case IMAGETYPE_GIF:
                $imageResourceId = imagecreatefromgif($file); 
                $file = self::imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                imagegif($file,$folderPath. $fileNewName. ".". $ext);
                break;

            case IMAGETYPE_JPEG:
                $imageResourceId = imagecreatefromjpeg($file); 
                $file = self::imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                imagejpeg($file,$folderPath. $fileNewName. ".". $ext);
                break;

            default:
                exit;
                break;
        }
        move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
		$this->model->updateUserData(
			['hinh' => $fileNewName. ".". $ext], 
			['manv' => $manv ]
		);
	}

	private function imageResize($imageResourceId,$width,$height) {
	    $targetWidth =200;
	    $targetHeight =200;
	    $file=imagecreatetruecolor($targetWidth,$targetHeight);
	    imagecopyresampled($file,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $width,$height);
	    return $file;
	}

	public function editPermission($manv){
		$manv = de_id($manv);
		$error = '';
		$msg = '';
		$nhanvien = $this->model->getUser(['manv' => $manv]);
		$action = input('action', 'p', '');
		if( !empty($action) ){
			$token  = input('token', 'p', '');
			if($action !== 'update-permission-action'){
				$error = 'Action đã bị thay đổi';
			}elseif($token !== $this->session->get_session('edit-permission-token')){
				$error = 'Token không khớp ';
			}else{
				$quyennhanvien = input('quyennhanvien', 'p', '');
				if( $quyennhanvien === '' ){
					$error = 'Không được để trống trường này!';
				}
				else{
					$dataUpdate = [ 'quyennhanvien' => $quyennhanvien ];
					$this->model->updateUserData($dataUpdate, ['manv' => $manv]);
					$msg = 'Cập nhật thành công';
					$nhanvien = $this->model->getUser(['manv' => $manv]);
				}
			}
		}
		$token = md5($this->randomToken(10).time());
		$this->session->set_session('edit-permission-token', $token);
		$this->load_view('header-no-angular', [
			'base_url' 	=> 'modules/header/', 
			'title' 	=> 'Phân quyền - '.$nhanvien['tendaydu'],
			'user' 		=> $this->userInfo
		], 'header');
		$this->load_view('menu-top',['modules' 	=> $this->modules, 'mod' => 'hr'], 'header');
		$this->load_view('edit-permission', [
			'user' 		=> $nhanvien,
			'types' 	=> $this->model->getUserType(),
			'error' 	=> $error,
			'msg' 		=> $msg,
			'token' 	=> $token,
		]);
		$this->load_view('footer',[], 'footer');
	}

}