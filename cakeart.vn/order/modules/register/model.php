<?php 
/**
 * 
 */
class Register_model extends Model{
	protected $table;
	public function __construct(){
		parent::__construct();
		$this->table = USER_TABLE;
		$this->db->connect_to('tinyorder');
	}

	public function getUser($data){
		return $this->db->get_row($this->table, $data);
	}
	public function addUser($data){
		return $this->db->add_row($data, $this->table);
	}
}
?>