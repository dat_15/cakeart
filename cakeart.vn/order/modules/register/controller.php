<?php
/**
* index controller
*/
class Register extends Controller {
	private $session;
	private $model;
	private $callbackUrl;
	function __construct(){
		parent::__construct(__CLASS__);
		$this->session = new Session();
		$this->model = $this->load_model(__CLASS__);
		if(count($this->userInfo)) {
			header('Location: '. $this->session->get_session('goToUrl') );
			die();
		}
	}

	function index(){
		$error = '';
		$data = [];
		$action = input('action', 'p', '');
		if( !empty($action) ){
			$token  = input('token', 'p', '');
			if($action !== 'register-action'){
				$error = 'Action đã bị thay đổi';
			}elseif($token !== $this->session->get_session('token')){
				$error = 'Token không khớp ' . $token . ' ad ' . $this->session->get_session('token');
			}else{
				$username = input('username', 'p', '');
				$password = input('password', 'p', '');
				$email = input('email', 'p', '');
				$phonenumber = input('phonenumber', 'p', '');
				if($username === '' || $password === ''){
					$error = 'Không được để trống tên đăng nhập hoặc mật khẩu';
				}else{
					$user_by_username = $this->model->getUser([
						'tendangnhap' => $username
					]);
					$user_by_email = $this->model->getUser([
						'email' => $email
					]);
					$user_by_phonenumber = $this->model->getUser([
						'sodt' => $phonenumber
					]);
					if( count($user_by_username) || count($user_by_email) || count($user_by_phonenumber) ){
						$error = "Tài khoản đã tồn tại.";
					}elseif(
						$this->model->addUser([
							'sodt' 			=> $phonenumber,
							'tendangnhap' 	=> $username,
							'matkhau' 		=> $password,
							'email' 		=> $email,
							'tendaydu' 		=> $username,
							'quyen' 		=> 0,
							'admin' 		=> 0,
							'thongbao' 		=> '',
							'ngaysinh' 		=> date("Y-m-d"),
							'diachi' 		=> '',
							'facebook' 		=> '',
							'hinh' 			=> '',
							'ngaybatdau' 	=> date("Y-m-d"),
							'mucluongthang' => 0.0,
							'mucluonggio' 	=> 0.0,
							'dangdilam' 	=> 0,
							'digiaobanh' 	=> 0,
							'ngaynghiviec' 	=> date("Y-m-d"),
							'hometown' 		=> '',
							'congviec' 		=> 'sales',
							'note' 			=> ''
						])
					){
						$this->session->set_session('flash-message', 'Tạo tài khoản thành công.');
						header('Location: '.LOGIN_PAGE);
					}else{
						$error = "Xảy ra lỗi trong quá trình tạo tài khoản. Vui lòng thử lại.";
					}
				}
			}
			$data = input();
		}
		$token = md5($this->randomToken(10).time());
		$this->session->set_session('token', $token);
		$this->load_view('index', [
			'base_url' => 'modules/header/', 
			'token' => $token,
			'error' => $error,
			'data'  => $data,
		]);
	}
}