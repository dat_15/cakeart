<?php 

/**
 * 
 */
class Header extends Controller {
	
	function __construct(){
		parent::__construct(__CLASS__);
	}

	public function index(){
		$this->load_view('main-header', ['user' => $this->userInfo]);
	}
}

 ?>