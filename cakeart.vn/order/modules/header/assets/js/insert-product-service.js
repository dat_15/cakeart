var addProductApp = angular.module('add-product-app', []);
let domain = "http://www.cakeart.vn/order/";

addProductApp.service('productService', ['$http', function ($http) {

	this.findProduct = (data, callback) => {
		var req = {
		    method: 'POST',
		    url: domain+'api/find_product',
		    data: data,
		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}, 
		    transformRequest: function(obj) { 
			    var str = []; 
			    for(var p in obj) 
			    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p])); 
			    return str.join("&"); 
		    }
		};

		$http(req).then((res) => {
		    callback(res.data);
		}, (err) => {
		    console.log(err);
		});
	}

	this.saveOrderDetail=(data, callback)=>{
		var req = {
			method: 'POST',
			url: domain + 'api/saveOrderDetail',
			data: data,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			transformRequest: function (obj) {
				var str = [];
				for (var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			}
		};

		$http(req).then((res) => {
			callback(res.data);
		}, (err) => {
			console.log(err);
		});
	}

	this.addProduct = (data, callback) => {
		var req = {
			method: 'POST',
			url: domain + 'api/addProduct',
			data: data,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			transformRequest: function (obj) {
				var str = [];
				for (var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			}
		};

		$http(req).then((res) => {
			callback(res.data);
		}, (err) => {
			console.log(err);
		});
	}

}]);

