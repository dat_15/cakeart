
    supplierApp.service('supplierService', ['$http', function ($http) {
        this.deleteSupplier = (_id, callback) => {
            $.get(domain+'inventory/deletesupplier/'+_id, function(data) {
                callback(JSON.parse(data));
            });
        }

        this.saveSupplier = (data, callback) => {
            $.post(domain+'inventory/savesupplier', data, function (res) {
                callback( JSON.parse(res) );
            });
        }

        this.addNewSupplier = (data, callback) => {
            $.post(domain+'inventory/addnewsupplier', data, function (res) {
                callback( JSON.parse(res) );
            });
        }
    }]);

    // Groups Management Controller
    supplierApp.controller('suppliers-controller', ['$scope', '$sce', '$timeout', '$interval', 'supplierService',
        function($scope, $sce, $timeout, $interval, supplierService) {
            let service = supplierService;
            $scope.listSuppliers = _listSuppliers;
            $scope.isEditting = false;
            $scope.currentSupplierID = 0;
            $scope.newSupplier = {
                name: '',
                phone_number: '',
                address: '',
                goods: ''
            }
            $scope.showLoading = true;

            $scope.editSupplier = function(supplier){
                $scope.isEditting = true;
                $scope.currentSupplierID = supplier.id;
            }

            $scope.addNewSupplier = () => {
                if($scope.newSupplier.name == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập tên nhà cung cấp!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                }
                if($scope.newSupplier.phone_number == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập số liên hệ!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                }
                if($scope.newSupplier.address == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập địa chỉ nhà cung cấp!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                }
                if($scope.newSupplier.goods == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập mặt hàng cung cấp!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                }
                service.addNewSupplier($scope.newSupplier, res => {
                    if(res.status){
                        $scope.newSupplier.id = res.supplierID;
                        var new_data = angular.copy($scope.newSupplier);
                        $scope.listSuppliers.push(new_data);
                        UIkit.notification({
                            message: '<span uk-icon="check"></span> Thêm nhà cung cấp thành công!',
                            status: 'success',
                            pos: 'top-right', 
                            timeout: 1500
                        }); 
                        $timeout(function(){
                            $scope.newSupplier = {
                                name: '',
                                phone_number: '',
                                address: '',
                                goods: ''
                            };
                        }, 500);
                    }else{
                        UIkit.notification({
                            message: '<span uk-icon="warning"></span> '+res.msg,
                            status: 'warning',
                            pos: 'top-right',
                            timeout: 1500
                        }); 
                    }
                });
            }

            $scope.saveSupplier = supplier => {
                if(supplier.name == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập tên nhà cung cấp!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                }
                if(supplier.phone_number == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập số liên hệ!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                }
                if(supplier.address == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập địa chỉ nhà cung cấp!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                }
                if(supplier.goods == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập mặt hàng cung cấp!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                }
                service.saveSupplier({
                    name: supplier.name,
                    phone_number: supplier.phone_number,
                    address: supplier.address,
                    goods: supplier.goods,
                    id: supplier.id
                }, function(res){
                    if(res.status){
                        UIkit.notification({
                            message: '<span uk-icon="check"></span> Cập nhật thành công!',
                            status: 'success',
                            pos: 'top-right',
                            timeout: 1500
                        }); 
                    }else{
                        UIkit.notification({
                            message: '<span uk-icon="warning"></span> Chưa cập nhật thành công!',
                            status: 'warning',
                            pos: 'top-right',
                            timeout: 1500
                        }); 
                    }
                });
                $scope.isEditting = false;
                $scope.currentSupplierID = 0;
                
            }

            $scope.deleteSupplier = (supplier, e) => {
                e.preventDefault();
                e.stopPropagation();
                UIkit.modal.confirm('Bạn có chắc chắn muốn xóa nhà cung cấp này!').then(function () {
                    service.deleteSupplier(supplier.id, res => {
                        if(res.status){
                            UIkit.notification({
                                message: '<span uk-icon="check"></span> Xóa nhà cung cấp thành công!',
                                status: 'success',
                                pos: 'top-right',
                                timeout: 1500
                            });
                            $timeout(function(){
                                $scope.listSuppliers = $scope.listSuppliers.filter(item => {
                                    return item.id !== supplier.id;
                                });
                            }, 1);
                        }else{
                            UIkit.notification({
                                message: '<span uk-icon="warning"></span> Đã xảy ra sự cố. Vui lòng thử lại!',
                                status: 'warning',
                                pos: 'top-right',
                                timeout: 1500
                            }); 
                        }
                    })
                }, function () {});
            }

            $timeout(function(){
                $scope.showLoading = false;
            }, 1000);
        }
    ]);