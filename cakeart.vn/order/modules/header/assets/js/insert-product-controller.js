// Groups Management Controller
addProductApp.controller('add-product-controller', ['$scope', '$rootScope', '$sce', '$timeout', '$compile', '$interval', 'productService',
    function($scope, $rootScope, $sce, $timeout, $compile, $interval, productService) {
    	let service = productService;
        $scope.suggestionProducts = [];
        $scope.addedProducts = _products;
        $scope.showLoading = false;
        $scope.showResults = false;
        $scope.selectedProduct = {
            quantity: '',
            loaibanh: '',
            dongia: '',
            kichthuoc: '',
            chieucao: '',
            hinhdang: '',
            sotang: '',
            mausac: '',
            cotbanh: '',
            nhanbanh: '',
            chutrenbanh: '',
            hinhketqua: 'no.jpg',
            soluong: 0
        };

    	$scope.keyword = '';
    	let timer = null;
    	$scope.findProductsSuggestion = (_type) => {
    		$scope.showLoading = true;
    		clearTimeout(timer); 
       		timer = setTimeout(function(){
                service.findProduct({keyword : $scope.keyword, type: _type}, res => {
                    $scope.showLoading = false;
                    if(res.status && Array.isArray(res.data)){
	    				$timeout(function(){
                            $scope.suggestionProducts = res.data;
                            $scope.showResults = true;
	    				}, 100);
	    			}else{
                        $scope.noResult = true;
                    }
	    		});
                $timeout(function(){
                    $scope.showLoading = false;
                }, 3000);
            }, 1000);
        }
        
        $scope.selectProduct = product => {
            product.soluong = parseInt(product.soluong);
            $scope.selectedProduct = product;
            $scope.showResults = false;
        }

        $scope.removeProduct = (product) => {
            $scope.selectedProducts = $scope.selectedProducts.filter(p => {
                return p.mahd !== product.mahd;
            });
        }

        $scope.removeaddedProducts = (product) => {
            $scope.addedProducts = $scope.addedProducts.filter(p => {
                return p.loaibanh !== product.loaibanh;
            });
        }
        $scope.render_img = (img_src, size = 40) => {
            var temp_src = img_src.replace(/%20/g, ' ');
            if (temp_src.length > 16) {
                return $sce.trustAsHtml(`
                    <a href="http://tinycake.vn/order/thuvienbanh/`+ temp_src+`"caption="">
                        <img width="`+size+`px" height="`+size+`px" src="http://tinycake.vn/order/thuvienbanh/`+ temp_src +`">
                    </a>
                    `);
            }else{
                return $sce.trustAsHtml(`
                    <a href="http://tinycake.vn/order/images/hinhketqua/small`+ temp_src+`" data-caption="">
                        <img width="`+size+`px" height="`+size+`px" src="http://tinycake.vn/order/images/hinhketqua/small`+ temp_src +`">
                    </a>
                `);
            }
        }

        $scope.editSelectedProduct = p => {
            $scope.selectedProduct = p;
        }
        
        $scope.addProduct = () => {
            var keys = ['loaibanh', 'dongia'];
            for (let index = 0; index < keys.length; index++) {
                if ($scope.selectedProduct[keys[index]].trim() == ''){
                    UIkit.notification({ message: 'Vui lòng không để trống các trường này', status: 'warning' });
                    return false;
                }
            }
            if ( !$scope.addedProducts.length || !$scope.addedProducts.filter(p => {return p.mahd == $scope.selectedProduct.mahd;}).length ){
                let data = $scope.selectedProduct;
                data.makh = _makh;
                data.order = _order;
                $scope.addedProducts.push(data);
                $timeout(function () {
                    $scope.selectedProduct = {
                        quantity: '',
                        loaibanh: '',
                        dongia: '',
                        kichthuoc: '',
                        chieucao: '',
                        hinhdang: '',
                        sotang: '',
                        mausac: '',
                        cotbanh: '',
                        nhanbanh: '',
                        chutrenbanh: '',
                        soluong: 0
                    };
                }, 100);
            }else{
                for (let index = 0; index < $scope.addedProducts.length; index++) {
                    if ($scope.addedProducts[index].mahd == $scope.selectedProduct.mahd){
                        $scope.addedProducts[index] = $scope.selectedProduct.mahd;
                        $timeout(function () {
                            $scope.selectedProduct = {
                                quantity: '',
                                loaibanh: '',
                                dongia: '',
                                kichthuoc: '',
                                chieucao: '',
                                hinhdang: '',
                                sotang: '',
                                mausac: '',
                                cotbanh: '',
                                nhanbanh: '',
                                chutrenbanh: '',
                                hinhketqua: 'no.jpg',
                                soluong: 0
                            };
                        }, 100);
                        break;
                    }
                    
                }
            }
        }

        

        $scope.saveData = () => {
            if (!$scope.addedProducts.length){
                UIkit.notification({ message: 'Tạo sản phẩm mới cho đơn hàng này', status: 'warning' });
                return false;
            }
            for (let index = 0; index < $scope.addedProducts.length; index++) {
                if($scope.addedProducts[index].is_added == undefined){
                    service.addProduct($scope.addedProducts[index], res => {
                        $scope.addedProducts[index].mahd = res.status;
                        $scope.addedProducts[index].is_added = true;
                        if (index === $scope.addedProducts.length - 1) {
                            UIkit.notification({
                                message: 'Lưu dữ liệu thành công',
                                status: 'success'
                            });
                            return false;
                        } 
                    });
                }
            }
        }
    }
]);
