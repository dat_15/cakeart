// Groups Management Controller
addCustomerApp.controller('add-customer-controller', ['$scope', '$rootScope', '$sce', '$timeout', '$compile', '$interval', 'customerService',
    function($scope, $rootScope, $sce, $timeout, $compile, $interval, customerService) {
    	let service = customerService;
    	$scope.suggestionCustomers = [];
        $scope.showLoading = false;
        $scope.showAddingForm = true;
        $scope.noResult = false;
        $scope.dang_nhap_ten=false;
    	$scope.keyword = '';
        $scope.tenkhClass = '';
        $scope.tenkhClass = '';
        $scope.sdtClass = '';
        $scope.emailClass = '';
    	$scope.customer = {
			tenkh : '',
			sdt : '',
			diachi : '',
			email : '',
			fb : '',
			nguoinhan: '',
			stdnguoinhan: '',
			ghichu: ''
    	};
    	let timer = null;
    	$scope.findCustomerSuggestion = () => {
    		$scope.showLoading = true;
            $scope.dang_nhap_ten=true;
    		clearTimeout(timer); 
       		timer = setTimeout(function(){
                console.log($scope.keyword);
                service.findCustomer({keyword : $scope.keyword}, res => {
                    $scope.showLoading = false;
                    if(res.status && Array.isArray(res.data)){
                        $scope.noResult = false;
        				$timeout(function(){
    	    				$scope.suggestionCustomers = res.data;
        				}, 1);
                    }else{
                        $scope.noResult = true;
                    }
	    		});
       		}, 1000);
    	}

        $scope.find_customer_by_key = (field) => {
            clearTimeout(timer); 
            timer = setTimeout(function(){
                var _val = '';
                switch(field) {
                    case 'tenkh':
                        _val = $scope.customer.tenkh.trim();
                        break;
                    case 'sdt':
                        _val = $scope.customer.sdt.trim();
                        break;
                    case 'email':
                        _val = $scope.customer.email.trim();
                        break;
                    default :
                        _val = $scope.customer.tenkh.trim();
                        break;
                }
                if(_val === ''){
                    switch(field) {
                        case 'tenkh':
                            $scope.tenkhClass = 'uk-form-danger';
                            break;
                        case 'sdt':
                            $scope.sdtClass = 'uk-form-danger';
                            break;
                        case 'email':
                            $scope.emailClass = 'uk-form-danger';
                            break;
                    }
                }else{
                    service.findCustomerByKey({
                        key: field,
                        value: _val
                    }, res => {
                        if(res.status){
                            switch(field) {
                                case 'tenkh':
                                    $scope.tenkhClass = 'uk-form-danger';
                                    break;
                                case 'sdt':
                                    $scope.sdtClass = 'uk-form-danger';
                                    break;
                                case 'email':
                                    $scope.emailClass = 'uk-form-danger';
                                    break;
                            }
                        }else{
                            switch(field) {
                                case 'tenkh':
                                    $scope.tenkhClass = 'uk-form-success';
                                    break;
                                case 'sdt':
                                    $scope.sdtClass = 'uk-form-success';
                                    break;
                                case 'email':
                                    $scope.emailClass = 'uk-form-success';
                                    break;
                            }
                        }
                    });
                }
            }, 700);
            
        }

        $scope.selectCustomer = customer => {
            window.location.replace(window.location.href + '/' + customer.makh);
        }
    }
]);
function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 10.785349, lng: 106.681938},
        //disableDefaultUI: true,
        zoom: 18,
        gestureHandling: 'cooperative',
        mapTypeId: 'roadmap',
        styles: [{
            featureType:"road",
            elementType: "labels",
            stylers: [{ visibility: "simplified" }]
        }]
    },{
        featureType:'road.local',
        elementType: 'labels.text.fill',
        stylers:[{color:'$806b63'}]
    });
    var input = document.getElementById('pac-input');

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    var shop = new google.maps.Marker({
        position: {lat: 10.785349, lng: 106.681938},
        map: map,
        title: 'CakeArt Shop'
    });

    var bounds = new google.maps.LatLngBounds();
    bounds.extend(shop.position);
    var infowindow = new google.maps.InfoWindow();
    var service=new google.maps.places.PlacesService(map)
    var geocoder = new google.maps.Geocoder;
    var marker2 = new google.maps.Marker({
      map: map,
      anchorPoint: new google.maps.LatLng(10.785349, 106.681938),
      draggable:true,
    });
    marker2.addListener('click', function() {
        infowindow.open(map, marker2);
    });
    var service = new google.maps.DistanceMatrixService();
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer({
        draggable: true,
        suppressMarkers: true,
        map: map
    });
    google.maps.event.addListener(marker2,'dragend', handleEvent);
    function handleEvent(event) {
        geocoder.geocode({'location': {
            lat: event.latLng.lat(), 
            lng: event.latLng.lng()
        }}, function(results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    input.value= results[0].formatted_address;
                    var origin1 = new google.maps.LatLng(10.785349, 106.681938);
                    var origin2 = '183B Trần Quốc Thảo';
                    var destinationA = results[0].formatted_address;
                    var destinationB = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());
                    $(".lat_lng").val(event.latLng.lat()+','+event.latLng.lng());
                    bounds.extend(marker2.position);

                    service.getDistanceMatrix({
                        origins: [origin1, origin2],
                        destinations: [destinationA, destinationB],
                        travelMode: 'DRIVING'
                    }, function(res){
                        map.fitBounds(bounds);
                        var shipping_fee = res.rows[0].elements[1].distance.value*5;
                        shipping_fee = shipping_fee <= 20000 ? 20000 : parseInt(shipping_fee/1000)*1000;
                        infowindow.setContent(`
                            <div><strong>`+results[0].formatted_address+`</strong></div>
                            <div>Khoảng cách đến CakeArt: <strong> `+res.rows[0].elements[1].distance.text + `</strong></div>
                            <div>Phí ship: <strong>`+shipping_fee.toLocaleString()+` <sup>đ</sup></strong></div>
                            <div>Thời gian giao: <strong>`+res.rows[0].elements[1].duration.text+`</strong></div>
                            `);
                        infowindow.open(map, marker2);
                        directionsService.route({
                            origin: origin2,
                            destination: results[0].formatted_address,
                            travelMode: 'DRIVING'
                        }, function(directionsresponse, status) {
                            directionsDisplay.setDirections(directionsresponse);
                        });
                    });
                }
            }
        });
    }

    autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker2.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
        
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }
      
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  

        }
        var origin1 = new google.maps.LatLng(10.785349, 106.681938);
        var origin2 = '183B Trần Quốc Thảo';
        var destinationA = place.name;
        var destinationB = place.geometry.location;
        var service = new google.maps.DistanceMatrixService();
        $(".lat_lng").val(place.geometry.location.lat()+','+place.geometry.location.lng());
        service.getDistanceMatrix({
            origins: [origin1, origin2],
            destinations: [destinationA, destinationB],
            travelMode: 'DRIVING'
        }, function(res){
            marker2.setPosition(place.geometry.location);
            marker2.setVisible(true);
            bounds.extend(marker2.position);
            map.fitBounds(bounds);
            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }
            var shipping_fee = res.rows[0].elements[1].distance.value*5;
            shipping_fee = shipping_fee <= 20000 ? 20000 : parseInt(shipping_fee/1000)*1000;
            infowindow.setContent(`
                <div><strong>`+address+`</strong></div>
                <div>Khoảng cách đến CakeArt: <strong> `+res.rows[0].elements[1].distance.text + `</strong></div>
                <div>Phí ship: <strong>`+shipping_fee.toLocaleString()+` <sup>đ</sup></strong></div>
                <div>Thời gian giao: <strong>`+res.rows[0].elements[1].duration.text+`</strong></div>
                `);
            
            infowindow.open(map, marker2);
            directionsService.route({
                origin: origin2,
                destination: place.name,
                travelMode: 'DRIVING'
            }, function(directionsresponse, status) {
                directionsDisplay.setDirections(directionsresponse);
            });
        });

    });
}