
    orderApp.service('orderService', ['$http', function ($http) {
        this.getWorkOfOrder = (_order_id, callback) => {
            $.get(domain + 'deliveryorder/getWorkOfOrder?order_id=' + _order_id, function(data) {
                callback(JSON.parse(data));
            });
        }

        this.deleteTask = (_id, callback) => {
            $.get(domain+'deliveryorder/deleteTask?id='+_id, function(data) {
                callback(JSON.parse(data));
            });
        }

        this.saveTask = (data, callback) => {
            $.ajax({
                url: domain+'deliveryorder/saveTask',
                type: 'POST',
                data: data,
                success: function (res) {
                    callback( JSON.parse(res) );
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }

        this.addNewTask = (data, callback) => {
            $.post(domain+'deliveryorder/addNewTask', data, function (res) {
                callback( JSON.parse(res) );
            });
        }
    }]);

    // Groups Management Controller
    orderApp.controller('deliveryorder-controller', ['$scope', '$sce', '$timeout', '$interval', 'orderService',
        function($scope, $sce, $timeout, $interval, orderService) {
            let service = orderService;
            $scope.listDates = [];
            $scope.listOrder = [];
            $scope.listTasks = [];
            $scope.listUsers = [];
            $scope.isEditting = false;
            $scope.currentTaskID = 0;
            $scope.curOrder = {};
            $scope.newTask = {
                tienthanhtoan: 0,
                phi_ship: 0,
                note: '',
                manv: 0,
                mahd: 0,
                id: 0
            }
            $scope.showLoading = true;
            let c_date = new Date();
            $scope._date = c_date.getFullYear()  + "-" + (c_date.getMonth()+1) + "-" + c_date.getDate();
            $scope.goToDate = () => {
                $scope.showLoading = true;
                $scope.listOrder = _listOrder;
                $scope.listUsers = _listUser;
                $scope.listOrder = $scope.listOrder.reduce(function (r, a) {
                    r[a.ngaygiao] = r[a.ngaygiao] || [];
                    a.thoigian = new Date(a.thoigian*1000);
                    r[a.ngaygiao].push(a);
                    return r;
                }, Object.create(null));
                $scope.listDates = Object.keys ($scope.listOrder);
                   
                $timeout(function(){
                    $scope.showLoading = false;
                }, 1500);
            }

            $scope.showEditOrderForm = function(order){
                $scope.curOrder = order;
                $scope.newTask.mahd = order.mahd;
                $scope.newTask.tienthanhtoan = $scope.curOrder.soluong * $scope.curOrder.dongia
                service.getWorkOfOrder(order.mahd, (res) => {
                    $scope.listTasks = [];
                    if(res.status > 0){
                        $timeout(function(){
                            $scope.listTasks = res.data;
                        }, 20);
                    }else{
                        UIkit.notification({
                            message: 'Đơn hàng không tồn tại. Vui lòng xem lại',
                            status: 'warning',
                            pos: 'top-right',
                            timeout: 1500
                        }); 
                    }
                })
            }

            $scope.parseJSON=function(strObj){
                return strObj.slice(1,-1).replace(/"/g, '').split(",");
            }

            $scope.toVND = val => { 
                return parseInt(val).toLocaleString();
            }

            $scope.addNewTask = () => {
                if($scope.newTask.tienthanhtoan == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span>Chưa nhập tiền khách thanh toán!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                }
                if($scope.newTask.manv == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span>Chưa chọn nhân viên thực hiện!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                }
                service.addNewTask($scope.newTask, res => {
                    if(res.status){
                        $scope.newTask.id = res.taskID;
                        var new_data = angular.copy($scope.newTask);
                        $scope.listTasks.push(new_data);
                        UIkit.notification({
                            message: '<span uk-icon="check"></span>Thêm công việc thành công!',
                            status: 'success',
                            pos: 'top-right',
                            timeout: 1500
                        }); 
                        $timeout(function(){
                            $scope.newTask.tienthanhtoan = 0;
                            $scope.newTask.phi_ship = 0;
                            $scope.newTask.manv = 0;
                            $scope.newTask.note = '';
                            $scope.newTask.id = 0;
                        }, 100);
                    }else{
                        UIkit.notification({
                            message: '<span uk-icon="warning"></span>Đã xảy ra sự cố. Vui lòng thử lại!',
                            status: 'warning',
                            pos: 'top-right',
                            timeout: 1500
                        }); 
                    }
                });
            }

            $scope.editTask = task => {
                $scope.isEditting = true;
                $scope.currentTaskID = task.id;
            }

            $scope.saveTask = task => {
                var formData = new FormData();
                formData.append('tienthanhtoan', task.tienthanhtoan );
                formData.append('manv', task.manv );
                formData.append('note', task.note );
                formData.append('mahd', task.mahd );
                formData.append('phi_ship', task.phi_ship );
                formData.append('id', task.id );
                service.saveTask(formData, function(res){
                });
                $scope.isEditting = false;
                $scope.currentTaskID = 0;
            }

            $scope.deleteTask = (task, e) => {
                e.preventDefault();
                e.stopPropagation();
                UIkit.modal.confirm('Bạn có chắc chắn muốn xóa công việc này!').then(function () {
                    service.deleteTask(task.id, res => {
                        if(res.status){
                            UIkit.notification({
                                message: '<span uk-icon="check"></span>Xóa công việc thành công!',
                                status: 'success',
                                pos: 'top-right',
                                timeout: 1500
                            });
                            $timeout(function(){
                                $scope.listTasks = $scope.listTasks.filter(item => {
                                    return item.id !== task.id;
                                });
                            }, 1);
                        }else{
                            UIkit.notification({
                                message: '<span uk-icon="warning"></span>Đã xảy ra sự cố. Vui lòng thử lại!',
                                status: 'warning',
                                pos: 'top-right',
                                timeout: 1500
                            }); 
                        }
                    })
                    
                }, function () {});
            }

            $scope.getUser = uid => {
                return $scope.listUsers.filter(user => {
                    return user.manv === uid;
                })[0].tendaydu;
            }

            $scope.goToDate();
        }
    ]);