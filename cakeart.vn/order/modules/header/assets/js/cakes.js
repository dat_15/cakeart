var categoryApp = angular.module('categories-app', []);
let domain = "http://www.cakeart.vn/order/";

categoryApp.service('orderService', ['$http', function ($http) {
	
	this.getAllCat = (callback) => {
		var req = {
		    method: 'GET',
		    url: domain+'api/get_all_danhmucbanh'
		};

		$http(req).then((res) => {
		    callback(res.data);
		}, (err) => {
		    callback({ status: 0, message: 'error' });
		    console.log('getTicketGroupUser error ', err);
		    console.log(err);
		});
	}

	this.countAllOrder = (page, callback) => {
		var req = {
		    method: 'GET',
		    url: domain+'api/count_all/'+page
		};

		$http(req).then((res) => {
		    callback(res.data);
		}, (err) => {
		    callback({ status: 0, message: 'error' });
		    console.log('Call API ERROR: ', err);
		    console.log(err);
		});
	}

	this.getOrderPage = (page, callback) => {
		var req = {
		    method: 'GET',
		    url: domain+'api/get_page_danhsachbanh/'+page
		};

		$http(req).then((res) => {
		    callback(res.data);
		}, (err) => {
		    callback({ status: 0, message: 'error' });
		    console.log('Call API ERROR: ', err);
		    console.log(err);
		});
	}

	this.updateOrder = (data, callback) => {
		var req = {
		    method: 'POST',
		    url: domain+'api/update_order',
		    data: data,
		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}, 
		    transformRequest: function(obj) { 
			    var str = []; 
			    for(var p in obj) 
			    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p])); 
			    return str.join("&"); 
		    }
		};

		$http(req).then((res) => {
		    callback(res.data);
		}, (err) => {
		    callback({ status: 0, message: 'error' });
		    console.log('getTicketGroupUser error ', err);
		    console.log(err);
		});
	}

	this.updateCatOfOrder = (data, callback) => {
		var req = {
		    method: 'POST',
		    url: domain+'api/update_cat_of_order',
		    data: data,
		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}, 
		    transformRequest: function(obj) { 
			    var str = []; 
			    for(var p in obj) 
			    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p])); 
			    return str.join("&"); 
		    }
		};

		$http(req).then((res) => {
		    callback(res.data);
		}, (err) => {
		    callback({ status: 0, message: 'error' });
		    console.log('getTicketGroupUser error ', err);
		    console.log(err);
		});
	}

	this.searchKeyWord = (key, page = 1, callback) => {
		var req = {
		    method: 'GET',
		    url: domain+'api/search/'+key+'/'+page
		};

		$http(req).then((res) => {
		    callback(res.data);
		}, (err) => {
		    callback({ status: 0, message: 'error' });
		    console.log('Call API ERROR: ', err);
		    console.log(err);
		});
	}


	this.countValidResult = (key, callback) => {
		var req = {
		    method: 'GET',
		    url: domain+'api/count_all_search_sesult/'+key
		};

		$http(req).then((res) => {
		    callback(res.data);
		}, (err) => {
		    callback({ status: 0, message: 'error' });
		    console.log('Call API ERROR: ', err);
		    console.log(err);
		});
	}
}]);

categoryApp.filter('to_trusted', ['$sce', function ($sce) {
	return function (text) {
		return $sce.trustAsHtml(text);
	};
}]);
categoryApp.directive('compile', ['$compile', function ($compile) {
    return function(scope, element, attrs) {
      scope.$watch(
        function(scope) {
          return scope.$eval(attrs.compile);
        },
        function(value) {
          element.html(value);
          $compile(element.contents())(scope);
        }
    );
  };
}]);

// Groups Management Controller
categoryApp.controller('categories-management-controller', ['$scope', '$rootScope', '$sce', '$timeout', '$compile', '$interval', 'orderService',
    function($scope, $rootScope, $sce, $timeout, $compile, $interval, orderService) {
    	let service = orderService;
    	$scope.currentListOrder = [];
    	$scope.listCatLevel = [];
    	$scope.allOrder = 0;
    	$scope.currPage = 1;
    	$scope.totalPage = 1;
    	$scope.showLoading = true;
    	$scope.isFiltering = false;
    	$scope.keywordToFilter = '';
    	$scope.onInit = () => {

	    	$scope.showLoading = true;

    		$(document).on('click', '.main-field', function(){
    			$(this).toggleClass('uk-hidden');
    			let editor = $(this).siblings().toggleClass('uk-hidden').find('input,textarea');
    			editor.focus();
    			editor.focusout(function(event) {
    				let _this = $(event.target), order_stt = _this.attr('data-stt'), field = _this.attr('data-type');
    				service.updateOrder({
	        			order: order_stt,
	        			field: field,
	        			value: event.target.value
	        		}, res => {
	        			_this.parent().addClass('uk-hidden').parent().find('.main-field').removeClass('uk-hidden');
	        		});
    			});
    		});

    		service.getAllCat(res => {
				if(res.status == 1){
					$scope.listCatLevel = res.data;
				}
			});


			let url = new URL(window.location.href);
			$scope.keywordToFilter = url.searchParams.get("key");
			if($scope.keywordToFilter == undefined){

	    		service.getOrderPage(1, res => {
	    			if(res.status == 1){
	    				$scope.currentListOrder = angular.copy(res.data);
	    			}
	    		});

	    		service.countAllOrder('danhsachbanh', res => {
	    			if(res.status === 1){
	    				$scope.allOrder = parseInt(res.data);
	    				$scope.currPage = 1;
	    				$scope.totalPage = Math.round($scope.allOrder/10);
	    			}
	    		});
			}
    		else{
    			service.searchKeyWord($scope.keywordToFilter, 1, res => {
    				if(res.status == 1){
    					$scope.currentListOrder = angular.copy(res.data);
    					service.countValidResult($scope.keywordToFilter, allRes => {
    						if(allRes.status == 1){
    							$scope.currPage = 1;
    							$scope.allOrder = parseInt(allRes.data);
    							$scope.totalPage = Math.ceil(parseInt(allRes.data)/10);
    						}
    					});
    				}
    			});
    		}
			$timeout(function(){
				$scope.showLoading = false;
			}, 1000);
    	}

    	$scope.filter = e => {
    		if(e.keyCode === 13 && $scope.keywordToFilter.trim() != ''){
    			$scope.isFiltering = true;
				let newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?key='+$scope.keywordToFilter.trim();
		        window.history.pushState({path:newurl},'',newurl);
    			service.searchKeyWord($scope.keywordToFilter, 1, res => {
    				if(res.status == 1){
    					$scope.currentListOrder = angular.copy(res.data);
    					service.countValidResult($scope.keywordToFilter, allRes => {
    						if(allRes.status == 1){
    							$scope.currPage = 1;
    							$scope.allOrder = parseInt(allRes.data);
    							$scope.totalPage = Math.ceil(parseInt(allRes.data)/10);
    						}
    					});
    				}
    			});
    		}
    	}

    	$scope.getCatSelectorTemplate = (order) => {
    		return `
    			<ul style="padding-left: 0">
		            <li 
		            	class="{{parent.children.length ? 'folder' : 'file'}}" 
		            	ng-repeat="parent in listCatLevel"
		            	ng-click="selecteCat($event, parent, `+order+`)">
		                <span uk-icon="icon: tag" ng-show="!parent.children.length"></span> 
		                {{parent.name}}
		                <ul ng-show="parent.children.length">
		                    <li 
		                    	class="{{child.children.length ? 'folder' : 'file'}}" 
		                    	ng-repeat="child in parent.children"
		                    	ng-click="selecteCat($event, child, `+order+`)">
		                        <span uk-icon="icon: tag" ng-show="!child.children.length"></span> 
		                        {{child.name}}
		                        <ul ng-show="child.children.length">
		                            <li 
		                            	class="{{grandChild.children.length ? 'folder' : 'file'}}" 
		                            	ng-repeat="grandChild in child.children" 
		                            	ng-click="selecteCat($event, grandChild, `+order+`)">
		                            	<span uk-icon="icon: tag" ng-show="!grandChild.children.length"></span> 
		                            	{{grandChild.name}}
						                <ul ng-show="grandChild.children.length">
				                            <li class="file" 
				                            	ng-repeat="superGrandChild in grandChild.children" 
				                            	ng-click="selecteCat($event, superGrandChild, `+order+`)">
				                            	<span uk-icon="icon: tag"></span> {{superGrandChild.name}}
				                            </li>
				                        </ul>
		                            </li>
		                        </ul>
		                    </li>		                   
		                </ul>
		            </li>
		        </ul>
    		`;
    	}


    	$scope.selecteCat = ($event, cat, order) => {
    		$event.stopPropagation();
    		let item = $($event.target);
    		if (item.hasClass('folder') ) item.toggleClass('folder-open');
    		$timeout(function () {
	    		if(item.hasClass('file')){
	    			$timeout(function(){
						for (let i = 0; i < $scope.currentListOrder.length; i++) {
							if($scope.currentListOrder[i].stt == order){
								let currOder = $scope.currentListOrder[i];
								let catOrder = currOder.catCode == null ? [] : currOder.catCode.split(',');
								let catNames = currOder.catCode == null ? [] : currOder.catName.split(',');
								if(catOrder.indexOf(cat.code) > -1){
									UIkit.notification({
									    message:`<span uk-icon='icon: warning'></span> Đã tồn tại danh mục '` + cat.name + `'! Vui lòng chọn danh mục khác.`,
									    status: 'warning',
									    pos: 'top-right',
									    timeout: 1500
									});	
									break;
								}
								if(catOrder.length == 3){
									catOrder.shift();
									catNames.shift();
								}
								catOrder.push(cat.code);
								catNames.push(cat.name);
								currOder.catCode = catOrder.join(',');
								currOder.catName = catNames.join(',');
					    		service.updateCatOfOrder({
					    			order: order,
					    			code: catOrder.join(','),
					    			name: catNames.join(',')
					    		}, res => {
					    			if(res.status == 1){
										UIkit.notification({
										    message:`<span uk-icon='icon: check'></span> Đã chọn danh mục '` + cat.name+`'`,
										    status: 'success',
										    pos: 'top-right',
										    timeout: 1500
										});	
										
					    			}else{
					    				UIkit.notification({
										    message:`<span uk-icon='icon: warning'></span> Chưa được chọn danh mục '` + cat.name + `'! Có thể do lỗi đường truyền. Vui lòng thử lại sau.`,
										    status: 'warning',
										    pos: 'top-right',
										    timeout: 1500
										});	
					    			}
					    		});
					    		break;
			    			}
						}
					}, 100);
	    		}
    		}, 100);
    	}
    	$scope.goToPage = page => {
    		$scope.currPage = page;
			$('html, body').animate({
		      	scrollTop: $("table#order-table").offset().top
		    }, 1000);
		    if($scope.isFiltering){
		    	service.searchKeyWord($scope.keywordToFilter, page, res => {
    				if(res.status == 1){
    					$scope.currentListOrder = angular.copy(res.data);
    				}
    			});
		    }else{
	    		service.getOrderPage(page, res => {
	    			if(res.status == 1){
	    				$scope.currentListOrder = angular.copy(res.data);
	    			}
	    		});
		    }
    	}
    	$scope.updateNewProduct = ($event, order_stt, field) => {
    		console.log(order_stt);
		        service.updateOrder({
        			order: order_stt,
        			field: field,
        			value: order_stt.newproduct?1:0
        		}, res => {
        			if(res.status == 1){
        				UIkit.notification({
						    message:`<span uk-icon='icon: check'></span> Cập nhật thành công`,
						    status: 'success',
						    pos: 'top-right',
						    timeout: 3000
						});	
        			}else{
						UIkit.notification({
						    message:`<span uk-icon='icon: warning'></span> Cập nhật thất bại`,
						    status: 'success',
						    pos: 'top-right',
						    timeout: 3000
						});	
        			}
        		});
    			//$($event.target).parent().addClass('uk-hidden').parent().find('.main-field').removeClass('uk-hidden');
    			//$event.preventDefault();
    			//$event.stopPropagation();
		    }
    	
    	$scope.updateOnEdited = ($event, order_stt, field) => {
    		var keyCode = $event.which || $event.keyCode;
		    if (keyCode === 13) {
		        service.updateOrder({
        			order: order_stt,
        			field: field,
        			value: $event.target.value
        		}, res => {
        			if(res.status == 1){
        				UIkit.notification({
						    message:`<span uk-icon='icon: check'></span> Cập nhật thành công`,
						    status: 'success',
						    pos: 'top-right',
						    timeout: 3000
						});	
        			}else{
						UIkit.notification({
						    message:`<span uk-icon='icon: warning'></span> Cập nhật thất bại`,
						    status: 'success',
						    pos: 'top-right',
						    timeout: 3000
						});	
        			}
        		});
    			$($event.target).parent().addClass('uk-hidden').parent().find('.main-field').removeClass('uk-hidden');
    			$event.preventDefault();
    			$event.stopPropagation();
		    }
    	}


    	$scope.updateTagsOnEdited = ($event, order, index) => {
    		var keyCode = $event.which || $event.keyCode;
		    if (keyCode === 13) {
	    		let listKey = '';
	    		for (var i = 0; i < $scope.currentListOrder.length; i++) {
	    			if($scope.currentListOrder[i].stt == order.stt){
				    	$timeout(() => {
							switch (index) {
				    			case 1:
				    				listKey = $scope.currentListOrder[i].keyword1 ==='' ? '["'+$event.target.value+'"]' : $scope.currentListOrder[i].keyword1.replace(']', ',"'+$event.target.value+'"]');
				    				$scope.currentListOrder[i].keyword1 = listKey;
				    				break;
				    			case 2:
				    				listKey = $scope.currentListOrder[i].keyword2 === '' ? '["'+$event.target.value+'"]' : $scope.currentListOrder[i].keyword1.replace(']', ',"'+$event.target.value+'"]');
				    				$scope.currentListOrder[i].keyword2 = listKey;
				    				break;
				    			case 3:
				    				listKey = $scope.currentListOrder[i].keyword3 === '' ? '["'+$event.target.value+'"]' : $scope.currentListOrder[i].keyword1.replace(']', ',"'+$event.target.value+'"]');
				    				$scope.currentListOrder[i].keyword3 = listKey;
				    				break;
				    			case 4:
				    				listKey = $scope.currentListOrder[i].keyword4 === '' ? '["'+$event.target.value+'"]' : $scope.currentListOrder[i].keyword1.replace(']', ',"'+$event.target.value+'"]');
				    				$scope.currentListOrder[i].keyword4 = listKey;
				    				break;
				    		}
				    		
				    		service.updateOrder({
			        			order: order.stt,
			        			field: 'keyword'+index,
			        			value: listKey
			        		}, res => {$event.target.value = '';});
						}, 100);
	    				break;
	    			}
	    		}
		    }
    	}

    	$scope.deleteKeyword = (keyword, order_stt, index) => {
    		let listKey;
    		let order = $scope.currentListOrder.filter(order => {
    			return order.stt == order_stt;
    		})[0];
    		switch (index) {
    			case 1:
    				listKey = order.keyword1;
    				break;
    			case 2:
    				listKey = order.keyword2;
    				break;
    			case 3:
    				listKey = order.keyword3;
    				break;
    			case 4:
    				listKey = order.keyword4;
    				break;
    		}
    		for (let i = 0; i < $scope.currentListOrder.length; i++) {
    			if($scope.currentListOrder[i].stt == order.stt){
    				$timeout(() => {
    					switch (index) {
			    			case 1:
			    				$scope.currentListOrder[i].keyword1 = listKey.replace('"'+keyword+'"', '').replace(', ', ',');
			    				break;
			    			case 2:
			    				$scope.currentListOrder[i].keyword2 = listKey.replace('"'+keyword+'"', '').replace(', ', ',');
			    				break;
			    			case 3:
			    				$scope.currentListOrder[i].keyword3 = listKey.replace('"'+keyword+'"', '').replace(', ', ',');
			    				break;
			    			case 4:
			    				$scope.currentListOrder[i].keyword4 = listKey.replace('"'+keyword+'"', '').replace(', ', ',');
			    				break;
			    		}
			    		service.updateOrder({
		        			order: order_stt,
		        			field: 'keyword'+index,
		        			value: listKey.replace('"'+keyword+'"', '').replace(', ', ',')
		        		}, res => {
		        		});
    				}, 100);
    				break;
    			}
    		}

    	}

    	$scope.updateOrderClap = (type, order) => {
    		for (let i = 0; i < $scope.currentListOrder.length; i++) {
    			if($scope.currentListOrder[i].stt == order.stt){
    				$timeout(() => {
    					if(type== 'plus'){
    						$scope.currentListOrder[i].clap++;
    					}else{
    						$scope.currentListOrder[i].clap--;
    					}
			    		service.updateOrder({
		        			order: order.stt,
		        			field: 'clap',
		        			value: $scope.currentListOrder[i].clap
		        		}, res => {});
    				}, 100);
    				break;
    			}
    		}
    	}
    	$scope.updateOrderBestSeller = (type, order) => {
    		for (let i = 0; i < $scope.currentListOrder.length; i++) {
    			if($scope.currentListOrder[i].stt == order.stt){
    				$timeout(() => {
    					if(type== 'plus'){
    						$scope.currentListOrder[i].bestseller++;
    					}else{
    						$scope.currentListOrder[i].bestseller--;
    					}
			    		service.updateOrder({
		        			order: order.stt,
		        			field: 'bestseller',
		        			value: $scope.currentListOrder[i].bestseller
		        		}, res => {});
    				}, 100);
    				break;
    			}
    		}
    	}
    	$scope.updateOrderNewProduct = (type, order) => {
    		for (let i = 0; i < $scope.currentListOrder.length; i++) {
    			if($scope.currentListOrder[i].stt == order.stt){
    				$timeout(() => {
    					if(type== 'plus'){
    						$scope.currentListOrder[i].newproduct++;
    					}else{
    						$scope.currentListOrder[i].newproduct--;
    					}
			    		service.updateOrder({
		        			order: order.stt,
		        			field: 'newproduct',
		        			value: $scope.currentListOrder[i].newproduct
		        		}, res => {});
    				}, 100);
    				break;
    			}
    		}
    	}

    	$scope.getTagsTemplate = (tag, order, index) => {
    		let tags = $scope.parseJSON(tag);
    		if(!tags.length) return '';
    		let template = '';
    		for (var i = 0; i < tags.length; i++) {
    			if(tags[i] == '') continue;
    			template += `<span class="keyInChip" uk-tooltip="`+tags[i]+`">`+tags[i]+`
		        	<span class="deleteKey" ng-click="deleteKeyword('`+tags[i]+`', `+order.stt+`, `+index+`)">
			        	<span uk-icon="icon: close; ratio: 0.7"></span>
		        	</span>
	        	</span>`;
    		}
    		return template;
    	}


    	$scope.getCatsOfOrderTemplate = (cat, code) => {

    		if(cat == null) return '';
    		let tags = cat.split(',');
    		let codes = code.split(',');
    		if(!tags.length) return '';
    		let template = '';
    		for (var i = 0; i < tags.length; i++) {
    			if(tags[i] == '') continue;
    			template += `<span class="keyInChip" uk-tooltip="{{findParents('`+codes[i]+`')}}">`+tags[i]+`</span>`;
    		}
    		return template;
    	}


    	$scope.findParents = (catCode) => {
    		for (let i = 0; i < $scope.listCatLevel.length; i++) {
    			if($scope.listCatLevel[i].code == catCode) return $scope.listCatLevel[i].name;
    			if(!$scope.listCatLevel[i].children.length) continue;
    			for (let j = 0; j < $scope.listCatLevel[i].children.length; j++) {
    				if($scope.listCatLevel[i].children[j].code == catCode) return $scope.listCatLevel[i].name + ' >> ' + $scope.listCatLevel[i].children[j].name;
    				if( !$scope.listCatLevel[i].children[j].children.length ) continue;
    				for (let k = 0; k < $scope.listCatLevel[i].children[j].children.length; k++)	{
    					if($scope.listCatLevel[i].children[j].children[k].code == catCode){
	    					return $scope.listCatLevel[i].name + ' >> ' + 
	    						$scope.listCatLevel[i].children[j].name + ' >> ' + 
	    						$scope.listCatLevel[i].children[j].children[k].name;
    					}
    					if( !$scope.listCatLevel[i].children[j].children[k].children.length ) continue;
    					for (let l = 0; l < $scope.listCatLevel[i].children[j].children[k].children.length; l++)	{
	    					if($scope.listCatLevel[i].children[j].children[k].children[l].code == catCode){
		    					return $scope.listCatLevel[i].name + ' >> ' + 
		    						$scope.listCatLevel[i].children[j].name + ' >> ' + 
		    						$scope.listCatLevel[i].children[j].children[k].name + ' >> ' + 
		    						$scope.listCatLevel[i].children[j].children[k].children[l].name;
	    					}
	    				}
    				}
    			}
    		}
    	}

    	$scope.parseJSON=function(strObj){
    		return strObj.slice(1,-1).replace(/"/g, '').split(",");
		}

    	$scope.onInit();
    }
]);