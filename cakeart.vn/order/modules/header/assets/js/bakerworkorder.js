
    orderApp.service('orderService', ['$http', function ($http) {
        this.getWorkOfOrder = (_order_id, callback) => {
            $.get(domain + 'bakerworkorder/getWorkOfOrder?order_id=' + _order_id, function(data) {
                callback(JSON.parse(data));
            });
        }

        this.deleteTask = (_id, callback) => {
            $.get(domain+'bakerworkorder/deleteTask?id='+_id, function(data) {
                callback(JSON.parse(data));
            });
        }

        this.saveTask = (data, callback) => {
            $.ajax({
                url: domain+'bakerworkorder/saveTask',
                type: 'POST',
                data: data,
                success: function (res) {
                    callback( JSON.parse(res) );
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }

        this.addNewTask = (data, callback) => {
            $.post(domain+'bakerworkorder/addNewTask', data, function (res) {
                callback( JSON.parse(res) );
            });
        }
    }]);

    orderApp.directive("ngUploadChange",function(){
        return{
            scope:{
                ngUploadChange:"&"
            },
            link:function($scope, $element, $attrs){
                $element.on("change",function(event){
                    $scope.$apply(function(){
                        $scope.ngUploadChange({$event: event})
                    })
                })
                $scope.$on("$destroy",function(){
                    $element.off();
                });
            }
        }
    });

    // Groups Management Controller
    orderApp.controller('bakerworkorder-controller', ['$scope', '$sce', '$timeout', '$interval', 'orderService',
        function($scope, $sce, $timeout, $interval, orderService) {
            let service = orderService;
            $scope.listDates = [];
            $scope.listOrder = [];
            $scope.listTasks = [];
            $scope.listUsers = [];
            $scope.isEditting = false;
            $scope.currentTaskID = 0;
            $scope.curOrder = {};
            $scope.tempIMG = '';
            $scope.newTask = {
                congviec: '',
                note: '',
                status: 'not-start',
                nhanvien: 0,
                mahd: 0,
                id: 0
            }
            $scope.showLoading = true;
            let c_date = new Date();
            $scope._date = c_date.getFullYear()  + "-" + (c_date.getMonth()+1) + "-" + c_date.getDate();
            $scope.goToDate = () => {
                $scope.showLoading = true;
                $scope.listOrder = _listOrder;
                $scope.listUsers = _listUser;
                $scope.listOrder = $scope.listOrder.reduce(function (r, a) {
                    r[a.ngaygiao] = r[a.ngaygiao] || [];
                    a.thoigian = new Date(a.thoigian*1000);
                    r[a.ngaygiao].push(a);
                    return r;
                }, Object.create(null));
                $scope.listDates = Object.keys ($scope.listOrder);
                   
                $timeout(function(){
                    $scope.showLoading = false;
                }, 1500);
            }

            $scope.showEditOrderForm = function(order){
                $scope.curOrder = order;
                $scope.newTask.mahd = order.mahd;
                service.getWorkOfOrder(order.mahd, (res) => {
                    $scope.listTasks = [];
                    if(res.status > 0){
                        $timeout(function(){
                            // for (var i = 0; i < res.data.length; i++) {
                                $scope.listTasks = res.data;
                            // }
                        }, 20);
                    }else{
                        UIkit.notification({
                            message: 'Đơn hàng không tồn tại. Vui lòng xem lại',
                            status: 'warning',
                            pos: 'top-right',
                            timeout: 1500
                        }); 
                    }
                })
            }

            $scope.parseJSON=function(strObj){
                return strObj.slice(1,-1).replace(/"/g, '').split(",");
            }

            $scope.toVND = val => { 
                return parseInt(val).toLocaleString();
            }

            $scope.addNewTask = () => {
                if($scope.newTask.congviec == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span>Chưa nhập công việc!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                }
                if($scope.newTask.nhanvien == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span>Chưa chọn nhân viên thực hiện!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                }
                service.addNewTask($scope.newTask, res => {
                    if(res.status){
                        $scope.newTask.id = res.taskID;
                        var new_data = angular.copy($scope.newTask);
                        $scope.listTasks.push(new_data);
                        $scope.newTask.status = 'not-start';
                        $scope.newTask.hinhketqua = '';
                        UIkit.notification({
                            message: '<span uk-icon="check"></span>Thêm công việc thành công!',
                            status: 'success',
                            pos: 'top-right', 

                            timeout: 1500
                        }); 
                        $timeout(function(){
                            $scope.newTask.congviec = '';
                            $scope.newTask.nhanvien = '';
                            $scope.newTask.note = '';
                            $scope.newTask.id = 0;
                        }, 500);
                    }else{
                        UIkit.notification({
                            message: '<span uk-icon="warning"></span>Đã xảy ra sự cố. Vui lòng thử lại!',
                            status: 'warning',
                            pos: 'top-right',
                            timeout: 1500
                        }); 
                    }
                });
            }

            $scope.editTask = task => {
                $scope.isEditting = true;
                $scope.currentTaskID = task.id;
                $scope.tempIMG = task.hinhketqua == '' ? 'http://tinycake.vn/order/images/hinhminhhoa/smallno.jpg' : 'http://cakeart.vn/order/images/productionorder/'+task.hinhketqua;
            }

            $scope.saveTask = task => {
                var file = document.querySelector('input[type=file]').files[0];
                var formData = new FormData();
                if(file != undefined){
                    if (!file.type.match(/image.*/)) {
                        UIkit.notification({
                            message:`<span uk-icon='icon: warning'></span> File bạn chọn không phải hình ảnh.`,
                            status: 'warning',
                            pos: 'top-right',
                            timeout: 1500
                        });
                        event.value = '';
                        document.querySelector('input[type=file]').value = '';
                    } else{
                        formData.append("hinh", file);
                    }
                }
                formData.append('congviec', task.congviec );
                formData.append('nhanvien', task.nhanvien );
                formData.append('note', task.note );
                formData.append('mahd', task.mahd );
                formData.append('hinhketqua', task.hinhketqua );
                formData.append('status', task.status );
                formData.append('id', task.id );
                service.saveTask(formData, function(res){
                    if(file != undefined){
                        if(res.status){
                            for(var j = 0, length2 = $scop.listTasks.length; j < length2; j++){
                                if($scope.listTasks[j].id == task.id){
                                    $timeout(function(){
                                        $scope.listTasks[j].hinhketqua = res.hinh;
                                    }, 1);
                                    break;
                                }
                            }
                        }
                    }
                });
                $scope.isEditting = false;
                $scope.currentTaskID = 0;
                $scope.tempIMG = '';
                
            }

            $scope.deleteTask = (task, e) => {
                e.preventDefault();
                e.stopPropagation();
                UIkit.modal.confirm('Bạn có chắc chắn muốn xóa công việc này!').then(function () {
                    service.deleteTask(task.id, res => {
                        if(res.status){
                            UIkit.notification({
                                message: '<span uk-icon="check"></span>Xóa công việc thành công!',
                                status: 'success',
                                pos: 'top-right',
                                timeout: 1500
                            });
                            $timeout(function(){
                                $scope.listTasks = $scope.listTasks.filter(item => {
                                    return item.id !== task.id;
                                });
                            }, 1);
                        }else{
                            UIkit.notification({
                                message: '<span uk-icon="warning"></span>Đã xảy ra sự cố. Vui lòng thử lại!',
                                status: 'warning',
                                pos: 'top-right',
                                timeout: 1500
                            }); 
                        }
                    })
                    
                }, function () {});
            }

            // $scope.getUser = uid => {
            //     return $scope.listUsers.filter(user => {
            //         return user.manv === uid;
            //     })[0].tendaydu;
            // }
            $scope.getUser = uid => {
                return $scope.listUsers.filter(user => {
                    return user.manv === uid;
                })[0].tendaydu;
            }

            $scope.uploadImage = (event) => {
                var reader, file = document.querySelector('input[type=file]').files[0];
                if (!file.type.match(/image.*/)) {
                    UIkit.notification({
                        message:`<span uk-icon='icon: warning'></span> File bạn chọn không phải hình ảnh.`,
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    });
                    event.value = '';
                    document.querySelector('input[type=file]').value = '';
                } else{
                    if ( window.FileReader ) {
                        reader = new FileReader();
                        reader.onloadend = function (e) { 
                            $timeout(function(){
                                $scope.tempIMG = e.target.result;
                            }, 10);
                        };
                        reader.readAsDataURL(file);
                    }
                    var formData = new FormData();
                    formData.append("hinh", file);
                }
            }

            $scope.goToDate();
        }
    ]);