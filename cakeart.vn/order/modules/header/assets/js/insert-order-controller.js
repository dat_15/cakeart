// Groups Management Controller
addCustomerApp.controller('add-customer-controller', ['$scope', '$rootScope', '$sce', '$timeout', '$compile', '$interval', 'customerService',
    function($scope, $rootScope, $sce, $timeout, $compile, $interval, customerService) {
    	let service = customerService;
    	$scope.suggestionProducts = [];
        $scope.showLoading = false;
    	$scope.keyword = '';
    	let timer = null;
    	$scope.findProductsSuggestion = () => {
    		$scope.showLoading = true;
    		clearTimeout(timer); 
       		timer = setTimeout(function(){
       //          service.findProduct({keyword : $scope.keyword}, res => {
       //              $scope.showLoading = false;
       //              if(res.status && Array.isArray(res.data)){
	    		// 		$timeout(function(){
		    	// 			$scope.suggestionProducts = res.data;
	    		// 		}, 100);
	    		// 	}else{
       //                  $scope.noResult = true;
       //              }
	    		// });
                $timeout(function(){
                    $scope.showLoading = false;
                }, 3000);
            }, 1000);
    	}

        $scope.goToTop = () => {
            $('html, body').animate({
                scrollTop: 50
            }, 'slow');
        };
    }
]);