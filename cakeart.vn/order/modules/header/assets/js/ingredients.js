
    ingredientApp.service('ingredientService', ['$http', function ($http) {
        this.deleteIngredient = (_id, callback) => {
            $.get(domain+'inventory/deleteingredient/'+_id, function(data) {
                callback(JSON.parse(data));
            });
        }

        this.saveIngredient = (data, callback) => {
            $.post(domain+'inventory/saveingredient', data, function (res) {
                callback( JSON.parse(res) );
            });
        }

        this.addNewIngredient = (data, callback) => {
            $.post(domain+'inventory/addnewingredient', data, function (res) {
                callback( JSON.parse(res) );
            });
        }
    }]);

    // Groups Management Controller
    ingredientApp.controller('ingredients-controller', ['$scope', '$sce', '$timeout', '$interval', 'ingredientService',
        function($scope, $sce, $timeout, $interval, ingredientService) {
            let service = ingredientService;
            $scope.listIngredients = _listIngredients;
            $scope.listSuppliers = _listSuppliers;
            $scope.isEditting = false;
            $scope.currentIngredientID = 0;
            $scope.newIngredient = {
                supplier_id: '',
                name: '',
                description: '',
                price: '',
                unit: '',
                inventory: ''
            }
            $scope.showLoading = true;

            $scope.editIngredient = function(ingredient){
                $scope.isEditting = true;
                $scope.currentIngredientID = ingredient.id;
            }

            $scope.addNewIngredient = () => {
                if($scope.newIngredient.supplier_id == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa chọn nhà cung cấp!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                    return;
                }
                if($scope.newIngredient.name == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập tên nguyên liệu!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                    return;
                }
                if($scope.newIngredient.description == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập mô tả nguyên liệu!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                    return;
                }
                if($scope.newIngredient.price == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập giá nguyên liệu!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                    return;
                }
                if($scope.newIngredient.unit == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập đơn vị tính nguyên liệu!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                    return;
                }
                if($scope.newIngredient.inventory == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập tồn kho đầu kỳ!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                    return;
                }
                service.addNewIngredient($scope.newIngredient, res => {
                    if(res.status){
                        $scope.newIngredient.id = res.ingredientID;
                        var new_data = angular.copy($scope.newIngredient);
                        $scope.listIngredients.push(new_data);
                        UIkit.notification({
                            message: '<span uk-icon="check"></span> Thêm nguyên liệu thành công!',
                            status: 'success',
                            pos: 'top-right', 

                            timeout: 1500
                        }); 
                        $timeout(function(){
                            $scope.newIngredient = {
                                supplier_id: 0,
                                name: '',
                                description: '',
                                price: '',
                                unit: '',
                                inventory: ''
                            };
                        }, 500);
                    }else{
                        UIkit.notification({
                            message: '<span uk-icon="warning"></span> '+res.msg,
                            status: 'warning',
                            pos: 'top-right',
                            timeout: 1500
                        }); 
                    }
                });
            }

            $scope.getSupplierName = _id => {
                return $scope.listSuppliers.filter(s => {
                    return s.id = _id;
                })[0].name;
            }

            $scope.saveIngredient = ingredient => {
                if(ingredient.supplier_id == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa chọn nhà cung cấp!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                    return;
                }
                if(ingredient.name == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập tên nguyên liệu!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                    return;
                }
                if(ingredient.description == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập mô tả nguyên liệu!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                    return;
                }
                if(ingredient.price == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập giá nguyên liệu!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                    return;
                }
                if(ingredient.unit == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập đơn vị tính nguyên liệu!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                    return;
                }
                if(ingredient.inventory == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập tồn kho đầu kỳ!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                    return;
                }
                service.saveIngredient(
                    ingredient, 
                    function(res){
                    if(res.status){
                        UIkit.notification({
                            message: '<span uk-icon="warning"></span> Cập nhật thành công!',
                            status: 'success',
                            pos: 'top-right',
                            timeout: 1500
                        }); 
                    }else{
                        UIkit.notification({
                            message: '<span uk-icon="warning"></span> Chưa cập nhật thành công!',
                            status: 'warning',
                            pos: 'top-right',
                            timeout: 1500
                        }); 
                    }
                });
                $scope.isEditting = false;
                $scope.currentIngredientID = 0;
                
            }

            $scope.deleteIngredient = (ingredient, e) => {
                e.preventDefault();
                e.stopPropagation();
                UIkit.modal.confirm('Bạn có chắc chắn muốn xóa nguyên liệu này!').then(function () {
                    service.deleteIngredient(ingredient.id, res => {
                        if(res.status){
                            UIkit.notification({
                                message: '<span uk-icon="check"></span> Xóa nguyên liệu thành công!',
                                status: 'success',
                                pos: 'top-right',
                                timeout: 1500
                            });
                            $timeout(function(){
                                $scope.listIngredients = $scope.listIngredients.filter(item => {
                                    return item.id !== ingredient.id;
                                });
                            }, 1);
                        }else{
                            UIkit.notification({
                                message: '<span uk-icon="warning"></span> Đã xảy ra sự cố. Vui lòng thử lại!',
                                status: 'warning',
                                pos: 'top-right',
                                timeout: 1500
                            }); 
                        }
                    })
                    
                }, function () {});
            }

            $timeout(function(){
                $scope.showLoading = false;
            }, 1000);
        }
    ]);