
    goodsApp.service('goodsService', ['$http', function ($http) {
        this.deleteGoods = (_id, callback) => {
            $.get(domain+'inventory/deletegoods/'+_id, function(data) {
                callback(JSON.parse(data));
            });
        }

        this.saveGoods = (data, callback) => {
            $.post(domain+'inventory/savegoods', data, function (res) {
                callback( JSON.parse(res) );
            });
        }

        this.addNewGoods = (data, callback) => {
            $.post(domain+'inventory/addnewgoods', data, function (res) {
                callback( JSON.parse(res) );
            });
        }
    }]);

    // Groups Management Controller
    goodsApp.controller('goods-controller', ['$scope', '$sce', '$timeout', '$interval', 'goodsService',
        function($scope, $sce, $timeout, $interval, goodsService) {
            let service = goodsService;
            $scope.listIngredients = _listIngredients;
            $scope.listGoods = _listGoods;
            $scope.isEditting = false;
            $scope.currentGoodsID = 0;
            $scope.newGoods = {
                ingredient_id: '',
                name: '',
                quantity: 0,
                price: '',
                delivery_quantity: 0
            }
            $scope.showLoading = true;

            $scope.editGoods = function(goods){
                $scope.isEditting = true;
                $scope.currentGoodsID = goods.id;
            }

            $scope.addNewGoods = () => {
                console.log($scope.newGoods);
                if($scope.newGoods.ingredient_id == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa chọn nguyên liệu!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                    return;
                }
                if($scope.newGoods.name == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập tên mặt hàng!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                    return;
                }
                if($scope.newGoods.quantity == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập số lượng hàng!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                    return;
                }
                if($scope.newGoods.price == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập giá mặt hàng!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                    return;
                }
                if($scope.newGoods.delivery_quantity == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập số lượng nhận!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                    return;
                }
                service.addNewGoods($scope.newGoods, res => {
                    if(res.status){
                        $scope.newGoods._id = res.goodsID;
                        var new_data = angular.copy($scope.newGoods);
                        $scope.listGoods.push(new_data);
                        UIkit.notification({
                            message: '<span uk-icon="check"></span> Thêm nguyên liệu thành công!',
                            status: 'success',
                            pos: 'top-right', 

                            timeout: 1500
                        }); 
                        $timeout(function(){
                            $scope.newGoods = {
                                ingredient_id: '',
                                name: '',
                                quantity: 0,
                                price: '',
                                delivery_quantity: 0
                            };
                        }, 500);
                    }else{
                        UIkit.notification({
                            message: '<span uk-icon="warning"></span> '+res.msg,
                            status: 'warning',
                            pos: 'top-right',
                            timeout: 1500
                        }); 
                    }
                });
            }

            $scope.getIngredientName = _id => {
                var Ing = $scope.listIngredients.filter(s => {
                    return s._id == _id;
                });
                return Ing.length ? Ing[0].name : '';
            }

            $scope.getIngredientPrice = _id => {
                var Ing = $scope.listIngredients.filter(s => {
                    return s._id == _id;
                });
                return Ing.length ? Ing[0].price : 0;
            }

            $scope.saveGoods = goods => {
                if(goods.ingredient_id == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa chọn nguyên liệu!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                    return;
                }
                if(goods.name == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập tên mặt hàng!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                    return;
                }
                if(goods.quantity == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập số lượng hàng!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                    return;
                }
                if(goods.price == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập giá mặt hàng!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                    return;
                }
                if(goods.delivery_quantity == ''){
                    UIkit.notification({
                        message: '<span uk-icon="warning"></span> Chưa nhập số lượng nhận!',
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                    return;
                }
                service.saveGoods(
                    goods, 
                    function(res){
                    if(res.status){
                        UIkit.notification({
                            message: '<span uk-icon="warning"></span> Cập nhật thành công!',
                            status: 'success',
                            pos: 'top-right',
                            timeout: 1500
                        }); 
                    }else{
                        UIkit.notification({
                            message: '<span uk-icon="warning"></span> Chưa cập nhật thành công!',
                            status: 'warning',
                            pos: 'top-right',
                            timeout: 1500
                        }); 
                    }
                });
                $scope.isEditting = false;
                $scope.currentGoodsID = 0;
                
            }

            $scope.deleteGoods = (goods, e) => {
                e.preventDefault();
                e.stopPropagation();
                UIkit.modal.confirm('Bạn có chắc chắn muốn xóa mặt hàng này!').then(function () {
                    service.deleteGoods(goods._id, res => {
                        if(res.status){
                            UIkit.notification({
                                message: '<span uk-icon="check"></span> Xóa mặt hàng thành công!',
                                status: 'success',
                                pos: 'top-right',
                                timeout: 1500
                            });
                            $timeout(function(){
                                $scope.listGoods = $scope.listGoods.filter(item => {
                                    return item.id !== goods.id;
                                });
                            }, 1);
                        }else{
                            UIkit.notification({
                                message: '<span uk-icon="warning"></span> Đã xảy ra sự cố. Vui lòng thử lại!',
                                status: 'warning',
                                pos: 'top-right',
                                timeout: 1500
                            }); 
                        }
                    })
                    
                }, function () {});
            }

            $timeout(function(){
                $scope.showLoading = false;
            }, 1000);
        }
    ]);