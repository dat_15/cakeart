var categoryApp = angular.module('categories-app', []);
let domain = "http://www.cakeart.vn/order/";
// let domain = "http://localhost/excel_reader/";

categoryApp.service('orderService', ['$http', function ($http) {
	this.getAllCat = (callback) => {
		var req = {
		    method: 'GET',
		    url: domain+'api/get_all_danhmucbanh'
		};

		$http(req).then((res) => {
		    callback(res.data);
		}, (err) => {
		    callback({ status: 0, message: 'error' });
		    console.log('getTicketGroupUser error ', err);
		    console.log(err);
		});
	}

	this.addNewNode = (data, callback)=> {
		$.ajax({
			url: domain +'api/add_node',
			type: 'POST',
			data: data,
		})
		.done(function(res) {
			res = JSON.parse(res);
			callback(res);
		});
		
	}

	this.updateNode = (data, callback) => {
		$.ajax({
			url: domain +'api/update_node',
			type: 'POST',
			data: data,
		})
		.done(function(res) {
			res = JSON.parse(res);
			callback(res);
		});
	}

	this.deleteNode = (nodeID, callback) => {
		$.ajax({
			url: domain +'api/remove_node/'+nodeID,
			type: 'DELETE'
		})
		.done(function(res) {
			res = JSON.parse(res);
			callback(res);
		});
	}

}]);
categoryApp.directive('compile', ['$compile', function ($compile) {
    return function(scope, element, attrs) {
      scope.$watch(
        function(scope) {
          return scope.$eval(attrs.compile);
        },
        function(value) {
          element.html(value);
          $compile(element.contents())(scope);
        }
    );
  };
}]);

// Groups Management Controller
categoryApp.controller('categories-management-controller', ['$scope', '$rootScope', '$sce', '$timeout', '$compile', '$interval', 'orderService',
    function($scope, $rootScope, $sce, $timeout, $compile, $interval, orderService) {
    	let service = orderService;
    	$scope.listCatLevel = [];
    	$scope.onInit = () => {
    		service.getAllCat(res => {
    			if(res.status == 1){
    				$scope.listCatLevel = angular.copy(res.data);
    			}
    		});
	    	
			$timeout(function(){
				$scope.showLoading = false;
			}, 1000);
    	}


    	$scope.getCatSelectorTemplate = () => {
    		return `
    			<ul >
		            <li 
		            	class="{{parent.children.length ? 'folder' : 'file'}}" 
		            	ng-repeat="parent in listCatLevel"
		            	ng-click="selecteCat($event)">
		                <span uk-icon="icon: tag" ng-show="!parent.children.length"></span> 
		                <div class="uk-inline" style="width: auto;">
                        	<span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: pencil"></span> 
			            	<input 
			            		class="uk-input uk-form-blank" 
			            		type="text" value="{{parent.name}}"
			            		data-stt="{{parent.id}}" 
			            		ng-keyup="updateOnEnter($event)"
			            	>
			            </div>
		                <div class="uk-align-right">
		                <span uk-icon="icon: plus-circle" ng-click="addNode($event, parent)"></span>
		                	<span uk-icon="icon: close" ng-click="removeNode($event, parent.id)"></span>
		                </div>
		                <ul ng-show="parent.children.length">
		                    <li 
		                    	class="{{child.children.length ? 'folder' : 'file'}}" 
		                    	ng-repeat="child in parent.children"
		                    	ng-click="selecteCat($event)">
		                        <span uk-icon="icon: tag" ng-show="!child.children.length"></span> 
		                        <div class="uk-inline" style="width: auto;">
		                        	<span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: pencil"></span> 
					            	<input 
					            		class="uk-input uk-form-blank" 
					            		type="text" value="{{child.name}}"
					            		data-stt="{{child.id}}" 
					            		ng-keyup="updateOnEnter($event)"
					            	>
					            </div>
		                        <div class="uk-align-right">
		                        	<span uk-icon="icon: plus-circle" ng-click="addNode($event, child)"></span>
				                	<span uk-icon="icon: close" ng-click="removeNode($event, child.id)"></span>
				                </div>
		                        <ul ng-show="child.children.length">
		                            <li class="{{grandChild.children.length ? 'folder' : 'file'}}" ng-repeat="grandChild in child.children" 
		                            ng-click="selecteCat($event)">
		                            	<span uk-icon="icon: tag" ng-show="!grandChild.children.length"></span> 
										<div class="uk-inline" style="width: auto;">
				                        	<span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: pencil"></span> 
							            	<input 
							            		class="uk-input uk-form-blank" 
							            		type="text" value="{{grandChild.name}}"
							            		data-stt="{{grandChild.id}}" 
							            		ng-keyup="updateOnEnter($event)"
							            	>
							            </div>
		                            	<div class="uk-align-right">
						                	<span uk-icon="icon: plus-circle" ng-click="addNode($event, grandChild)"></span>
						                	<span uk-icon="icon: close" ng-click="removeNode($event, grandChild.id)"></span>
						                </div>
						                <ul ng-show="grandChild.children.length">
				                            <li class="file" 
				                            	ng-repeat="superGrandChild in grandChild.children" 
				                            	ng-click="selecteCat($event)">
				                            	<span uk-icon="icon: tag"></span> 
				                            	<div class="uk-inline" style="width: auto;">
						                        	<span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: pencil"></span> 
									            	<input 
									            		class="uk-input uk-form-blank" 
									            		type="text" value="{{superGrandChild.name}}"
									            		data-stt="{{superGrandChild.id}}" 
									            		ng-keyup="updateOnEnter($event)"
									            	>
									            </div>
				                            	<div class="uk-align-right">
								                	<span uk-icon="icon: close" ng-click="removeNode($event, superGrandChild.id)"></span>
								                </div>
				                            </li>
				                        </ul>
		                            </li>
		                        </ul>
		                    </li>		                   
		                </ul>
		            </li>
		        </ul>
    		`;
    	}

    	$scope.addNode = (e, parent) => {
    		service.addNewNode({parent: parent.code}, res => {
    			if(res.status == 1){
					$scope.updateNodeDataOnUI(parent.code, res.data);
					let ul = $(e.target).parents('div.uk-align-right').siblings('ul');
					ul.parent().addClass('folder-open');
    			}

    		});
    	}

    	$scope.updateNodeDataOnUI = (code, data) => {
    		for (let i = 0; i < $scope.listCatLevel.length; i++) {
    			if($scope.listCatLevel[i].code == code){
    				$timeout(function(){
	    				$scope.listCatLevel[i].children.push(data);
    				}, 100);
    				break;
    			} 
    			if(!$scope.listCatLevel[i].children.length) continue;
    			for (let j = 0; j < $scope.listCatLevel[i].children.length; j++) {
    				if($scope.listCatLevel[i].children[j].code == code) {
    					$timeout(function(){
		    				$scope.listCatLevel[i].children[j].children.push(data);
	    				}, 100);
	    				break;
    				}
    				if( !$scope.listCatLevel[i].children[j].children.length ) continue;
    				for (let k = 0; k < $scope.listCatLevel[i].children[j].children.length; k++)	{
    					if($scope.listCatLevel[i].children[j].children[k].code == code){
	    					$timeout(function(){
			    				$scope.listCatLevel[i].children[j].children[k].children.push(data);
		    				}, 100);
		    				break;
    					}
    					if( !$scope.listCatLevel[i].children[j].children[k].children.length ) continue;
    					for (let l = 0; l < $scope.listCatLevel[i].children[j].children[k].children.length; l++)	{
	    					if($scope.listCatLevel[i].children[j].children[k].children[l].code == code){
		    					$timeout(function(){
				    				$scope.listCatLevel[i].children[j].children[k].children[l].children.push(data);
			    				}, 100);
			    				break;
	    					}
	    				}
    				}
    			}
    		}
    	}

    	$scope.updateOnEnter = e => {
    		if(e.keyCode === 13 && e.target.value.trim() !== ''){
    			service.updateNode({
    				id: e.target.dataset['stt'],
    				name: e.target.value.trim()
    			}, res => {
    				if(res.status == 1){
						UIkit.notification({
						    message:`<span uk-icon='icon: check'></span> Cập nhật thành công`,
						    status: 'success',
						    pos: 'top-right',
						    timeout: 1500
						});	
						
	    			}else{
	    				UIkit.notification({
						    message:`<span uk-icon='icon: warning'></span> Chưa cập nhật được `,
						    status: 'warning',
						    pos: 'top-right',
						    timeout: 1500
						});	
	    			}
    			});
    		}
    	}

    	$scope.removeNode = (e, nodeID) => {
    		service.deleteNode(nodeID, res => {
    			if(res.status == 1)
					$scope.removeNodeDataOnUI(nodeID);
				else 
					UIkit.notification({
					    message:`<span uk-icon='icon: warning'></span> Không xóa được node này `,
					    status: 'warning',
					    pos: 'top-right',
					    timeout: 1500
					});	
			});
    	}

    	$scope.removeNodeDataOnUI = (nodeID) => {
    		for (let i = 0; i < $scope.listCatLevel.length; i++) {
    			if($scope.listCatLevel[i].id == nodeID){
    				$timeout(function(){
	    				$scope.listCatLevel.splice(i, 1);
    				}, 100);
    				break;
    			} 
    			if($scope.listCatLevel[i].children == undefined || !$scope.listCatLevel[i].children.length) continue;
    			for (let j = 0; j < $scope.listCatLevel[i].children.length; j++) {
    				if($scope.listCatLevel[i].children[j].id == nodeID) {
    					$timeout(function(){
		    				$scope.listCatLevel[i].children.splice(j, 1);
	    				}, 100);
	    				break;
    				}
    				if($scope.listCatLevel[i].children[j].children == undefined || !$scope.listCatLevel[i].children[j].children.length ) continue;
    				for (let k = 0; k < $scope.listCatLevel[i].children[j].children.length; k++)	{
    					if($scope.listCatLevel[i].children[j].children[k].id == nodeID){
	    					$timeout(function(){
			    				$scope.listCatLevel[i].children[j].children.splice(k, 1);
		    				}, 100);
		    				break;
    					}
    					if( $scope.listCatLevel[i].children[j].children[k].children == undefined || !$scope.listCatLevel[i].children[j].children[k].children.length ) continue;
    					for (let l = 0; l < $scope.listCatLevel[i].children[j].children[k].children.length; l++)	{
	    					if($scope.listCatLevel[i].children[j].children[k].children[l].id == nodeID){
		    					$timeout(function(){
				    				$scope.listCatLevel[i].children[j].children[k].children.splice(l, 1);
			    				}, 100);
			    				break;
	    					}
	    				}
    				}
    			}
    		}
    	}

    	$scope.selecteCat = ($event) => {
    		$event.stopPropagation();
    		let item = $($event.target);
    		if (item.hasClass('folder') ) item.toggleClass('folder-open');
    		
    	}
    	$scope.onInit();
    }
]);