<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $title;?></title>
    <base href="<?php echo base_url.$base_url; ?>">
    <!-- LIB Styles-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Cache-Control" content="private" />
    <meta http-equiv="Expires" content="86400000" />
    <meta http-equiv="Cache-Control" content="max-age=86400000" />
    <link rel="stylesheet" href="assets/lib/uikit/css/uikit.min.css?v=<?php echo time(); ?>" />
    <link rel="stylesheet" href="assets/lib/uikit/css/icon.css?v=<?php echo time(); ?>" />
    <script src="assets/js/jquery.min.js?v=<?php echo time(); ?>" type="text/javascript"></script>

</head>
<body>
