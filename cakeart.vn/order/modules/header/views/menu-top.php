<style>
    nav.uk-navbar-sidebar.uk-margin.uk-navbar ul {
        display: inline-block;
        text-align: left;
        width: 320px;
        overflow-x: hidden;
        height: 100vh;
    }
    nav.uk-navbar-sidebar.uk-margin.uk-navbar {
        width: 320px;
        overflow: hidden;
        height: 100vh;
        position: fixed;
        background: #000;
        color: #fff;
        top: 0;
        left: -320px;
        margin-top: 0px!important;
        overflow-y: auto;
        z-index: +1000;
        -webkit-transition: all .25s linear;
           -moz-transition: all .25s linear;
            -ms-transition: all .25s linear;
             -o-transition: all .25s linear;
                transition: all .25s linear;
    }
    nav.uk-navbar-sidebar.uk-margin.uk-navbar ul li {
        width: 100%;
        text-align: left;
        /* overflow: hidden; */
        padding: 5px;
        display: block;
        height: 40px;
        text-overflow: ellipsis;
        white-space: nowrap;
        position: relative;
    }
    nav.uk-navbar-sidebar.uk-margin.uk-navbar ul li a {
        text-align: left;
        color: #fff;
        width: 300px;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
        display: inline-block;
        height: 40px;
        line-height: 40px;
    }
    nav.uk-navbar-sidebar.uk-margin.uk-navbar ul li div.uk-navbar-dropdown{
        width: 300px;
        left: 5px;
        height: 150px;
        z-index: +1000;
    }
    nav.uk-navbar-sidebar.uk-margin.uk-navbar ul li div.uk-navbar-dropdown ul li a{
        color: #000;
    }
    ::-webkit-scrollbar {
        width: 6px;
        height: 6px;
    }

    ::-webkit-scrollbar-track {
        webkit-box-shadow: inset 0 0 6px #E3EBEF;
        -webkit-border-radius: 10px;
        border-radius: 10px;
        background: #E3EBEF;
    }

    ::-webkit-scrollbar-thumb {
        -webkit-border-radius: 10px;
        border-radius: 10px;
        background: #9FB6C3;
        -webkit-box-shadow: none;
    }

    ::-webkit-scrollbar-thumb:window-inactive {
        background: #9FB6C3;
    }
    #menu-btn{
        position: fixed; 
        top: 10px;
        z-index: +100000;
        left: 10px;
        -webkit-transition: all .25s linear;
           -moz-transition: all .25s linear;
            -ms-transition: all .25s linear;
             -o-transition: all .25s linear;
                transition: all .25s linear;
    }
    #toggle-menu:checked ~ .uk-navbar-sidebar{
        left: 0;
    }
    #toggle-menu:checked ~ label.uk-button#menu-btn[for=toggle-menu]{
        left: 330px;
    }
</style>
<input type="checkbox" id="toggle-menu" class="uk-hidden">
<label class="uk-button uk-button-secondary" id="menu-btn" for="toggle-menu">
    <span uk-icon="menu"></span>
</label>
<nav class="uk-navbar-sidebar uk-margin" uk-navbar>
    <ul class="uk-navbar-nav">
        <li>
            <a href="javascript:void(0)" title="">
                <label for="toggle-menu" style="width: 100%;">
                    <span uk-icon="close" style="width: 100%;"></span>
                </label>
            </a>
        </li>
        <?php foreach ($modules as $item): ?>
            <li class="">
                <a 
                    href="<?php echo DOMAIN.$item['ten_modules']; ?>"  
                    uk-tooltip="<?php echo html_entity_decode($item['mota'], ENT_QUOTES | ENT_XML1, 'UTF-8'); ?>">
                    <?php echo html_entity_decode($item['mota'], ENT_QUOTES | ENT_XML1, 'UTF-8'); ?>
                </a>
            </li>
        <?php endforeach ?>                
        <li style="height: 80px;">
            <a href="<?php echo DOMAIN . 'profile'; ?>">
                <span uk-icon="user"></span>
            </a>
            <div class="uk-navbar-dropdown" uk-dropdown="pos: top-justify">
                <ul class="uk-nav uk-navbar-dropdown-nav">
                    <li class="uk-active">
                        <a href="<?php echo DOMAIN . 'profile'; ?>">
                            Thông tin cá nhân
                        </a>
                    </li>
                    <li><a href="<?php echo DOMAIN.'logout'; ?>">Thoát</a></li>
                </ul>
            </div>
        </li>
    </ul>
</nav>
<?php if(isset($dark_mode)):?>
    <style>
        h1, h2, h3, h4, h5, h6 {
            color: #fff;
        }
        a {
            color: #fff;
        }
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
            background-color: #020e1d;
            color: #fff;
        }
        .uk-table-striped tbody tr:nth-of-type(odd), .uk-table-striped>tr:nth-of-type(odd){
            background-color: #1a2f4a;
        }
        button.uk-button {
            background-color: transparent;
            border: 1px solid;
        }
        .uk-input, .uk-select, .uk-textarea{
            background-color: rgba(0, 0, 0, 0.4);
            color: #e5e5e5;
        }
    </style>

<?php endif;?>