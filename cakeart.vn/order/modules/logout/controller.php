<?php
/**
* index controller
*/
class Logout extends Controller {
	private $session;
	function __construct(){
		parent::__construct(__CLASS__);
		$this->session = new Session();
		$callbackURL = $this->session->get_session('goToUrl');
		// $this->session->delete_session('goToUrl');
		$this->session->delete_session('isLoggedIn');
		$this->session->delete_session('manv');
		$this->session->delete_session('tendangnhap');
		// $this->session->destroy_session();
		$this->session->set_session('goToUrl', $callbackURL);
		header('Location: '. LOGIN_PAGE);
	}

	function index(){
	}
}