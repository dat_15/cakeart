<link rel="stylesheet" href="assets/css/order-management.css?v=<?php echo time();?>" />
<script src="assets/js/categories-selector.js?v=<?php echo time(); ?>" type="text/javascript"></script>
<div class="uk-section" ng-app="categories-app" ng-controller="categories-management-controller">
	<div class="uk-container">
        <div class="uk-card uk-card-default uk-card-body uk-text-left" compile="getCatSelectorTemplate()"></div>
	</div>
</div>
