<?php 

/**
* API go here
*/
class Cats extends Controller{
	private $db;
	private $catTable = 'tiendat';
	// private $catTable = 'test';
	private $cakeTable = 'danhsachbanh';

	private $modules = [];
	function __construct() {
		parent::__construct(__CLASS__);
		$this->db = new DB();
		$this->user_model = $this->load_model('users');
		// Chuyển trang
		if(empty($this->userInfo)) {
			header('Location: '.LOGIN_PAGE);
			die;
		}
		if( !$this->user_model->isAccessibleModule('cats', (int) $this->userInfo['manv']) ) {
			header('Location: '.DOMAIN);
			die;
		}
		$this->modules = $this->user_model->getUserModules((int) $this->userInfo['manv']);
	}

	function index(){
		$this->load_view('main-header', [
			'base_url' => 'modules/header/', 
			'user' => $this->userInfo, 
			'title' => 'Quản lý danh mục sản phẩm'
		], 'header');
		$this->load_view('menu-top',['modules' => $this->modules, 'mod' => 'cats'], 'header');
		$this->load_view('index');
		$this->load_view('footer',[], 'footer');
	}
    
}
