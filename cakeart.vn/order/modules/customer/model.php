<?php 
/**
 * 
 */
class Customer_model extends Model{
	protected $table;
	public function __construct(){
		parent::__construct();
		$this->db->connect_to('tinyorder');
	}

	public function getCustomer($where){
		return $this->db->get_row(CUSTOMER_TABLE, $where);
	}

	public function get_row($table, $where){
		return $this->db->get_row(CUSTOMER_TABLE, $where);
	}

    public function  getAllCustomer($limit, $offset){
        if($offset < 0) $offset = 0;
        $sql = "SELECT kh.makh,kh.tenkh,kh.sdt, kh.diachi,od.ngaygiao, DATE_FORMAT(from_unixtime(od.thoigian), '%d/%m/%Y') as ngaydathang, kh.nhanvien, sum(od.soluong*od.dongia) as doanhthu, count(kh.makh) as solandathang FROM ".CUSTOMER_TABLE." as kh join ".INSERT_ORDER_TABLE." as od on od.makh=kh.makh group by kh.makh ORDER BY kh.makh DESC LIMIT $offset, $limit";
        return $this->db->query($sql);
    }

	public function getCustomers($filter, $limit = PER_PAGE, $offset = 0, $having = []){
		if($offset < 0) $offset = 0;
        $sql = "SELECT kh.makh,kh.tenkh,kh.sdt, kh.diachi,od.ngaygiao, DATE_FORMAT(from_unixtime(od.thoigian), '%d/%m/%Y') as ngaydathang, kh.nhanvien, sum(od.soluong*od.dongia) as doanhthu, count(kh.makh) as solandathang FROM ".CUSTOMER_TABLE." as kh join ".INSERT_ORDER_TABLE." as od on od.makh=kh.makh";
        $where = self::buildFilter($filter);
        if(count($filter)){
            $sql .= ' WHERE ' . $where;
        }
        if(count($having)){
        	$having = "HAVING " . self::buildFilter($having);
        }
        else{
        	$having = '';
        }
        $sql .= " group by kh.makh $having ORDER BY kh.makh DESC LIMIT $offset, $limit";
        // echo $sql;
		return $this->db->query($sql);
	}
    
    private function buildFilter($filter){
        $query = '';
        $total = count($filter);
        $i = 1;
        foreach ($filter as $con) {
            switch ( strtoupper($con['opera']) ){
                case 'LIKE':
                    $query .= ' lower(' . $con['key'] . ') LIKE \'%' . $con['value'] . '%\' ';
                    break;
                case 'EQUAL':
                    $query .= ' (' . $con['key'] . '=\'' . $con['value'] . '\') ';
                    break;
                case 'BETWEEN':
                    $value = explode(' - ', $con['value']);
                    $query .= ' str_to_date(' . $con['key'] . ', \'%d/%m/%Y\') BETWEEN \'' . trim($value[0]) . '\' AND \'' . trim($value[1]) . '\'' ;
                    break;
                case 'BETWEEN-DATE':
                    $value = explode(' - ', $con['value']);
                    $query .= ' ' . $con['key'] . ' >= ' . $value[0] . ' AND ' . $con['key'] . ' <= ' . $value[1] . '' ;
                    break;
                case 'GREATER':
                    $query .= ' (' . $con['key'] . ' >= ' . $con['value'] . ' ) ';
                    break;
                case 'LESS-THAN':
                    $query .= ' (' . $con['key'] . ' <= ' . $con['value'] . ' ) ';
                    break;
                default:
                	continue;
                    break;
            }
            if($i++ < $total) $query .= ' AND ';
        }
        return $query;
    }

	public function countAllCustomers($filter, $having = []){
		$sql = "SELECT COUNT(tmp.makh) as total FROM (SELECT kh.makh,kh.tenkh,kh.sdt, kh.diachi,od.ngaygiao, DATE_FORMAT(from_unixtime(od.thoigian), '%d/%m/%Y') as ngaydathang, kh.nhanvien, sum(od.soluong*od.dongia) as doanhthu FROM ".CUSTOMER_TABLE." as kh join ".INSERT_ORDER_TABLE." as od on od.makh=kh.makh";
        $where = self::buildFilter($filter);
        if (count($filter)) {
            $sql .= ' WHERE ' . $where;
        }
        if(count($having)){
        	$having = "HAVING " . self::buildFilter($having);
        }
        else{
        	$having = '';
        }
        $sql .= " group by kh.makh $having ORDER BY kh.makh DESC) tmp;";
        // echo $sql;die;
        $total = $this->db->query($sql);
		if( count($total) ){
			$total = $total[0]["total"];
		}
		else {
			$total = 0;
		}
		return $total;
	}

    public function addCustomer($data){
        return $this->db->add_row($data, CUSTOMER_TABLE);
    }

    public function getLastCustomer(){
        return $this->db->query("SELECT MAX(makh) as makh FROM ".CUSTOMER_TABLE.";");
    }
	
}
?>