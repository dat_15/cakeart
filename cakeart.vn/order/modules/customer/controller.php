<?php
/**
* index controller
*/
class Customer extends Controller {
	private $session;
	private $model;
	private $modules;
	function __construct(){
		parent::__construct(__CLASS__);
		$this->session = new Session();
		$this->model = $this->load_model(__CLASS__);
		$this->user_model = $this->load_model('users');
		if(empty($this->userInfo)){
			header('Location: ' . LOGIN_PAGE);
			die();
		} 
		if( !$this->user_model->isAccessibleModule('customer', (int) $this->userInfo['manv']) ) {
			header('Location: '.DOMAIN);
			die();
		}
		$this->modules = $this->user_model->getUserModules((int) $this->userInfo['manv']);
	}

	function index($page = 1){
        $filter = [];
        $having = [];
        $makh = input('makh', 'p', '');

        if( empty($makh) && !isset($_POST['makh']) ){
        	$makh = $this->session->get_session('filter_makh');
        }
        $this->session->set_session('filter_makh', $makh);
        if (!empty($makh)) {
	        $filter[] = [
	            'key' => 'kh.makh',
	            'value' => $makh,
	            'opera' => 'EQUAL'
	        ];
        }

        $tenkh = input('tenkh', 'p', '');
        if( empty($tenkh) && !isset($_POST['tenkh']) ){
        	$tenkh = $this->session->get_session('filter_tenkh');
        }
        $this->session->set_session('filter_tenkh', $tenkh);
        if(!empty($tenkh)){
	        $filter[] = [
	            'key' => 'kh.tenkh',
	            'value' => strtolower($tenkh),
	            'opera' => 'LIKE'
	        ];
        }

        $sdt = input('sdt', 'p', '');
        if( empty($sdt) && !isset($_POST['sdt']) ){
        	$sdt = $this->session->get_session('filter_sdt');
        }
        $this->session->set_session('filter_sdt', $sdt);
        if ( !empty($sdt) ) {
	        $filter[] = [
	            'key' => 'kh.sdt',
	            'value' => $sdt,
	            'opera' => 'LIKE'
	        ];
        }
        $diachi = input('diachi', 'p', '');
        if( empty($diachi) && !isset($_POST['diachi']) ){
        	$diachi = $this->session->get_session('filter_diachi');
        }
        $this->session->set_session('filter_diachi', $diachi);
        if ( !empty($diachi) ) {
	        $filter[] = [
	            'key' => 'kh.diachi',
	            'value' => strtolower($diachi),
	            'opera' => 'LIKE'
	        ];
        }
		
		$mindoanhthu = input('mindoanhthu', 'p', '');
        if( empty($mindoanhthu) && !isset($_POST['mindoanhthu']) ){
        	$mindoanhthu = $this->session->get_session('filter_mindoanhthu');
        }
		$this->session->set_session('filter_mindoanhthu', $mindoanhthu);
        if ( !empty($mindoanhthu) ) {
        	$maxdoanhthu = input('maxdoanhthu', 'p', '');
	        if( empty($maxdoanhthu) && !isset($_POST['maxdoanhthu']) ){
	        	$maxdoanhthu = $this->session->get_session('filter_maxdoanhthu');
	        }
	        if($maxdoanhthu < $mindoanhthu){
	        	$this->session->set_session('filter_mindoanhthu', $maxdoanhthu);
				$this->session->set_session('filter_maxdoanhthu', $mindoanhthu);
				$mindoanhthu = $maxdoanhthu;
				$maxdoanhthu = $this->session->get_session('filter_maxdoanhthu');

	        }else{
				$this->session->set_session('filter_maxdoanhthu', $maxdoanhthu);
	        }
	        $having[] = [
	            'key' => 'sum(od.soluong*od.dongia)',
	            'value' => $maxdoanhthu*1000000,
	            'opera' => 'LESS-THAN'
	        ];
	        $having[] = [
	            'key' => 'sum(od.soluong*od.dongia)',
	            'value' => $mindoanhthu*1000000,
	            'opera' => 'GREATER'
	        ];
        }

        $ngaygiao = input('ngaygiao', 'p', '');
        if( empty($ngaygiao) && !isset($_POST['ngaygiao']) ){
        	$ngaygiao = $this->session->get_session('filter_ngaygiao');
        }
        $this->session->set_session('filter_ngaygiao', $ngaygiao);
        if ( !empty($ngaygiao) ) {
        	// $dateTime = DateTime::createFromFormat('d/m/Y', $ngaygiao); 
	        $filter[] = [
	            'key' => 'od.ngaygiao',
	            // 'value' => $dateTime->format('Y-m-d 00:00:00'),
	            'value' => $ngaygiao,
	            'opera' => 'BETWEEN'
	        ];
        }

        $ngaydathang = input('ngaydathang', 'p', '');
        if( empty($ngaydathang) && !isset($_POST['ngaydathang']) ){
        	$ngaydathang = $this->session->get_session('filter_ngaydathang');
        }
        $this->session->set_session('filter_ngaydathang', $ngaydathang);
        if ( !empty($ngaydathang) ) {
        	$value = explode(' - ', $ngaydathang);
	        $filter[] = [
	            'key' => 'od.thoigian',
	            'value' => strtotime($value[0]) . ' - ' . strtotime($value[1]),
	            'opera' => 'BETWEEN-DATE'
	        ];
        }
        
      
		$total = $this->model->countAllCustomers($filter, $having);
		$pagination = new Pagination([
			'total' 		=> $total, 
			'perPage' 		=> PER_PAGE*3,
			'pageToShow' 	=> 4,
			'currentPage' 	=> $page,
			'link' 			=> DOMAIN . 'customer',
		]);
		$page = $pagination->getCurrentPage();
		$this->load_view('main-header', [
			'base_url' 	=> 'modules/header/', 
			'title' 	=> 'Quản lý khách hàng',
			'user' 		=> $this->userInfo
		], 'header');
		$this->load_view('menu-top',['modules' => $this->modules, 'mod' => 'customer'], 'header');
		$this->load_view('index', [
			'customers' => $this->model->getCustomers($filter, PER_PAGE*3, ($page-1)*PER_PAGE*3, $having ),
			'pagination'=> $pagination
		]);
		$this->load_view('footer',[], 'footer');
	}

	// // Chuyển đổi chuỗi tiếng Việt có dấu sang không dấu
	private function cleanString($text) {
        // $text = str_replace('/', '-', $text);
        // $text = str_replace('"', '', $text);
        $text = strtolower($text);
        $utf8 = array(
            '/[áàâãăạảắẳẵằặấầẩẫậ]/u' => 'a',
            '/[íìịĩỉ]/u'             => 'i',
            '/[éèêẹẽếềễệẻể]/u'       => 'e',
            '/[óòôõọỏơờởớợỡồổốộ]/u'  => 'o',
            '/[úùũụủưứừửữự]/u'       => 'u',
            '/[đ]/u'                 => 'd',
            '/–/'                    => '-', // UTF-8 hyphen to "normal" hyphen
            '/[’‘‹›‚]/u'             => '', // Literally a single quote
            '/[“”«»„]/u'             => '' // Double quote
            
        );
        if ($remove_space) {
            $utf8['/ /'] = '-';
        }
        return preg_replace(array_keys($utf8), array_values($utf8), $text);
    }

	public function exportExcel(){
		/** Include PHPExcel */
		require_once dirname(__FILE__) . '/../../libs/PHPExcel.php';
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set document properties
		$objPHPExcel->getProperties()
		 ->setTitle("Office 2007 XLSX Test Document")
		 ->setSubject("Office 2007 XLSX Test Document")
		 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
		 ->setKeywords("office 2007 openxml php")
		 ->setCategory("Test result file");
		// Add some data
		$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'STT')
            ->setCellValue('B1', 'Mã khách hàng')
            ->setCellValue('C1', 'Họ và tên')
            ->setCellValue('D1', 'Số điện thoại')
            ->setCellValue('E1', 'Ngày giao')
            ->setCellValue('F1', 'Ngày đặt')
            ->setCellValue('G1', 'Số đơn')
            ->setCellValue('H1', 'Doanh thu')
            ->setCellValue('I1', 'Địa chỉ');

		// Rename worksheet
		
		$result = $this->model->getAllCustomer( 10000, 0 );
		$index = 2;
		foreach ($result as $kh) {
			$objPHPExcel->setActiveSheetIndex(0)
	            ->setCellValue('A'.$index, $index-1)
	            ->setCellValue('B'.$index, $kh['makh'])
	            ->setCellValue('C'.$index, $kh['tenkh'])
	            ->setCellValue('D'.$index, $kh['sdt'])
	            ->setCellValue('E'.$index, $kh['ngaygiao'])
	            ->setCellValue('F'.$index, $kh['ngaydathang'])
	            ->setCellValue('G'.$index, $kh['solandathang'])
	            ->setCellValue('H'.$index, number_format($kh['doanhthu']).' đ')
	            ->setCellValue('I'.$index, $kh['diachi']);
	        $index++;
		}
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle('Danh sách khách hàng');
		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="khach-hang.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// // If you're serving to IE over SSL, then the following may be needed
		// header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		// header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		// header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		// header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}

}