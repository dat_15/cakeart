<link rel="stylesheet" href="assets/css/users-management.css?v=<?php echo time();?>" />
<script type="text/javascript" src="assets/js/moment.min.js?v=<?php echo time();?>"></script>
<script type="text/javascript" src="assets/js/daterangepicker.min.js?v=<?php echo time();?>"></script>
<link rel="stylesheet" type="text/css" href="assets/css/daterangepicker.css?v=<?php echo time();?>" />
<div class="uk-section" id="user-profile">
	<div class="uk-container">
		<h3>
			<caption>Danh sách khách hàng</caption>
			<a href="<?php echo DOMAIN ?>customer/exportExcel" target="_blank">
				<button class="uk-button uk-button-small  uk-button-primary" type="button">
					Xuất dữ liệu
				</button>
			</a>
			<button href="#toggle-animation" class="uk-button uk-button-small uk-button-primary" type="button" uk-toggle="target: #toggle-animation; animation: uk-animation-fade">
		    	Bộ lọc khách hàng 
		    </button>
		</h3>
		<div hidden id="toggle-animation" class="uk-margin-small-top">
			<form action="" method="POST" accept-charset="utf-8">
		    	<div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid">
				    <div>
					    <div class="uk-inline">
						    <span class="uk-form-icon" uk-icon="icon: hashtag"></span>
				            <input 
				            	type="text" 
				            	class="uk-input" 
				            	value="<?php echo Session::get_session('filter_makh'); ?>" 
				            	name="makh" 
				            	placeholder="Mã khách hàng">
					    </div>
				    </div>
		    		<div>
				    	<div class="uk-inline">
						    <span class="uk-form-icon" uk-icon="icon: users"></span>
						    <input 
						    	class="uk-input" type="text" 
						    	name="tenkh" 
				            	value="<?php echo Session::get_session('filter_tenkh'); ?>" 
						    	placeholder="Họ tên">
					    </div>
				    </div>
				    <div>
					    <div class="uk-inline">
						    <span class="uk-form-icon" uk-icon="icon: receiver"></span>
						    <input 
						    	class="uk-input" type="text" 
						    	name="sdt" placeholder="Số điện thoại" 
						    	value="<?php echo Session::get_session('filter_sdt'); ?>" >
					    </div>
				    </div>
				    <div>
					    <div class="uk-inline">
						    <span class="uk-form-icon" uk-icon="icon: location"></span>
						    <input 
						    	class="uk-input" name="diachi" 
						    	type="text" placeholder="Địa chỉ" 
						    	value="<?php echo Session::get_session('filter_diachi'); ?>">
					    </div>
				    </div>
		    	</div>
			    
				<div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid">
					<div>
				    	<div class="uk-inline">
					    	<span class="uk-form-icon" uk-icon="icon: calendar"></span>
						    <input 
						    	class="uk-input datepicker" 
						    	name="ngaygiao" type="text" 
						    	placeholder="Ngày Giao" 
						    	value="<?php echo Session::get_session('filter_ngaygiao'); ?>" >
				    	</div>
					</div>
					<div>
				    	<div class="uk-inline">
					    	<span class="uk-form-icon" uk-icon="icon: calendar"></span>
						    <input 
						    	class="uk-input datepicker" 
						    	name="ngaydathang" type="text" 
						    	placeholder="Ngày Đặt" 
						    	value="<?php echo Session::get_session('filter_ngaydathang'); ?>" >
				    	</div>
					</div>
					<div>
				    	<div class="uk-inline">
					    	<span class="uk-form-icon" uk-icon="icon: cart"></span>
					    	<div class="input_dual_range">
								<input 
									type="range" 
									class="lower range" 
									name="mindoanhthu" 
									step="1" min="0" 
									max="250" 
									value="<?php echo Session::get_session('filter_mindoanhthu'); ?>"/>
								<input 
									type="range" 
									class="upper range" 
									name="maxdoanhthu" 
									step="1" min="0" 
									max="250" 
									value="<?php echo Session::get_session('filter_maxdoanhthu'); ?>"/>
								<div class="fill"></div>
								<div class="info">
									<p class="iLower">0 tr</p>
									<p class="iUpper">250 tr</p>
								</div>
							</div>
				    	</div>
					</div>
				</div>
				<button class="uk-button uk-button-primary uk-margin-small-top" type="submit">
					Tìm kiếm
				</button>
				<button type="reset" class="uk-button uk-button-default uk-margin-small-top">Xóa dữ liệu</button>

			</form>
		</div>
		<?php echo $pagination->getTemplate(); ?>
	</div>
	<div class="uk-container" id="main-content">
		<table class="uk-table uk-table-divider uk-table-striped uk-table-hover uk-table-justify uk-overflow-auto">
			<thead>
				<tr>
					<th>STT</th>
					<th style="min-width: 10%">Mã KH</th>
					<th>Họ tên</th>
					<th>Số điện thoại</th>
					<th>Ngày giao</th>
					<th>Ngày đặt</th>
					<th>Số đơn</th>
					<th>Doanh Thu</th>
					<th style="width: 20%">Địa chỉ</th>
				</tr>
			</thead>
			<tbody>
				<?php $index = 1; foreach ($customers as $kh): ?>
					<tr>
						<td><?php echo $index++; ?></td>
						<td><?php echo $kh['makh'] ?></td>
						<td><?php echo $kh['tenkh'] ?></td>
						<td><?php echo $kh['sdt'] ?></td>
						<td><?php echo $kh['ngaygiao'] ?></td>
						<td>
							<?php echo $kh['ngaydathang'] ?>
						</td>
						<td>
							<?php echo $kh['solandathang'] ?>
						</td>
						<td>
							<?php echo number_format($kh['doanhthu']); ?> <sup>đ</sup>
						</td>
						<td class="uk-preserve-width">
							<?php echo $kh['diachi'] ?>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
		<?php echo $pagination->getTemplate(); ?>
	</div>
</div>
<script>
	jQuery(document).ready(function($) {
		$('input.datepicker').daterangepicker({
			buttonClasses: 'uk-button uk-button-small uk-button-default',
			applyButtonClasses: 'uk-button-primary',
			autoUpdateInput: false,
			locale: {
				format: 'YYYY-M-DD',
          		cancelLabel: 'Hủy',
          		applyLabel: 'Chọn'
      		}
		}).on('apply.daterangepicker', function(ev, picker) {
		  	$(this).val(picker.startDate.format('YYYY-M-DD') + ' - ' + picker.endDate.format('YYYY-M-DD'));
		});
		$(".upper").on("input", setFill);
		$(".lower").on("input", setFill);

		$(".upper").on("change", setFill);
		$(".lower").on("change", setFill);


		var max = $(".upper").attr("max");
		var min = $(".lower").attr("min");

		function setFill(evt) {
			var valUpper = $(".upper").val();
			var valLower = $(".lower").val();
			if (parseInt(valLower) > parseInt(valUpper)) {
				var trade = valLower;
				valLower = valUpper;
				valUpper = trade;
			}
			
			var width = valUpper * 100 / max;
			var left = valLower * 100 / max;
			$(".fill").css("left", "calc(" + left + "%)");
			$(".fill").css("width", width - left + "%");
			
			// Update info
			if (parseInt(valLower) == min) {
				$(".iLower").html(min + " tr");
			} else {
				$(".iLower").html(parseInt(valLower) + ' tr');
			}
			if (parseInt(valUpper) == max) {
				$(".iUpper").html(max + ' tr');
			} else {
				$(".iUpper").html(parseInt(valUpper) + ' tr');
			}
		}
	});
</script>