<script src="assets/lib/vuejs/vue.min.js?v=<?php echo time(); ?>"></script>
<div class="uk-container" id="manage-user-type">
    <h3 class="uk-margin-medium-top">
        Toàn bộ kiểu nhân viên trên hệ thống
        <button 
            class="uk-button uk-button-primary uk-align-right" 
            v-on:click="showUpAddTypeModal()">
            <span uk-icon="plus"></span> Thêm mới
        </button>
    </h3>
    <table class="uk-table uk-table-hover uk-table-divider">
        <thead>
            <tr>
                <th>Kiểu</th>
                <th>Mô tả</th>
                <th>Trạng thái</th>
                <th>Hành động</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="type in types" v-bind:class="[type.status ? active : deactive]">
                <td>{{type.type}}</td>
                <td>{{type.mota}}</td>
                <td>
                    <span v-if="!type.status">Vô hiệu hóa</span>
                    <span v-if="type.status">Đang hoạt động</span>
                </td>
                <td>
                    <button class="uk-button uk-button-default" type="button">
                        <span uk-icon="settings"></span>
                    </button>
                    <div uk-dropdown="mode: click">
                        <ul class="uk-nav uk-dropdown-nav">
                            <li class="uk-active">
                                <a v-on:click="showUpEditTypeModal(type)">
                                    <span uk-icon="cog"></span> Sửa
                                </a>
                            </li>
                            <li class="uk-active">
                                <a v-on:click="showUpModifierTypeModal(type)">
                                    <span uk-icon="server"></span> Thiết lập quyền
                                </a>
                            </li>
                            <li uk-active>
                                <a v-on:click="showDeleteTypeModal(type)">
                                    <span uk-icon="close" v-show="type.status"></span> 
                                    <span uk-icon="check" v-show="!type.status"></span> 
                                    {{type.status ? 'Vô hiệu hóa' : 'Kích hoạt lại'}}
                                </a>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

    <!-- Editting user type modal -->
    <div id="modal-edit-type" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
            <h2 class="uk-modal-title">Chỉnh sửa kiểu nhân viên {{selected.type}}</h2>
            <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
                <div>
                    <div class="uk-inline">
                        <span class="uk-form-icon" uk-icon="icon: user"></span>
                        <input 
                            class="uk-input" 
                            v-model="selected.type" 
                            type="text" 
                            placeholder="subcriber" 
                            required>
                    </div>
                </div>
            </div>
            <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
                <div>
                    <label class="uk-form-label">Mô tả ngắn </label>
                    <div class="uk-inline">
                        <textarea 
                            rows="5" class="uk-textarea"
                            v-model="selected.mota" 
                            type="text" 
                            placeholder="Người dùng có quyền này ..." 
                            required>
                        </textarea>
                    </div>
                </div>
            </div>
            <p class="uk-text-right">
                <button class="uk-button uk-button-default uk-modal-close" type="button">Hủy</button>
                <button 
                    class="uk-button uk-button-primary" 
                    type="button" v-on:click="updateUserType()"
                    >Lưu lại</button>
            </p>
        </div>
    </div>

    <!-- Modifier user type modal -->
    <div id="modal-modifier-type" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
            <h3 class="uk-modal-title">Sửa quyền truy cập nhóm {{selected.type}}</h3>
            <div class="uk-margin uk-grid-small uk-grid">
                <label class="uk-width-1-2 uk-margin-small-top" v-for="mod in modules">
                    <input 
                        class="uk-checkbox" name="mod" 
                        v-bind:value="mod.ten_modules" 
                        v-bind:checked="mod.id>0" 
                        type="checkbox"> 
                    {{mod.mota}}
                </label>
            </div>

            <p class="uk-text-right">
                <button class="uk-button uk-button-default uk-modal-close" type="button">Hủy</button>
                <button 
                    class="uk-button uk-button-primary" 
                    type="button" v-on:click="updateUserTypePermission()"
                    >Lưu lại</button>
            </p>
        </div>
    </div>

    <!-- Adding user type modal -->
    <div id="modal-add-type" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
            <h2 class="uk-modal-title">Thêm kiểu nhân viên mới</h2>
            <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
                <div>
                    <div class="uk-inline">
                        <span class="uk-form-icon" uk-icon="icon: user"></span>
                        <input 
                            class="uk-input" 
                            v-model="newType.type" 
                            type="text" 
                            placeholder="subcriber" 
                            required>
                    </div>
                </div>
            </div>
            <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
                <div>
                    <label class="uk-form-label">Mô tả ngắn </label>
                    <div class="uk-inline">
                        <textarea 
                            rows="5" class="uk-textarea"
                            v-model="newType.mota" 
                            type="text" 
                            placeholder="Người dùng có quyền này ..." 
                            required>
                        </textarea>
                    </div>
                </div>
            </div>
            <p class="uk-text-right">
                <button class="uk-button uk-button-default uk-modal-close" type="button">Hủy</button>
                <button 
                    class="uk-button uk-button-primary" 
                    type="button" v-on:click="addUserType()"
                    >Lưu lại</button>
            </p>
        </div>
    </div>
</div>
<script>
    var app = new Vue({
        el: '#manage-user-type',
        data:  {
            active: 'uk-alert-success',
            deactive: 'uk-alert-warning',
            modules: [],
            selected: {
                type: '',
                mota: '',
                id: ''
            },
            newType: {
                type: '',
                mota: ''
            },
            types: [
                <?php foreach ($types as $type): ?>
                    { 
                        mota    : `<?php echo $type['mota']; ?>`, 
                        type    : '<?php echo $type['kieunhanvien'] ?>',
                        status  : <?php echo $type['status'] ?>,
                        id      : "<?php echo $type['id'] ?>"
                    },
                <?php endforeach ?>
            ]
        },

        methods: {
            showUpEditTypeModal: function(e){
                this.selected = e;
                UIkit.modal('#modal-edit-type').show();
            },
            showUpModifierTypeModal: function(e){
                this.selected = e;
                this.getTypeAccess(e.type);
                UIkit.modal('#modal-modifier-type').show();
            },
            showUpAddTypeModal: function(){
                UIkit.modal('#modal-add-type').show();
            },
            updateUserType: function(){
                var _this = this;
                if(_this.selected.mota.trim() === '' ){
                    UIkit.notification("<span uk-icon='icon: warning'></span> Bạn chưa nhập mô tả cho kiểu người dùng", {status: 'warning'});
                    return false;
                }
                else if(_this.selected.type.trim() === '' ){
                    UIkit.notification("<span uk-icon='icon: warning'></span> Bạn chưa nhập kiểu người dùng", {status: 'warning'});
                    return false;
                }
                else{
                    $.post(
                        '<?php echo DOMAIN; ?>Phanquyen/updatePermission', 
                        {
                            action  : `update-permission-action`,
                            token   : `<?php echo $token; ?>`,
                            mota    : _this.selected.mota,
                            type    : _this.selected.type,
                            id      : _this.selected.id
                        }, 
                        function(res_data) {
                            res_data = JSON.parse(res_data);
                            if(res_data.status){
                                for(var i = 0; i < _this.types.length; i++){
                                    if(_this.types[i].id === _this.selected.id){
                                        _this.types[i].mota = _this.selected.mota;
                                        _this.types[i].type = _this.selected.type;
                                        break;
                                    }
                                }
                                UIkit.modal('#modal-edit-type').hide();
                            }
                            else{
                                UIkit.notification(
                                    "<span uk-icon='icon: warning'></span> " + res_data.message, 
                                    { status: 'warning' }
                                );
                                return false;
                            }
                        }
                    );
                }

            },
            updateUserTypePermission: function(){
                var _this = this,
                modules = $("input[name=mod]:checked").map( function() { return $(this).val(); } ).get();
                if( !modules.length ){
                    UIkit.notification(
                        "<span uk-icon='icon: warning'></span> Bạn chưa chọn module cho nhóm người dùng", 
                        {status: 'warning'}
                    );
                    return false;
                }
                else{
                    $.post(
                        '<?php echo DOMAIN; ?>Phanquyen/updateTypePermission', 
                        {
                            action  : `update-type-permission-action`,
                            token   : `<?php echo $token; ?>`,
                            type    : _this.selected.type,
                            id      : _this.selected.id,
                            modules : modules
                        }, 
                        function(res_data) {
                            res_data = JSON.parse(res_data);
                            if(res_data.status){
                                UIkit.notification(
                                    "<span uk-icon='icon: check'></span> " + res_data.message, 
                                    { status: 'success', pos : 'top-right' }
                                );
                                UIkit.modal('#modal-modifier-type').hide();
                                return false;
                            }
                            else{
                                UIkit.notification(
                                    "<span uk-icon='icon: warning'></span> " + res_data.message, 
                                    { status: 'warning' }
                                );
                                return false;
                            }
                        }
                    );
                }

            },
            addUserType: function(){
                var _this = this;
                if(_this.newType.mota.trim() === '' ){
                    UIkit.notification("<span uk-icon='icon: warning'></span> Bạn chưa nhập mô tả cho kiểu người dùng", {status: 'warning'});
                    return false;
                }
                else if(_this.newType.type.trim() === '' ){
                    UIkit.notification("<span uk-icon='icon: warning'></span> Bạn chưa nhập kiểu người dùng", {status: 'warning'});
                    return false;
                }
                else{
                    $.post(
                        '<?php echo DOMAIN; ?>Phanquyen/addPermission', 
                        {
                            action  : `add-permission-action`,
                            token   : `<?php echo $token; ?>`,
                            mota    : _this.newType.mota,
                            type    : _this.newType.type
                        }, 
                        function(res_data) {
                            res_data = JSON.parse(res_data);
                            if(res_data.status){
                                _this.types.push({
                                    mota    : _this.newType.mota,
                                    type    : _this.newType.type,
                                    id      : res_data.id
                                })
                                UIkit.modal('#modal-add-type').hide();
                            }
                            else{
                                UIkit.notification(
                                    "<span uk-icon='icon: warning'></span> " + res_data.message, 
                                    { status: 'warning' }
                                );
                                return false;
                            }
                        }
                    );
                }

            },
            showDeleteTypeModal: function(type){
                var _this = this;
                UIkit.modal.confirm(
                    type.status ? 
                        'Bạn có chắc muốn vô hiệu hóa nhóm người dùng này!' :
                        'Bạn có chắc muốn kích hoạt lại nhóm người dùng này!'
                    ).then(function() {
                    $.post(
                        '<?php echo DOMAIN; ?>Phanquyen/deactiveType', 
                        {
                            action  : `deactive-permission-action`,
                            token   : `<?php echo $token; ?>`,
                            mota    : type.mota,
                            type    : type.type,
                            status  : parseInt(type.status) || type.status == true ? 1: 0,
                            id      : type.id
                        }, 
                        function(res_data) {
                            res_data = JSON.parse(res_data);
                            if(res_data.status){
                                for(var i = 0; i < _this.types.length; i++){
                                    if(_this.types[i].id === type.id){
                                        _this.types[i].status = !_this.types[i].status;
                                        break;
                                    }
                                }
                            }
                            else{
                                UIkit.notification(
                                    "<span uk-icon='icon: warning'></span> " + res_data.message, 
                                    { status: 'warning' }
                                );
                                return false;
                            }
                        }
                    );
                }, function () {
                });
            },
            getTypeAccess: function(type_name){
                var _this = this;
                $.get('<?php echo DOMAIN ?>Phanquyen/getTypeAccess/'+type_name, function(res) {
                    var resData = JSON.parse(res);
                    _this.modules = resData.data;
                });
            }
        }
    });
</script>