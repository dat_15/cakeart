<script src="assets/lib/vuejs/vue.min.js?v=<?php echo time(); ?>"></script>
<div class="uk-container" id="manage-user-mods">
    <h3 class="uk-margin-medium-top">
        Quản lý modules trên hệ thống
        <button 
            class="uk-button uk-button-primary uk-align-right" 
            v-on:click="showUpAddModuleModal()">
            <span uk-icon="plus"></span> Thêm mới
        </button>
    </h3>
    <table class="uk-table uk-table-hover uk-table-divider">
        <thead>
            <tr>
                <th>Kiểu</th>
                <th>Mô tả</th>
                <th>Hành động</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="mod in modules">
                <td>{{mod.ten}}</td>
                <td>{{mod.mota}}</td>
                <td>
                    <button class="uk-button uk-button-default" type="button">
                        <span uk-icon="settings"></span>
                    </button>
                    <div uk-dropdown="mode: click">
                        <ul class="uk-nav uk-dropdown-nav">
                            <li class="uk-active">
                                <a v-on:click="showUpEditModuleModal(mod)">
                                    <span uk-icon="cog"></span> Sửa
                                </a>
                            </li>
                            <li uk-active>
                                <a v-on:click="showDeleteModuleModal(mod)">
                                    <span uk-icon="close"></span> 
                                    Xóa module này
                                </a>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

    <!-- Editting user type modal -->
    <div id="modal-edit-module" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
            <h2 class="uk-modal-title">Chỉnh sửa module {{selected.ten}}</h2>
            <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
                <div>
                    <label class="uk-form-label">Mô tả ngắn </label>
                    <div class="uk-inline">
                        <textarea 
                            rows="5" class="uk-textarea"
                            v-model="selected.mota" 
                            type="text" 
                            placeholder="Người dùng có quyền này ..." 
                            required>
                        </textarea>
                    </div>
                </div>
            </div>
            <p class="uk-text-right">
                <button class="uk-button uk-button-default uk-modal-close" type="button">Hủy</button>
                <button 
                    class="uk-button uk-button-primary" 
                    type="button" v-on:click="updateUserMod()"
                    >Lưu lại</button>
            </p>
        </div>
    </div>

    <!-- Adding user type modal -->
    <div id="modal-add-module" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
            <h2 class="uk-modal-title">Thêm module mới</h2>
            <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
                <div>
                    <div class="uk-inline">
                        <select class="uk-select" v-model="newModule.ten" required>
                            <option v-for="dir in dirs" v-bind:value="dir">
                                {{ dir }}
                            </option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
                <div>
                    <label class="uk-form-label">Mô tả ngắn </label>
                    <div class="uk-inline">
                        <textarea 
                            rows="5" class="uk-textarea"
                            v-model="newModule.mota" 
                            type="text" 
                            placeholder="Mô tả chức năng của module ..." 
                            required>
                        </textarea>
                    </div>
                </div>
            </div>
            <p class="uk-text-right">
                <button class="uk-button uk-button-default uk-modal-close" type="button">Hủy</button>
                <button 
                    class="uk-button uk-button-primary" 
                    type="button" v-on:click="addUserModule()"
                    >Lưu lại</button>
            </p>
        </div>
    </div>
</div>
<script>
    var app = new Vue({
        el: '#manage-user-mods',
        data:  {
            selected: {
                ten: '',
                mota: '',
                id: ''
            },
            newModule: {
                ten: '',
                mota: ''
            },
            dirs: [
                <?php foreach ($dirs as $dir): ?>
                    '<?php echo $dir; ?>', 
                <?php endforeach ?>
            ],
            modules: [
                <?php foreach ($modules as $mod): ?>
                    { 
                        mota : `<?php echo $mod['mota']; ?>`, 
                        ten  : '<?php echo $mod['ten_modules'] ?>',
                        id   : "<?php echo $mod['id'] ?>"
                    },
                <?php endforeach ?>
            ]
        },

        methods: {
            showUpEditModuleModal: function(e){
                this.selected = e;
                UIkit.modal('#modal-edit-module').show();
            },
            showUpAddModuleModal: function(){
                UIkit.modal('#modal-add-module').show();
            },
            updateUserMod: function(){
                var _this = this;
                if(_this.selected.mota.trim() === '' ){
                    UIkit.notification("<span uk-icon='icon: warning'></span> Bạn chưa nhập mô tả cho module", {status: 'warning'});
                    return false;
                }
                else{
                    $.post(
                        '<?php echo DOMAIN; ?>Phanquyen/updateMod', 
                        {
                            action  : `update-mod-action`,
                            token   : `<?php echo $token; ?>`,
                            mota    : _this.selected.mota,
                            id      : _this.selected.id
                        }, 
                        function(res_data) {
                            res_data = JSON.parse(res_data);
                            if(res_data.status){
                                for(var i = 0; i < _this.modules.length; i++){
                                    if(_this.modules[i].id === _this.selected.id){
                                        _this.modules[i].mota = _this.selected.mota;
                                        break;
                                    }
                                }
                                UIkit.modal('#modal-edit-module').hide();
                            }
                            else{
                                UIkit.notification(
                                    "<span uk-icon='icon: warning'></span> " + res_data.message, 
                                    { status: 'warning' }
                                );
                                return false;
                            }
                        }
                    );
                }

            },
            addUserModule: function(){
                var _this = this;
                if(_this.newModule.mota.trim() === '' ){
                    UIkit.notification("<span uk-icon='icon: warning'></span> Bạn chưa nhập mô tả cho module", {status: 'warning'});
                    return false;
                }
                else if(_this.newModule.ten.trim() === '' ){
                    UIkit.notification("<span uk-icon='icon: warning'></span> Bạn chưa chọn tên module", {status: 'warning'});
                    return false;
                }
                else{
                    $.post(
                        '<?php echo DOMAIN; ?>Phanquyen/addModule', 
                        {
                            action  : `add-module-action`,
                            token   : `<?php echo $token; ?>`,
                            mota    : _this.newModule.mota,
                            ten     : _this.newModule.ten
                        }, 
                        function(res_data) {
                            res_data = JSON.parse(res_data);
                            if(res_data.status){
                                _this.modules.push({
                                    mota : _this.newModule.mota,
                                    ten  : _this.newModule.ten,
                                    id   : res_data.id
                                });
                                _this.dirs.splice(_this.dirs.indexOf(_this.newModule.ten), 1);
                                UIkit.modal('#modal-add-module').hide();
                            }
                            else{
                                UIkit.notification(
                                    "<span uk-icon='icon: warning'></span> " + res_data.message, 
                                    { status: 'warning' }
                                );
                                return false;
                            }
                        }
                    );
                }

            },
            showDeleteModuleModal: function(mod){
                var _this = this;
                UIkit.modal.confirm(
                        'Bạn có chắc muốn xóa module này!'
                    ).then(function() {
                    $.post(
                        '<?php echo DOMAIN; ?>Phanquyen/deleteModule', 
                        {
                            action  : `delete-module-action`,
                            token   : `<?php echo $token; ?>`,
                            mota    : mod.mota,
                            ten     : mod.ten,
                            id      : mod.id
                        }, 
                        function(res_data) {
                            res_data = JSON.parse(res_data);
                            if(res_data.status){
                                for(var i = 0; i < _this.modules.length; i++){
                                    if(_this.modules[i].id === mod.id){
                                        _this.dirs.push(_this.modules[i].ten);
                                        _this.modules.splice(i, 1);
                                        break;
                                    }
                                }
                            }
                            else{
                                UIkit.notification(
                                    "<span uk-icon='icon: warning'></span> " + res_data.message, 
                                    { status: 'warning' }
                                );
                                return false;
                            }
                        }
                    );
                }, function () {
                });
            }
        }
    });
</script>