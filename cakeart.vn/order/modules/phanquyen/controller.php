<?php
/**
* index controller
*/
class Phanquyen extends Controller {
    private $session;
    private $model;
    private $user_model;
    private $callbackUrl;
    private $modules = [];
    public function __construct()
    {
        parent::__construct(__CLASS__);
        $this->session = new Session();
        $this->model = $this->load_model();
        $this->user_model = $this->load_model('users');
        $this->callbackUrl = $this->session->get_session('goToUrl');
        if (!count($this->userInfo)) {
            header('Location: '.$this->callbackUrl);
            die();
        }
        $this->modules = $this->user_model->getUserModules((int) $this->userInfo['manv']);
    }

    public function index() {
        $error = '';
        $msg = '';

        $types = $this->user_model->getUserType();
        foreach ($types as &$type) {
            $type['id'] = en_id($type['id']);
        }
        $token = md5($this->randomToken(10).time());
        $this->session->set_session('permission-management-token', $token);
        $this->load_view('header-no-angular', [
            'base_url' 	=> 'modules/header/',
            'title' 	=> 'Phân quyền người dùng',
            'user' 		=> $this->userInfo
        ], 'header');
        $this->load_view('menu-top', ['modules' => $this->modules], 'header');
        $this->load_view('index', [
            'user' 		=> $this->userInfo,
            'token'     => $token,
            'types'     => $types
        ]);
        $this->load_view('footer', [], 'footer');
    }

    public function modules() {
        $error = '';
        $msg = '';

        $dirs = array_filter(glob('modules/*'));
        foreach ($dirs as &$dir) {
            $dir = str_replace('modules/', '', $dir);
        }
        $all_modules = $this->user_model->getModuleNames();
        foreach ($all_modules as &$mod) {
            $mod['id'] = en_id($mod['id']);
            if( in_array($mod['ten_modules'], $dirs) ){
                $dirs = array_diff( $dirs, [$mod['ten_modules']] );
            }
        }
        $token = md5($this->randomToken(10).time());
        $this->session->set_session('modules-management-token', $token);
        $this->load_view('header-no-angular', [
            'base_url'  => 'modules/header/',
            'title'     => 'Phân quyền người dùng',
            'user'      => $this->userInfo
        ], 'header');
        $this->load_view('menu-top', ['modules' => $this->modules], 'header');
        $this->load_view('modules', [
            'token'     => $token,
            'dirs'      => $dirs,
            'modules'   => $all_modules
        ]);
        $this->load_view('footer', [], 'footer');
    }

    public function updatePermission(){
        $action = input('action', 'p', '');
        if (empty($action)) {
            send_json([
                'status' => 0,
                'message' => 'Thiếu action'
            ]);
        }
        elseif ($action !== 'update-permission-action') {
            send_json([
                'status' => 0,
                'message' => 'Action không khớp'
            ]);
        }
        $token = input('token', 'p', '');
        if (empty($token)) {
            send_json([
                'status' => 0,
                'message' => 'Thiếu token'
            ]);
        }
        elseif ($token !== $this->session->get_session('permission-management-token') ) {
            send_json([
                'status' => 0,
                'message' => 'Không khớp token'
            ]);
        }
        $mota   = input('mota', 'p', '');
        $type   = input('type', 'p', '');
        $id     = input('id', 'p', '');
        if( empty($mota) || empty($type) || empty($id) ){
            send_json([
                'status' => 0,
                'message' => 'Không được để trống 1 trong 3 trường này'
            ]);
        }
        $user_type = $this->user_model->get_user_type( [ 'id' => de_id($id)] );
        if(count($user_type) == 1){
            $this->user_model->updateUserType(
                [
                    'mota' => $mota,
                    'kieunhanvien' => $type
                ],
                [ 'id' => de_id($id)]
            );
            send_json([
                'status' => 1,
                'message' => 'Cập nhật thành công'
            ]);
        }
        else{
            send_json([
                'status' => 0,
                'message' => 'Thông tin không đúng'
            ]);
        }
    }

    public function updateMod(){
        $action = input('action', 'p', '');
        if (empty($action)) {
            send_json([
                'status' => 0,
                'message' => 'Thiếu action'
            ]);
        }
        elseif ($action !== 'update-mod-action') {
            send_json([
                'status' => 0,
                'message' => 'Action không khớp'
            ]);
        }
        $token = input('token', 'p', '');
        if (empty($token)) {
            send_json([
                'status' => 0,
                'message' => 'Thiếu token'
            ]);
        }
        elseif ($token !== $this->session->get_session('modules-management-token') ) {
            send_json([
                'status' => 0,
                'message' => 'Không khớp token'
            ]);
        }
        $mota   = input('mota', 'p', '');
        $id     = input('id', 'p', '');
        if( empty($mota) || empty($id) ){
            send_json([
                'status' => 0,
                'message' => 'Không được để trống trường này'
            ]);
        }
        $user_type = $this->user_model->get_module( [ 'id' => de_id($id)] );
        if(count($user_type)){
            $this->user_model->update_module( ['mota' => $mota], [ 'id' => de_id($id)] );
            send_json([
                'status' => 1,
                'message' => 'Cập nhật thành công'
            ]);
        }
        else{
            send_json([
                'status' => 0,
                'message' => 'Thông tin không đúng'
            ]);
        }
    }
    

    public function addPermission(){
        $action = input('action', 'p', '');
        if (empty($action)) {
            send_json([
                'status' => 0,
                'message' => 'Thiếu action'
            ]);
        }
        elseif ($action !== 'add-permission-action') {
            send_json([
                'status' => 0,
                'message' => 'Action không khớp'
            ]);
        }
        $token = input('token', 'p', '');
        if (empty($token)) {
            send_json([
                'status' => 0,
                'message' => 'Thiếu token'
            ]);
        }
        elseif ($token !== $this->session->get_session('permission-management-token') ) {
            send_json([
                'status' => 0,
                'message' => 'Không khớp token'
            ]);
        }
        $mota   = input('mota', 'p', '');
        $type   = input('type', 'p', '');
        if( empty($mota) || empty($type) ){
            send_json([
                'status' => 0,
                'message' => 'Không được để trống 1 trong 2 trường này'
            ]);
        }
        $user_type = $this->user_model->get_user_type( [
            'mota' => $mota,
            'kieunhanvien' => $type
        ] );
        if( !count($user_type)){
            $id = $this->user_model->addUserType(
                [
                    'mota' => $mota,
                    'kieunhanvien' => $type
                ]
            );
            if($id){
                send_json([
                    'status' => 1,
                    'message' => 'Thêm thành công',
                    'id' => en_id($id)
                ]);
            }
            else{
                send_json([
                    'status' => 0,
                    'message' => 'Đã có lỗi xảy ra'
                ]);
            }
        }
        else{
            send_json([
                'status' => 0,
                'message' => 'Kiểu nhân viên đã tồn tại'
            ]);
        }
    }

    public function addModule(){
        $action = input('action', 'p', '');
        if (empty($action)) {
            send_json([
                'status' => 0,
                'message' => 'Thiếu action'
            ]);
        }
        elseif ($action !== 'add-module-action') {
            send_json([
                'status' => 0,
                'message' => 'Action không khớp'
            ]);
        }
        $token = input('token', 'p', '');
        if (empty($token)) {
            send_json([
                'status' => 0,
                'message' => 'Thiếu token'
            ]);
        }
        elseif ($token !== $this->session->get_session('modules-management-token') ) {
            send_json([
                'status' => 0,
                'message' => 'Không khớp token'
            ]);
        }
        $mota   = input('mota', 'p', '');
        $ten   = input('ten', 'p', '');
        if( empty($mota) || empty($ten) ){
            send_json([
                'status' => 0,
                'message' => 'Không được để trống 1 trong 2 trường này'
            ]);
        }
        $mod = $this->user_model->get_module( [
            'mota' => $mota,
            'ten' => $ten
        ] );
        if( !count($mod)){
            $id = $this->user_model->addModule(['mota' => $mota, 'ten_modules' => $ten]);
            if($id){
                send_json([
                    'status' => 1,
                    'message' => 'Thêm thành công',
                    'id' => en_id($id)
                ]);
            }
            else{
                send_json([
                    'status' => 0,
                    'message' => 'Đã có lỗi xảy ra'
                ]);
            }
        }
        else{
            send_json([
                'status' => 0,
                'message' => 'Module đã tồn tại'
            ]);
        }
    }

    public function deactiveType(){
        $action = input('action', 'p', '');
        if (empty($action)) {
            send_json([
                'status' => 0,
                'message' => 'Thiếu action'
            ]);
        }
        elseif ($action !== 'deactive-permission-action') {
            send_json([
                'status' => 0,
                'message' => 'Action không khớp'
            ]);
        }
        $token = input('token', 'p', '');
        if (empty($token)) {
            send_json([
                'status' => 0,
                'message' => 'Thiếu token'
            ]);
        }
        elseif ($token !== $this->session->get_session('permission-management-token') ) {
            send_json([
                'status' => 0,
                'message' => 'Không khớp token'
            ]);
        }
        $mota   = input('mota', 'p', '');
        $type   = input('type', 'p', '');
        $id     = input('id', 'p', '');
        $status = input('status', 'p', '');
        if( empty($mota) || empty($type) || empty($id) || !is_int( (int) $status) ){
            send_json([
                'status' => 0,
                'message' => 'Không được để trống 1 trong 4 trường này'. $status
            ]);
        }
        $user_type = $this->user_model->get_user_type( [
            'mota' => $mota,
            'kieunhanvien' => $type,
            'status' => (int) $status,
            'id' => de_id($id)
        ] );
        if( count($user_type) ){
            $this->user_model->updateUserType(
                [
                    'status' => (int) $status ? 0 : 1
                ],
                [ 'id' => de_id($id)]
            );
            send_json([
                'status' => 1,
                'message' => 'Cập nhật thành công'
            ]);
        }
        else{
            send_json([
                'status' => 0,
                'message' => 'Kiểu nhân viên không tồn tại'
            ]);
        }
    }

    public function getTypeAccess($type_name){
        $modules = $this->user_model->getUserModulesByType($type_name);
        $dirs = $this->user_model->getModuleNames();
        $modules_name = array_column( $modules, 'ten_modules' );
        foreach ($dirs as &$dir) {
            if( !in_array($dir['ten_modules'], $modules_name) ){
                $modules[] = [
                    'ten_modules' => $dir['ten_modules'],
                    'id' => 0,
                    'mota' => $dir['mota']
                ];
            }
        }
        send_json([
            'data' => $modules
        ]);
    }

    public function updateTypePermission(){
        $action = input('action', 'p', '');
        if (empty($action)) {
            send_json([
                'status' => 0,
                'message' => 'Thiếu action'
            ]);
        }
        elseif ($action !== 'update-type-permission-action') {
            send_json([
                'status' => 0,
                'message' => 'Action không khớp'
            ]);
        }
        $token = input('token', 'p', '');
        if (empty($token)) {
            send_json([
                'status' => 0,
                'message' => 'Thiếu token'
            ]);
        }
        elseif ($token !== $this->session->get_session('permission-management-token') ) {
            send_json([
                'status' => 0,
                'message' => 'Không khớp token'
            ]);
        }
        $modules   = input('modules', 'p', '');
        $type   = input('type', 'p', '');
        $id     = input('id', 'p', '');
        if( empty($modules) || empty($type) || empty($id) ){
            send_json([
                'status' => 0,
                'message' => 'Không được để trống 1 trong 3 trường này'
            ]);
        }
        $user_type = $this->user_model->get_user_type( [ 'id' => de_id($id), 'kieunhanvien' => $type ] );
        if(count($user_type) == 1){
            $modulesID = array_column( $this->user_model->getModulesID($modules), 'id' );
            $this->user_model->updateUserTypePermission(
                [
                    'accessibleModules' => implode(',', $modulesID)
                ],
                [ 'type' => de_id($id)]
            );
            send_json([
                'status' => 1,
                'message' => 'Cập nhật thành công'
            ]);
        }
        else{
            send_json([
                'status' => 0,
                'message' => 'Thông tin không đúng'
            ]);
        }
    }

    public function deleteModule(){
        $action = input('action', 'p', '');
        if (empty($action)) {
            send_json([
                'status' => 0,
                'message' => 'Thiếu action'
            ]);
        }
        elseif ($action !== 'delete-module-action') {
            send_json([
                'status' => 0,
                'message' => 'Action không khớp'
            ]);
        }
        $token = input('token', 'p', '');
        if (empty($token)) {
            send_json([
                'status' => 0,
                'message' => 'Thiếu token'
            ]);
        }
        elseif ($token !== $this->session->get_session('modules-management-token') ) {
            send_json([
                'status' => 0,
                'message' => 'Không khớp token'
            ]);
        }
        $mota   = input('mota', 'p', '');
        $ten   = input('ten', 'p', '');
        $id     = input('id', 'p', '');
        if( empty($mota) || empty($ten) || empty($id) ){
            send_json([
                'status' => 0,
                'message' => 'Không được để trống 1 trong 3 trường này'
            ]);
        }
        $mod = $this->user_model->get_module( [
            'mota' => $mota,
            'ten_modules' => $ten,
            'id' => de_id($id)
        ] );
        if( count($mod) ){
            $this->user_model->deleteModule([
                'mota' => $mota,
                'ten_modules' => $ten,
                'id' => de_id($id)
            ]);
            send_json([
                'status' => 1,
                'message' => 'Cập nhật thành công'
            ]);
        }
        else{
            send_json([
                'status' => 0,
                'message' => 'Kiểu nhân viên không tồn tại'
            ]);
        }
    }
    
}
