<?php
/**
* index controller
*/
class Login extends Controller {
	private $session;
	private $model;
	function __construct(){
		parent::__construct(__CLASS__);
		$this->session = new Session();
		$this->model = $this->load_model(__CLASS__);
		if(count($this->userInfo)) {
			header('Location: '. Session::get_session('goToUrl'));
			die;
		}
	}

	function index(){
		$error = '';
		$data = [];
		$action = input('action', 'p', '');
		if( !empty($action) ){
			$token = input('token', 'p', '');
			if($action !== 'login-action'){
				$error = 'Action đã bị thay đổi';
			}elseif($token !== $this->session->get_session('token')){
				$error = 'Token không khớp';
			}else{
				$username = input('username', 'p', '');
				$password = input('password', 'p', '');
				if($username === '' || $password === ''){
					$error = 'Không được để trống tên đăng nhập hoặc mật khẩu';
				}
				// elseif(!$this->checkValidUsername($username)){
				// 	$error = 'Tên đăng nhập không đúng định dạng. Chỉ chứa ký tự in hoa, in thường, chữ số và gạch dưới. Dài từ 8-30 ký tự.';
				// }
				// elseif(!$this->checkValidPassword($password)){
				// 	$error = 'Mật khẩu không đúng định dạng. Chứa ít nhất 1 ký tự in hoa, in thường, chữ số. Độ dài từ 8-30 ký tự. Không chứa ký tự đặc biệt ngoài ! @ #';
				// }
				else{
					$user = $this->model->getUser([
						'tendangnhap' => $username,
						'matkhau' => $password
					]);
					if( count($user) ){
						$this->session->set_session('isLoggedIn', true);
						$this->session->set_session('manv', $user['manv']);
						$this->session->set_session('tendangnhap', $user['tendangnhap']);
						header('Location: ' . $this->session->get_session('goToUrl'));
						die;
					}else{
						$error = "Tài khoản không đúng.";
					}
				}
			}
			$data = input();
		}
		$token = md5($this->randomToken(10).time());
		$this->session->set_session('token', $token);
		$this->load_view('index', [
			'base_url' => 'modules/header/', 
			'token' => $token,
			'error' => $error,
			'data'  => $data,
			'msg' 	=> $this->session->get_session('flash-message')
		]);
		$this->session->delete_session('flash-message');
	}
}