<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Đăng nhập</title>
	<base href="<?php echo base_url.$base_url; ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Cache-Control" content="private" />
	<meta http-equiv="Expires" content="86400000" />
	<meta http-equiv="Cache-Control" content="max-age=86400000" />
	<!-- LIB Styles-->
	<link rel="stylesheet" href="assets/lib/uikit/css/uikit.min.css" />
	<style type="text/css" media="screen">
		.uk-cover-container{
		    position: relative;
		    z-index: 1;
		    overflow: hidden;
		}
		.uk-cover-container.uk-text-center:before {
		    content: '';
		    background-repeat: no-repeat;
		    background-position: center center;
		    background-size: cover;
		    background-image: url('assets/img/bg.jpg');
		    width: 120%;
		    height: 120%;
		    display: block;
		    position: fixed;
		    top: 0;
		    left: 0;
		    z-index: 10;
		    overflow: hidden;
		    filter: blur(20px);
		}

		#login-form{
		    z-index: 10000;
		}

		.uk-button-round{
			border-radius: 50%;
			padding: 0;
			width: 45px;
		    height: 45px;
		}

		#login-form .uk-card-body form .submit-segment{
			padding: 0;
			margin: 0;
			width: 100%;
		}

		#login-form .uk-card-body form .submit-segment button{
			float: left;
			padding-left: 10px;
			padding-right: 10px;
		}

		@media only screen and (max-width: 640px) {
		    #login-form .uk-card-media-left {
		        display: none;
		    }

		    #login-form > div {
		        background-color: transparent;
		        box-shadow: none;
		    }
		    #login-form .uk-card-body .uk-card-title{
		    	color: #fff;
		    }
			#login-form form input {
	            height: 45px;
			    background: rgba(228, 228, 228, 0.58);
			    color: #fff;
			    outline: none;
		    }

		    #login-form form span.uk-icon {
			    color: #fff;
		    }
		    #login-form form input:focus{
		    	border-color: #fff;
		    }

		}
	</style>

	<!-- LIB JS -->
	<script src="assets/lib/uikit/js/uikit.min.js"></script>
	<script src="assets/lib/uikit/js/uikit-icons.min.js"></script>
	
</head>
<body>
	<div>
		<div class="uk-cover-container uk-flex uk-text-center" uk-height-viewport>
		    <div class="uk-margin-auto uk-margin-auto-vertical uk-width-1-1@s uk-width-1-2@l uk-width-3-4@m" id="login-form">
				<div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>
				    <div class="uk-card-media-left uk-cover-container">
				        <img src="assets/img/bg-small.jpg" alt="" uk-cover>
				        <canvas width="600" height="400"></canvas>
				    </div>
				    <div>
				        <div class="uk-card-body">
				            <h3 class="uk-card-title">
				            	<span uk-icon="icon: sign-in; ratio: 2"></span> Mời đăng nhập
				            </h3>
				            <?php if ($error !== ''): ?>
				            	<div class="uk-alert-warning uk-padding-small" uk-alert>
				            		<a class="uk-alert-close" uk-close></a>
				            		<span uk-icon="warning"></span> <?php echo $error; ?>
				            	</div>
				            <?php endif ?>
				            <?php if ($msg !== '' && $msg !== NULL): ?>
				            	<div class="uk-alert-success uk-padding-small" uk-alert>
				            		<a class="uk-alert-close" uk-close></a>
				            		<span uk-icon="check"></span> <?php echo $msg; ?>
				            	</div>
				            <?php endif ?>
					    	<form method="POST" action="">
								<input type="hidden" name="token" value="<?php echo $token;?>" required>
				            	<input type="hidden" name="action" value="login-action" required>
							    <div class="uk-margin">
							        <div class="uk-inline uk-width-1-1@s">
							            <span class="uk-form-icon" uk-icon="icon: user"></span>
							            <input 
							            	class="uk-input" 
							            	name="username" 
							            	value="<?php echo isset($data['username']) ? $data['username'] : '' ; ?>" 
							            	type="text" 
							            	pattern="[A-Za-z0-9_]{4,32}" 
							            	title="Tên đăng nhập chỉ chứa số và ký tự. Độ dài từ 4 - 32 ký tự"
							            	placeholder="Tên đăng nhập * "  autocomplete="off"
							            	required >
							        </div>
							    </div>
								<div class="uk-margin">
							        <div class="uk-inline uk-width-1-1@s">
							            <span class="uk-form-icon" uk-icon="icon: lock"></span>
							            <input 
								            class="uk-input" 
								            name="password" 
								            value="<?php echo isset($data['password']) ? $data['password'] : '' ; ?>" 
								            type="password" 
								            placeholder="Mật khẩu * "  autocomplete="off" 
								            required >
							        </div>
							    </div>
								<div class="uk-inline submit-segment">
								    <button class="uk-button uk-width-1-2 uk-button-primary">Đăng nhập</button>
								    <a href="<?php echo REGISTER_PAGE; ?>" class="uk-button uk-width-1-2 uk-button-danger">Đăng ký</a>
								</div>
							</form>
				        </div>
				    </div>
				</div>

		    </div>
		</div>
	</div>
</body>
</html>