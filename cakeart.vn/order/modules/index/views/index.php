<link rel="stylesheet" href="assets/css/order-management.css?v=<?php echo time();?>" />
<script src="assets/js/cakes.js?v=<?php echo time(); ?>" type="text/javascript"></script>
<div class="uk-section" ng-app="categories-app" ng-controller="categories-management-controller">
	<div class="uk-container">
		<form class="">
		    <div class="uk-margin uk-align-right">
		        <div class="uk-inline">
		            <a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: search"></a>
		            <input class="uk-input" type="text" ng-model="keywordToFilter" ng-keyup="filter($event)">
		        </div>
		    </div>

		</form>
	</div>
	<div class="uk-container">
		<ul 
			class="uk-pagination uk-flex-right uk-margin-medium-top" 
			ng-show="allOrder" uk-margin>
			<li>Go to page number <input class="uk-input" type="number" ng-model="currPage" placeholder="Nhập số trang cần đến" ng-keyup="goToPage(currPage)"></li>
		    <li ng-show="currPage > 4" ng-click="goToPage(1)"><a href="">Đầu</a></li>
		    <li ng-show="currPage > 1" ng-click="goToPage(currPage-1)"><a href="">Lùi</a></li>
		    <li ng-show="currPage - 4 > 0" ng-click="goToPage(currPage-4)"><a href="">{{currPage - 4}}</a></li>
		    <li ng-show="currPage - 3 > 0" ng-click="goToPage(currPage-3)"><a href="">{{currPage - 3}}</a></li>
		    <li ng-show="currPage - 2 > 0" ng-click="goToPage(currPage-2)"><a href="">{{currPage - 2}}</a></li>
		    <li ng-show="currPage - 1 > 0" ng-click="goToPage(currPage-1)"><a href="">{{currPage - 1}}</a></li>
		    <li class="uk-active"><span>{{currPage}}</span></li>
		    <li ng-show="currPage + 1 <= totalPage" ng-click="goToPage(currPage+1)"><a href="">{{currPage + 1}}</a></li>
		    <li ng-show="currPage + 2 <= totalPage" ng-click="goToPage(currPage+2)"><a href="">{{currPage + 2}}</a></li>
		    <li ng-show="currPage + 3 <= totalPage" ng-click="goToPage(currPage+3)"><a href="">{{currPage + 3}}</a></li>
		    <li ng-show="currPage + 4 <= totalPage" ng-click="goToPage(currPage+4)"><a href="">{{currPage + 4}}</a></li>
		    <li ng-show="currPage < totalPage - 1" ng-click="goToPage(currPage+1)"><a href="">Tiếp</a></li>
		    <li ng-show="currPage < totalPage - 5" ng-click="goToPage(totalPage)"><a href="">Cuối</a></li>
		</ul>
	</div>
	<div class="uk-container" id="main-table">
		<table id="order-table" class="uk-table uk-table-divider uk-table-striped uk-table-hover">
		    <thead>
		        <tr>
		            <th style="min-width: 150px;">Mã bánh</th>
		            <th style="min-width: 150px;">Tên bánh</th>
		            <th style="min-width: 150px;">Kích thước</th>
		            <th style="min-width: 150px;">Giá</th>
		            <th style="min-width: 150px;">Tiếng Việt</th>
		            <th style="min-width: 150px;">Tiếng Anh</th>
		            <th class="uk-table-expand">Mô tả</th>
		            <th style="min-width: 150px;">Số clap</th>
		            <th style="min-width: 150px;">New Product</th>
		            <th style="min-width: 150px;">Best seller</th>
		            <th class="uk-table-expand">Hình</th>
		            <th class="uk-table-expand">Danh mục</th>
		            <th class="uk-table-expand">Key 1</th>
		            <th class="uk-table-expand">Key 2</th>
		            <th class="uk-table-expand">Key 3</th>
		            <th class="uk-table-expand">Key 4</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr ng-repeat="order in currentListOrder">
		            <td>{{order.mabanh}}</td>
		            <td>{{order.tenbanh}}</td>
		            <td>
						<span class="main-field ">
							{{order.kichthuoc}}
							<i uk-icon="icon: pencil"></i>
						</span>
		            	<div class="uk-inline uk-hidden">
				            <a class="uk-form-icon" uk-icon="icon: pencil"></a>
			            	<input 
			            		class="uk-input uk-form-blank" 
			            		type="text" placeholder="...cm" 
			            		ng-model="order.kichthuoc" 
			            		data-stt="{{order.stt}}" data-type="kichthuoc" 
			            		ng-keyup="updateOnEdited($event, order, 'kichthuoc')"
			            	>
			            </div>
		            </td>
		            <td>
		            	<span class="main-field ">
							{{order.gia}}
							<i uk-icon="icon: pencil"></i>
						</span>
		            	<div class="uk-inline uk-hidden">
				            <a class="uk-form-icon" uk-icon="icon: pencil"></a>
			            	<input 
			            		class="uk-input uk-form-blank" 
			            		type="text" placeholder="$ 10.000" 
			            		ng-model="order.gia" 
			            		data-stt="{{order.stt}}" data-type="gia" 
			            		ng-keyup="updateOnEdited($event, order, 'gia')"
			            	>
			            </div>
		            </td>
		            <td>
		            	<span class="main-field ">
							{{order.tiengviet}}
							<i uk-icon="icon: pencil"></i>
						</span>
		            	<div class="uk-inline uk-hidden">
				            <a class="uk-form-icon" uk-icon="icon: pencil"></a>
			            	<input 
			            		class="uk-input uk-form-blank" 
			            		type="text" placeholder="Tiếng Việt" 
			            		ng-model="order.tiengviet" 
			            		data-stt="{{order.stt}}" data-type="tiengviet" 
			            		ng-keyup="updateOnEdited($event, order, 'tiengviet')"
			            	>
			            </div>
		            </td>
		            <td>
		            	<span class="main-field ">
							{{order.tienganh}}
							<i uk-icon="icon: pencil"></i>
						</span>
		            	<div class="uk-inline uk-hidden">
				            <a class="uk-form-icon" uk-icon="icon: pencil"></a>
			            	<input 
			            		class="uk-input uk-form-blank" 
			            		type="text" placeholder="Tiếng Anh" 
			            		ng-model="order.tienganh" 
			            		data-stt="{{order.stt}}" data-type="tienganh" 
			            		ng-keyup="updateOnEdited($event, order, 'tienganh')"
			            	>
			            </div>
		            </td>
		            <td>
		            	<span class="main-field ">
							{{order.mota}}
							<i uk-icon="icon: pencil"></i>
						</span>
		            	<div class="uk-inline uk-hidden">
			            	<textarea class="uk-textarea" ng-keyup="updateOnEdited($event, order, 'mota')" rows="5" data-stt="{{order.stt}}" data-type="mota" ng-model="order.mota" placeholder="Mô tả về danh mục">{{order.mota}}</textarea>
			            </div>
		            </td>
		            <td>
		            	<div class="uk-inline uk-text-center">
		            		<i uk-icon="icon: plus-circle" style="cursor: pointer; margin-right: 20px" ng-click="updateOrderClap('plus', order)"></i>
			            	<strong class="h4">
								{{order.clap}}
							</strong>
		            		<i uk-icon="icon: minus-circle" style="cursor: pointer; margin-left: 20px;" ng-click="updateOrderClap('minus', order)"></i>
		            	</div>
		            </td>
		            <td>
		            	<div class="uk-inline uk-text-center">
		            		<i uk-icon="icon: plus-circle" style="cursor: pointer; margin-right: 20px" ng-click="updateOrderNewProduct('plus', order)"></i>
			            	<strong class="h4">
								{{order.newproduct}}
							</strong>
		            		<i uk-icon="icon: minus-circle" style="cursor: pointer; margin-left: 20px;" ng-click="updateOrderNewProduct('minus', order)"></i>
		            	</div>
		            </td>
		            <td>
		            	<div class="uk-inline uk-text-center">
		            		<i uk-icon="icon: plus-circle" style="cursor: pointer; margin-right: 20px" ng-click="updateOrderBestSeller('plus', order)"></i>
			            	<strong class="h4">
								{{order.bestseller}}
							</strong>
		            		<i uk-icon="icon: minus-circle" style="cursor: pointer; margin-left: 20px;" ng-click="updateOrderBestSeller('minus', order)"></i>
		            	</div>
		            </td>
		            <td>
		            	<img class="order-image" ng-src="{{'http://www.tinycake.vn/order/thuvienbanh/'+order.hinh}}" alt="{{order.tenbanh}}" width="100">
		            </td>
		            <td>
		            	<p ng-show="order.catName != ''" compile="getCatsOfOrderTemplate(order.catName, order.catCode)"></p>
						<div class="uk-inline">
						    <button class="uk-button uk-button-default" type="button">Chọn</button>
						    <div uk-dropdown="mode: click" compile="getCatSelectorTemplate(order.stt)">
						        
						    </div>
						</div>
					</td>
					<td>
						<div class="uk-inline input-tag">
				            <a class="uk-form-icon" uk-icon="icon: tag"></a>
			            	<input 
			            		class="uk-input uk-form-blank" 
			            		type="text" placeholder="từ khóa" 
			            		ng-keyup="updateTagsOnEdited($event, order, 1)"
			            	>
			            </div>
			        	<div compile="getTagsTemplate(order.keyword1, order, 1)"></div>
			        </td>
					<td>
						<div class="uk-inline input-tag">
				            <a class="uk-form-icon" uk-icon="icon: tag"></a>
			            	<input 
			            		class="uk-input uk-form-blank" 
			            		type="text" placeholder="từ khóa" 
			            		ng-keyup="updateTagsOnEdited($event, order, 2)"
			            	>
			            </div>
			        	<div compile="getTagsTemplate(order.keyword2, order, 2)"></div>
					</td>
					<td>
						<div class="uk-inline input-tag">
				            <a class="uk-form-icon" uk-icon="icon: tag"></a>
			            	<input 
			            		class="uk-input uk-form-blank" 
			            		type="text" placeholder="từ khóa" 
			            		ng-keyup="updateTagsOnEdited($event, order, 3)"
			            	>
			            </div>
			            <div compile="getTagsTemplate(order.keyword3, order, 3)"></div>
					</td>
					<td>
						<div class="uk-inline input-tag">
				            <a class="uk-form-icon" uk-icon="icon: tag"></a>
			            	<input 
			            		class="uk-input uk-form-blank" 
			            		type="text" placeholder="từ khóa" 
			            		ng-keyup="updateTagsOnEdited($event, order, 4)"
			            	>
			            </div>
			        	<div compile="getTagsTemplate(order.keyword4, order, 4)"></div>
					</td>
		        </tr>
		    </tbody>
		</table>
	</div>
	<div class="uk-container">
		<ul 
			class="uk-pagination uk-flex-right uk-margin-medium-top" 
			ng-show="allOrder" uk-margin>
		    <li ng-show="currPage > 4" ng-click="goToPage(1)"><a href="">Đầu</a></li>
		    <li ng-show="currPage > 1" ng-click="goToPage(currPage-1)"><a href="">Lùi</a></li>
		    <li ng-show="currPage - 4 > 0" ng-click="goToPage(currPage-4)"><a href="">{{currPage - 4}}</a></li>
		    <li ng-show="currPage - 3 > 0" ng-click="goToPage(currPage-3)"><a href="">{{currPage - 3}}</a></li>
		    <li ng-show="currPage - 2 > 0" ng-click="goToPage(currPage-2)"><a href="">{{currPage - 2}}</a></li>
		    <li ng-show="currPage - 1 > 0" ng-click="goToPage(currPage-1)"><a href="">{{currPage - 1}}</a></li>
		    <li class="uk-active"><span>{{currPage}}</span></li>
		    <li ng-show="currPage + 1 < totalPage" ng-click="goToPage(currPage+1)"><a href="">{{currPage + 1}}</a></li>
		    <li ng-show="currPage + 2 < totalPage" ng-click="goToPage(currPage+2)"><a href="">{{currPage + 2}}</a></li>
		    <li ng-show="currPage + 3 < totalPage" ng-click="goToPage(currPage+3)"><a href="">{{currPage + 3}}</a></li>
		    <li ng-show="currPage + 4 < totalPage" ng-click="goToPage(currPage+4)"><a href="">{{currPage + 4}}</a></li>
		    <li ng-show="currPage < totalPage - 1" ng-click="goToPage(currPage+1)"><a href="">Tiếp</a></li>
		    <li ng-show="currPage < totalPage - 5" ng-click="goToPage(totalPage)"><a href="">Cuối</a></li>
		</ul>
	</div>
</div>
