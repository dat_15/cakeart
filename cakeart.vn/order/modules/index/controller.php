<?php
/**
* index controller
*/
class Index extends Controller {
	private $modules;
	function __construct(){
		parent::__construct();
		Session::set_session('goToUrl', DOMAIN);
		$this->user_model = $this->load_model('users');
		if(empty($this->userInfo)) header('Location: '.LOGIN_PAGE);
		$this->modules = $this->user_model->getUserModules((int) $this->userInfo['manv']);
	}

	function index(){
		$this->load_view('main-header', [
			'base_url' => 'modules/header/', 
			'user' => $this->userInfo, 
			'title' => 'Quản lý danh sách bánh',
			'page' => 'index'
		], 'header');
		$this->load_view('menu-top', [
			'modules' 	=> $this->modules, 
			'mod' 		=> 'index'
		], 'header');
		$this->load_view('index', ['base_url' => 'modules/index/']);
		$this->load_view('footer',[], 'footer');
	}

}