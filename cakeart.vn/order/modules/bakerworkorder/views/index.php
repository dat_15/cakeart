<script type="text/javascript" src="assets/js/moment.min.js?v=1528388700"></script>
<script type="text/javascript" src="assets/js/daterangepicker.min.js?v=1528388700"></script>
<link rel="stylesheet" type="text/css" href="assets/css/daterangepicker.css?v=1528388700" />
<style>
    td ul li::before {
      content: "- ";
    }
    td ul li {
        display: inline;
        list-style: square;
        padding: 1px;
    }
    td ul li b {
        color: saddlebrown;
    }
    .header {
        text-align: center;
        font-size: 22px;
    }
    .uk-lightbox{
        background-color: rgba(102, 102, 102, 0.6);
    }
    thead {
        background-color: #333;
        color: #fff;
        font-weight: bold;
    }
    th {
        color: #fff !important;
        font-weight: bold !important;
        font-size: 12px !important;
    }
    *+.uk-grid-margin-small, .uk-grid+.uk-grid-small, .uk-grid-small>.uk-grid-margin{
        margin-top: 0;
    }
    .loading{
        width: 100%;
        height: 100vh;
        margin: 0 auto;
        text-align: center;
        float: none;
        display: block;
        z-index: +1000;
        position: absolute;
        background: #fff;
        padding-top: 30%;
    }
    .uk-padding-small{padding: 5px!important;}
    .input-editting {
        border: 0;
        height: 30px!important;
        background: transparent;
        border-bottom: 1px dashed;
    }
    table.uk-table.uk-table-divider.uk-table-hover.uk-table-striped.uk-table-justify.uk-table-middle{
        width: 100%;
        overflow-x: auto;
    }
</style>
<script type="text/javascript">
    let _listOrder = [];
    <?php foreach ($orders as $order): ?>
        _listOrder.push(<?php echo json_encode($order);?>);
    <?php endforeach ?>
    let _listUser = [];
    <?php foreach ($users as $user): ?>
        _listUser.push(<?php echo json_encode($user);?>);
    <?php endforeach ?>
    var orderApp = angular.module('order-app', []);
    let domain = "<?php echo DOMAIN; ?>";

</script>

<script type="text/javascript" src="assets/js/bakerworkorder.js?v=<?= time()?>"></script>

<div class="uk-section" ng-app="order-app" ng-controller="bakerworkorder-controller">
    <form class="uk-container uk-form uk-margin-bottom" action="" method="POST">
        <div class="uk-grid">
            <div class="uk-width-3-5">
                <input class="uk-input go-to-datepicker" name="from" value="<?= $from; ?>" type="text" >
            </div>
            <div class="uk-width-2-5">
                <button class="uk-button" type="submit">
                    Đi tới ngày này
                </button>
            </div>
        </div>
    </form>
    <div>
        <div class="loading" uk-spinner="ratio: 3" ng-show="showLoading"></div>
        <div ng-show="listDates.length" ng-repeat="dat in listDates">
            
            <div class='header'>Đơn hàng ngày {{dat}}</div>
            <table class="uk-table uk-table-divider uk-table-hover uk-table-striped uk-table-justify uk-table-middle ">
                <thead>
                    <th width="4%">Giờ/Tên/Mã ĐH</th>
                    <th width="10%">Loại bánh</th>
                    <th width="4%">Kích Thước / Chiều Cao</th>
                    <th width="40%">Thông tin</th>
                    <th width="30%">Kết quả sản xuất</th>
                    <th width="4%">Giờ nhập / Dữ liệu</th>
                    <th width="4%">Minh họa</th>
                    <th width="4%">Kết quả/QL</th>
                </thead>
                <tbody>
                    <tr ng-repeat="order in listOrder[dat]" >
                        <td>{{order.giogiao}}<br>{{order.tenKH}}<br>{{order.mahd}}</td>
                        <td>{{order.loaibanh}}</td>
                        <td>{{order.kichthuoc}} <p ng-show="order.chieucao !=''">Cao: {{order.chieucao}}</p></td>
                        <td>
                            <ul>
                                <li ng-show="order.soluong > 0">Số lượng: <b>{{order.soluong}} cái</b></li>
                                <li ng-show="order.mota != ''">Mô tả: <b>{{order.mota}}</b></li>
                                <li ng-show="order.mausac != ''">Màu: <b>{{order.mausac}}</b></li>
                                <li ng-show="order.nhanbanh != ''">Nhân: <b>{{order.nhanbanh}}</b></li>
                                <li ng-show="order.cotbanh != ''">Cốt: <b>{{order.cotbanh}}</b></li>
                                <li ng-show="order.sotang > 0"><b>{{order.sotang}} tầng</b></li>
                                <li ng-show="order.diachi != ''">Địa chỉ: <b>{{order.diachi}}</b></li>
                                <li ng-show="order.chutrenbanh != ''">Chữ trên bánh: <b>{{order.chutrenbanh}}</b></li>
                            </ul>
                        </td>
                        <td>
                            <a href="http://cakeart.vn/order/images/productionorder/{{hinh}}" 
                                ng-repeat="hinh in getResultImage(order.mahd)">
                                <img width="40px" height="40px" ng-src="http://cakeart.vn/order/images/productionorder/{{hinh}}">
                            </a>
                        </td>
                        <td>{{ order.thoigian | date:'HH:mm, d/M/y' }}
                            <a ng-show="order.nguyenlieu!=''" href="http://tinycake.vn/order/images/album/{{order.mahd}}" target="_blank">Xem</a>
                        </td>
                        <td>
                            <div uk-lightbox>
                                <a href="http://tinycake.vn/order/images/hinhminhhoa/small{{order.hinhminhhoa}}"  data-caption="{{order.loaibanh}}">
                                    <img width="40px" height="40px" ng-src="http://tinycake.vn/order/images/hinhminhhoa/small{{order.hinhminhhoa}}">
                                </a>
                            </div>
                            
                        </td>
                        <td>
                            <div uk-lightbox>
                                <a
                                    ng-href="order.hinhketqua !== 'no.jpg' ? 'http://tinycake.vn/order/thuvienbanh/' + order.hinhketqua : 'http://tinycake.vn/order/images/hinhminhhoa/smallno.jpg'" 
                                    data-caption="{{order.loaibanh}}"
                                >
                                    <img ng-show="order.hinhketqua !== 'no.jpg'" width="40px" height="40px" ng-src="http://tinycake.vn/order/thuvienbanh/{{order.hinhketqua}}">
                                    <img ng-show="order.hinhketqua === 'no.jpg'" width="40px" height="40px" src="http://tinycake.vn/order/images/hinhminhhoa/smallno.jpg">
                                </a>
                            </div>
                            <button uk-icon="icon: pencil" 
                                uk-toggle="target: #edit-order" 
                                type="button" 
                                ng-click="showEditOrderForm(order)">
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>


        <!-- This is the modal -->
        <div id="edit-order" class="uk-modal-full" uk-modal data-uk-modal="{bgclose:false, modal: false, keyboard: false}">
            <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical uk-height-1-1" uk-overflow-auto>
                <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
                <h3 class="uk-modal-title uk-text-center">
                    Thông tin đơn hàng của {{curOrder.tenKH}}
                </h3>
                <h4 class="uk-text-center"></h4>
                    <div class="uk-width-3-4@xl uk-width-3-4@l uk-width-1-1 uk-align-center">
                        <table class="uk-table uk-table-divider uk-table-hover uk-table-striped uk-table-justify uk-table-middle ">
                         <thead>
                            <th width="150">Giờ/Tên/MãĐH</th>
                            <th width="150">Loại bánh</th>
                            <th width="50">Kích Thước / Chiều Cao</th>
                            <th width="400">Thông tin</th>
                            <th width="300">Kết quả sản xuất</th>
                            <th width="50">Giờ nhập / Dữ liệu</th>
                            <th width="60">Minh họa</th>
                            <th width="60">Kết quả</th>
                        </thead>
                        <tbody>
                            <tr >
                                <td>{{curOrder.giogiao}}<br>{{curOrder.tenKH}}<br>{{curOrder.mahd}}</td>
                                <td>{{curOrder.loaibanh}}</td>
                                <td>{{curOrder.kichthuoc}} <p ng-show="curOrder.chieucao !=''">Cao: {{curOrder.chieucao}}</p></td>
                                <td>
                                    <ul>
                                        <li ng-show="curOrder.soluong > 0">Số lượng: <b>{{curOrder.soluong}} cái</b></li>
                                        <li ng-show="curOrder.mota != ''">Mô tả: <b>{{curOrder.mota}}</b></li>
                                        <li ng-show="curOrder.mausac != ''">Màu: <b>{{curOrder.mausac}}</b></li>
                                        <li ng-show="curOrder.nhanbanh != ''">Nhân: <b>{{curOrder.nhanbanh}}</b></li>
                                        <li ng-show="curOrder.cotbanh != ''">Cốt: <b>{{curOrder.cotbanh}}</b></li>
                                        <li ng-show="curOrder.sotang > 0"><b>{{curOrder.sotang}} tầng</b></li>
                                        <li ng-show="curOrder.diachi != ''">Địa chỉ: <b>{{curOrder.diachi}}</b></li>
                                        <li ng-show="curOrder.chutrenbanh != ''">Chữ trên bánh: <b>{{curOrder.chutrenbanh}}</b></li>
                                    </ul>
                                </td>
                                <td></td>
                                <td>
                                    {{ curOrder.thoigian | date:'H:m, d-M-y' }}
                                    <a ng-show="curOrder.nguyenlieu!=''" href="http://tinycake.vn/order/images/album/{{curOrder.mahd}}" target="_blank">Xem</a>
                                </td>
                                <td>
                                    <div uk-lightbox>
                                        <a href="http://tinycake.vn/order/images/hinhminhhoa/small{{curOrder.hinhminhhoa}}"  data-caption="{{curOrder.loaibanh}}">
                                            <img width="40px" height="40px" ng-src="http://tinycake.vn/order/images/hinhminhhoa/small{{curOrder.hinhminhhoa}}">
                                        </a>
                                    </div>
                                    
                                </td>
                                <td>
                                    <div uk-lightbox>
                                        <a
                                            ng-href="curOrder.hinhketqua !== 'no.jpg' ? 'http://tinycake.vn/order/thuvienbanh/' + curOrder.hinhketqua : 'http://tinycake.vn/order/images/hinhminhhoa/smallno.jpg'" 
                                            data-caption="{{curOrder.loaibanh}}"
                                        >
                                            <img ng-show="curOrder.hinhketqua !== 'no.jpg'" width="40px" height="40px" ng-src="http://tinycake.vn/order/thuvienbanh/{{curOrder.hinhketqua}}">
                                            <img ng-show="curOrder.hinhketqua === 'no.jpg'" width="40px" height="40px" src="http://tinycake.vn/order/images/hinhminhhoa/smallno.jpg">
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                    
                <div class="uk-width-3-4@xl uk-width-3-4@l uk-width-1-1 uk-align-center">
                    <table class="uk-table uk-table-striped uk-table-hover uk-table-divider">
                        <thead>
                            <th>Công việc</th>
                            <th>Ghi chú cho công việc</th>
                            <th>Người làm</th>
                            <th>Trạng thái</th>
                            <th>Ảnh kết quả sản xuất</th>
                            <!-- <th>Time</th> -->
                            <th>Hành động</th>
                        </thead>
                        <tbody>
                            <!-- Danh sách công việc -->
                            <tr ng-repeat="task in listTasks">
                                <td>
                                    <span ng-show="!isEditting || currentTaskID !== task.id">{{task.congviec}}</span>
                                    <input type="text" class="uk-input input-editting" ng-model="task.congviec" ng-show="isEditting && currentTaskID == task.id">
                                </td>
                                <td>
                                    <span ng-show="!isEditting || currentTaskID !== task.id">{{task.note}}</span>
                                    <input type="text" class="uk-input input-editting" ng-model="task.note" ng-show="isEditting && currentTaskID == task.id">
                                </td>
                                <td>
                                    <span ng-show="!isEditting || currentTaskID !== task.id">{{getUser(task.nhanvien)}}</span>
                                    <select type="text" class="uk-select input-editting" ng-model="task.nhanvien"  ng-show="isEditting && currentTaskID == task.id">
                                        <option disabled="disabled" value="0">Chọn người thực hiện</option>
                                        <option ng-repeat="nhanvien in listUsers" ng-value="nhanvien.manv" ng-selected="task.nhanvien === nhanvien.manv">
                                            {{nhanvien.tendaydu}}
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <div ng-show="!isEditting || currentTaskID !== task.id">
                                        <span class="uk-label uk-padding-small" ng-show="task.status == 'working'">
                                            <span uk-icon="paint-bucket"></span> 
                                            Đang làm việc
                                        </span>

                                        <span class="uk-label uk-padding-small uk-label-success" ng-show="task.status == 'done'">
                                            <span uk-icon="happy"></span> 
                                            Đã hoàn thành
                                        </span>

                                        <span class="uk-label uk-padding-small uk-label-warning" ng-show="task.status == 'not-start'">
                                            <span uk-icon="warning"></span> 
                                            Chưa bắt đầu
                                        </span>

                                    </div>
                                    <select type="text" class="uk-select input-editting" ng-model="task.status" ng-show="isEditting && currentTaskID == task.id">
                                        <option disabled="disabled">Chọn trạng thái công việc</option>
                                        <option ng-selected="task.status == 'not-start'" value="not-start">
                                            Chưa bắt đầu
                                        </option>
                                        <option ng-selected="task.status == 'working'" value="working">
                                            Đang làm việc
                                        </option>
                                        <option ng-selected="task.status == 'done'" value="done">
                                            Đã hoàn thành
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <div uk-lightbox ng-show="!isEditting || currentTaskID !== task.id">
                                        <a href="{{task.hinhketqua != '' ? 'http://cakeart.vn/order/images/productionorder/' + task.hinhketqua : 'http://tinycake.vn/order/images/hinhminhhoa/smallno.jpg'}}" 
                                        >
                                            <img ng-show="task.hinhketqua !== ''" width="40px" height="40px" ng-src="http://cakeart.vn/order/images/productionorder/{{task.hinhketqua}}">
                                            <img ng-show="task.hinhketqua === ''" width="40px" height="40px" src="http://tinycake.vn/order/images/hinhminhhoa/smallno.jpg">
                                        </a>
                                    </div>
                                    <label for="upload-hinhketqua" ng-show="isEditting && currentTaskID == task.id">
                                        <span uk-icon="cloud-upload"></span>
                                    </label>
                                    <img ng-src="{{tempIMG}}" style="width: 50px; height: 50px; border-radius: 3px; margin-left: 20px" ng-show="isEditting && currentTaskID == task.id">
                                    
                                </td>
                                <!-- <td>
                                    {{ task.time | date:'yyyy-MM-dd HH:mm:ss'}}
                                </td> -->
                                <td>
                                    <button class="uk-button uk-button-small" 
                                        ng-click="editTask(task)" 
                                        ng-show="!isEditting || currentTaskID !== task.id">
                                        <span uk-icon="pencil"></span>
                                    </button>
                                    <button class="uk-button uk-button-small" 
                                        ng-click="saveTask(task)" 
                                        ng-show="isEditting && currentTaskID == task.id">
                                        <span uk-icon="check"></span>
                                    </button>
                                    <button class="uk-button uk-button-small" 
                                        ng-click="deleteTask(task, $event)" 
                                        ng-show="!isEditting || currentTaskID !== task.id">
                                        <span uk-icon="trash"></span>
                                    </button>
                                </td>
                            </tr>
                            <input type="file" class="uk-hidden" id="upload-hinhketqua" ng-upload-change="uploadImage($event)">
                            <!-- Form thêm mới công việc -->
                            <tr>
                                <td>
                                    <input type="text" class="uk-input" ng-model="newTask.congviec" placeholder="Mô tả công việc">
                                </td>
                                <td>
                                    <input type="text" class="uk-input" ng-model="newTask.note" placeholder="Ghi chú cho công việc">
                                </td>
                                <td>
                                    <select type="text" class="uk-select" ng-model="newTask.nhanvien" >
                                        <option disabled="disabled" value="0" selected="selected">Chọn người thực hiện</option>
                                        <option ng-repeat="nhanvien in listUsers" ng-value="nhanvien.manv">
                                            {{nhanvien.tendaydu}}
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <span class="uk-label uk-padding-small uk-label-warning">Chưa bắt đầu</span>
                                </td>
                                <td>
                                    <span uk-icon="cloud-upload"></span>
                                </td>
                                <td>
                                    <button class="uk-button" ng-click="addNewTask()">Thêm</button>
                                </td>
                            </tr>

                            
                            <!-- Chưa phân công -->
                            <tr class="uk-text-muted uk-text-center" ng-show="!listTasks.length">
                                <td colspan="6">
                                    <h3>
                                        Chưa phân công cho nhân viên
                                    </h3>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <h3 class="uk-text-muted uk-text-center" ng-show="!listDates.length"><i>Chưa có đơn hàng nào</i></h3>
    </div>
</div>
<script>
    window.onload = function(){
        
        $('input.go-to-datepicker').daterangepicker({
            buttonClasses: 'uk-button uk-button-small uk-button-default',
            applyButtonClasses: 'uk-button-primary',
            autoUpdateInput: true,
            timePicker24Hour: false,
            timePicker: false,
            singleDatePicker: true,
            locale: {
                format: 'YYYY-M-DD',
                cancelLabel: 'Hủy',
                applyLabel: 'Chọn'
            }
        });
    }
</script>