<?php
class bakerworkorder extends Controller {
    private $session;
    private $model;
    private $user_model;
    private $callbackUrl;
    private $modules = [];
    public function __construct() {
        parent::__construct(__CLASS__);
        $this->session = new Session();
        $this->model = $this->load_model();
        $this->user_model = $this->load_model('users');
        $this->callbackUrl = $this->session->get_session('goToUrl');
        if ($this->callbackUrl == '' || $this->callbackUrl == null) {
            $this->callbackUrl = DOMAIN;
        }
        if (!count($this->userInfo)) {
            header('Location: '.$this->callbackUrl);
        }
        if( !$this->user_model->isAccessibleModule('orders_input', (int) $this->userInfo['manv']) ) header('Location: '.DOMAIN);
        $this->modules = $this->user_model->getUserModules((int) $this->userInfo['manv']);
    }


    public function index() {
        $from = input('from', 'gpc', 'NOW');
        $nextweek = new DateTime($from);
        $from = new DateTime($from);
        $nextweek->add(new DateInterval('P7D'));
        $res = $this->model->getOrderByRang($from->format('Y-m-d'), $nextweek->format('Y-m-d'));
        $this->load_view('main-header', [
            'base_url'  => 'modules/header/',
            'title'     => 'Quản lý đơn hàng từ ngày '.$from->format('Y-m-d') . ' đến ' . $nextweek->format('Y-m-d'),
            'user'      => $this->userInfo
        ], 'header');
        $this->load_view('menu-top', ['modules' => $this->modules], 'header');
        $this->load_view(
            'index', 
            [
                'orders' => $res, 
                'from' => $from->format('Y-m-d'),
                'users' => $this->user_model->getUsersActive(100, 0, "manv,tendaydu"),
            ]
        );
        $this->load_view('footer', [], 'footer');
    }

    public function getWorkOfOrder(){
        $order_id = (int) input('order_id', '', 0);
        if ($order_id < 1) {
            echo json_encode([
                'status' => 0,
                'msg' => 'Order not found!'
            ]);
            die();
        }
        $order = $this->model->getOrderByID($order_id);
        if ($order === true || !count($order)) {
            echo json_encode([
            'status' => 0,
            'msg' => 'Order not found!'
            ]);
        }else{
            $schedue = $this->model->getWorkOfOrder($order_id);
            echo json_encode([
                'status' => count($schedue) ? 2 : 1,
                'data'  => $schedue,
                'msg'   => count($schedue) ? 'success' : 'Schedue not found'
            ]);
        }
    }

    public function addNewTask(){
        $data = input();
        $data['created_by'] = (int) $this->userInfo['manv'];
        $data['status2'] = 'not-start';
        $data['hinhketqua'] = '';
        unset($data['id']);
        $taskID = $this->model->addNewTaskToDB($data);
        if($taskID){
            echo json_encode([
                'status' => 1,
                'taskID' => $taskID
            ]);
        }else{
            echo json_encode([
                'status' => 0,
                'msg' => 'Failed'
            ]);
        }
    }

    public function deleteTask(){
        $id = input('id', '', 0);
        if(!$id){
            echo json_encode([
                'status' => 0,
                'msg' => 'No param id!'
            ]);
            die();
        }
        $this->model->deleteTask(['id' => $id]);
        echo json_encode([
            'status' => 1,
            'msg' => 'Xóa thành công!'
        ]);
        die();
    }

    public function saveTask(){
        $data = input();
        $this->model->updateTask(
            [
                'congviec' => $data['congviec'],
                'note'     => $data['note'],
                'nhanvien' => (int) $data['nhanvien'],
                'status'  => $data['status']
            ],
            [ 'id' => (int) $data['id'] ]
        );
        $hinh = '';
        if(!empty($_FILES) && isset($_FILES['hinh']) && $_FILES['hinh']['error'] == 0 ){
            $hinh = self::uploadImage((int) $data['nhanvien'], (int) $data['mahd'], (int) $data['id']);
        }
        echo json_encode([
            'status'=> 1,
            'msg'   => 'Cập nhật thành công!',
            'hinh'  => $hinh
        ]);
        die();
    }

    private function uploadImage($nhanvien, $mahd, $taskID){
        $file = $_FILES['hinh']['tmp_name']; 
        $sourceProperties = getimagesize($file);
        $fileNewName = $mahd.'-'.$nhanvien.'-'.time();
        $folderPath = __DIR__.'/../../images/productionorder/';
        $ext = pathinfo($_FILES['hinh']['name'], PATHINFO_EXTENSION);
        $imageType = $sourceProperties[2];
        switch ($imageType) {

            case IMAGETYPE_PNG:
                $imageResourceId = imagecreatefrompng($file); 
                $file = self::imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                imagepng($file,$folderPath. $fileNewName. ".". $ext);
                break;

            case IMAGETYPE_GIF:
                $imageResourceId = imagecreatefromgif($file); 
                $file = self::imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                imagegif($file,$folderPath. $fileNewName. ".". $ext);
                break;

            case IMAGETYPE_JPEG:
                $imageResourceId = imagecreatefromjpeg($file); 
                $file = self::imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                imagejpeg($file,$folderPath. $fileNewName. ".". $ext);
                break;

            default:
                exit;
                break;
        }
        move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
        $this->model->updateTask(
            [
                'hinhketqua' =>  $fileNewName. ".". $ext
            ],
            [ 'id' => (int) $taskID ]
        );
        return $fileNewName. ".". $ext;
    }

    private function imageResize($imageResourceId,$width,$height) {
        $targetWidth =700;
        $targetHeight =700;
        $file=imagecreatetruecolor($targetWidth,$targetHeight);
        imagecopyresampled($file,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $width,$height);
        return $file;
    }
}

 ?>
