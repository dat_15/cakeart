<?php 

class sales_orders_model extends Model{
	public function __construct(){
		parent::__construct();
		$this->db->connect_to('tinyorder');
	}

    public function getOrderByRang($from, $to){
        $sql = "SELECT HD.*,KH.tenKH, KH.sdt  FROM `nhaphoadon` as HD INNER JOIN khachhang as KH ON KH.makh= HD.makh WHERE DATE_FORMAT(STR_TO_DATE(HD.ngaygiao, '%d/%m/%Y'), '%Y-%m-%d') BETWEEN '" . $from . "' AND '" . $to . "' ORDER BY DATE_FORMAT(STR_TO_DATE(HD.ngaygiao, '%d/%m/%Y'), '%Y-%m-%d'), HD.giogiao ASC;";
        return $this->db->query($sql);
    }

    public function update_order( $data, $where ){
    	return $this->db->update( INSERT_ORDER_TABLE, $data, $where);
    }

    public function addOrder($data){
        //echo "in add Order function";
        return $this->db->add_row($data, ORDER_TABLE);
    }

    public function get_order($where){
        return $this->db->get_row(ORDER_TABLE, $where);
    }

    public function get_products($where){
        return $this->db->get_rows(INSERT_ORDER_TABLE, $where, 10 );
    }

    public function addProduct($data) {
        return $this->db->add_row($data, INSERT_ORDER_TABLE);
    }

    public function addOrderDetail($data){
        return $this->db->add_row($data, ORDER_DETAIL_TABLE);
    }

    public function get_order_detail($order_id){
        return $this->db->query("SELECT * FROM " .ORDER_DETAIL_TABLE . "  JOIN " . INSERT_ORDER_TABLE . " ON " .ORDER_DETAIL_TABLE . ".product_id=" . INSERT_ORDER_TABLE . ".mahd WHERE " .ORDER_DETAIL_TABLE . ".order_id=". (int) $order_id);
    }

    public function get_last_ID(){
        $sql = "SELECT MAX(mahd) AS last_id FROM `nhaphoadon`";
        $last_id = $this->db->query($sql);
        if(is_array($last_id)){
            return (int) $last_id[0]['last_id'];
        }
        return 0;
    }
}
 ?>