<script type="text/javascript" src="assets/js/moment.min.js?v=<?php echo time();?>"></script>
<script type="text/javascript" src="assets/js/daterangepicker.min.js?v=<?php echo time();?>"></script>
<link rel="stylesheet" type="text/css" href="assets/css/daterangepicker.css?v=<?php echo time();?>" />
<link rel="stylesheet" type="text/css" href="assets/css/insert-order.css?v=<?php echo time();?>" />
<!-- Custom JS -->
<script src="assets/js/customer-service.js?v=<?php echo time();?>" type="text/javascript"></script>
<script src="assets/js/customer-controller.js?v=<?php echo time();?>" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB4FlkxMf7mAN8vg3t12jfc-BW3Liie1t0&libraries=places&callback=initMap&language=vi&region=VI" async defer></script>
<div class="uk-section" ng-app="add-customer-app" ng-controller="add-customer-controller">
    <div class="uk-container">
        <h3 class="uk-heading-line uk-text-center"><span>{{showAddingForm ? 'Thêm đơn hàng' : 'Tìm kiếm khách hàng' }}</span></h3>
        <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
            <div>
                <div class="uk-inline" ng-show="!showAddingForm">
                    <span class="uk-form-icon" uk-icon="icon: search"></span>
                    <input class="uk-input"  ng-keyup="findCustomerSuggestion()" ng-model="keyword" type="text" placeholder="Họ tên | SĐT | Địa chỉ">
                </div>
            </div>
        </div>
        <form ng-show="showAddingForm" action="<?= base_url.'api/add_new_customer' ?>" method="GET"> 
            <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
                <div>
                    <label class="uk-form-label" for="form-stacked-text">Tên khách hàng (*):</label>
                    <div class="uk-inline">
                        <span class="uk-form-icon" uk-icon="icon: user"></span>
                        <input class="uk-input" name='tenkh' type="text" placeholder="Nguyễn Văn A" required>
                    </div>
                </div>
                <div>
                    <label class="uk-form-label" for="form-stacked-text">Số điện thoại (*): </label>
                    <div class="uk-inline">
                        <span class="uk-form-icon" uk-icon="icon: receiver"></span>
                        <input class="uk-input" ng-model="customer.sdt" name="sdt" type="tel" placeholder="0123 456 7890" ng-class="sdtClass" ng-keyup="find_customer_by_key('sdt')" required>
                    </div>
                </div>
                <div>
                    <label class="uk-form-label" for="form-stacked-text">Địa chỉ:</label>
                    <div class="uk-inline">
                        <input id="pac-input" class="uk-input" name="diachi" ng-model="customer.diachi" type="text"
                            placeholder="Giao hàng đến địa chỉ" required>
                        <span class="uk-form-icon" uk-icon="icon: location"></span>
                        <span id="check-ok-button">
                            <span uk-icon="check"></span>
                        </span>
                        <input type="hidden" class="lat_lng" name="lat_lng">
                    </div>
                </div>
            </div>
            <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
                <div>
                    <label class="uk-form-label" for="form-stacked-text">Email (*): </label>
                    <div class="uk-inline">
                        <span class="uk-form-icon" uk-icon="icon: mail"></span>
                        <input class="uk-input" ng-model="customer.email" name="email" placeholder="example@email.com" ng-class="emailClass" ng-keyup="find_customer_by_key('email')" required>
                    </div>
                </div>
                <div>
                    <label class="uk-form-label" for="form-stacked-text">Mạng xã hội: </label>
                    <div class="uk-inline">
                        <span class="uk-form-icon" uk-icon="icon: facebook"></span>
                        <input class="uk-input" ng-model="customer.fb" name="fb" type="text" placeholder="Facebook/Skype">
                    </div>
                </div>
                <div>
                    <label class="uk-form-label" for="form-stacked-text">Ngày sinh: </label>
                    <div class="uk-inline">
                        <span class="uk-form-icon" uk-icon="icon: calendar"></span>
                        <input class="uk-input" ng-model="customer.ngaysinh" name="ngaysinh" type="text" placeholder="1/1/1990">
                    </div>
                </div>
            </div>
            <div hidden id="toggle-animation" class="uk-margin-small-top">
                <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid">
                    <!-- <div>
                        <label class="uk-form-label" for="form-stacked-text">Ảnh đại diện: </label>
                        <div class="uk-inline uk-margin-small-top">
                            <span class="uk-form-icon" uk-icon="icon: camera"></span>
                            <input type="file" class="uk-input" placeholder="Chọn ảnh đại diện">
                        </div>
                    </div> -->
                    <div>
                        <label class="uk-form-label" for="form-stacked-text">Tên người nhận: </label>
                        <div class="uk-inline uk-margin-small-top">
                            <span class="uk-form-icon" uk-icon="icon: user"></span>
                            <input class="uk-input" ng-model="customer.nguoinhan" name="nguoinhan" type="text" placeholder="Nguyễn Văn B">
                        </div>
                    </div>
                    <div>
                        <label class="uk-form-label" for="form-stacked-text">Số điện thoại người nhận: </label>
                        <div class="uk-inline uk-margin-small-top">
                            <span class="uk-form-icon" uk-icon="icon: receiver"></span>
                            <input class="uk-input" ng-model="customer.stdnguoinhan" name="stdnguoinhan" type="tel" placeholder="0987 654 321">
                        </div>
                    </div>
                </div>
                
                <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid">
                    <label class="uk-form-label" for="form-stacked-text">Ghi chú: </label>
                    <div class="uk-inline">
                        <textarea class="uk-textarea" ng-model="customer.ghichu" name="ghichu" rows="5" placeholder="Ghi chú thêm: Số điện thoại khác, Email khác, ...">{{customer.ghichu}}</textarea>
                    </div>
                </div>
            </div>
            <button href="#toggle-animation" class="uk-button uk-button-primary uk-margin-small-top uk-margin-small-bottom uk-padding-remove-vertical uk-padding-small" type="button" uk-toggle="target: #toggle-animation; animation: uk-animation-fade">
                <span uk-icon="icon: plus-circle"></span>
                Thông tin khác 
            </button>
            <button class="uk-button uk-button-primary uk-margin-small-top uk-margin-small-bottom uk-padding-remove-vertical uk-padding-small" type="submit">
                Thêm khách hàng 
            </button>
            <div class="map-wrapper">
                <div id="map"></div>
            </div>
        </form>


        <!-- Loading phần gợi ý -->
        <div ng-show="showLoading" class="uk-margin-medium-top">
            <div uk-spinner></div>
        </div>  

        <!-- Hiển thị thông tin gợi ý -->
        <div id="suggestion-wrapper" ng-if="suggestionCustomers.length && showAddingForm" uk-grid>
            <div class="uk-width-1-1 uk-animation-slide-bottom-small" ng-repeat="customer in suggestionCustomers">
                <!-- Hiển thị chi tiết gợi ý -->
                <div class="uk-padding-remove suggestion-customer">
                    <!-- Header -->
                    <span class="customername" uk-tooltip="title: {{customer.tenkh}}">{{customer.tenkh}}</span>
                    <span class="customer-phone">
                        <span uk-icon="icon: receiver"></span> 
                        <i>{{customer.sdt == '' ? 'Chưa có thông tin' : customer.sdt}}</i>
                    </span>
                    <span class="customer-address" uk-tooltip="title: {{customer.diachi}}">
                        <span uk-icon="icon: location; ratio: 0.7"></span> 
                        <i>{{customer.diachi == '' ? 'Chưa có thông tin' : customer.diachi}}</i>
                    </span>
                    <span class="select-customer" ng-click="selectCustomer(customer)">
                        Chọn
                    </span>
                </div>
            </div>
        </div>
        <div class="uk-width-1-1 uk-animation-slide-bottom-small uk-text-center uk-margin-small-top" ng-show="!suggestionCustomers.length && noResult && !showAddingForm">
            <h4>Không có kết quả phù hợp. </h4>
            <button class="uk-button uk-button-danger" ng-click="showAddingForm = !showAddingForm">
                <i uk-icon="plus-circle"></i>
                Tạo mới người dùng
            </button>
        </div>
    </div>
</div>
    
<script>
    window.onload = function(){
        $('input.go-to-datepicker').daterangepicker({
            buttonClasses: 'uk-button uk-button-small uk-button-default',
            applyButtonClasses: 'uk-button-primary',
            autoUpdateInput: true,
            timePicker24Hour: false,
            timePicker: false,
            singleDatePicker: true,
            locale: {
                format: 'YYYY-M-DD',
                cancelLabel: 'Hủy',
                applyLabel: 'Chọn'
            }
        });
        var width = window.innerWidth;
        if(width < 500) { // Mobiles devices
            $("#pac-input").focusin(function() {
                $("#pac-input").parent().addClass('active-address-input-on-mobile');
                $('.map-wrapper').addClass('active-address-map-on-mobile');
            });
            $("#check-ok-button").click(function() {
                $("#pac-input").parent().removeClass('active-address-input-on-mobile');
                $('.map-wrapper').removeClass('active-address-map-on-mobile');
            });
        }
    }
</script>