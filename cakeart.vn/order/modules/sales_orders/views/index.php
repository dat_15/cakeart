<script type="text/javascript" src="assets/js/moment.min.js?v=1528388700"></script>
<script type="text/javascript" src="assets/js/daterangepicker.min.js?v=1528388700"></script>
<link rel="stylesheet" type="text/css" href="assets/css/daterangepicker.css?v=1528388700" />
<style>
    .header {
        text-align: center;
        font-size: 22px;
    }
    .uk-lightbox{
        background-color: rgba(102, 102, 102, 0.6);
    }
    thead {
        background-color: #333;
        color: #fff;
        font-weight: bold;
    }
    th {
        color: #fff !important;
        font-weight: bold !important;
        font-size: 12px !important;
    }
    *+.uk-grid-margin-small, .uk-grid+.uk-grid-small, .uk-grid-small>.uk-grid-margin{
        margin-top: 0;
    }
    .loading{
        width: 100%;
        height: 100vh;
        margin: 0 auto;
        text-align: center;
        float: none;
        display: block;
        z-index: +1000;
        position: absolute;
        background: #fff;
        padding-top: 30%;
    }
</style>
<script type="text/javascript">
    let _listOrder = [];
    <?php foreach ($orders as $order): ?>
        _listOrder.push(<?php echo json_encode($order);?>);
    <?php endforeach ?>
    var orderApp = angular.module('order-app', []);
    let domain = "<?php echo DOMAIN; ?>";

    orderApp.service('orderService', ['$http', function ($http) {
        
        
        this.getAllOrderOf = (data, callback) => {
            
            $.post(domain+'sales_orders/getOrders', data, function(data) {
                callback(JSON.parse(data));
            });
        }

        this.updateOrder = (data, callback) => {
            $.post(domain+'sales_orders/updateOrder', data, function(data) {
                callback(data);
            });
        }

    }]);

    // Groups Management Controller
    orderApp.controller('sale-order-management-controller', ['$scope', '$sce', '$timeout', '$interval', 'orderService',
        function($scope, $sce, $timeout, $interval, orderService) {
            let service = orderService;
            $scope.listDates = [];
            $scope.listOrder = [];
            $scope.curOrder = {};
            $scope.showLoading = true;
            let c_date = new Date();
            $scope._date = c_date.getFullYear()  + "-" + (c_date.getMonth()+1) + "-" + c_date.getDate();
            $scope.goToDate = () => {
                $scope.showLoading = true;
                
                $scope.listOrder = _listOrder;
                $scope.listOrder = $scope.listOrder.reduce(function (r, a) {
                    r[a.ngaygiao] = r[a.ngaygiao] || [];
                    a.thoigian = new Date(a.thoigian*1000);
                    r[a.ngaygiao].push(a);
                    return r;
                }, Object.create(null));
                $scope.listDates = Object.keys ($scope.listOrder);
                   
                $timeout(function(){
                    $scope.showLoading = false;
                }, 1500);
            }

            $scope.showEditOrderForm = function(order){
                $scope.curOrder = order;
            }

            $scope.parseJSON=function(strObj){
                return strObj.slice(1,-1).replace(/"/g, '').split(",");
            }

            $scope.toVND = val => { 
                return parseInt(val).toLocaleString();
            }

            $scope.saveOrder = () => {
                service.updateOrder({'order' : $scope.curOrder}, res => {
                    res = JSON.parse(res);
                    if (res.status) {
                    }
                    UIkit.notification({
                        message:res.msg,
                        status: 'warning',
                        pos: 'top-right',
                        timeout: 1500
                    }); 
                });
            }
            $scope.goToDate();
        }
    ]);
</script>

<div class="uk-section" ng-app="order-app" ng-controller="sale-order-management-controller">
    <form class="uk-container uk-form uk-margin-bottom" action="" method="POST">
        <div class="uk-grid">
            <div class="uk-width-3-5">
                <input class="uk-input go-to-datepicker" name="from" value="<?= $from; ?>" type="text" >
            </div>
            <div class="uk-width-2-5">
                <button class="uk-button" type="submit">
                    Đi tới ngày này
                </button>
            </div>
        </div>
    </form>
    <div>
        <div class="loading" uk-spinner="ratio: 3" ng-show="showLoading"></div>
        <div ng-show="listDates.length" ng-repeat="dat in listDates">
            
            <div class='header'>Đơn hàng ngày {{dat}}</div>
            <table class="uk-table uk-table-divider uk-table-hover uk-table-striped uk-table-justify uk-table-middle">
                <thead>
                    <th width="50">Mã DH</th>
                    <th width="50">
                        Thời gian giao
                    </th>
                    <th width="100">Tên khách</th>
                    <th width="100">Loại bánh</th>
                    <th width="50">Kích Thước</th>
                    <th width="50">Chiều Cao</th>
                    <th width="500">Thông tin</th>
                    <th width="50">Giờ nhập</th>
                    <th width="50">Dữ liệu</th>
                    <th width="60">Minh họa</th>
                    <th width="60">Kết quả</th>
                    <th width="30">Quản lý</th>
                </thead>
                <tbody>
                    <tr ng-repeat="order in listOrder[dat]" >
                        <td>{{order.mahd}}</td>
                        <td>{{order.giogiao}}</td>
                        <td>{{order.tenKH}}</td>
                        <td>{{order.loaibanh}}</td>
                        <td>{{order.kichthuoc}}</td>
                        <td>{{order.chieucao}}</td>
                        <td>
                            <ul>
                                <li ng-show="order.dongia !== 0">Đơn giá: {{ toVND(order.dongia)}} <sup>đ</sup></li>
                                <li ng-show="order.mota != ''">Mô tả: {{order.mota}}</li>
                                <li ng-show="order.mausac != ''">Màu: {{order.mausac}}</li>
                                <li ng-show="order.nhanbanh != ''">Nhân: {{order.nhanbanh}}</li>
                                <li ng-show="order.cotbanh != ''">Cốt: {{order.cotbanh}}</li>
                                <li ng-show="order.sotang > 0">{{order.sotang}} tầng</li>
                                <li ng-show="order.soluong > 0">{{order.soluong}} cái</li>
                                <li ng-show="order.diachi != ''">Địa chỉ: {{order.diachi}}</li>
                                <li ng-show="order.chutrenbanh != ''">Chữ trên bánh: {{order.chutrenbanh}}</li>
                            </ul>
                        </td>
                        <td>{{ order.thoigian | date:'H:m, d-M-y' }}</td>
                        <td>
                            <a ng-if="order.nguyenlieu!=''" href="http://tinycake.vn/order/images/album/{{order.mahd}}" target="_blank">Xem</a>
                        </td>
                        <td>
                            <div uk-lightbox>
                                <a href="http://tinycake.vn/order/images/hinhminhhoa/small{{order.hinhminhhoa}}"  data-caption="{{order.loaibanh}}">
                                    <img width="40px" height="40px" ng-src="http://tinycake.vn/order/images/hinhminhhoa/small{{order.hinhminhhoa}}">
                                </a>
                            </div>
                            
                        </td>
                        <td>
                            <div uk-lightbox>
                                <a
                                    ng-href="order.hinhketqua !== 'no.jpg' ? 'http://tinycake.vn/order/thuvienbanh/' + order.hinhketqua : 'http://tinycake.vn/order/images/hinhminhhoa/smallno.jpg'" 
                                    data-caption="{{order.loaibanh}}"
                                >
                                    <img ng-show="order.hinhketqua !== 'no.jpg'" width="40px" height="40px" ng-src="http://tinycake.vn/order/thuvienbanh/{{order.hinhketqua}}">
                                    <img ng-show="order.hinhketqua === 'no.jpg'" width="40px" height="40px" src="http://tinycake.vn/order/images/hinhminhhoa/smallno.jpg">
                                </a>
                            </div>
                        </td>
                        <td>
                            <button uk-icon="icon: pencil" uk-toggle="target: #edit-order" type="button" ng-click="showEditOrderForm(order)"></button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>


        <!-- This is the modal -->
        <div id="edit-order" class="uk-modal-full" uk-modal data-uk-modal="{bgclose:false, modal: false, keyboard: false}">
            <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical uk-height-1-1" uk-overflow-auto>
                <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
                <h3 class="uk-modal-title">Thông tin đơn hàng #{{curOrder.mahd}}</h3>
                <h4>Khách hàng: {{curOrder.tenKH}} - Ngày giao bánh: {{curOrder.giogiao+' - '+curOrder.ngaygiao}}</h4>
                <form >
                    <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="loaibanh">Loại bánh</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="loaibanh" type="text" ng-model="curOrder.loaibanh" >
                            </div>
                        </div>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="kichthuoc">Kích thước</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="kichthuoc" type="text" ng-model="curOrder.kichthuoc" placeholder="Đơn vị: cm...">
                            </div>
                        </div>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="hinhdang">Hình dáng</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="hinhdang" type="text" ng-model="curOrder.hinhdang" placeholder="Tròn, vuông...">
                            </div>
                        </div>
                        
                    </div>
                    <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="sotang">Số tầng</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="sotang" type="text" ng-model="curOrder.sotang" >
                            </div>
                        </div>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="cotbanh">Cốt bánh</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="cotbanh" type="text" ng-model="curOrder.cotbanh" placeholder="">
                            </div>
                        </div>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="mausac">Màu sắc</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="mausac" type="text" ng-model="curOrder.mausac" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="nhanbanh">Nhân bánh</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="nhanbanh" type="text" ng-model="curOrder.nhanbanh" >
                            </div>
                        </div>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="diachi">Địa chỉ</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="diachi" type="text" ng-model="curOrder.diachi" placeholder="">
                            </div>
                        </div>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="chutrenbanh">Chữ ghi trên bánh</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="chutrenbanh" type="text" ng-model="curOrder.chutrenbanh" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="soluong">Số lượng</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="soluong" type="text" ng-model="curOrder.soluong" >
                            </div>
                        </div>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="dongia">Đơn giá</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="dongia" type="text" ng-model="curOrder.dongia" placeholder="">
                            </div>
                        </div>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="chieucao">Chiều cao</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="chieucao" type="text" ng-model="curOrder.chieucao" placeholder="">
                            </div>
                        </div>
                    </div>

                    <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="luuy">Lưu ý</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="luuy" type="text" ng-model="curOrder.luuy" >
                            </div>
                        </div>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="tiencoc">Thanh toán đợt 1</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="tiencoc" type="text" ng-model="curOrder.tiencoc" placeholder="">
                            </div>
                        </div>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="giogiao">Sửa ngày nhận</label>
                            <div class="uk-form-controls">
                                <input class="uk-input datepicker" id="giogiao" type="text" placeholder="" ng-value="curOrder.giogiao+' - '+curOrder.ngaygiao">
                            </div>
                        </div>
                    </div>
                    
                    <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="trangthaidonhang">Trạng thái đơn hàng</label>
                            <div class="uk-form-controls">
                                <select id="trangthaidonhang" class="uk-select" ng-model="curOrder.trangthaidonhang" ng-value="curOrder.trangthaidonhang">
                                    <option>Chưa chọn</option>
                                    <option ng-selected="cuOrder.trangthaidonhang == 0" value="0">0 - Đã nhận order</option>
                                    <option ng-selected="cuOrder.trangthaidonhang == 1" value="1">1 - Hoàn tất cốt bánh</option>
                                    <option ng-selected="cuOrder.trangthaidonhang == 2" value="2">2 - Hoàn tất cốt bánh, hoàn tất phủ bánh</option>
                                    <option ng-selected="cuOrder.trangthaidonhang == 3" value="3">3 - Hoàn tất đồ trang trí, chưa có cốt bánh</option>
                                    <option ng-selected="cuOrder.trangthaidonhang == 4" value="4">4 - Hoàn tất đồ trang trí, hoàn tất cốt bánh, chưa phủ</option>
                                    <option ng-selected="cuOrder.trangthaidonhang == 5" value="5">5 - Hoàn tất đồ trang trí, hoàn tất cốt bánh, hoàn tất phủ</option>
                                    <option ng-selected="cuOrder.trangthaidonhang == 6" value="6">6 - Đã ráp bánh</option>
                                    <option ng-selected="cuOrder.trangthaidonhang == 7" value="7">7 - Đã chụp hình</option>
                                    <option ng-selected="cuOrder.trangthaidonhang == 8" value="8">8 - Đã đóng gói</option>
                                    <option ng-selected="cuOrder.trangthaidonhang == 9" value="9">9 - Đã đi giao</option>
                                    <option ng-selected="cuOrder.trangthaidonhang == 10" value="10">10 - Khách đã nhận bánh</option>
                                    <option ng-selected="cuOrder.trangthaidonhang == 11" value="11">11 - Khách hủy đơn hàng</option>
                                    <option ng-selected="cuOrder.trangthaidonhang == 12" value="12">12 - Hoàn tất đơn hàng</option>
                                </select>
                            </div>
                        </div>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="nhombanh">Thanh toán đợt 1</label>
                            <div class="uk-form-controls">
                                <select id="nhombanh" class="uk-select" ng-model="curOrder.nhombanh" ng-value="curOrder.nhombanh">
                                    <option>Chưa chọn</option>
                                    <option  ng-selected="curOrder.nhombanh == 'WD'" value="WD">WD - Woman Day Cake</option>
                                    <option  ng-selected="curOrder.nhombanh == 'WF'" value="WF">WF - Wedding Fondant Cake</option>
                                    <option  ng-selected="curOrder.nhombanh == 'WC'" value="WC">WC - Wedding Cream Cake</option>
                                    <option  ng-selected="curOrder.nhombanh == 'TB'" value="TB">TB - Teabreak</option>
                                    <option  ng-selected="curOrder.nhombanh == 'ST'" value="ST">ST - Sweet Table</option>
                                    <option  ng-selected="curOrder.nhombanh == 'MT'" value="MT">MT - Mooncake Traditional</option>
                                    <option  ng-selected="curOrder.nhombanh == 'MP'" value="MP">MP - Mooncake Piglet</option>
                                    <option  ng-selected="curOrder.nhombanh == 'MA'" value="MA">MA - Mooncake Animal</option>
                                    <option  ng-selected="curOrder.nhombanh == 'EI'" value="EI">EI - Edible Image Cake</option>
                                    <option  ng-selected="curOrder.nhombanh == 'CN'" value="CN">CN - Cookie New Year</option>
                                    <option  ng-selected="curOrder.nhombanh == 'CF'" value="CF">CF - Cupcake Fondant</option>
                                    <option  ng-selected="curOrder.nhombanh == 'CC'" value="CC">CC - Cupcake Cream</option>
                                    <option  ng-selected="curOrder.nhombanh == 'CI'" value="CI">CI - Cookie Icing</option>
                                    <option  ng-selected="curOrder.nhombanh == 'XC'" value="XC">XC - Christmas cake</option>
                                    <option  ng-selected="curOrder.nhombanh == 'CP'" value="CP">CP - Cakepop</option>
                                    <option  ng-selected="curOrder.nhombanh == 'CB'" value="CB">CB - Cake for Celebrity</option>
                                    <option  ng-selected="curOrder.nhombanh == 'BF'" value="BF">BF - Birthday Fondant Cake</option>
                                    <option  ng-selected="curOrder.nhombanh == 'BC'" value="BC">BC - Birthday cake cream</option>
                                    <option  ng-selected="curOrder.nhombanh == 'AC'" value="AC">AC - Anniversary Cake For Companies</option>
                                    <option  ng-selected="curOrder.nhombanh == 'JE'" value="JE">JE - Jelly 3D</option>
                                    <option  ng-selected="curOrder.nhombanh == 'CN01'" value="CN01">CN01 - Candied Coconut Ribbon</option>
                                    <option  ng-selected="curOrder.nhombanh == 'CN02'" value="CN02">CN02 - Dried soursop</option>
                                    <option  ng-selected="curOrder.nhombanh == 'CN0101'" value="CN0101">CN0101 - Greentea Candied Coconut Ribbon</option>
                                    <option  ng-selected="curOrder.nhombanh == 'CN0102'" value="CN0102">CN0102 - Cocoa Candied Coconut Ribbon</option>
                                    <option  ng-selected="curOrder.nhombanh == 'CN0103'" value="CN0103">CN0103 - Durian Candied Coconut Ribbon</option>
                                    <option  ng-selected="curOrder.nhombanh == 'CN0104'" value="CN0104">CN0104 - Gac Candied Coconut Ribbon</option>
                                    <option  ng-selected="curOrder.nhombanh == 'CN0105'" value="CN0105">CN0105 - Magenta Candied Coconut Ribbon</option>
                                    <option  ng-selected="curOrder.nhombanh == 'CN0106'" value="CN0106">CN0106 - Vanilla Candied Coconut Ribbon</option>
                                    <option  ng-selected="curOrder.nhombanh == 'CN0107'" value="CN0107">CN0107 - Turmeric Candied Coconut Ribbon</option>
                                </select>
                            </div>
                        </div>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="tiendo">Tiến độ</label>
                            <div class="uk-form-controls">
                                <select id="tiendo" class="uk-select" ng-model="curOrder.tiendo" ng-value="curOrder.tiendo">
                                    <option ng-selected="curOrder.tiendo == 1" value="1">Lead</option>   
                                    <option ng-selected="curOrder.tiendo == 2" value="2">Quotation</option>   
                                    <option ng-selected="curOrder.tiendo == 3" value="3">Order</option> 
                                    <option ng-selected="curOrder.tiendo == 4" value="4">Sales</option> 
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="comment">Comment</label>
                            <div class="uk-form-controls">
                                <textarea class="uk-textarea" id="comment" ng-model="curOrder.comment" rows="5">{{curOrder.comment}}</textarea>
                            </div>
                        </div>
                    </div>                    
                    
                    <button class="uk-button uk-button-primary uk-margin-small-top" type="button" ng-click="saveOrder()">
                        Lưu lại 
                    </button>
                </form>
            </div>
        </div>
        <h3 class="uk-text-muted uk-text-center" ng-show="!listDates.length"><i>Chưa có đơn hàng nào</i></h3>
    </div>
</div>
<script>
    window.onload = function(){
        // $('input.datepicker').daterangepicker({
        //     buttonClasses: 'uk-button uk-button-small uk-button-default',
        //     applyButtonClasses: 'uk-button-primary',
        //     autoUpdateInput: false,
        //     timePicker24Hour: true,
        //     timePicker: true,
        //     singleDatePicker: true,
        //     locale: {
        //         format: 'YYYY-M-DD',
        //         cancelLabel: 'Hủy',
        //         applyLabel: 'Chọn'
        //     }
        // });
        $('input.go-to-datepicker').daterangepicker({
            buttonClasses: 'uk-button uk-button-small uk-button-default',
            applyButtonClasses: 'uk-button-primary',
            autoUpdateInput: true,
            timePicker24Hour: false,
            timePicker: false,
            singleDatePicker: true,
            locale: {
                format: 'YYYY-M-DD',
                cancelLabel: 'Hủy',
                applyLabel: 'Chọn'
            }
        });
    }
</script>