<script type="text/javascript" src="assets/js/moment.min.js?v=<?php echo time();?>"></script>
<link rel="stylesheet" type="text/css" href="assets/css/daterangepicker.css?v=<?php echo time();?>" />
<link rel="stylesheet" type="text/css" href="assets/css/select2.min.css?v=<?php echo time();?>" />
<!-- Custom JS -->
<script type="text/javascript" src="assets/js/daterangepicker.min.js?v=<?php echo time();?>"></script>
<script src="assets/js/select2.min.js?v=<?php echo time();?>" type="text/javascript"></script>
<script src="assets/js/insert-order-service.js?v=<?php echo time();?>" type="text/javascript"></script>
<script src="assets/js/insert-order-controller.js?v=<?php echo time();?>" type="text/javascript"></script>
<style>
div#suggestion-wrapper {
    position: fixed;
    top: 120px;
    width: calc(100%);
    border: 1px solid #eee;
    padding-right: 15px;
    overflow-y: auto;
    overflow-x: hidden;
    height: calc(100vh - 120px);
    scroll-behavior: smooth;
    -webkit-overflow-scrolling: touch;
    -o-overflow-scrolling: touch;
    -moz-overflow-scrolling: touch;
    background: #fff;
}
.fixed-top{
    position: fixed;
    top: 50px;
}
.loading{
    -webkit-animation-name: spin;
  -webkit-animation-duration: 1000ms;
  -webkit-animation-iteration-count: infinite;
  -webkit-animation-timing-function: linear;
  -moz-animation-name: spin;
  -moz-animation-duration: 1000ms;
  -moz-animation-iteration-count: infinite;
  -moz-animation-timing-function: linear;
  -ms-animation-name: spin;
  -ms-animation-duration: 1000ms;
  -ms-animation-iteration-count: infinite;
  -ms-animation-timing-function: linear;
}
@-moz-keyframes spin {
    from { -moz-transform: rotate(0deg); }
    to { -moz-transform: rotate(360deg); }
}
@-webkit-keyframes spin {
    from { -webkit-transform: rotate(0deg); }
    to { -webkit-transform: rotate(360deg); }
}
@keyframes spin {
    from {transform:rotate(0deg);}
    to {transform:rotate(360deg);}
}
</style>
<div class="uk-section" ng-app="add-customer-app" ng-controller="add-customer-controller">
    <div class="uk-container">
        <h3 class="uk-heading-line uk-text-center uk-margin-small-top"><span>Thêm đơn hàng</span></h3>
        <h4>
            Khách hàng: <strong><?php echo $customer['tenkh'];?></strong><br>
            Số điện thoại: <strong><?php echo $customer['sdt'];?></strong>
        </h4>
        <?php if ($msg !== '' && $msg !== NULL): ?>
            <div class="uk-alert-success uk-padding-small" uk-alert>
                <a class="uk-alert-close" uk-close></a>
                <span uk-icon="check"></span> <?php echo $msg; ?>
            </div>
        <?php endif ?>
        <form action="<?= base_url . 'api/add_new_order' ?>" method="POST">
            <div>
                <label for="nhanvien">Nhân viên nhận đơn hàng (*)</label>
                <div class="uk-inline">
                    <span class="uk-form-icon" uk-icon="icon: user"></span>
                    <select class="uk-select" name="order_taken_by" id="order_taken_by" value="<?php echo en_id($user['manv']);?>" required>
                        <option value="<?php echo en_id($user['manv']); ?>"><?php echo $user['tendaydu']; ?></option>
                    </select>
                </div>
            </div>
            <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
                <div>
                    <label for="diachi">Địa chỉ giao (*)</label>
                    <div class="uk-inline">
                        <span class="uk-form-icon" uk-icon="icon: location"></span>
                        <input class="uk-input" name="diachi" diachi="diachi" value="<?php echo $customer['diachi'];?>" type="text" placeholder="Địa chỉ: Số 183B Trần Quốc Thảo P9 Q3" required>
                        <input type="hidden" name="customer_id" value="<?= en_id($customer['makh']); ?>">
                    </div>
                </div>
                
                <div>
                    <label>Loại đơn hàng (*)</label>
                    <div class="uk-inline">
                        <label><input class="uk-radio" type="radio" value="banh-kem" name="order_type" checked> Bánh kem</label>
                        <label><input class="uk-radio" type="radio" value="banh-trung-thu" name="order_type"> Bánh trung thu</label>
                    </div>
                </div>
                <div>
                    <div class="uk-inline">
                        <label><input class="uk-checkbox" name="tang" value="1" type="checkbox"> Đây là bánh tặng</label>
                    </div>
                </div>
            </div>
            <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
                <div>
                    <label for="order_required_delivery_time">Có thể giao từ ngày</label>
                    <div class="uk-inline">
                        <span class="uk-form-icon" uk-icon="icon: calendar"></span>
                        <input class="uk-input datepicker" name="order_required_delivery_time" id="order_required_delivery_time" type="text" placeholder="">
                    </div>
                </div>
                <div>
                    <label for="order_delivery_time">Giao trước ngày (*)</label>
                    <div class="uk-inline">
                        <span class="uk-form-icon" uk-icon="icon: calendar"></span>
                        <input class="uk-input datepicker" name="order_delivery_time" id="order_delivery_time" placeholder="" required>
                    </div>
                </div>
            </div>
            <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid">
                <div>
                    <label for="muondo">Mượn đồ</label>
                    <div class="uk-inline">
                        <textarea class="uk-textarea" name="muondo" id="muondo" rows="5" placeholder="Mượn dao, nĩa..."></textarea>
                    </div>
                </div>
                <div>
                    <label for="comment">Ghi chú</label>
                    <div class="uk-inline">
                        <textarea class="uk-textarea" name="comment" id="comment" rows="5" placeholder="Ghi chú thêm: Số điện thoại khác, Email khác, ..."></textarea>
                    </div>
                </div>
                
            </div>
            <button class="uk-button uk-button-primary uk-margin-small-top" type="submit">
                Thêm đơn hàng
            </button>
        </form>
    </div>
    <div id="suggestion-wrapper" ng-show="showLoading"></div>
</div>
    
<script>
    var DOMAIN = '<?= DOMAIN ?>';
    window.onload = function(){
        $('input.datepicker').daterangepicker({
            buttonClasses: 'uk-button uk-button-small uk-button-default',
            applyButtonClasses: 'uk-button-primary',
            autoUpdateInput: true,
            singleDatePicker: true,
            timePicker: true,
            timePicker24Hour: true,
            locale: {
                 format: 'YYYY-MM-DD HH:mm:SS ',
                 cancelLabel: 'Hủy',
                 applyLabel: 'Chọn'
             }
        });

        $("#order_taken_by").select2({
            ajax: {
                url: DOMAIN+'api/findUsers',
                dataType: 'json',
                processResults: function (data) {
                    return {
                        results: data.items
                    };
                },
            },
        })
        .on('select2:select', function (e) {
            $("#order_taken_by").val(e.params.data.id);
        });
    }
</script>