<link rel="stylesheet" type="text/css" href="assets/css/insert-order.css?v=<?php echo time(); ?>" />
<script>
var _makh = "<?= en_id($order['customer_id']);?>";
var _order = "<?= en_id($order['order_number']);?>";
var _products = [];
<?php foreach($products as $p):?>
    _products.push({
            id: <?= $p['id'];?>,
            order_id: <?= $p['order_id'];?>,
            product_id: <?= $p['product_id'];?>,
            quantity: <?= $p['quantity'];?>,
            mahd: <?= $p['mahd'];?>,
            makh: <?= $p['makh'];?>,
            diachi: "<?= $p['diachi'];?>",
            loaibanh: "<?= $p['loaibanh'];?>",
            nhombanh: "<?= $p['nhombanh'];?>",
            kichthuoc: "<?= $p['kichthuoc'];?>",
            chieucao: "<?= $p['chieucao'];?>",
            hinhdang: "<?= $p['hinhdang'];?>",
            sotang: "<?= $p['sotang'];?>",
            mausac: "<?= $p['mausac'];?>",
            cotbanh: "<?= $p['cotbanh'];?>",
            nhanbanh: "<?= $p['nhanbanh'];?>",
            chutrenbanh: "<?= $p['chutrenbanh'];?>",
            soluong: "<?= $p['soluong'];?>",
            dongia: "<?= $p['dongia'];?>",
            tiencoc: "<?= $p['tiencoc'];?>",
            thanhtoan1: "<?= $p['thanhtoan1'];?>",
            thanhtoan2: "<?= $p['thanhtoan2'];?>",
            thanhtoan3: "<?= $p['thanhtoan3'];?>",
            mota: "<?= $p['mota'];?>",
            thoigian: "<?= $p['thoigian'];?>",
            ngaygiao: "<?= $p['ngaygiao'];?>",
            giogiao: "<?= $p['giogiao'];?>",
            hinhminhhoa: "<?= $p['hinhminhhoa'];?>",
            tiendo: "<?= $p['tiendo'];?>",
            hinhketqua: "<?= $p['hinhketqua'];?>",
            nguyenlieu: "<?= $p['nguyenlieu'];?>",
            muondo: "<?= $p['muondo'];?>",
            tang: "<?= $p['tang'];?>",
            comment: "<?= $p['comment'];?>",
            sortdate: "<?= $p['sortdate'];?>",
            mail_status: "<?= $p['mail_status'];?>",
            order_status: "<?= $p['order_status'];?>",
            payment_status: "<?= $p['payment_status'];?>",
            luuy: "<?= $p['luuy'];?>",
            trangthaidonhang: "<?= $p['trangthaidonhang'];?>",
            confirm_payment_admin: "<?= $p['confirm_payment_admin'];?>",
            admin_receiver: "<?= $p['admin_receiver'];?>",
            confirm_payment_owner: "<?= $p['confirm_payment_owner'];?>",
            owner_receiver: "<?= $p['owner_receiver'];?>",
            delivery_by: "<?= $p['delivery_by'];?>",
            delivery_fee: "<?= $p['delivery_fee'];?>",
            delivery_payment_receive_time: "<?= $p['delivery_payment_receive_time'];?>",
            admin_payment_receive_time: "<?= $p['admin_payment_receive_time'];?>",
            owner_payment_receive_time: "<?= $p['owner_payment_receive_time'];?>",
            nuong: "<?= $p['nuong'];?>",
            phu: "<?= $p['phu'];?>",
            trangtri: "<?= $p['trangtri'];?>",
            rap: "<?= $p['rap'];?>",
            chuphinh: "<?= $p['chuphinh'];?>",
            chotkhach: "<?= $p['chotkhach'];?>",
            donggoi: "<?= $p['donggoi'];?>",
            giao: "<?= $p['giao'];?>",
            thanhtoan: "<?= $p['thanhtoan'];?>",
            sales: "<?= $p['sales'];?>",
            order_taken_by: "<?= $p['order_taken_by'];?>",
            colorcode: "<?= $p['colorcode'];?>",
            order_type_id: "<?= $p['order_type_id'];?>",
            luuydacbiet: "<?= $p['luuydacbiet'];?>",
            selected_status: "<?= $p['selected_status'];?>",
            is_added: true
    });
<?php endforeach;?>

</script>
<script src="assets/js/insert-product-service.js?v=<?php echo time(); ?>" type="text/javascript"></script>
<script src="assets/js/insert-product-controller.js?v=<?php echo time(); ?>" type="text/javascript"></script>
<style>
div#suggestion-wrapper {
    margin: 5px auto;
    width: calc(600px);
    border: 1px solid #eee;
    padding-right: 0;
    overflow-y: auto;
    overflow-x: hidden;
    height: calc(100vh - 140px);
    scroll-behavior: smooth;
    border-radius: 3px;
    -webkit-overflow-scrolling: touch;
    -o-overflow-scrolling: touch;
    -moz-overflow-scrolling: touch;
}

@media only screen and (max-width: 600px){
    div#suggestion-wrapper {
        width: calc(100% - 20px);
    }
}

div#suggestion-wrapper::-webkit-scrollbar {
    width: 3px;
    height: 6px;
}
div#suggestion-wrapper .uk-grid-match {
    padding: 10px;
}
</style>
<div class="uk-section" ng-app="add-product-app" ng-controller="add-product-controller">
    <div class="uk-container">
        
        <?php if ($msg !== '' && $msg !== null) : ?>
            <div class="uk-alert-success uk-padding-small" uk-alert>
                <a class="uk-alert-close" uk-close></a>
                <span uk-icon="check"></span> <?php echo $msg; ?>
            </div>
        <?php endif ?>
    </div>
    <div class="uk-container uk-margin-small-bottom">
        <h4 class="uk-text-center">
            Tìm bánh tham khảo
        </h4>
        <!-- Form tìm kiếm sản phẩm -->
        <div class="uk-text-center">
            <div class="uk-inline uk-width-4-5 uk-width-4-5@s uk-width-3-5@m uk-width-2-5@l">
                <!-- <span class="uk-form-icon" uk-icon="icon: search"></span> -->
                <input class="uk-input" 
                    ng-keyup="findProductsSuggestion('<?= $order['order_type'] ?>')" 
                    ng-model="keyword" type="text" placeholder="Tìm kiếm sản phẩm" 
                    ng-focus="showResults = true">
            </div>
        </div>
        <!-- Hiển thị thông tin gợi ý -->
        <div id="suggestion-wrapper" ng-if="suggestionProducts.length && showResults" uk-grid>
            <div class="uk-grid-match uk-width-1-1 uk-grid-small 
                uk-animation-slide-bottom-small" 
                ng-repeat="product in suggestionProducts" 
                uk-grid ng-click="selectProduct(product)" 
                >
                <!-- Hiển thị chi tiết gợi ý -->
                <div class="uk-width-1-4">
                    <div uk-lightbox ng-bind-html="render_img(product.hinhketqua)">
                    </div>
                </div>
                <div class="uk-width-3-4 uk-text-justify">
                    {{product.mota == '' ? product.loaibanh : product.mota}}
                </div>
            </div>
        </div>
        <div class="uk-width-1-1 uk-animation-slide-bottom-small uk-text-center uk-margin-small-top" ng-show="!suggestionProducts.length && noResult">
            <h4>Không có kết quả phù hợp. Vui lòng thử với từ khóa khác</h4>
        </div>
       
        <h3 class="uk-text-center uk-margin-bottom"><span>Thêm sản phẩm</span></h3>
        <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
            <div>
                <div uk-lightbox ng-bind-html="render_img(selectedProduct.hinhketqua, 90)">
                </div>
            </div>
            <div>
                <div class="js-upload" uk-form-custom>
                    <input type="file">
                    <button class="uk-button uk-button-default" type="button" tabindex="-1">Đổi ảnh khác</button>
                </div>
            </div>
            <div></div>
            <div></div>
        </div>
        <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
            <div>
                <div class="uk-inline">
                    <!-- <span class="uk-form-icon" uk-icon="icon: receiver"></span> -->
                    <input class="uk-input" name="loaibanh" ng-model="selectedProduct.loaibanh" required type="text" placeholder="Tên bánh (*)">
                </div>
            </div>
            <div>
                <div class="uk-inline">
                    <!-- <span class="uk-form-icon" uk-icon="icon: location"></span> -->
                    <input class="uk-input" name="dongia" ng-model="selectedProduct.dongia" type="text" placeholder="Giá (*)" required>
                </div>
            </div>
            <div>
                <div class="uk-inline">
                    <!-- <span class="uk-form-icon" uk-icon="icon: user"></span> -->
                    <input class="uk-input" name="kichthuoc" ng-model="selectedProduct.kichthuoc" type="text" placeholder="Kích thước" required>
                </div>
            </div>
            <div>
                <div class="uk-inline">
                    <!-- <span class="uk-form-icon" uk-icon="icon: user"></span> -->
                    <input class="uk-input" name="chieucao" ng-model="selectedProduct.chieucao" type="text" placeholder="Chiều cao" required>
                </div>
            </div>
        </div>

        <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
            <div>
                <div class="uk-inline">
                    <!-- <span class="uk-form-icon" uk-icon="icon: receiver"></span> -->
                    <input class="uk-input" name="hinhdang" ng-model="selectedProduct.hinhdang" required type="text" placeholder="Hình dáng">
                </div>
            </div>
            <div>
                <div class="uk-inline">
                    <!-- <span class="uk-form-icon" uk-icon="icon: location"></span> -->
                    <input class="uk-input" name="sotang" ng-model="selectedProduct.sotang" type="text" placeholder="Số tầng" required>
                </div>
            </div>
            <div>
                <div class="uk-inline">
                    <!-- <span class="uk-form-icon" uk-icon="icon: user"></span> -->
                    <input class="uk-input" name="mausac" ng-model="selectedProduct.mausac" type="text" placeholder="Màu sắc" required>
                </div>
            </div>
            <div>
                <div class="uk-inline">
                    <!-- <span class="uk-form-icon" uk-icon="icon: user"></span> -->
                    <input class="uk-input" type="text" placeholder="Cốt bánh" name="cotbanh" ng-model="selectedProduct.cotbanh" required>
                </div>
            </div>
        </div>

        <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
            <div>
                <div class="uk-inline">
                    <!-- <span class="uk-form-icon" uk-icon="icon: receiver"></span> -->
                    <input class="uk-input" name="nhanbanh" ng-model="selectedProduct.nhanbanh" required type="text" placeholder="Nhân bánh">
                </div>
            </div>
            <div>
                <div class="uk-inline">
                    <!-- <span class="uk-form-icon" uk-icon="icon: location"></span> -->
                    <input class="uk-input" name="chutrenbanh" ng-model="selectedProduct.chutrenbanh" type="text" placeholder="Chữ trên bánh" required>
                </div>
            </div>
            <div>
                <div class="uk-inline">
                    <!-- <span class="uk-form-icon" uk-icon="icon: user"></span> -->
                    <input class="uk-input" name="soluong" ng-model="selectedProduct.soluong" type="number" placeholder="Số lượng" required>
                </div>
            </div>
            <div>
                <div class="uk-inline">
                    <button class="uk-button" ng-click="addProduct()">Thêm bánh</button>
                </div>
            </div>
        </div>
        

        <div ng-show="addedProducts.length">
            <h4 class="uk-text-center">
                Danh sách bánh tạo mới
            </h4>
            <table class="uk-table uk-table-divider uk-table-striped">
                <thead>
                    <th>Ảnh</th>
                    <th>Tên bánh</th>
                    <th>Giá</th>
                    <th>Kich thước</th>
                    <th>Chiều cao</th>
                    <th>Hình dáng</th>
                    <th>Số tầng</th>
                    <th>Màu sắc</th>
                    <th>Cốt bánh</th>
                    <th>Nhân bánh</th>
                    <th>Chữ trên bánh</th>
                    <th>Số lượng</th>
                    <th>Xóa</th>
                </thead>
                <tbody>
                    <tr ng-repeat="p in addedProducts">
                        <td>
                            <div uk-lightbox ng-bind-html="render_img(p.hinhketqua)">
                            </div>
                        </td>
                        <td>{{p.loaibanh}}</td>
                        <td>{{p.dongia}}</td>
                        <td>{{p.kichthuoc}}</td>
                        <td>{{p.chieucao}}</td>
                        <td>{{p.hinhdang}}</td>
                        <td>{{p.sotang}}</td>
                        <td>{{p.mausac}}</td>
                        <td>{{p.cotbanh}}</td>
                        <td>{{p.nhanbanh}}</td>
                        <td>{{p.chutrenbanh}}</td>
                        <td>{{p.soluong}}</td>
                        <td>
                            <button class="uk-button" ng-click="removeSelectedProduct(p)">Xóa</button>
                            <button class="uk-button" ng-click="editSelectedProduct(p)">Sửa</button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <button class="uk-button uk-button-primary" ng-click="saveData()">Lưu lại</button>
        </div>
    </div>
    
</div>