<?php

class sales_orders extends Controller {
    private $session;
    private $model;
    private $user_model;
    private $customer_model;
    private $callbackUrl;
    private $modules = [];
    public function __construct() {
        parent::__construct(__CLASS__);
        $this->session = new Session();
        $this->model = $this->load_model();
        $this->user_model = $this->load_model('users');
        $this->customer_model = $this->load_model('customer');
        $this->callbackUrl = $this->session->get_session('goToUrl');
        if ($this->callbackUrl == '' || $this->callbackUrl == null) {
            $this->callbackUrl = DOMAIN;
        }
        if (!count($this->userInfo)) {
            header('Location: '.LOGIN_PAGE);
            die;
        }
        if( !$this->user_model->isAccessibleModule('orders_input', (int) $this->userInfo['manv']) ) {
            header('Location: '.DOMAIN);
            die;
        }
        $this->modules = $this->user_model->getUserModules((int) $this->userInfo['manv']);
    }

    public function index() {
        $from = input('from', 'gpc', 'NOW');
        $nextweek = new DateTime($from);
        $from = new DateTime($from);
        $nextweek->add(new DateInterval('P7D'));
        $res = $this->model->getOrderByRang($from->format('Y-m-d'), $nextweek->format('Y-m-d'));
        $this->load_view('main-header', [
            'base_url'  => 'modules/header/',
            'title'     => 'Quản lý đơn hàng từ ngày '.$from->format('Y-m-d') . ' đến ' . $nextweek->format('Y-m-d'),
            'user'      => $this->userInfo
        ], 'header');
        $this->load_view('menu-top', ['modules' => $this->modules], 'header');
        $this->load_view('index', ['orders' => $res, 'from' => $from->format('Y-m-d')]);
        $this->load_view('footer', [], 'footer');
    }

    

    function updateOrder(){
        $order = input('order', 'P', '');
        if(empty($order) || $order === null){
            echo json_encode([
                'status' => 0,
                'msg'   => 'No param order'
            ]);
            die();
        }
        $mahd = $order['mahd'];
        unset($order['mahd']);
        unset($order['tenKH']);
        unset($order['hinhminhhoa']);
        unset($order['hinhketqua']);
        unset($order['sdt']);
        unset($order['$$hashKey']);
        $this->model->update_order( $order, ['mahd' => $mahd ] );
        echo json_encode([
            'status' => 1,
            'msg' => 'Cập nhật thành công'
        ]);
    }

    public function insertorder($makh = ''){
        if($makh != '' && de_id($makh) != ''){
            $makh = de_id($makh);
            $customer = $this->customer_model->getCustomer(['makh' => $makh]);
            if(!count($customer)){
                $this->load_view('main-header', [
                    'base_url'  => 'modules/header/',
                    'title'     => 'Tìm kiếm người dùng',
                    'user'      => $this->userInfo
                ], 'header');
                $this->load_view('menu-top', ['modules' => $this->modules], 'header');
                $this->load_view('find-customer', []);
                $this->load_view('footer', [], 'footer');
            }else{
                $this->load_view('main-header', [
                    'base_url'  => 'modules/header/',
                    'title'     => 'Tạo đơn hàng mới - '.$customer['tenkh'],
                    'user'      => $this->userInfo
                ], 'header');
                $this->load_view('menu-top', ['modules' => $this->modules], 'header');
                $this->load_view('insert-order', ['customer' => $customer, 'msg' => $this->session->get_session('flash-message'), 'user' => $this->userInfo]);
                $this->load_view('footer', [], 'footer');
                $this->session->delete_session('flash-message');
            }
        }else{
            $this->load_view('main-header', [
                'base_url'  => 'modules/header/',
                'title'     => 'Tìm kiếm người dùng',
                'user'      => $this->userInfo
            ], 'header');
            $this->load_view('menu-top', ['modules' => $this->modules], 'header');
            $this->load_view('find-customer', []);
            $this->load_view('footer', [], 'footer');
        }
    }

    public function add_product_to_order($order_id){
        if( empty($order_id) ){
            send_json([
                'status' => 0,
                'msg' => 'Missing value'
            ]);
        }
        $order_id = (int) de_id($order_id);
        if( empty($order_id) || $order_id < 0 ){
            send_json([
                'status' => 0,
                'msg' => 'Invalid argument'
            ]);
        }

        $order = $this->model->get_order(['order_number' => $order_id]);
        if(!is_array($order) || empty($order)){
            send_json([
                'status' => 0,
                'msg' => 'Order is not exist'
            ]);
        }

        $products = $this->model->get_order_detail($order_id);


        $this->load_view('main-header', [
            'base_url' => 'modules/header/',
            'title' => 'Thêm sản phẩm vào đơn hàng',
            'user' => $this->userInfo
        ], 'header');
        $this->load_view('menu-top', [
            'modules' => $this->modules
        ], 'header');
        $this->load_view('insert-product', ['order' => $order, 'products' => $products]);
        $this->load_view('footer', [], 'footer');
    }

}

 ?>
