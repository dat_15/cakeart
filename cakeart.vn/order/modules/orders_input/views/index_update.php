<script type="text/javascript" src="assets/js/moment.min.js?v=1528388700"></script>
<script type="text/javascript" src="assets/js/daterangepicker.min.js?v=1528388700"></script>
<link rel="stylesheet" type="text/css" href="assets/css/daterangepicker.css?v=1528388700" />
<style>
    .header {
        text-align: center;
        font-size: 22px;
    }
    .uk-lightbox{
        background-color: rgba(102, 102, 102, 0.6);
    }
    thead {
        background-color: #333;
        color: #fff;
        font-weight: bold;
    }
    th {
        color: #fff !important;
        font-weight: bold !important;
        font-size: 12px !important;
    }
    *+.uk-grid-margin-small, .uk-grid+.uk-grid-small, .uk-grid-small>.uk-grid-margin{
        margin-top: 0;
    }
</style>
<script type="text/javascript">
    var orderApp = angular.module('order-app', []);
    let domain = "<?php echo DOMAIN; ?>";

    orderApp.service('orderService', ['$http', function ($http) {
        
        
        this.getAllOrderOf = (data, callback) => {
            var req = {
                method: 'POST',
                url: domain+'sales_orders/getOrders',
                data: data
            };

            $http(req).then((res) => {
                callback(res.data);
            }, (err) => {
                callback({ status: 0, message: 'error' });
                console.log('getTicketGroupUser error ', err);
                console.log(err);
            });
        }


        this.countValidResult = (key, callback) => {
            var req = {
                method: 'GET',
                url: domain+'api/count_all_search_sesult/'+key
            };

            $http(req).then((res) => {
                callback(res.data);
            }, (err) => {
                callback({ status: 0, message: 'error' });
                console.log('Call API ERROR: ', err);
                console.log(err);
            });
        }
    }]);

    // Groups Management Controller
    orderApp.controller('sale-order-management-controller', ['$scope', '$sce', '$timeout', '$interval', 'orderService',
        function($scope, $sce, $timeout, $interval, orderService) {
            let service = orderService;
            $scope.listDates = [];
            $scope.listOrder = [];
            $scope.curOrder = {};
            $scope.onInit = () => {
                service.getAllOrderOf([], function (res){
                    if(res.status){
                        $scope.listOrder = angular.copy(res.data);
                        $scope.curDate = $scope.listOrder[0].ngaygiao;
                        $scope.listOrder = $scope.listOrder.reduce(function (r, a) {
                            r[a.ngaygiao] = r[a.ngaygiao] || [];
                            a.thoigian = new Date(a.thoigian*1000);
                            r[a.ngaygiao].push(a);
                            return r;
                        }, Object.create(null));
                        $scope.listDates = Object.keys ($scope.listOrder);
                    }else{

                    }
                })
            }

            $scope.showEditOrderForm = function(order){
                $scope.curOrder = order;
            }

            $scope.parseJSON=function(strObj){
                return strObj.slice(1,-1).replace(/"/g, '').split(",");
            }

            $scope.onInit();
        }
    ]);
</script>
<div class="uk-section" ng-app="order-app" ng-controller="sale-order-management-controller">
    <div ng-if="listDates.length">
        <div ng-repeat="dat in listDates">
            
            <div class='header'>Đơn hàng ngày {{dat}}</div>
            <table class="uk-table uk-table-divider uk-table-hover uk-table-striped uk-table-justify uk-table-middle">
                <thead>
                    <th width="50">Mã DH</th>
                    <th width="40">
                        Thời gian giao
                    </th>
                    <th width="100">Tên khách</th>
                    <th width="100">Loại bánh</th>
                    <th width="50">Kích Thước</th>
                    <th width="50">Chiều Cao</th>
                    <th width="500">Thông tin</th>
                    <th width="50">Giờ nhập</th>
                    <th width="50">Dữ liệu</th>
                    <th width="60">Minh họa</th>
                    <th width="60">Kết quả</th>
                    <th width="30">Quản lý</th>
                </thead>
                <tbody>
                    <tr ng-repeat="order in listOrder[dat]" >
                        <td>{{order.mahd}}</td>
                        <td>{{order.giogiao}}</td>
                        <td>{{order.tenKH}}</td>
                        <td>{{order.loaibanh}}</td>
                        <td>{{order.kichthuoc}}</td>
                        <td>{{order.chieucao}}</td>
                        <td>
                            <ul>
                                <li ng-show="order.dongia > 0">Đơn giá: {{order.dongia}} <sup>đ</sup></li>
                                <li ng-show="order.mota != ''">Mô tả: {{order.mota}}</li>
                                <li ng-show="order.mausac != ''">Màu: {{order.mausac}}</li>
                                <li ng-show="order.nhanbanh != ''">Nhân: {{order.nhanbanh}}</li>
                                <li ng-show="order.cotbanh != ''">Cốt: {{order.cotbanh}}</li>
                                <li ng-show="order.sotang > 0">{{order.sotang}} tầng</li>
                                <li ng-show="order.soluong > 0">{{order.soluong}} cái</li>
                                <li ng-show="order.diachi != ''">Địa chỉ: {{order.diachi}}</li>
                                <li ng-show="order.chutrenbanh != ''">Chữ trên bánh: {{order.chutrenbanh}}</li>
                            </ul>
                        </td>
                        <td>{{ order.thoigian | date:'H:m, d-M-y' }}</td>
                        <td>
                            <a ng-show="order.nguyenlieu!=''" href="http://tinycake.vn/order/images/album/{{order.mahd}}" target="_blank"><img  width="40px" height="40px" ng-src="http://www.tinycake.vn/order/{{order.nguyenlieu}}"></img></a>
                        </td>
                        <td>
                            <div uk-lightbox>
                                <a href="http://tinycake.vn/order/images/hinhminhhoa/small{{order.hinhminhhoa}}"  data-caption="{{order.loaibanh}}">
                                    <img width="80px" height="80px" ng-src="http://tinycake.vn/order/images/hinhminhhoa/small{{order.hinhminhhoa}}">
                                </a>
                            </div>
                            
                        </td>
                        <td>
                            <div uk-lightbox>
                                <a
                                    ng-href="order.hinhketqua !== 'no.jpg' ? 'http://tinycake.vn/order/thuvienbanh/' + order.hinhketqua : 'http://tinycake.vn/order/images/hinhminhhoa/smallno.jpg'" 
                                    data-caption="{{order.loaibanh}}"
                                >
                                    <img ng-show="order.hinhketqua !== 'no.jpg'" width="80px" height="80px" ng-src="http://tinycake.vn/order/thuvienbanh/{{order.hinhketqua}}">
                                    <img ng-show="order.hinhketqua === 'no.jpg'" width="80px" height="80px" src="http://tinycake.vn/order/images/hinhminhhoa/smallno.jpg">
                                </a>
                            </div>
                        </td>
                        <td>
                            <button uk-icon="icon: pencil" uk-toggle="target: #edit-order" type="button" ng-click="showEditOrderForm(order)"></button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>


        <!-- This is the modal -->
        <div id="edit-order" class="uk-modal-full" uk-modal>
            <div class="uk-modal-dialog uk-modal-body" uk-overflow-auto>
                <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
                <h3 class="uk-modal-title">Thông tin đơn hàng #{{curOrder.mahd}}</h3>
                <h4>Khách hàng: {{curOrder.tenKH}} - Ngày giao bánh: {{curOrder.giogiao+' - '+curOrder.ngaygiao}}</h4>
                <form >
                    <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="loaibanh">Loại bánh</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="loaibanh" type="text" ng-model="curOrder.loaibanh" >
                            </div>
                        </div>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="kichthuoc">Kích thước</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="kichthuoc" type="text" ng-model="curOrder.kichthuoc" placeholder="Đơn vị: cm...">
                            </div>
                        </div>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="hinhdang">Hình dáng</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="hinhdang" type="text" ng-model="curOrder.hinhdang" placeholder="Tròn, vuông...">
                            </div>
                        </div>
                        
                    </div>
                    <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="sotang">Số tầng</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="sotang" type="number" ng-model="curOrder.sotang|number" >
                            </div>
                        </div>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="cotbanh">Cốt bánh</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="cotbanh" type="text" ng-model="curOrder.cotbanh" placeholder="">
                            </div>
                        </div>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="mausac">Màu sắc</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="mausac" type="text" ng-model="curOrder.mausac" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="nhanbanh">Nhân bánh</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="nhanbanh" type="text" ng-model="curOrder.nhanbanh" >
                            </div>
                        </div>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="diachi">Địa chỉ</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="diachi" type="text" ng-model="curOrder.diachi" placeholder="">
                            </div>
                        </div>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="chutrenbanh">Chữ ghi trên bánh</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="chutrenbanh" type="text" ng-model="curOrder.chutrenbanh" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="soluong">Số lượng</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="soluong" type="number" ng-model="curOrder.soluong|number" >
                            </div>
                        </div>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="dongia">Đơn giá</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="dongia" type="text" ng-model="curOrder.dongia" placeholder="">
                            </div>
                        </div>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="chieucao">Chiều cao</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="chieucao" type="text" ng-model="curOrder.chieucao" placeholder="">
                            </div>
                        </div>
                    </div>

                    <div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="luuy">Lưu ý</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="luuy" type="text" ng-model="curOrder.luuy" >
                            </div>
                        </div>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="tiencoc">Thanh toán đợt 1</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="tiencoc" type="text" ng-model="curOrder.tiencoc" placeholder="">
                            </div>
                        </div>
                        <div class="uk-margin uk-margin-remove-top uk-margin-small">
                            <label class="uk-form-label" for="giogiao">Sửa ngày nhận</label>
                            <div class="uk-form-controls">
                                <input class="uk-input datepicker" id="giogiao" type="text" placeholder="" ng-value="curOrder.giogiao+' - '+curOrder.ngaygiao">
                            </div>
                        </div>
                    </div>
                    
                    
                    <button class="uk-button uk-button-primary uk-margin-small-top" type="button">
                        Lưu lại 
                    </button>
                </form>
            </div>
        </div>
    </div>
    <h4 class="text-muted text-center" ng-if="!listDates.length"><i>Chưa có đơn hàng nào</i></h4>
</div>
<script>
    jQuery(document).ready(function($) {
        $('input.datepicker').daterangepicker({
            buttonClasses: 'uk-button uk-button-small uk-button-default',
            applyButtonClasses: 'uk-button-primary',
            autoUpdateInput: false,
            timePicker24Hour: true,
            timePicker: true,
            singleDatePicker: true,
            // locale: {
            //     format: 'YYYY-M-DD',
            //     cancelLabel: 'Hủy',
            //     applyLabel: 'Chọn'
            // }
        })
        // .on('apply.daterangepicker', function(ev, picker) {
        //     $(this).val(picker.startDate.format('YYYY-M-DD') + ' - ' + picker.endDate.format('YYYY-M-DD'));
        // });
    });
</script>