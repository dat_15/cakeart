<?php 

/**
 * 
 */
class orders_input_model extends Model{
	public function __construct(){
		parent::__construct();
		$this->db->connect_to('tinyorder');
	}

    public function getOrderByRang($from, $to){
        $sql = "SELECT HD.*,KH.tenKH, KH.sdt, date_format(convert_tz(from_unixtime(HD.thoigian),'-03:00','+12:00'),'%e/%c/%Y') AS ngaynhap, date_format(convert_tz(from_unixtime(HD.thoigian),'-03:00','+12:00'),'%H:%i') AS gionhap FROM `nhaphoadon` as HD INNER JOIN khachhang as KH ON KH.makh= HD.makh WHERE HD.thoigian BETWEEN unix_timestamp('" . $from . "') AND unix_timestamp('" . $to . "') ORDER BY HD.thoigian DESC;";
        return $this->db->query($sql);
    }

    public function update_order( $data, $where ){
    	return $this->db->update( INSERT_ORDER_TABLE, $data, $where);
    }
}
 ?>