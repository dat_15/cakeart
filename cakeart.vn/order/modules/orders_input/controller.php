<?php

class orders_input extends Controller {
    private $session;
    private $model;
    private $user_model;
    private $callbackUrl;
    private $modules = [];
    public function __construct() {
        parent::__construct(__CLASS__);
        $this->session = new Session();
        $this->model = $this->load_model();
        $this->user_model = $this->load_model('users');
        $this->callbackUrl = $this->session->get_session('goToUrl');
        if ($this->callbackUrl == '' || $this->callbackUrl == null) {
            $this->callbackUrl = DOMAIN;
        }
        if (!count($this->userInfo)) {
            header('Location: '.$this->callbackUrl);
        }
        if( !$this->user_model->isAccessibleModule('orders_input', (int) $this->userInfo['manv']) ) header('Location: '.DOMAIN);
        $this->modules = $this->user_model->getUserModules((int) $this->userInfo['manv']);
    }

    public function index() {
        $from = input('from', 'gpc', 'NOW');
        $lastWeek = new DateTime($from);
        $date = new DateTime($from);
        $lastWeek->sub(new DateInterval('P7D'));
        $res = $this->model->getOrderByRang($lastWeek->format('Y-m-d H:i:s'),$date->format('Y-m-d H:i:s'));
        //$res = $this->model->getOrderByRang($lastWeek,$date);
        $this->load_view('main-header', [
            'base_url'  => 'modules/header/',
            'title'     => 'Quản lý đơn hàng theo ngày nhập',
            'user'      => $this->userInfo
        ], 'header');
        $this->load_view('menu-top', ['modules' => $this->modules], 'header');
        $this->load_view('index', ['orders' => $res, 'from' => $date->format('Y-m-d')]);
        $this->load_view('footer', [], 'footer');
    }

    function updateOrder(){
        $order = input('order', 'P', '');
        if(empty($order) || $order === null){
            echo json_encode([
                'status' => 0,
                'msg'   => 'No param order'
            ]);
            die();
        }
        $mahd = $order['mahd'];
        unset($order['mahd']);
        unset($order['tenKH']);
        unset($order['hinhminhhoa']);
        unset($order['hinhketqua']);
        unset($order['sdt']);
        $this->model->update_order( $order, ['mahd' => $mahd ] );
        echo json_encode([
            'status' => 1,
            'msg' => 'Cập nhật thành công'
        ]);
    }

}

 ?>
