<?php 
/**
 * 
 */
class Users_model extends Model{
	protected $table;
	public function __construct(){
		parent::__construct();
		$this->table = USER_TABLE;
		$this->db->connect_to('tinyorder');
	}

	public function getUser($where){
		return $this->db->get_row($this->table, $where);
	}

	public function get_row($table, $where){
		return $this->db->get_row($this->table, $where);
	}
	public function getUsers($limit = PER_PAGE, $offset = 0){
		return $this->db->get_table($this->table, $limit, $offset);
	}
	public function getUsersActive($limit = PER_PAGE, $offset = 0, $fields = '*'){
		$query = "SELECT $fields FROM $this->table WHERE `dangdilam`=1 LIMIT $offset, $limit;";
		return $this->db->query($query);
	}
	public function queryUsers($sql){
		return $this->db->query($sql);
	}

	public function countAllUsers(){
		$total = $this->db->count_all($this->table);
		if( count($total) ){
			$total = $total["COUNT(*)"];
		}
		else {
			$total = 0;
		}
		return $total;
	}
	public function countActiveUsers(){
		$query = "SELECT COUNT(*) FROM $this->table WHERE `dangdilam`=1;";
		$total = $this->db->query($query);
		if( count($total) ){
			$total = $total[0]["COUNT(*)"];
		}
		else {
			$total = 0;
		}
		return $total;
	}

	public function updateUserData($data, $where){
		return $this->db->update( $this->table, $data, $where);
	}

	public function getUserAccessibles($uid){
		$query = "SELECT accessibleModules as AM FROM `quyennhanvien` WHERE type = (SELECT id FROM (SELECT * FROM kieunhanvien) as UT WHERE UT.id = (SELECT quyennhanvien FROM `nhanvien` WHERE manv = $uid) );";
		$res = $this->db->query ($query);
		if(is_array($res) && count($res)){
			return $res[0]['AM'];
		}
		return [];
	}


	public function getUserAccessiblesByType($type){
		$query = "SELECT accessibleModules as AM FROM `quyennhanvien` WHERE type = (SELECT id FROM (SELECT * FROM kieunhanvien) as UT WHERE UT.kieunhanvien = '$type' );";
		$res = $this->db->query ($query);
		if(is_array($res) && count($res)){
			return $res[0]['AM'];
		}
		return [];
	}

	public function isAccessibleModule($module, $uid){
		$modules = self::getUserAccessibles($uid);
		if( !count( $modules ) ) return false;
		$sql = "SELECT * FROM modules WHERE ten_modules='$module' AND id IN ( $modules );";
		$res = $this->db->query ($sql);
		return is_array($res) && count($res);
	}

	public function getUserModules($uid){
		$modules = self::getUserAccessibles($uid);
		if( !count( $modules ) ) return false;
		$sql = "SELECT * FROM modules WHERE id IN ( $modules );";
		$res = $this->db->query ($sql);
		if(is_array($res) && count($res)){
			return $res;
		}
		return [];
		$this->db->connect_to('tinyorder');
	}

	public function getUserModulesByType($type){
		$modules = self::getUserAccessiblesByType($type);
		if( !count( $modules ) ) return false;
		$sql = "SELECT * FROM modules WHERE id IN ( $modules );";
		// $this->db->VN_tranfer();
		$res = $this->db->query ($sql);
		if(is_array($res) && count($res)){
			return $res;
		}
		return [];
		$this->db->connect_to('tinyorder');
	}

	public function getUserType(){
		$this->db->VN_tranfer();
		return $this->db->get_table(USER_TYPE_TABLE);
	}

	public function updateUserType($data, $where){
		$this->db->VN_tranfer();
		return $this->db->update( USER_TYPE_TABLE , $data, $where);
	}

	public function update_module($data, $where){
		return $this->db->update( USER_MODULES_TABLE , $data, $where);
	}

	public function updateUserTypePermission($data, $where){
		return $this->db->update( USER_PERMISSION_TABLE , $data, $where);
	}

	public function get_user_type($where){
		$this->db->VN_tranfer();
		return $this->db->get_rows( USER_TYPE_TABLE, $where);
	}
	public function get_module($where){
		return $this->db->get_row( USER_MODULES_TABLE, $where);
	}

	public function addUserType($data){
		$this->db->VN_tranfer();
		return $this->db->add_row( $data, USER_TYPE_TABLE );
	}
	public function getModulesID($data){
		$sql = "SELECT id FROM `" . USER_MODULES_TABLE . "` WHERE `ten_modules` IN ('" . implode("','",$data) . "');";
		return $this->db->query($sql);
	}

	public function getModuleNames(){
		$sql = "SELECT * FROM `" . USER_MODULES_TABLE . "`;";
		return $this->db->query($sql);
	}

	public function addModule($data){
		return $this->db->add_row( $data, USER_MODULES_TABLE );
	}


	public function deleteModule($data){
		return $this->db->delete( USER_MODULES_TABLE, $data );
	}

	public function findUsers($key){
		return $this->db->get_rows(USER_TABLE, ['tendaydu like' => $key, 'quyennhanvien' => 3]);
	}
}
?>