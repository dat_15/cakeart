<?php
/**
* index controller
*/
class Profile extends Controller {
	private $session;
	private $model;
	private $callbackUrl;
	private $modules = [];
	function __construct(){
		parent::__construct(__CLASS__);
		$this->session = new Session();
		$this->model = $this->load_model(__CLASS__);
		$this->user_model = $this->load_model('users');
		$this->callbackUrl = $this->session->get_session('goToUrl');
		if(!count($this->userInfo)) {
			header('Location: '.LOGIN_PAGE);
			die;
		}
		$this->modules = $this->user_model->getUserModules((int) $this->userInfo['manv']);
	}

	function index(){
		$error = '';
		$msg = '';

		$action = input('action', 'p', '');
		if( $action ){
			$token  = input('token', 'p', '');
			if($action !== 'update-profile-action'){
				$error = 'Action đã bị thay đổi';
			}elseif($token !== $this->session->get_session('update-profile-token')){
				$error = 'Token không khớp ';
			}else{
				$tendaydu 	= input('tendaydu', 'p', '');
				$email 		= input('email', 'p', '');
				$sodt 		= input('sodt', 'p', '');
				$diachi 	= input('diachi', 'p', '');
				$ngaysinh 	= input('ngaysinh', 'p', '');
				$facebook 	= input('facebook', 'p', '');
				if(
					$tendaydu === '' || 
					$email === '' ||
					$sodt === '' ||
					$diachi === '' ||
					$ngaysinh === '' ||
					$facebook === ''
				){
					$error = 'Không được để trống các trường dưới đây!';
				}
				// elseif(!$this->checkValidUsername($username)){
				// 	$error = 'Tên đăng nhập không đúng định dạng. Chỉ chứa ký tự in hoa, in thường, chữ số và gạch dưới. Dài từ 8-30 ký tự.';
				// }
				// elseif(!$this->checkValidPassword($password)){
				// 	$error = 'Mật khẩu không đúng định dạng. Chứa ít nhất 1 ký tự in hoa, in thường, chữ số. Độ dài từ 8-30 ký tự. Không chứa ký tự đặc biệt ngoài ! @ #';
				// }
				else{
					$dataUpdate = [
						'tendaydu' 	=> $tendaydu,
						'email' 	=> $email,
						'sodt' 		=> $sodt,
						'diachi' 	=> $diachi,
						'ngaysinh' 	=> $ngaysinh,
						'facebook' 	=> $facebook
					];
					$this->model->updateUserData($dataUpdate, ['manv' => $this->userInfo['manv']]);
					
					if(
						isset($_FILES['hinh']) &&
						$_FILES['hinh']['error'] == 0 && 
						$_FILES['hinh']['size'] > 0
					){
						$allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
						$detectedType = exif_imagetype($_FILES['hinh']['tmp_name']);
						if ($_FILES["hinh"]["size"] > 5000000) {
						    echo $error = "File bạn chọn quá lớn.";
						}
						elseif(!in_array($detectedType, $allowedTypes)){
							$error = 'File bạn chọn không đúng định dạng';
						}else{
							unlink(__DIR__.'/../../../intranet/images/nhanvien/'.$this->userInfo['hinh']);
							self::uploadImage();
						}
					}
					$this->check_auth();
					$msg = 'Cập nhật thành công';
				}
			}
		}
		$token = md5($this->randomToken(10).time());
		$this->session->set_session('update-profile-token', $token);
		$this->load_view('header-no-angular', [
			'base_url' 	=> 'modules/header/', 
			'title' 	=> 'Thông tin cá nhân',
			'user' 		=> $this->userInfo
		], 'header');
		$this->load_view('menu-top',['modules' => $this->modules], 'header');
		$this->load_view('index', [
			'user' 		=> $this->userInfo,
			'error' 	=> $error,
			'msg' 		=> $msg,
			'token' 	=> $token,
		]);
		$this->load_view('footer',[], 'footer');
	}

	private function uploadImage(){
		$file = $_FILES['hinh']['tmp_name']; 
		$sourceProperties = getimagesize($file);
        $fileNewName = time();
        $folderPath = __DIR__.'/../../../intranet/images/nhanvien/';
        $ext = pathinfo($_FILES['hinh']['name'], PATHINFO_EXTENSION);
        $imageType = $sourceProperties[2];
        switch ($imageType) {

            case IMAGETYPE_PNG:
                $imageResourceId = imagecreatefrompng($file); 
                $file = self::imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                imagepng($file,$folderPath. $fileNewName. ".". $ext);
                break;

            case IMAGETYPE_GIF:
                $imageResourceId = imagecreatefromgif($file); 
                $file = self::imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                imagegif($file,$folderPath. $fileNewName. ".". $ext);
                break;

            case IMAGETYPE_JPEG:
                $imageResourceId = imagecreatefromjpeg($file); 
                $file = self::imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                imagejpeg($file,$folderPath. $fileNewName. ".". $ext);
                break;

            default:
                exit;
                break;
        }
        move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
		$this->model->updateUserData(
			['hinh' => $fileNewName. ".". $ext], 
			['manv' => $this->userInfo['manv'] ]
		);
	}

	private function imageResize($imageResourceId,$width,$height) {
	    $targetWidth =200;
	    $targetHeight =200;
	    $file=imagecreatetruecolor($targetWidth,$targetHeight);
	    imagecopyresampled($file,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $width,$height);
	    return $file;
	}
}