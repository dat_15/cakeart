<script src="assets/lib/vuejs/vue.min.js?v=<?php echo time(); ?>"></script>
<script src="assets/lib/vuejs/vue-resource.min.js?v=<?php echo time(); ?>"></script>
<div class="uk-section" id="user-profile">
	<div class="uk-container">
		<div class="uk-margin-medium-top" uk-grid>
			<div class="uk-width-2-3@m" >
				<?php if ($error !== ''): ?>
	            	<div class="uk-alert-warning uk-padding-small" uk-alert>
	            		<a class="uk-alert-close" uk-close></a>
	            		<span uk-icon="warning"></span> 
	            		<?php echo $error; ?>
	            	</div>
	            <?php endif ?>
	            <?php if ($msg !== '' && $msg !== NULL): ?>
	            	<div class="uk-alert-success uk-padding-small" uk-alert>
	            		<a class="uk-alert-close" uk-close></a>
	            		<span uk-icon="check"></span> <?php echo $msg; ?>
	            	</div>
	            <?php endif ?>
				<form action="" method="POST" enctype="multipart/form-data">
					<input type="hidden" name="token" value="<?php echo $token;?>" required>
	            	<input type="hidden" name="action" value="update-profile-action" required>
					<div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
					    <div>
					    	<div class="uk-inline">
							    <span class="uk-form-icon" uk-icon="icon: user"></span>
							    <input 
							    	class="uk-input" 
							    	name="tendaydu" 
							    	v-model="user.tendaydu" 
							    	type="text" 
							    	placeholder="Nguyễn Văn A" 
							    	required>
					    	</div>
					    </div>
					    <div>
					    	<div class="uk-inline">
							    <span class="uk-form-icon" uk-icon="icon: receiver"></span>
							    <input 
							    	class="uk-input" 
							    	name="sodt" 
							    	v-model="user.sodt" 
							    	type="text" 
							    	placeholder="01234567890" 
							    	required>
							</div>
					    </div>
					    <div>
					    	<div class="uk-inline">
							    <span class="uk-form-icon" uk-icon="icon: location"></span>
							    <input 
							    	class="uk-input" 
							    	name="diachi" 
							    	v-model="user.diachi" 
							    	type="text" 
							    	placeholder="Số 183B Trần Quốc Thảo" 
							    	required>
							</div>
					    </div>
					</div>
					<div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
					    <div>
					    	<div class="uk-inline">
							    <span class="uk-form-icon" uk-icon="icon: mail"></span>
							    <input 
							    	class="uk-input" 
							    	type="email" 
							    	name="email" 
							    	v-model="user.email" 
							    	placeholder="example@email.com" 
							    	required>
						    </div>
					    </div>
					    <div>
						    <div class="uk-inline">
							    <span class="uk-form-icon" uk-icon="icon: facebook"></span>
							    <input 
							    	class="uk-input" 
							    	name="facebook" 
							    	v-model="user.facebook" 
							    	type="text" 
							    	placeholder="Facebook/Skype">
						    </div>
					    </div>
					    <div>
						    <div class="uk-inline">
							    <span class="uk-form-icon" uk-icon="icon: calendar"></span>
							    <input 
							    	class="uk-input" 
							    	name="ngaysinh" 
							    	v-model="user.ngaysinh" 
							    	type="text" 
							    	placeholder="1990/12/31" 
							    	required>
						    </div>
					    </div>
					</div>
				    <button class="uk-button uk-button-primary uk-margin-small-top" type="submit">
				    	Cập nhật thông tin
				    </button>
				    <div class="js-upload  uk-margin-small-top" uk-form-custom>
					    <input type="file" name="hinh" v-on:change="uploadImage">
					    <button class="uk-button uk-button-default" type="button" tabindex="-1">Chọn avatar</button>
					</div>

				</form>
			</div>
			<div class="uk-width-1-3@m" >
				<!-- Hiển thị chi tiết gợi ý -->
	    		<div class="uk-card uk-card-default uk-padding-remove uk-card-hover">

	    			<!-- Header -->
		    		<div class="uk-card-header uk-padding-small">
		    			<div class="uk-grid-small uk-flex-middle" uk-grid>
				            <div class="uk-width-auto">
				                <span 
				                	class="uk-icon-button" 
				                	uk-icon="icon: user" 
				                	v-if="user.hinh == ''">
			                	</span>
			                	<img 
			                		class="uk-border-circle" 
			                		width="40" height="40" 
			                		v-bind:src="user.hinh" 
			                		v-bind:alt="user.tendaydu" 
			                		v-if="user.hinh !== ''">
				            </div>
				            <div class="uk-width-expand">
							    <h5 class="no-wrap uk-margin-small-bottom" v-bind:uk-tooltip="user.tendaydu">
							    	{{user.tendaydu}}
							    </h5>
							    <p class="uk-text-meta uk-margin-remove-top">
				                	<time>
				                		{{user.ngaybatdau}}
				                	</time>
				                </p>
				            </div>
				        </div>
				    </div>

				    <!-- Body -->
				    <div class="uk-card-body uk-padding-small">
				    	<h6 class="uk-margin-small-top uk-margin-remove-bottom">
				    		<span class="uk-margin-small-left" uk-icon="icon: mail"></span> 
							<i>{{user.email == '' ? 'Chưa có thông tin' : user.email}}</i>
				    	</h6>
				    	<h6 class="uk-margin-small-top uk-margin-remove-bottom">
				    		<span class="uk-margin-small-left" uk-icon="icon: receiver"></span> 
							<i>{{user.sodt == '' ? 'Chưa có thông tin' : user.sodt}}</i>
				    	</h6>
				    	<h6 
				    		class="no-wrap uk-margin-small-top uk-margin-remove-bottom" 
				    		v-bind:uk-tooltip="user.diachi == '' ? 'Chưa có thông tin' : user.diachi">
				    		<span class="uk-margin-small-left" uk-icon="icon: location"></span> 
							<i>{{user.diachi == '' ? 'Chưa có thông tin' : user.diachi}}</i>
				    	</h6>
				    </div>
	    		</div>
			</div>
		</div>
	</div>
</div>
<script>

	var app = new Vue({
	  	el: '#user-profile',
	  	data:  {
		    user: {
		    	tendaydu: "<?php echo $user['tendaydu'];?>",
		    	email: "<?php echo $user['email'];?>",
		    	sodt: "<?php echo $user['sodt'];?>",
		    	diachi: "<?php echo $user['diachi'];?>",
		    	ngaybatdau: "<?php echo $user['ngaybatdau'];?>",
		    	ngaysinh: "<?php echo $user['ngaysinh'];?>",
		    	facebook: "<?php echo $user['facebook'];?>",
		    	hinh: "<?php echo $user['hinh'] == '' ? '' : EMPLOYER_IMAGE_PATH . $user['hinh'];?>",
		    }
	  	},

	  	methods: {
	  		uploadImage: function(event){
	  			var reader, file = document.querySelector('input[type=file]').files[0], _this = this;
	  			if (!file.type.match(/image.*/)) {
	  				UIkit.notification({
					    message:`<span uk-icon='icon: warning'></span> File bạn chọn không phải hình ảnh.`,
					    status: 'warning',
					    pos: 'top-right',
					    timeout: 1500
					});
					event.value = '';
					document.querySelector('input[type=file]').value = '';
      			} else{
      				if ( window.FileReader ) {
					  	reader = new FileReader();
					  	reader.onloadend = function (e) { 
					    	_this.user.hinh = e.target.result;
					  	};
					  	reader.readAsDataURL(file);
					}
      			}
	  		}
	  	}
	})
</script>
</body>
</html>