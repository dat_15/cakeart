<?php 
/**
 * 
 */
class Profile_model extends Model{
	protected $table;
	public function __construct(){
		parent::__construct();
		$this->table = USER_TABLE;
		$this->db->connect_to('tinyorder');
	}

	public function getUser($data){
		return $this->db->get_row($this->table, $data);
	}

	public function updateUserData($data, $where){
		return $this->db->update( $this->table, $data, $where);
	}
}
?>