<?php 

/**
 * 
 */
class productionorder_model extends Model{
	public function __construct(){
		parent::__construct();
		$this->db->connect_to('tinyorder');
	}

    public function getOrderByRang($from, $to){
        $sql = "SELECT HD.*,KH.tenKH, KH.sdt  FROM `nhaphoadon` as HD INNER JOIN khachhang as KH ON KH.makh= HD.makh WHERE DATE_FORMAT(STR_TO_DATE(HD.ngaygiao, '%d/%m/%Y'), '%Y-%m-%d') BETWEEN '" . $from . "' AND '" . $to . "' and lcase(loaibanh) not like 'ship' ORDER BY DATE_FORMAT(STR_TO_DATE(HD.ngaygiao, '%d/%m/%Y'), '%Y-%m-%d'), HD.giogiao ASC;";
        return $this->db->query($sql);
    }

    public function getOrderByID($order_id){
        $sql = "SELECT HD.*,KH.tenKH, KH.sdt  FROM `nhaphoadon` as HD INNER JOIN khachhang as KH ON KH.makh= HD.makh WHERE HD.mahd=$order_id;";
        return $this->db->query($sql);
    }

    public function getWorkOfOrder($order_id){
        return $this->db->get_rows(WORK_ORDER_TABLE, ['mahd' => (int) $order_id]);
    }

    public function getWorkOfOrderByUser($order_id, $nhavien){
        return $this->db->get_rows(WORK_ORDER_TABLE, ['mahd' => (int) $order_id, 'nhanvien' => (int) $nhanvien]);
    }

    public function addNewTaskToDB($data){
        return $this->db->add_row($data, WORK_ORDER_TABLE);
    }
    
    public function deleteTask($where){
        return $this->db->delete(WORK_ORDER_TABLE, $where);
    }

    public function updateTask($data, $where){
        return $this->db->update(WORK_ORDER_TABLE, $data, $where);
    }
}
 ?>