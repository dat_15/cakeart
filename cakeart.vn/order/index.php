<?php
error_reporting(E_ALL);
session_start();
date_default_timezone_set('Asia/Ho_Chi_Minh');
// Define base url
require 'bootstrap/const.php';
require 'bootstrap/db.php';
require 'bootstrap/bootstrap.php';
require 'bootstrap/controller.php';
require 'bootstrap/model.php';
require 'bootstrap/session.php';
require 'bootstrap/pagination.php';
require 'bootstrap/functions.php';

$app = new Bootstrap();
