-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Máy chủ: mysql.tinycake.vn
-- Thời gian đã tạo: Th4 09, 2018 lúc 07:07 PM
-- Phiên bản máy phục vụ: 5.6.34-log
-- Phiên bản PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `tinyorder`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nhanvien`
--

CREATE TABLE `nhanvien` (
  `manv` int(11) NOT NULL,
  `tendangnhap` varchar(20) NOT NULL,
  `matkhau` varchar(20) NOT NULL,
  `tendaydu` varchar(60) NOT NULL,
  `quyen` int(1) NOT NULL COMMENT '0: lịch giao bánh, 1: admin thường, 2: yummy order, 3:checkin',
  `admin` int(1) NOT NULL COMMENT '=1 : xem được thông tin đặc biệt',
  `thongbao` varchar(500) NOT NULL,
  `sodt` varchar(12) NOT NULL,
  `ngaysinh` date NOT NULL,
  `diachi` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `hinh` varchar(200) NOT NULL,
  `ngaybatdau` date NOT NULL,
  `mucluongthang` double NOT NULL,
  `mucluonggio` double NOT NULL,
  `dangdilam` tinyint(1) NOT NULL,
  `digiaobanh` tinyint(1) NOT NULL,
  `ngaynghiviec` date NOT NULL,
  `hometown` varchar(200) NOT NULL,
  `congviec` varchar(10) NOT NULL,
  `note` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `nhanvien`
--

INSERT INTO `nhanvien` (`manv`, `tendangnhap`, `matkhau`, `tendaydu`, `quyen`, `admin`, `thongbao`, `sodt`, `ngaysinh`, `diachi`, `email`, `facebook`, `hinh`, `ngaybatdau`, `mucluongthang`, `mucluonggio`, `dangdilam`, `digiaobanh`, `ngaynghiviec`, `hometown`, `congviec`, `note`) VALUES
(1, 'quyentran', 'Tiny@#1234567', 'Tráº§n Thá»‹ Thu QuyÃªn', 1, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 6500000, 0, 0, 0, '0000-00-00', '', 'sales', ''),
(2, 'Yummy', 'Yummy@123456', 'Yummy', 2, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 0, 0, 0, 0, '0000-00-00', '', '', ''),
(3, 'tiendung', 'Tiny@#1234567', 'Tráº§n Tiáº¿n DÅ©ng', 1, 1, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 1, 0, 1, 1, '0000-00-00', '', 'sales', ''),
(4, 'tinypretty', 'Tiny12345', 'tinypretty', 0, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 0, 0, 0, 0, '0000-00-00', '', 'sales', ''),
(5, 'Tiny Mooncake', 'Tiny@#123456', 'Tiny Mooncake', 1, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 0, 0, 0, 0, '0000-00-00', '', 'sales', ''),
(6, 'Tiny Teabreak', 'Tiny1234', 'Tiny Teabreak', 1, 1, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 0, 0, 0, 0, '0000-00-00', '', '', ''),
(7, 'Tiny Newyear', 'Tiny1234', 'Tiny Newyear', 3, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 0, 0, 0, 0, '0000-00-00', '', '', ''),
(8, 'huanpham', 'Tiny1234', 'Pháº¡m Huy HuÃ¢n', 0, 0, '', '', '1994-08-18', '', '', '', '', '0000-00-00', 4000000, 0, 0, 0, '2017-03-31', '', '', ''),
(9, 'huynguyen', 'Tiny1234', 'Nguyá»…n HoÃ ng Huy', 0, 0, '', '', '1993-11-04', '', '', '', '', '2013-03-18', 4300000, 0, 1, 0, '0000-00-00', '', '', ''),
(13, 'vananh', 'Tiny@#1234567', 'Nguyá»…n Thá»‹ VÃ¢n Anh', 1, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 1, 0, 1, 0, '0000-00-00', '', 'sales', ''),
(17, 'thuyanh', 'Tiny@1234#', 'ThuÃ½ Anh', 0, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 0, 0, 0, 0, '0000-00-00', '', '', ''),
(20, 'Tram', 'Tram123456', 'TrÆ°Æ¡ng Ngá»c TrÃ¢m', 3, 0, '', '01636561303', '1995-01-19', '220-78 XÃ´ Viáº¿t Nghá»‡ TÄ©nh P21 Q. BÃ¬nh Tháº¡nh', 'truongngoctram95@gmail.com', 'TrÃ¢m TrÆ°Æ¡ng', '', '2015-12-07', 0, 18000, 1, 0, '0000-00-00', '', '', 'before 26/5 - 16500'),
(21, 'NhutAnh', 'NhutAnh123456', 'Pháº¡m Nhá»±t Anh', 3, 0, '', '0903184379', '1996-10-31', '108 ÄÆ°á»ng 3158A Pháº¡m Tháº¿ Hiá»ƒn P7 Q8', 'pna.phamnhutanh@gmail.com', '0903184379', '', '2016-05-03', 0, 18000, 1, 0, '0000-00-00', 'Sai gon', '', '31/5 15000'),
(22, 'Trang', 'Trang123456', 'Nguyá»…n Thá»‹ Trang', 3, 0, '', '01868497660', '1997-12-11', '68/24/8 PhÆ°á»ng Thá»›i An, Quáº­n 12, TPHCM', 'Lenguyentrangnhan@gmail.com', 'Aris Nguyen', '', '2016-05-03', 0, 17000, 0, 0, '0000-00-00', 'Nam Äá»‹nh', 'Upload', '30/6/2016-15000'),
(23, 'kieutran', 'CannotKieu123456', 'Tráº§n Thá»‹ Diá»…m Kiá»u', 3, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 0, 0, 0, 0, '0000-00-00', '', '', ''),
(24, 'KimNgan', 'KimNgan123456', 'Nguyá»…n Thá»‹ Kim NgÃ¢n - 0903964259', 3, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 0, 0, 0, 0, '0000-00-00', '', '', ''),
(26, 'TrucNha', 'TrucNha123456', 'BÃ¹i HÃ  TrÃºc NhÃ£ - 01204895629', 3, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 0, 0, 0, 0, '0000-00-00', '', '', ''),
(27, 'NgocLuan', 'LuanTiny1234', 'Nguyá»…n Ngá»c LuÃ¢n', 3, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 0, 0, 0, 0, '0000-00-00', '', '', ''),
(28, 'ThachThao', 'ThachThao123456', 'Phan Tháº¡ch Tháº£o - 09617448', 3, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 0, 0, 0, 0, '0000-00-00', '', '', ''),
(29, 'HanhVy', 'HanhVy123456', 'Nguyá»…n Thá»‹ Háº¡nh Vy - 096', 3, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 0, 0, 0, 0, '0000-00-00', '', '', ''),
(30, 'MyAnh', 'MyAnh123456', 'Tráº§n Thá»‹ Má»¹ Anh - 01268882364', 3, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 0, 15000, 0, 0, '0000-00-00', '', '', ''),
(31, 'LyDang', 'LyDang1234', 'Äáº·ng Thá»‹ Ly', 3, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 0, 0, 0, 0, '0000-00-00', '', '', ''),
(32, 'PhuongThuy', 'PhuongThuy1234', 'ÄÃ o PhÆ°Æ¡ng ThÃ¹y', 3, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 0, 0, 0, 0, '0000-00-00', '', '', ''),
(33, 'HienVo', 'HienVo1234', 'VÃµ Thá»‹ Hiá»n', 3, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 0, 0, 0, 0, '0000-00-00', '', '', ''),
(35, 'DeHuynh', 'DeHuynh1234', 'Huá»³nh Táº¥n Äá»‡', 3, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 0, 0, 0, 0, '0000-00-00', '', '', ''),
(36, 'TienLe', 'TienLe1234', 'LÃª Ngá»c Thá»§y TiÃªn - 01207633848', 3, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 0, 15000, 0, 0, '0000-00-00', '', '', ''),
(37, 'AnhDuong', 'AnhDuong1234', 'DÆ°Æ¡ng Ngá»c Anh - 0169972526', 3, 0, '', '01662774024', '1995-01-27', '52/13 Khu phố 3 phường Tân Kiểng Q7', 'fourleafclover271@gmail.com', 'Ánh Dương', '', '2016-06-13', 0, 15000, 1, 0, '0000-00-00', '', '', ''),
(38, 'TuyenLe', 'TuyenLe1234', 'LÃª Thá»‹ Kim Tuyá»n', 3, 0, '', '0988112608', '1996-03-24', '262 Ä‘Æ°á»ng sá»‘ 6 nguyá»…n VÄƒn Nghi P7 GÃ² Váº¥p', 'kimtuyenle9669@gmail.com', 'Kim Tuyen Le', '', '2016-08-01', 0, 15000, 1, 0, '0000-00-00', '', '', ''),
(39, 'DuyenTran', 'DuyenTran1234', 'Tráº§n Thá»‹ Linh DuyÃªn - 0906612764', 3, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 0, 0, 0, 0, '0000-00-00', '', '', ''),
(40, 'BaoTran', 'BaoTran1234', 'VÅ© Nguyá»…n Báº£o TrÃ¢n - 0907947705', 3, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 0, 15000, 0, 0, '0000-00-00', '', '', ''),
(41, 'NhiDao', 'NhiDao1234', 'ÄÃ o Nguyá»…n BÄƒng Nhi - 0904398610', 0, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 0, 0, 0, 0, '0000-00-00', '', '', ''),
(42, 'ThanhDoan', 'ThanhDoan1234', 'ÄoÃ n Thanh', 0, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 0, 0, 0, 0, '0000-00-00', '', '', ''),
(43, 'ThoaPham', 'ThoaPham1234', 'Pháº¡m Thá»‹ Kim Thoa', 3, 0, '', '01626177422', '1997-03-30', '377-20 CÃ¡ch Máº¡ng ThÃ¡ng TÃ¡m P12 Q10', 'thoapham300397@gmail.com', 'Thoa Pham', '', '2016-08-01', 0, 15000, 0, 0, '0000-00-00', '', '', ''),
(44, 'NganNguyen', 'NganNguyen1234', 'Nguyá»…n PhÆ°á»›c ThiÃªn NgÃ¢n', 3, 0, '', '01207061179', '1997-04-12', '138-41A TrÆ°Æ¡ng CÃ´ng Äá»‹nh P14, TÃ¢n BÃ¬nh', 'thienngan997@gmail.com', '01207061179', '', '2016-11-22', 0, 15000, 0, 0, '0000-00-00', '', '', ''),
(45, 'QuynhNguyen', 'QuynhNguyen1234', 'Nguyá»…n Nháº­t TrÃºc Quá»³nh', 3, 0, '', '0985275520', '1997-01-17', '84-20k HÃ²a HÆ°ng p13 q10', 'nguyenquynh1701@gmail.com', 'truc quynh nguyen', '', '2016-11-18', 0, 15000, 1, 0, '0000-00-00', '', '', ''),
(46, 'NiTran', 'NiTran1234', 'Tráº§n Ngá»c ThÃ¹y Ni', 3, 0, '', '0946990303', '1995-02-11', '12 TÃ´n Äáº£n P13 Q4', 'tranngocthuyni@gmail.com', 'ny tran', '', '2016-10-25', 0, 15000, 0, 0, '0000-00-00', '', '', ''),
(47, 'NgocMai', 'NgocMai1234', 'Mai Thá»‹ BÃ­ch Ngá»c', 3, 0, '', '01654464574', '1998-02-14', '421 Tráº§n XuÃ¢n Soáº¡n Q7', 'bichngoc142198@gmail.com', 'bichngoc142198@gmail.com', '', '2016-11-28', 3500000, 0, 0, 0, '2016-12-11', '', '', ''),
(48, 'LoanTran', 'LoanTran1234', 'Tráº§n Sá»Ÿ Loan', 3, 0, '', '', '1978-01-01', '', '', '', '', '2016-08-01', 6000000, 0, 0, 0, '0000-00-00', '', '', ''),
(49, 'KhaiNguyen', 'KhaiNguyen1234', 'Nguyá»…n Tuáº¥n Kháº£i', 3, 0, '', '', '1978-01-01', '', '', '', '', '2016-08-01', 6000000, 0, 1, 0, '0000-00-00', '', '', ''),
(50, 'KhaDo', 'KhaDo1234', 'Äá»— TrÃ¢m Kha', 3, 0, '', '0917115372', '1997-12-23', '264/58 LÃª Quang Äá»‹nh P11 Q. BÃ¬nh Tháº¡nh', '', 'Tram Kha', '', '2016-12-08', 0, 15000, 1, 0, '0000-00-00', '', '', ''),
(51, 'ThaoDinh', 'ThaoDinh1234', 'Äinh Thu Tháº£o', 3, 0, '', '01676251674', '1995-10-27', '325/37/26 Báº¡ch Äáº±ng, BÃ¬nh Tháº¡nh ', 'dinhthuthao2710@gmail.com', 'Dinh Thu Thao', '', '2016-11-15', 0, 15000, 0, 0, '0000-00-00', '', '', ''),
(52, 'DiHa', 'DiHa1234', 'DÃ¬ HÃ ', 3, 0, '', '', '1978-01-01', 'VÆ°á»n chuá»‘i', '', '', '', '2016-11-15', 5000000, 0, 1, 0, '0000-00-00', '', '', ''),
(53, 'TrucPham', 'TrucPham1234', 'Pháº¡m Tráº§n My TrÃºc', 3, 0, '', '0975390770', '1993-08-03', '16 TÃ¢n Thuáº­n TÃ¢y Q7', 'phamtranmytruc.38@gmail.com', 'MyMy Truc', '', '2016-12-17', 0, 15000, 0, 0, '0000-00-00', '', '', ''),
(54, 'YenNguyen', 'YenNguyen1234', 'Nguyá»…n HoÃ ng Yáº¿n', 3, 0, '', '0908766882', '1991-02-08', '8/7 Nguyá»…n Thiá»‡n Thuáº­t P24 BÃ¬nh Tháº¡nh', 'hyen.nhy@gmail.com', 'Yen Hoang Nguyen', '', '2016-12-20', 0, 15000, 0, 0, '0000-00-00', '', '', ''),
(55, 'LeQuyen', 'LeQuyen1234', 'Tráº§n Thá»‹ Lá»‡ QuyÃªn', 3, 0, '', '0901313171', '1985-06-17', '24/4 Khu Phá»‘ HÃ²a Long, LÃ¡i ThiÃªu, Thuáº­n An, BÃ¬nh DÆ°Æ¡ng', 'quyentran789@gmail.com', 'quyentran789@gmail.com', '', '2016-12-21', 0, 15000, 0, 0, '0000-00-00', '', '', ''),
(56, 'HaTruong', 'HaTruong1234', 'TrÆ°Æ¡ng Thá»‹ HÃ ', 3, 0, '', '01679432692', '1996-09-08', '26/6 ÄÆ°á»ng sá»‘ 3, CÆ° XÃ¡ ÄÃ´ ThÃ nh, Q3', 'truongha.ueh@gmail.com', '01679432692', '', '2016-12-23', 0, 15000, 0, 0, '0000-00-00', '', '', ''),
(57, 'PhuongPham', 'PhuongPham1234', 'Pháº¡m Minh PhÆ°Æ¡ng', 3, 0, '', '0932746196', '1987-03-16', '49/40/38 Trá»‹nh ÄÃ¬nh Trá»ng, TÃ¢n PhÃº', 'minhphuong_sypham@yahoo.com', 'spham minhphuong', '', '2016-12-21', 0, 15000, 0, 0, '0000-00-00', '', '', ''),
(58, 'HieuVo', 'HieuVo1234', 'VÃµ Minh Hiáº¿u', 4, 0, '', '0906786841', '1995-09-22', 'Táº¡m trÃº: 8/1 B1 Quang Trung P8 GÃ² Váº¥p', 'ronyoo.orion@gmail.com', 'Martin Orion 100010232053322', '', '2016-09-01', 0, 15000, 1, 1, '0000-00-00', '', 'Delivery', ''),
(59, 'GiangDoan', 'GiangDoan1234', 'ÄoÃ n Thá»‹ HoÃ i Giang', 3, 0, '', '0937060495', '1993-04-20', '13 ÄÆ°á»ng 27 P BÃ¬nh An, Q2', 'jaydoan4@gmail.com', 'hailey vo ', '', '2017-01-02', 0, 15000, 1, 0, '0000-00-00', '', '', ''),
(60, 'BachCao', 'BachCao1234', 'Cao Thá»‹ NhÃ£ Báº¡ch', 3, 0, '', '0965005093', '1990-09-26', '322 LÃ½ ThÆ°á»ng Kiá»‡t P6 TÃ¢n BÃ¬nh', 'ctnb.bach@gmail.com', 'NhÃ£ Báº¡ch', '', '2017-01-02', 5000000, 0, 1, 0, '0000-00-00', '', '', ''),
(61, 'NhungNgo', 'NhungNgo1234', 'NgÃ´ Thá»‹ Ngá»c Nhung', 3, 0, '', '0977327090', '1995-11-01', '26D3 CÆ° XÃ¡ Äiá»‡n Lá»±c, Tráº§n NÃ£o P.BÃ¬nh An QuÃ¢n 2', 'hongnhung95.nn@gmail.com', 'Nhung Ngo', '', '2017-01-14', 0, 15000, 1, 0, '0000-00-00', 'Nam Äá»‹nh', '', ''),
(62, 'HaoTran', 'HaoTran123', 'Tráº§n KiÃªn Háº£o', 1, 0, '', '0978202008', '1983-04-16', 'ÄÆ°á»ng 13 P4 Q8', 'haohn2005@gmail.com', 'Tran Kien Hao', '', '2017-02-07', 3000000, 0, 1, 1, '0000-00-00', 'ChÃ¢u Äá»‘c', 'IT', ''),
(63, 'NgaDo', 'NgaDo1234', 'Äá»— Huá»³nh Báº£o Nga', 1, 0, '', '0906275307', '1987-07-29', '47/11 NguyÃªn Há»“ng P11 BÃ¬nh Tháº¡nh', 'baonga87@yahoo.com', 'https://www.facebook.com/baonga297', '', '2017-02-06', 4000000, 0, 0, 0, '2017-03-01', 'SÃ i gÃ²n', 'CS', ''),
(64, 'NgocHoang', 'NgocHoang1234', 'HoÃ ng Thá»‹ Linh Ngá»c', 1, 0, '', '0982250790', '1990-07-25', '105 Ä‘Æ°á»ng T6 PhÆ°á»ng TÃ¢y Tháº¡nh TÃ¢n PhÃº', 'hoanglinhngoc2507@yahoo.com', 'https://www.facebook.com/ngoc.hoang.31945', '', '2017-03-03', 4000000, 0, 1, 0, '0000-00-00', 'SÃ i gÃ²n', 'CS', '5 tr'),
(65, 'SyNguyen', 'SyNguyen1234', 'Nguyá»…n Tiáº¿n Sá»¹', 1, 0, '', '01676697419', '1993-06-15', '285/1 Nguyá»…n VÄƒn Trá»—i P10 PhÃº Nhuáº­n', 'NguyenSy.Lau@gmail.com', 'https://www.facebook.com/Sy.Anh1506', '', '2017-03-02', 4000000, 0, 1, 1, '0000-00-00', 'DakLak', 'CS', ''),
(66, 'ThienNguyen', 'ThienNguyen1234', 'Nguyá»…n VÄƒn ThiÃªn', 3, 0, '', '01646761295', '2000-11-01', 'ÄÆ°á»ng M TÃ¢y Tháº¡nh', 'nguyenvanthien769@gmail.com', 'https://www.facebook.com/profile.php?id=100015672451501', '', '2017-02-05', 3200000, 0, 1, 1, '0000-00-00', 'Ninh Thuáº­n', '', ''),
(67, 'AnVan', 'AnVan1234', 'VÄƒn Thá»‹ Ngá»c An', 3, 0, '', '0907335102', '1993-08-06', '86 Nguyá»…n ThÃ´ng P9 Q3', 'an.ngoc93@gmail.com', 'https://www.facebook.com/an.ngoc93', '', '2017-03-29', 4000000, 0, 0, 0, '0000-00-00', 'BÃ¬nh Thuáº­n', '', ''),
(68, 'PhuongHuynh', 'PhuongHuynh1234', 'Huá»³nh Yáº¿n PhÆ°Æ¡ng', 3, 0, '', '0903946248', '1989-04-25', '80A Ã‚u CÆ¡, P9 TÃ¢n BÃ¬nh', 'yphuong.sky@gmail.com', 'https://www.facebook.com/profile.php?id=100007525357270', '', '2017-03-27', 5000000, 0, 0, 0, '0000-00-00', 'SÃ i GÃ²n', '', 'start 4 mil, normal 5 mil, not sign contact, busy study at night, only work office hours'),
(69, 'guest', 'guest1234', 'KhÃ¡ch ghÃ© láº¥y', 0, 0, '', '', '0000-00-00', '', '', '', '', '0000-00-00', 0, 0, 1, 1, '0000-00-00', '', '', ''),
(70, 'PhatLe', 'PhatLe1234', 'LÃª Minh PhÃ¡t', 0, 0, '', '01295658213', '2001-05-04', '231/57/15 BÃ¬nh TiÃªn P8 Q6', 'likeandroid123456@gmail.com', 'https://www.facebook.com/minhphat.le.794', '', '2017-05-14', 0, 14000, 0, 1, '0000-00-00', '', '', ''),
(71, 'HongTang', 'HongTang1234', 'TÄƒng Cháº¥n Há»“ng', 0, 0, '', '0938151701', '2001-04-23', '73/5E VÄƒn ThÃ¢n P6 Q6', 'hongtang240@gmail.com', 'https://www.facebook.com/chanhong.tang', '', '2017-05-14', 0, 14000, 0, 1, '0000-00-00', '', '', ''),
(72, 'TanNguyen', 'TanNguyen1234', 'Nguyá»…n Minh TÃ¢n', 0, 0, '', '01222728191', '2001-01-25', '133/21/49/21 Ä‘Æ°á»ng sá»‘ 41 P16 q8', '', '', '', '2017-05-14', 0, 14000, 0, 1, '0000-00-00', '', '', ''),
(73, 'KieuPham', 'KieuPham1234', 'Pháº¡m Thá»‹ Kim Kiá»u', 0, 0, '', '01212912363', '1995-08-31', '1170/25 ÄÆ°á»ng 3/2 P12 Q11', '', 'kim kieu', '', '2017-06-02', 3500000, 0, 1, 0, '0000-00-00', '', '', ''),
(74, 'AnDang', 'AnDang1234', 'Dang Quoc An', 0, 0, '', '0981411602', '1996-12-10', '12D Quan Tre, Trung My Tay Q12', 'dangquocan12100510@gmail.com', '', '', '2017-07-04', 0, 18000, 1, 0, '0000-00-00', '', '', ''),
(75, 'Hoan', 'Hoan', 'Phan CÃ´ng Hoan', 0, 0, '', '01677892829', '1999-11-09', '134 Ä‘Æ°á»ng sá»‘ 3, cÆ° xÃ¡ Lá»¯ Gia P15 Q11', 'hoanphan911@gmail.com', '', '', '2017-08-01', 0, 17000, 1, 0, '0000-00-00', '', '', ''),
(76, 'ThanhNguyen', 'ThanhNguyen1234', 'Nguyá»…n Táº¥n ThÃ nh', 0, 0, '', '0939666062', '1987-06-16', '238/12 Äá»™i Cung P9 Q11', '', 'Thanh Nguyen', '', '2017-07-04', 1750000, 0, 1, 0, '0000-00-00', '', '', ''),
(77, 'AnPham', 'AnPham1234', 'Pháº¡m PhÆ°á»›c An', 0, 0, '', '01682455771', '1991-05-16', '21/41B Nguyá»…n Bá»‰nh KhiÃªm P Báº¿n NghÃ© Q1', 'anpham16051991@gmail.com', 'https://www.facebook.com/an.pham.16?ref=br_rs', '', '2017-08-01', 8000000, 0, 1, 0, '0000-00-00', 'Má»¹ TÃ¢n, Má»¹ Lá»™c, Tam BÃ¬nh VÄ©nh Long', '', ''),
(78, 'TrangLu', 'TrangLu1234', 'Lá»¯ Thá»‹ ThÃ¹y Trang', 1, 0, '', '01646183377', '1993-01-02', '115/62 LÃª Trá»ng Táº¥n P. SÆ¡n Ká»³, TÃ¢n PhÃº', 'luthithuytrang98@gmail.com', 'https://www.facebook.com/trang.thuy.33234571', '', '2017-08-18', 0, 17000, 1, 1, '0000-00-00', 'Thá»§ Thá»«a Long An', 'CS', ''),
(79, 'NhanNguyen', 'NhanNguyen1234', 'Nguyá»…n Thá»‹ Thanh NhÃ n', 1, 0, '', '01643812458', '1993-06-16', '115/62 LÃª Trá»ng Táº¥n P. SÆ¡n Ká»³, TÃ¢n PhÃº', 'thanhnhan135759@gmail.com', 'https://www.facebook.com/profile.php?id=100009951408239', '', '2017-08-18', 0, 17000, 1, 1, '0000-00-00', 'XuÃ¢n Lá»™c Äá»“ng Nai', 'CS', ''),
(80, 'TramTran', 'TramTran1234', 'Tráº§n ThÃºy TrÃ¢m', 0, 0, '', '01668007292', '1997-03-13', '56/24A ÄÆ°á»ng sá»‘ 27, P. SÆ¡n Ká»³, TÃ¢n PhÃº', 'tranthuytram13031997@gmail.com', 'https://www.facebook.com/welcometoThuyTram', '', '2017-08-18', 0, 17000, 1, 0, '0000-00-00', '', '', ''),
(81, 'KhanhLe', 'KhanhLe1234', 'LÃª Thá»‹ Thy KhÃ¡nh', 0, 0, '', '01228692556', '1997-03-21', '56/24A ÄÆ°á»ng sá»‘ 27, P. SÆ¡n Ká»³, TÃ¢n PhÃº', 'thykhanh2103@gmail.com', 'https://www.facebook.com/ThyyyKhanhhh', '', '2017-08-18', 0, 17000, 1, 0, '0000-00-00', '', '', ''),
(82, 'NganLe', 'NganLe1234', 'LÃª Thá»‹ Kim NgÃ¢n', 0, 0, '', '01202952079', '1997-10-02', '56/24A ÄÆ°á»ng sá»‘ 27, P. SÆ¡n Ká»³, TÃ¢n PhÃº', 'kimngan210.xx@gmail.com', 'https://www.facebook.com/profile.php?id=100009115540702', '', '2017-08-18', 0, 17000, 1, 0, '0000-00-00', '', '', ''),
(83, 'AnhTran', 'AnhTran1234', 'Tráº§n Minh Anh', 0, 0, '', '01656196622', '1998-04-04', '356/15 GÃ² Dáº§u TÃ¢n QuÃ½, TÃ¢n PhÃº', 'minhanht4498@gmail.com', 'https://www.facebook.com/profile.php?id=100011401001781', '', '2017-08-19', 0, 17000, 1, 0, '0000-00-00', '', '', ''),
(84, 'VuHo', 'VuHo1234', 'Há»“ Báº£o NguyÃªn VÅ©', 0, 0, '', '0938677090', '1998-06-28', '868/18 VÃµ VÄƒn Kiá»‡t P5 Q5', 'vu.hobao2806@gmail.com', 'https://www.facebook.com/profile.php?id=100006703869324', '', '2017-07-20', 5000000, 0, 1, 0, '0000-00-00', '', 'BÃ¡nh kem', 'NgÃ y lÃ m 4 tiáº¿ng bÃ¡nh kem 8am - 12am'),
(85, 'AnhTram', 'AnhTram1234', 'Nguyá»…n BÃ¹i Anh TrÃ¢m', 0, 0, '', '01664078247', '1995-11-07', '119a tráº§n vÄƒn dÆ° p13 q.tÃ¢n bÃ¬nh', 'anhtram71195@gmail.com', 'https://www.facebook.com/ny.nguyen.39?pnref=friends.search', '', '2017-08-26', 0, 17000, 1, 0, '0000-00-00', '', '', ''),
(86, 'ThuongLe', 'ThuongLe1234', 'LÃª HoÃ i ThÆ°Æ¡ng', 0, 0, '', '01689233862', '1995-09-23', '119a tráº§n vÄƒn dÆ° p13 q.tÃ¢n bÃ¬nh', 'lehoaithuong2395@gmail.com', 'https://www.facebook.com/le.h.thuong.31', '', '2017-08-26', 0, 17000, 1, 0, '0000-00-00', '', '', ''),
(87, 'TamTran', 'TamTran1234', 'Tráº§n Thá»‹ Thanh TÃ¢m', 0, 0, '', '0981533296', '1996-09-06', '56/24A ÄÆ°á»ng Sá»‘ 27, SÆ¡n Ká»³ TÃ¢n PhÃº', 'tranthithanhtam.1996.123@gmail.com', 'https://www.facebook.com/profile.php?id=100007628588426', '', '2017-08-18', 0, 17000, 1, 0, '0000-00-00', 'BÃ¬nh Äá»‹nh', '', ''),
(88, 'DungNguyen', 'DungNguyen1234', 'Nguyá»…n Thá»‹ ThÃ¹y Dung', 0, 0, '', '0968769510', '1999-02-13', '206 ÄÆ°á»ng sá»‘ 2, cÆ° xÃ¡ ÄÃ i Ra Äa P16 q6', 'tunguyen.9510@gmail.com', 'https://www.facebook.com/le.h.thuong.31', '', '2017-09-01', 0, 17000, 1, 0, '0000-00-00', 'Nghá»‡ An', '', '3-6pm-10am Ä‘i há»c Quan Há»‡ CÃ´ng ChÃºng'),
(89, 'ThuanLe', 'ThuanLe1234', 'LÃª Kim Thuáº­n', 0, 0, '', '01227141477', '1993-02-17', '153/22 QuÃ³c Lá»™ 13, P26 Q. BÃ¬nh Tháº¡nh', 'thuankim41@gmail.com', 'THuáº­n Kim', '', '2017-09-01', 0, 17000, 1, 0, '0000-00-00', 'SÃ i gÃ²n', '', ''),
(90, 'DiemNgoc', 'DiemNgoc1234', 'Nguyá»…n Ngá»c Diá»…m', 0, 0, '', '01689000907', '1993-02-28', '166 NgÃ´ Gia Tá»± P9 Q10', 'diemnguyentd@gmail.com', 'diemnguyentd@gmail.com', '', '2017-09-05', 6000000, 0, 1, 0, '0000-00-00', 'ChÃ¢u Äá»‘c - An Giang', '', ''),
(91, 'XuanBui', 'XuanBui1234', 'BÃ¹i Thá»‹ Ngá»c XuÃ¢n', 1, 0, '', '0938086265', '1995-12-29', '554/19 Cá»™ng hÃ²a P13 TÃ¢n BÃ¬nh', 'xuanbui2912cntp@gmail.com', 'https://www.facebook.com/xuan.bui.31149?fref=search', '', '2017-09-06', 0, 17000, 1, 0, '0000-00-00', 'Sa ÄÃ©c - Äá»“ng ThÃ¡p', '', ''),
(92, 'GiangTran', 'GiangTran1234', 'Tráº§n Má»¹ Giang', 1, 0, '', '0934070074', '1982-05-12', '19/6 Tráº§n BÃ¬nh Trá»ng P5 BÃ¬nh Tháº¡nh', 'jenny.mygiangtran@gmail.com', '', '', '2017-09-03', 0, 17000, 1, 0, '0000-00-00', 'SÃ i GÃ²n', '', 'Giang ca đêm kế toán'),
(93, 'ThuVo', 'ThuVo1234', 'VÃµ Tráº§n Anh ThÆ°', 0, 0, '', '01299060525', '1995-01-29', '107 LÃ´ C, chung cÆ° Chá»£ QuÃ¡n P1, Q5', 'thuvo1198@gmail.com', '', '', '2017-09-09', 0, 17000, 1, 0, '0000-00-00', 'Báº¡c LiÃªu', '', ''),
(94, 'VyTran', 'VyTran1234', 'Tráº§n Kim Vy', 0, 0, '', '0932095071', '1990-12-27', '176/8B Tráº§n Quá»‘c Tháº£o P7 Q3', 'kimvy2712@yahoo.com.vn', '', '', '2017-09-13', 0, 17000, 1, 0, '0000-00-00', 'SÃ i GÃ²n', '', ''),
(95, 'YenDoan', 'YenDoan1234', 'ÄoÃ n Nguyá»…n Há»“ng yáº¿n', 0, 0, '', '01222133362', '1995-05-26', '88/45 Phan SÃ o Nam P11 Q TÃ¢n BÃ¬nh', 'hongyen26595@gmail.com', '', '', '2017-09-13', 0, 17000, 1, 0, '0000-00-00', 'SÃ i GÃ²n', '', ''),
(96, 'TrucLyNguyen', 'TrucLyNguyen1234', 'Nguyá»…n Thá»‹ TrÃºc Ly', 0, 0, '', '012113079092', '1995-12-16', '43/38 HÆ°Æ¡ng Lá»™ 3 PhÆ°á»ng BÃ¬nh HÃ²a Quáº­n BÃ¬nh TÃ¢n', 'min207599@gmail.com', '', '', '2017-09-13', 0, 17000, 1, 0, '0000-00-00', 'BÃ¬nh Äá»‹nh', '', ''),
(97, 'AnhKieu', 'AnhKieu1234', 'Kiá»u Viá»‡t Anh', 0, 0, '', '0939290880', '1990-11-17', '359/67F LÃª VÄƒn Sá»¹ P12 Q3', 'vietanh.kieu90@gmail.com', '', '', '2017-09-01', 0, 17000, 1, 0, '0000-00-00', 'TrÃ  Vinh', '', ''),
(98, 'MyTran', 'MyTran1234', 'Tráº§n Thá»‹ TrÃ  My', 0, 0, '', '0985057241', '1996-12-18', 'Quang Trung - Gá» Váº¥p - cao Ä‘áº³ng Há»“ng Äá»©c', '', '', '', '2017-09-14', 0, 17000, 1, 0, '0000-00-00', 'BÃ  Rá»‹a VÅ©ng TÃ u', '', ''),
(99, 'HanhTrieu', 'HanhTrieu1234', 'Triá»‡u Háº£i Háº¡nh', 1, 0, '', '0915308278', '1990-07-02', '36H6 PhÆ°á»ng VÄ©nh PhÃº Thuáº­n An, BÃ¬nh DÆ°Æ¡ng', 'hanh.trieu2601@gmail.com', 'https://www.facebook.com/yolo0207', '', '2017-09-18', 0, 17000, 1, 0, '0000-00-00', 'BÃ¬nh DÆ°Æ¡ng', '', ''),
(100, 'TuyenThai', 'TuyenThai1234', 'ThÃ¡i HoÃ ng Tuyá»n', 1, 0, '', '0906658651', '1993-09-08', '63 Váº¡n Kiáº¿p P13 BÃ¬nh Tháº¡nh ; 2221/29B Huá»³nh Táº¥n PhÃ¡t NhÃ  BÃ¨', 'thaituyen0809@gmail.com', 'tuyenthai', '', '2017-10-10', 6000000, 0, 1, 0, '0000-00-00', 'SÃ i gÃ²n', 'CS', '7.5'),
(101, 'MaiPhan', 'MaiPhan1234', 'Phan Thá»‹ TÃ¢n Mai', 0, 0, '', '01207488639', '1985-02-20', '451 LÃª VÄƒn Viá»‡t Q9', 'o0iammai0o@yahoo.com', 'https://www.facebook.com/mai.phan.7355', '', '2017-10-01', 4000000, 0, 1, 0, '0000-00-00', 'Quáº£ng NgÃ£i', 'baker', ''),
(102, 'TuHa', 'TuHa1234', 'HÃ  Ngá»c TÃº', 0, 0, '', '0902869112', '1989-05-07', '31 TÃ¢n Khai P4 Q11', '', 'https://www.facebook.com/ella.ha3', '', '2017-09-17', 4000000, 0, 1, 0, '0000-00-00', 'SÃ i GÃ²n', 'baker', ''),
(103, 'NhuQuynh', 'QuynhNguyen1234', 'Nguyá»…n VÅ© NhÆ° Quá»³nh', 1, 0, '', '0972352802', '1990-07-16', '4.02 LÃ´ 3C Chung CÆ° PhÃº Thá» P15 Q11', 'quynhnguyen167@gmail.com', 'Quynh Lee', '', '2017-09-25', 4500000, 0, 1, 0, '0000-00-00', 'SÃ i GÃ²n', 'CS', '6 tr'),
(104, 'TrangNguyenTT', 'TrangNgueynTT1234', 'Nguyá»…n Thá»‹ ThÃ¹y Trang', 0, 0, '', '01682555151', '1993-02-24', '179 Tráº§n Thá»‹ Báº£y P. Hiá»‡p ThÃ nh Q. 12', 'thienngaden47@gmail.com', 'https://www.facebook.com/variant2324', '', '2017-12-20', 0, 18000, 1, 0, '0000-00-00', 'SÃ i GÃ²n', 'trang tri', 'Vân tay số 20'),
(105, 'HiepHo', 'HiepHo1234', 'Há»“ Báº£o Hiá»‡p', 0, 0, '', '01658349550', '1994-10-13', '127/4 HoÃ ng Hoa ThÃ¡m P13 TÃ¢n BÃ¬nh', 'baohiep243@gmail.com', 'https://www.facebook.com/hiep.ho.9843', '', '2017-12-21', 0, 18000, 1, 0, '0000-00-00', 'Gia Lai', 'trang tri', 'Vân tay số 21'),
(106, 'LinhVu', 'LinhVu1234', 'VÅ© Thá»‹ Linh', 0, 0, '', '01663899595', '1995-08-06', '127/4 HoÃ ng Hoa ThÃ¡m P13 TÃ¢n BÃ¬nh', 'vulinh0608@gmail.com', 'https://www.facebook.com/rosealone.thuylinh', '', '2017-12-21', 0, 18000, 1, 0, '0000-00-00', 'Gia Lai', 'trang tri', 'Vân tay số 22'),
(107, 'TramVu', 'TramVu1234', 'VÅ© Thá»‹ ThÃ¹y TrÃ¢m', 0, 0, '', '0909777369', '1988-10-10', '548/17 Äiá»‡n BiÃªn Phá»§ P21 BÃ¬nh Tháº¡nh', 'tramvu.must@gmail.com', 'https://www.facebook.com/', '', '2017-12-22', 0, 18000, 1, 0, '0000-00-00', 'Äá»“ng Nai - Thá»‘ng Nháº¥t', 'trang tri', 'Vân tay số 23'),
(108, 'NhiHuynh', 'NhiHuynh1234', 'Huá»³nh Há»¯u Tháº£o Nhi', 0, 0, '', '01286335674', '1997-04-10', '563/45 Nguyá»…n ÄÃ¬nh Chiá»ƒu P2 Q3', 'nhihuynh104@gmail.com', 'https://www.facebook.com/banana104', '', '2017-12-04', 0, 20000, 1, 0, '0000-00-00', 'sÃ i gÃ²n', 'trang tri', 'Vân tay số 18'),
(109, 'HaoVo', 'HaoVo321', 'VÃµ VÄƒn Háº£o', 1, 1, '', '0918193188', '1982-03-27', '20 Ä‘Æ°á»ng 34 An PhÃº Quáº­n 2', 'vanhao1103@yahoo.com', 'https://www.facebook.com/vovan.hao.982', '', '2018-02-06', 0, 0, 1, 0, '0000-00-00', 'sÃ i gÃ²n', '', ''),
(110, 'BaoNguyen', 'BaoNguyen123', 'Nguyá»…n ThÃ nh Báº£o', 0, 0, '', '0938393478', '2000-08-01', '595/24 An DÆ°Æ¡ng VÆ°Æ¡ng An Láº¡c BÃ¬nh TÃ¢n', '', 'https://www.facebook.com/nguyenthanhbao', '', '2018-02-21', 0, 18000, 1, 0, '0000-00-00', 'sÃ i gÃ²n', 'CL', 'Vân tay số 24'),
(111, 'DungKieu', 'DungKieu321', 'Kiá»u Tuáº¥n DÅ©ng', 1, 1, '', '0978214818', '1992-08-09', 'CÃ¡ch Máº¡ng ThÃ¡ng TÃ¡m', 'dungkieu@gmail.com', 'kieu dung', '', '2018-02-26', 12000000, 0, 1, 0, '0000-00-00', 'sÃ i gÃ²n', 'Marketing', ''),
(112, 'BaoTranMarketing', 'BaoTran321', 'Tráº§n HoÃ ng Báº£o', 1, 1, '', '0976330551', '1992-11-01', '125 Äá»“ng VÄƒn Cá»‘ng', 'tranhoangbao11192@gmail.com', 'Bao Tran', '', '2018-05-05', 0, 0, 1, 0, '0000-00-00', 'LÃ¢m Äá»“ng', 'Marketing', ''),
(113, 'HoangPham', 'HoangPham123', 'Pháº¡m VÄƒn HoÃ ng', 0, 0, '', '01664513331', '1989-06-11', '5/27 TrÆ°Æ¡ng ÄÄƒng Quáº¿ P1 GV', '', 'https://www.facebook.com/Vanhoang225', '', '2018-03-09', 0, 22000, 1, 0, '0000-00-00', 'Nha Trang', 'fondant', 'Vân tay số 24, se len 26k'),
(114, 'VyNguyen', 'VyNguyen1234', 'Nguyá»…n LÃª Tháº£o Vy', 1, 0, '', '0914275595', '1998-09-07', '947/9 Láº¡c Long QuÃ¢n, P11, Quáº­n TÃ¢n BÃ¬nh', 'nguyenlethaovy0709@gmail.com', 'nguyen le thao vy', '', '2018-03-11', 5000000, 0, 1, 0, '0000-00-00', 'SÃ i gÃ²n', 'CS', ' tr'),
(115, 'HaDao', 'HaDao123', 'ÄÃ o Thanh HÃ ', 0, 0, '', '0903885689', '1987-02-15', 'B105 khu dÃ¢n cÆ° DiÃªn Há»“ng Nguyá»…n VÄƒn Linh BÃ¬nh ChÃ¡nh', 'thanhhadao87@gmail.com', 'https://www.facebook.com/thanhha.dao.520', '', '2018-03-07', 5000000, 0, 1, 0, '0000-00-00', 'SÃ i gÃ²n', 'Decor', 'thử việc 5 triệu'),
(116, 'HuyTran', 'HuyTran123', 'Tráº§n Gia Huy', 0, 0, '', '01262968384', '1997-11-16', '118/27B Tráº§n Quang Diá»‡u P14 Q3', '', 'https://www.facebook.com/trangiahuy.tran.3386', '', '2018-03-09', 4000000, 0, 1, 0, '0000-00-00', 'Phan Thiáº¿t', 'fondant', 'Vân tay số 26,thử việc 4 triệu'),
(117, 'HauNguyen', 'HauNguyen123', 'Nguyá»…n LÃ¢m Má»¹ Háº­u', 0, 0, '', '0945851410', '1995-09-05', '6/11 nguyá»…n trung trá»±c p5 bÃ¬nh tháº¡nh', '', '', '', '2018-03-09', 0, 0, 1, 0, '0000-00-00', 'SÃ³c TrÄƒng sinh sÃ i gÃ²n', 'marketing', 'marketing'),
(118, 'DatNguyen', 'DatNguyen123', 'Nguyá»…n Tiáº¿n Äáº¡t', 0, 0, '', '01662727846', '1995-01-01', 'HÃ  Ná»™i', 'tiendatbt19@gmail.com', 'skype: tiendatbt19, https://www.facebook.com/ntd.15', '', '2018-04-03', 6000000, 0, 1, 0, '0000-00-00', 'HÃ  Ná»™i', 'IT', '6-8tr');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `nhanvien`
--
ALTER TABLE `nhanvien`
  ADD PRIMARY KEY (`manv`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `nhanvien`
--
ALTER TABLE `nhanvien`
  MODIFY `manv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
