<?php 

/**
* API go here
*/
class Api extends Controller{
	private $db;
	private $customerTable = 'khachhang';
	// private $catTable = 'test';
	function __construct() {
		parent::__construct(__CLASS__);
		$this->db = new DB();
	}

	/**
	 * Tìm kiếm từ khóa và phân trang
	 * @param key  [Từ khóa]
	 * @param  $page [Đánh số trang]
	 * @return        [json object]
	 */
	public function find_customer(){
		$tenkh = '';
		$std = '';
		$diachi = '';
		$email = '';
		$sql = "SELECT * FROM ".$this->customerTable." WHERE ";
		if(isset($_POST['tenkh']) && $_POST['tenkh'] != ''){
			$tenkh = strtolower($_POST['tenkh']);
			$sql .= ' lower(tenkh) LIKE "%'.$tenkh.'%" ';
		}
		if(isset($_POST['std']) && $_POST['std'] != ''){
			$std = $this->cleanString($_POST['std']);
			if($sql != "SELECT * FROM ".$this->customerTable." WHERE ") $sql .= " AND ";
			$sql .= ' lower(sdt) LIKE "%'.$std.'%" ';
		}
		if(isset($_POST['diachi']) && $_POST['diachi'] != ''){
			// $diachi = $this->cleanString($_POST['diachi']);
			$diachi = strtolower($_POST['diachi']);
			if($sql != "SELECT * FROM ".$this->customerTable." WHERE ") $sql .= " AND ";
			$sql .= ' lower(diachi) LIKE "%'.$diachi.'%" ';
		}
		if(isset($_POST['email']) && $_POST['email'] != ''){
			$email = $this->cleanString($_POST['email']);
			if($sql != "SELECT * FROM ".$this->customerTable." WHERE ") $sql .= " AND ";
			$sql .= ' lower(email) LIKE "%'.$email.'%" ';
		}
		
		$sql .= ' ORDER BY makh DESC LIMIT 100;';
		$res = $this->db->query($sql);
		echo json_encode(array(
			'status' => (is_array($res) && count($res)) ? 1 : 0,
			'data' => (is_array($res) && count($res)) ? $res : [],
			'message' => (is_array($res) && count($res)) ? 'success' : 'failed',
			'sql' => $sql
		));
	}


}
