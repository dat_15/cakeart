var addCustomerApp = angular.module('add-customer-app', []);
let domain = "http://www.cakeart.vn/order/";

addCustomerApp.service('customerService', ['$http', function ($http) {
	
	this.getAllCat = (callback) => {
		var req = {
		    method: 'GET',
		    url: domain+'api/get_all_danhmucbanh'
		};

		$http(req).then((res) => {
		    callback(res.data);
		}, (err) => {
		    callback({ status: 0, message: 'error' });
		    console.log('getTicketGroupUser error ', err);
		    console.log(err);
		});
	}


	this.findCustomer = (data, callback) => {
		var req = {
		    method: 'POST',
		    url: domain+'dev/api/find_customer',
		    data: data,
		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}, 
		    transformRequest: function(obj) { 
			    var str = []; 
			    for(var p in obj) 
			    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p])); 
			    return str.join("&"); 
		    }
		};

		$http(req).then((res) => {
		    callback(res.data);
		}, (err) => {
		    callback({ status: 0, message: 'error' });
		    console.log('getTicketGroupUser error ', err);
		    console.log(err);
		});
	}

}]);

