// Groups Management Controller
addCustomerApp.controller('add-customer-controller', ['$scope', '$rootScope', '$sce', '$timeout', '$compile', '$interval', 'customerService',
    function($scope, $rootScope, $sce, $timeout, $compile, $interval, customerService) {
    	let service = customerService;
    	$scope.suggestionUsers = [];
        $scope.showLoading = false;
    	$scope.noResult = false;
    	$scope.customer = {
			tenkh : '',
			std : '',
			diachi : '',
			email : '',
			fb : '',
			nguoinhan: '',
			stdnguoinhan: '',
			ghichu: ''
    	};
    	let timer = null;
    	$scope.findUserSuggestion = () => {
    		$scope.showLoading = true;
    		clearTimeout(timer); 
       		timer = setTimeout(function(){
                service.findCustomer($scope.customer, res => {
                    $scope.showLoading = false;
                    if(res.status && Array.isArray(res.data)){
                        $scope.noResult = false;
	    				$timeout(function(){
		    				$scope.suggestionUsers = res.data;
	    				}, 100);
	    			}else{
                        $scope.noResult = true;
                    }
	    		});
       		}, 1000);
    	}
    }
]);