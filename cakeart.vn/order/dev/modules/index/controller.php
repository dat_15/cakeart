<?php
/**
* index controller
*/
class Index extends Controller 
{
	
	function __construct()
	{
		parent::__construct();
	}

	function index(){
		$this->load_view('index', ['base_url' => 'modules/index/']);
	}

}