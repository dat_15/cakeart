<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Thêm mới khách hàng</title>
	<base href="<?php echo base_url.$base_url; ?>">
	<!-- LIB Styles-->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="assets/lib/uikit/css/uikit.min.css" />
	<link rel="stylesheet" href="assets/css/customer-management.css" />


	<!-- <script src="assets/js/jquery.min.js" type="text/javascript"></script> -->
	<script src="assets/js/angular.min.js" type="text/javascript"></script>

	<!-- LIB JS -->
	<script src="assets/lib/uikit/js/uikit.min.js"></script>
	<script src="assets/lib/uikit/js/uikit-icons.min.js"></script>
	

	<!-- Custom JS -->
	<script src="assets/js/customer-service.js" type="text/javascript"></script>
	<script src="assets/js/customer-controller.js" type="text/javascript"></script>

</head>
<body ng-app="add-customer-app">
	
	<div class="uk-section" ng-controller="add-customer-controller">
		<div class="uk-container">
			<h3 class="uk-heading-line uk-text-center"><span>Thêm khách hàng mới</span></h3>
			<p class="uk-text-lead uk-text-center">Người nhận order: {{user.fullname}}</p>
			<form >
				<div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
				    <div>
				    	<div class="uk-inline">
						    <span class="uk-form-icon" uk-icon="icon: user"></span>
						    <input class="uk-input" ng-keyup="findUserSuggestion()" ng-model="customer.tenkh" type="text" placeholder="Tên khách hàng: Nguyễn Văn A">
				    	</div>
				    </div>
				    <div>
				    	<div class="uk-inline">
						    <span class="uk-form-icon" uk-icon="icon: receiver"></span>
						    <input class="uk-input" ng-keyup="findUserSuggestion()" ng-model="customer.std" type="text" placeholder="Số điện thoại: 0123 456 7890">
						</div>
				    </div>
				    <div>
				    	<div class="uk-inline">
						    <span class="uk-form-icon" uk-icon="icon: location"></span>
						    <input class="uk-input" ng-keyup="findUserSuggestion()" ng-model="customer.diachi" type="text" placeholder="Địa chỉ: Số 183B Trần Quốc Thảo">
						</div>
				    </div>
				</div>
				<div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid" uk-grid>
				    <div>
				    	<div class="uk-inline">
						    <span class="uk-form-icon" uk-icon="icon: mail"></span>
						    <input class="uk-input" ng-keyup="findUserSuggestion()" ng-model="customer.email" placeholder="Email: example@email.com">
					    </div>
				    </div>
				    <div>
					    <div class="uk-inline">
						    <span class="uk-form-icon" uk-icon="icon: facebook"></span>
						    <input class="uk-input" ng-keyup="findUserSuggestion()" ng-model="customer.fb" type="text" placeholder="Mạng xã hội: Facebook/Skype">
					    </div>
				    </div>
				    <div>
					    <div class="uk-inline">
						    <span class="uk-form-icon" uk-icon="icon: calendar"></span>
						    <input class="uk-input" ng-keyup="findUserSuggestion()" ng-model="customer.ngaysinh" type="text" placeholder="Ngày sinh: 1/1/1990">
					    </div>
				    </div>
				</div>
			    <div hidden id="toggle-animation" class="uk-margin-small-top">
			    	<div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid">
					    <div>
						    <div class="uk-inline">
							    <span class="uk-form-icon" uk-icon="icon: camera"></span>
					            <input type="file" class="uk-input" placeholder="Chọn ảnh đại diện">
						    </div>
					    </div>
			    		<div>
					    	<div class="uk-inline">
							    <span class="uk-form-icon" uk-icon="icon: users"></span>
							    <input class="uk-input" ng-model="customer.nguoinhan" type="text" placeholder="Tên người nhận : Nguyễn Văn B">
						    </div>
					    </div>
					    <div>
						    <div class="uk-inline">
							    <span class="uk-form-icon" uk-icon="icon: receiver"></span>
							    <input class="uk-input" ng-model="customer.stdnguoinhan" type="text" placeholder="Số điện thoại người nhận : 0987 654 321">
						    </div>
					    </div>
			    	</div>
				    
					<div class="uk-grid-match uk-grid-small uk-child-width-expand@s uk-grid">
					    <div class="uk-inline">
						    <textarea class="uk-textarea" ng-model="customer.ghichu" rows="5" placeholder="Ghi chú thêm: Số điện thoại khác, Email khác, ...">{{customer.ghichu}}</textarea>
					    </div>
					</div>
				</div>
			    <button href="#toggle-animation" class="uk-button uk-button-primary uk-margin-small-top" type="button" uk-toggle="target: #toggle-animation; animation: uk-animation-fade">
			    	<span uk-icon="icon: plus-circle"></span>
			    	Thông tin khác 
			    </button>
			    <button class="uk-button uk-button-primary uk-margin-small-top" type="button">
			    	Thêm khách hàng 
			    </button>
			</form>
	

			<!-- Loading phần gợi ý -->
			<div ng-show="showLoading" class="uk-margin-medium-top">
				<div uk-spinner></div>
			</div>	

			<!-- Hiển thị thông tin gợi ý -->
			<div class="uk-margin-medium-top" ng-if="suggestionUsers.length" uk-grid>
		    	<div class="uk-width-1-1 uk-animation-slide-bottom-small" ng-repeat="user in suggestionUsers">
		    		<!-- Hiển thị chi tiết gợi ý -->
		    		<div class="uk-card uk-card-default uk-padding-remove uk-card-hover">

		    			<!-- Header -->
			    		<div class="uk-card-header uk-padding-small">
			    			<div class="uk-grid-small uk-flex-middle" uk-grid>
					            <div class="uk-width-expand">
								    <h5 class="no-wrap uk-margin-small-bottom" uk-tooltip="title: {{user.tenkh}}">{{user.tenkh}}</h5>
					            </div>
					        </div>
					    </div>
					    <!-- Body -->
					    <div class="uk-card-body uk-padding-small">
					    	<h6 class="uk-margin-small-top uk-margin-remove-bottom">
					    		<span class="uk-margin-small-left" uk-icon="icon: receiver"></span> 
								<i>{{user.sdt == '' ? 'Chưa có thông tin' : user.sdt}}</i>
					    	</h6>
					    	<h6 class="no-wrap uk-margin-small-top uk-margin-remove-bottom" uk-tooltip="title: {{user.diachi}}">
					    		<span class="uk-margin-small-left" uk-icon="icon: location"></span> 
								<i>{{user.diachi == '' ? 'Chưa có thông tin' : user.diachi}}</i>
					    	</h6>
					    </div>
		    		</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>